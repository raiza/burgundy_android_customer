package com.nutech.burgundy.notification;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.ui.view.promo.DialogNotificationActivity;
import com.nutech.burgundy.ui.view.splash.BlockedActivity;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

/**
 * Created by mactc08 on 1/10/17.
 */

public class FirebaseMsgService extends FirebaseMessagingService {

    private static final String TAG = " ====== fcm_msg ===== ";
    private NotificationUtils notificationUtils;

    private static final int NOTIFICATION_ID = (int) System.currentTimeMillis();

    private NotificationManager notificationManager;
    private SessionManager sessionManager;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log data to Log Cat
//        Log.e(TAG, "From: " + remoteMessage.getFrom());
//        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //create notification

        Map<String, String> data = remoteMessage.getData();
//        String Nick = data.get("Nick");
//
//        Log.e(TAG, "Nick: " + Nick);

//        createNotification(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
//        showNotification(remoteMessage.getNotification().getBody());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + Objects.requireNonNull(remoteMessage.getNotification().getLink()).getScheme());

            if (remoteMessage.getNotification().getLink() != null) {
                Uri deepLink = remoteMessage.getNotification().getLink();
                String url = deepLink.getScheme()+"://"+deepLink.getHost()+deepLink.getPath();
                if (url.equalsIgnoreCase(Constants.Key.URI_DEEP_LINK)) {
                    deepLinkNotification(remoteMessage.getNotification());
                } else {

                    handleNotification(remoteMessage.getNotification());
                }
            } else {

                handleNotification(remoteMessage.getNotification());
            }
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    public void showNotification (String message) {

        Log.e(TAG, "Image URL: " + message);

        Intent intent = new Intent(this, DialogNotificationActivity.class);
        intent.putExtra("body", message);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void deepLinkNotification(RemoteMessage.Notification notif) {
        Intent intent = new Intent(this,PrimaryActivity.class);

        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(notif.getLink());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @SuppressLint("WrongConstant")
    private void handleNotification(RemoteMessage.Notification notif) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            if (notificationManager == null) {
                notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(Constants.Key.NOTIF_CHANNEL_ID,
                        Constants.Key.NOTIF_CHANNEL_ID,
                        NotificationManager.IMPORTANCE_LOW);
                notificationChannel.setName(Constants.Key.NOTIF_CHANNEL_ID);
                notificationChannel.setDescription(Constants.Key.NOTIF_CHANNEL_ID);
                notificationManager.createNotificationChannel(notificationChannel);
            }

            Intent intent = new Intent();

            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(notif.getLink());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            //TODO: Set to API below Build.VERSION.SDK_INT
            NotificationCompat.Builder notificationCompat = new NotificationCompat.Builder(this)
                    //TODO: Set to API below Build.VERSION.SDK_INT
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(notif.getTitle())
                    .setContentText(notif.getBody())
                    .setContentIntent(pendingIntent)
                    .setChannelId(Constants.Key.NOTIF_CHANNEL_ID);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                notificationCompat.setCategory(Notification.CATEGORY_REMINDER);
            }

//        try {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                if (!JcAudioPlayer.getInstance().isPlaying()) {
//                    notificationCompat.setOngoing(false);
//                } else {
//                    notificationCompat.setOngoing(true);
//                }
//            }
//        } catch (Exception e) {
//            notificationCompat.setOngoing(false);
//        }

            Notification notification = notificationCompat.build();
            notificationManager.notify(NOTIFICATION_ID, notification);
        }// If the app is in background, firebase itself handles the notification

    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(this));
        }

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            String moodtrackId = data.getString("moodtrack_id");
            String storeId = data.getString("store_id");
            String status = data.getString("status");
            String type = data.getString("type");
            String deviceId = data.getString("device_id");
            String userId = data.getString("user_id");
            boolean isControl = data.getBoolean("is_control");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);
            Log.e(TAG, "moodtrackId: " + moodtrackId);
            Log.e(TAG, "storeId: " + storeId);
            Log.e(TAG, "status: " + status);
            Log.e(TAG, "type: " + type);
            Log.e(TAG, "deviceId: " + deviceId);
            Log.e(TAG, "user_id: " + userId);
            Log.e(TAG, "is_control: " + isControl);

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

                /*
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Constant.Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();

                */
                Intent resultIntent = new Intent(getApplicationContext(), PrimaryActivity.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                if (type.equalsIgnoreCase("controls")) {
                    if (sessionManager.isLoggedIn()) {
                        broadcastMoodtrackControls(moodtrackId,storeId,status,isControl);
                    } else {
                        showNotificationPlayMoodtrackControl(getApplicationContext(),moodtrackId,storeId,status);
                    }
                } else if (type.equalsIgnoreCase("check_device")) {
                    checkDevice(deviceId,userId,true);
                } else {
                    // check for image attachment
                    if (TextUtils.isEmpty(imageUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                    } else {
                        // image is present, show notification with image
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                        showNotification(imageUrl);
                    }
                }

            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), PrimaryActivity.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                if (!NotificationUtils.isActivityIsInBackground(getApplicationContext())) {
                    if (type.equalsIgnoreCase("controls")) {
                        if (sessionManager.isLoggedIn()) {
                            checkTypeControlsInBackground(type,moodtrackId,storeId,status,isControl);
                        } else {
                            showNotificationPlayMoodtrackControl(getApplicationContext(),moodtrackId,storeId,status);
                        }
                    } else if (type.equalsIgnoreCase("check_device")) {
                        checkDevice(deviceId,userId,false);
                    } else {
                        // check for image attachment
                        if (TextUtils.isEmpty(imageUrl)) {
                            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                        } else {
                            // image is present, show notification with image
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                            showNotification(imageUrl);
                        }
                    }
                } else {
                    KeyguardManager myKM = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
                    if( Objects.requireNonNull(myKM).inKeyguardRestrictedInputMode()) {
                        if (type.equalsIgnoreCase("controls")) {
                            if (sessionManager.isLoggedIn()) {
                                checkTypeControlsInBackground(type,moodtrackId,storeId,status,isControl);
                            } else {
                                showNotificationPlayMoodtrackControl(getApplicationContext(),moodtrackId,storeId,status);
                            }
                        } else if (type.equalsIgnoreCase("check_device")) {
                            checkDevice(deviceId,userId,true);
                        } else {
                            // check for image attachment
                            if (TextUtils.isEmpty(imageUrl)) {
                                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                            } else {
                                // image is present, show notification with image
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                                showNotification(imageUrl);
                            }
                        }

                    } else {
                        if (type.equalsIgnoreCase("controls")) {
                            if (sessionManager.isLoggedIn()) {
                                checkTypeControlsInBackground(type,moodtrackId,storeId,status,isControl);
                            } else {
                                showNotificationPlayMoodtrackControl(getApplicationContext(),moodtrackId,storeId,status);
                            }
                        } else if (type.equalsIgnoreCase("check_device")) {
                            checkDevice(deviceId,userId,true);
                        } else {
                            // check for image attachment
                            if (TextUtils.isEmpty(imageUrl)) {
                                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                            } else {
                                // image is present, show notification with image
//                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                                showNotification(imageUrl);
                            }
                        }
                    }

                }

            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    private void checkTypeControlsInBackground(String type, String moodtrackId, String storeId,
                                               String status, boolean isControl) {

        if (!status.equals(Constants.MoodtrackControl.PLAY)) {
            broadcastMoodtrackControls(moodtrackId,storeId,status,isControl);
        } else {
            startActivityMoodtrackControls(moodtrackId,storeId,status,isControl);
        }
    }

    private void checkDevice(String deviceId,String userId,boolean isActivity) {
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Log.e(TAG,"device : "+deviceId+", new device : "+android_id);
        Log.e(TAG,"user : "+userId);

        if (!android_id.equals(deviceId) && sessionManager.getUserStore().getId().equals(userId)) {
            if (isActivity) {
                Intent intent = new Intent(this, BlockedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                sessionManager.logout();
            }
        }
    }

    private void startActivityMoodtrackControls(String moodtrackId, String storeId, String status,
                                                boolean isControl) {
        Intent moodtrackIntent = new Intent(this,PrimaryActivity.class);

        moodtrackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        moodtrackIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        moodtrackIntent.putExtra("moodtrack_id",moodtrackId);
        moodtrackIntent.putExtra("store_id",storeId);
        moodtrackIntent.putExtra("status",status);
        moodtrackIntent.putExtra("is_control",isControl);

        startActivity(moodtrackIntent);
    }

    private void broadcastMoodtrackControls(String moodtrackId, String storeId, String status, boolean isControl) {
        Intent moodtrackIntent = new Intent("moodtrack_controls");
        moodtrackIntent.putExtra("moodtrack_id",moodtrackId);
        moodtrackIntent.putExtra("store_id",storeId);
        moodtrackIntent.putExtra("status",status);
        moodtrackIntent.putExtra("is_control",isControl);
        LocalBroadcastManager.getInstance(this).sendBroadcast(moodtrackIntent);
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationPlayMoodtrackControl(Context context,String moodtrackId, String storeId, String status) {
        notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationPlayMoodtrackControl(moodtrackId,storeId,status);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

}
