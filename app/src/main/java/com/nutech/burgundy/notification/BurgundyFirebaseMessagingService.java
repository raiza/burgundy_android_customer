package com.nutech.burgundy.notification;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.utils.Helper;

/**
 * Created by Arif Setiawan on 27/12/17.
 * Xeranta Mobile Solutions
 */

public class BurgundyFirebaseMessagingService  extends FirebaseMessagingService {

    private static final String TAG = "MyAndroidFCMIIDService";
    private SessionManager sessionManager;


    @Override
    public void onNewToken(@NonNull String s) {
        //Get hold of the registration token
        sessionManager = new SessionManager(Helper.getDefaultPreferences(getApplicationContext()));
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log the token
        Log.i(TAG, "Refreshed token: " + refreshedToken);
        sessionManager.setRegisterId(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Constants.Status.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

}
