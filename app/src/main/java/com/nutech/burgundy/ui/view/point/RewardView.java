package com.nutech.burgundy.ui.view.point;

import com.nutech.burgundy.data.model.DataReward;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * @author Dafian on 11/13/17
 */

public interface RewardView extends BaseView {

    void showRewardAll(List<DataReward> dataRewardList);

    void successAddPointExchnge(String succes);

    void errorAddPointExchange(String error);

    void showError(String error);
}
