package com.nutech.burgundy.ui.view.soundtrack;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.bumptech.glide.Glide;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.data.model.soundtrack.Sound;
import com.nutech.burgundy.presenter.soundtrack.EditMoodPresenter;
import com.nutech.burgundy.ui.adapter.soundtrack.DecadeAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.EnergyAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.GenresAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.SoundAdapter;
import com.nutech.burgundy.ui.base.BasePhotoActivity;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.watcher.RequiredWatcher;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditMoodtrackActivity extends BasePhotoActivity implements EditMoodView {

    public static final int REQUEST_CODE_CAMERA = 100;
    private static final String[] PERMISSIONS_LIST = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.t_input_title)
    TextInputLayout tInputTitle;
    @BindView(R.id.et_title_mood)
    TextInputEditText etTitle;
    @BindView(R.id.t_input_desc)
    TextInputLayout tInputDesc;
    @BindView(R.id.et_description_mood)
    TextInputEditText etDesc;
    @BindView(R.id.image_cover)
    RoundedImageView imgCover;
    @BindView(R.id.iv_info)
    ImageView ivInfo;
    @BindView(R.id._switch)
    Switch _Switch;
    @BindView(R.id.button_save)
    Button btnSave;
    @BindView(R.id.rv_energy_tag)
    RecyclerView rvEnergyTag;
    @BindView(R.id.rv_sound_tag)
    RecyclerView rvSoundTag;
    @BindView(R.id.rv_genre_tag)
    RecyclerView rvGenreTag;
    @BindView(R.id.rv_decade_tag)
    RecyclerView rvDecadeTag;
    @BindView(R.id.shimmer_energy)
    ShimmerFrameLayout shimmerEnergy;
    @BindView(R.id.shimmer_sound)
    ShimmerFrameLayout shimmerSound;
    @BindView(R.id.shimmer_genre)
    ShimmerFrameLayout shimmerGenre;
    @BindView(R.id.shimmer_decade)
    ShimmerFrameLayout shimmerDecade;
    @BindView(R.id.layout_error_energy)
    RelativeLayout errorLayoutEnergy;
    @BindView(R.id.layout_error_sound)
    RelativeLayout errorLayoutSound;
    @BindView(R.id.layout_error_genre)
    RelativeLayout errorLayoutGenre;
    @BindView(R.id.layout_error_decade)
    RelativeLayout errorLayoutDecade;

    private Intent intent;
    private EditMoodPresenter presenter;
    private String typeMood = "0";
    private Uri cropImageUri;
    private String baseImage = "";
    private ProgressDialog pDialog;
    private EnergyAdapter energyAdapter;
    private SoundAdapter soundAdapter;
    private GenresAdapter genresAdapter;
    private DecadeAdapter decadeAdapter;
    private ChipsLayoutManager chipsLayoutManager;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private List<String> energyListParam = new ArrayList<>();
    private List<String> soundListParam = new ArrayList<>();
    private List<String> genreListParam = new ArrayList<>();
    private List<String> decadeListParam = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_moodtrack);
        ButterKnife.bind(this);

        initViews();
        initEvents();
    }

    private void initViews() {

        intent = getIntent();
        presenter = new EditMoodPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        initPermission();

        etTitle.addTextChangedListener(new RequiredWatcher(context, tInputTitle));
        etDesc.addTextChangedListener(new RequiredWatcher(context, tInputDesc));

        loadingData();

    }

    private void initEvents() {
        ivClose.setOnClickListener(view -> {
            finish();
        });

        ivInfo.setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle(getResources().getString(R.string.label_info));
            alertDialog.setMessage(getString(R.string.msg_visible_moodtrack));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.button_ok),
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        });

        btnSave.setOnClickListener(view -> {
            Map<String, String> params = new HashMap<String, String>();

            if (etTitle.getText().toString().matches("") && etDesc.getText().toString().matches("")) {

            } else if (energyListParam.size() < 1) {
                Toast.makeText(context,R.string.empty_energy,Toast.LENGTH_SHORT).show();
            }

            if (soundListParam.size() < 1) {
                Toast.makeText(context,R.string.empty_sound,Toast.LENGTH_SHORT).show();
            }

            if (genreListParam.size() < 1) {
                Toast.makeText(context,R.string.empty_genre,Toast.LENGTH_SHORT).show();
            }

            if (decadeListParam.isEmpty()) {
                Toast.makeText(context,R.string.empty_decade,Toast.LENGTH_SHORT).show();
            }

            if (!etTitle.getText().toString().matches("") &&
                    !etDesc.getText().toString().matches("") &&
                    soundListParam.size() > 0 && energyListParam.size() > 0 &&
                    genreListParam.size() >0 && !decadeListParam.isEmpty()){
                try {

                    Log.e("mood ", genreListParam.toString());

                    params.put("id", intent.getStringExtra("id"));
                    params.put("name", etTitle.getText().toString());
                    params.put("description", etDesc.getText().toString());
                    params.put("public", typeMood);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        params.put("energy_ids", String.join(",",energyListParam));
                        params.put("sound_ids", String.join(",",soundListParam));
                        params.put("genre_ids", String.join(",",genreListParam));
                        params.put("decade_ids", String.join(",",decadeListParam));
                    } else {
                        params.put("energy_ids", TextUtils.join(",",energyListParam));
                        params.put("sound_ids", TextUtils.join(",",soundListParam));
                        params.put("genre_ids", TextUtils.join(",",genreListParam));
                        params.put("decade_ids", TextUtils.join(",",decadeListParam));
                    }
                    if (!baseImage.isEmpty())
                    {
                        params.put("image", baseImage);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                System.out.println("###### Params = "+params);
                showProgressDialog();
                presenter.updateMoodtrack(params);
            }

        });

        _Switch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                typeMood = "1";
            } else {
                typeMood = "0";
            }
        });

        imgCover.setOnClickListener(view -> {
            showDialogChoice();
        });
    }

    void initPermission() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS_LIST,
                        REQUEST_CODE_CAMERA);
            }
        } else {
//            dispatchTakePictureIntent();
        }
    }

    private void loadingData() {

        Map<String, String> params = new HashMap<String, String>();

        try {
            params.put("id", intent.getStringExtra("id"));
            params.put("genre_ids", intent.getStringExtra("genre_ids"));
            params.put("energy_ids", intent.getStringExtra("energy_ids"));
            params.put("sound_ids", intent.getStringExtra("sound_ids"));
            params.put("decade", intent.getStringExtra("decade"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("** Param ** "+ params);
        presenter.getData(params);
        presenter.getEnergy();
        presenter.getSound();
        presenter.getGenresAll();
        presenter.getDecadeAll();
    }

    @Override
    public void showMood(Moodtrack moodtrack) {

        etTitle.setText(moodtrack.getName());
        etDesc.setText(moodtrack.getDescription());

        if (moodtrack.getPublicPlaylist().equals("1")) {
            _Switch.setChecked(true);
            typeMood = "1";
        } else {
            _Switch.setChecked(false);
            typeMood = "0";
        }

        String url = Constants.URL_IMAGE_MOODTRACK + moodtrack.getImage();
        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(imgCover);

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showEditSuccess(String data) {
        hideProgressDialog();
        finish();
    }

    @Override
    public void showEditError(String data) {
        hideProgressDialog();
        finish();
    }

    @Override
    public void showEnergyList(List<Energy> dataEnergy) {
        shimmerEnergy.stopShimmer();
        shimmerEnergy.setVisibility(View.GONE);
        rvEnergyTag.setVisibility(View.VISIBLE);
        errorLayoutEnergy.setVisibility(View.GONE);
        if (dataEnergy.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvEnergyTag.setLayoutManager(chipsLayoutManager);
            energyAdapter = new EnergyAdapter(
                    this,
                    dataEnergy,
                    this,
                    Constants.Key.EDIT_MOOD,
                    intent.getStringExtra("energy_ids"));
            rvEnergyTag.setAdapter(energyAdapter);
        }
    }

    @Override
    public void showErrorEnergy(String error) {
        shimmerEnergy.stopShimmer();
        shimmerEnergy.setVisibility(View.GONE);
        rvEnergyTag.setVisibility(View.GONE);
        errorLayoutEnergy.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataEnergy error "+error);
    }

    @Override
    public void showSoundList(List<Sound> dataSound) {
        shimmerSound.stopShimmer();
        shimmerSound.setVisibility(View.GONE);
        rvSoundTag.setVisibility(View.VISIBLE);
        errorLayoutSound.setVisibility(View.GONE);
        if (dataSound.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvSoundTag.setLayoutManager(chipsLayoutManager);
            soundAdapter = new SoundAdapter(
                    this,
                    dataSound,
                    this,
                    intent.getStringExtra("sound_ids"),
                    Constants.Key.EDIT_MOOD);
            rvSoundTag.setAdapter(soundAdapter);
        }
    }

    @Override
    public void showErrorSound(String error) {
        shimmerSound.stopShimmer();
        shimmerSound.setVisibility(View.GONE);
        rvSoundTag.setVisibility(View.GONE);
        errorLayoutSound.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataSound error "+error);
    }

    @Override
    public void showGenreList(List<DataGenre> genres) {
        shimmerGenre.stopShimmer();
        shimmerGenre.setVisibility(View.GONE);
        rvGenreTag.setVisibility(View.VISIBLE);
        errorLayoutGenre.setVisibility(View.GONE);
        if (genres.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvGenreTag.setLayoutManager(chipsLayoutManager);
            genresAdapter = new GenresAdapter(
                    this,
                    genres,
                    this,
                    intent.getStringExtra("genre_ids"),
                    Constants.Key.EDIT_MOOD);
            rvGenreTag.setAdapter(genresAdapter);
        }
    }

    @Override
    public void showErrorGenre(String error) {
        shimmerGenre.stopShimmer();
        shimmerGenre.setVisibility(View.GONE);
        rvGenreTag.setVisibility(View.GONE);
        errorLayoutGenre.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataGenre error "+error);
    }

    @Override
    public void showDecadeList(List<Decade> decades) {

        try {
            shimmerDecade.stopShimmer();
            shimmerDecade.setVisibility(View.GONE);
            rvDecadeTag.setVisibility(View.VISIBLE);
            errorLayoutDecade.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (decades.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvDecadeTag.setLayoutManager(chipsLayoutManager);
            decadeAdapter = new DecadeAdapter(
                    this,
                    decades,
                    this,
                    intent.getStringExtra("decade"),
                    Constants.Key.EDIT_MOOD);
            rvDecadeTag.setAdapter(decadeAdapter);
        }
    }

    @Override
    public void showErrorDecade(String error) {
        shimmerDecade.stopShimmer();
        shimmerDecade.setVisibility(View.GONE);
        rvDecadeTag.setVisibility(View.GONE);
        errorLayoutDecade.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataGenre error "+error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Uri imageUri = CropImage.getPickImageResultUri(context, data);

                if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri)) {
                    cropImageUri = imageUri;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                Constants.PERMISSIONS_PHOTO,
                                CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                    }
                } else {
                    startCropImageMoodtrackActivity(imageUri);
                }
            } else {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == Activity.RESULT_OK) {

                Uri resultImageUri = result.getUri();
                imgCover.setImageURI(resultImageUri);
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                baseImage = Helper.convertToBase64(bitmap);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (cropImageUri != null && grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCropImageMoodtrackActivity(cropImageUri);
            } else {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setEnergysParam(List<String> ids) {
        energyListParam = ids;
        System.out.println("***** energyListParam = "+energyListParam.toString());
    }

    public void setSoundsParam(List<String> ids) {
        soundListParam = ids;

        System.out.println("***** soundListParam = "+soundListParam.toString());
    }

    public void setGenresParam(List<String> ids) {
        genreListParam = ids;
        System.out.println("***** genreListParam = "+genreListParam.toString());
    }

    public void setDecadeParam(List<String> decade) {
        decadeListParam = decade;
        System.out.println("***** decadeListParam = "+decadeListParam.toString());
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage(getString(R.string.msg_creating));
        pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }
}
