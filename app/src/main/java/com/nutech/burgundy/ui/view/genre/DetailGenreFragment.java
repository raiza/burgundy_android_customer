package com.nutech.burgundy.ui.view.genre;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.DetailGenrePresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * Created by Arif Setiawan on 10/12/17.
 * Xeranta Mobile Solutions
 */

public class DetailGenreFragment extends BaseFragment implements GenreView {

    @BindView(R.id.rv_tracks_genre)
    RecyclerView rvTracksGenre;
    @BindView(R.id.iv_backdrop)
    ImageView ivBackdrop;
    @BindView(R.id.iv_photo_genre)
    ImageView ivPhotoGenre;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    Unbinder unbinder;
    private DetailGenrePresenter presenter;
    private String genreId = "";
    private String genreImage = "";

    public DetailGenreFragment(String id, String image) {
        genreId = id;
        genreImage = image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new DetailGenrePresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_genre, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews();
        loadingIndicatorView.smoothToShow();
        presenter.getGenreTracks(genreId);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void initViews(){

        String ImageUrl = Constants.URL_IMAGE_GENRES + genreImage;
        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivPhotoGenre);

        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .transform(new BlurTransformation(getActivity()))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivBackdrop);
    }

    @Override
    public void showTracksList(final List<DataTracks> dataTracksList) {
        loadingIndicatorView.smoothToHide();
        rvTracksGenre.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracksGenre.setItemAnimator(new DefaultItemAnimator());
        rvTracksGenre.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        TracksAdapter adapter = new TracksAdapter(getActivity(), dataTracksList, navigation, "",
                new TracksAdapter.OnItemCheckListener() {
                    @Override
                    public void onItemCheck(DataTracks dataTracks) {

                    }

                    @Override
                    public void onItemUncheck(DataTracks dataTracks) {

                    }
                });
        rvTracksGenre.setAdapter(adapter);

        rvTracksGenre.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTracksGenre,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
    }
}
