package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.schedule.ScheduleFragment;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchedulesAdapter extends BaseAdapter<SchedulesAdapter.SchedulesHolder> {

    private List<DataSchedules> list;
    private Context context;
    private EditText etTimer;
    private ScheduleFragment parrent;

    public SchedulesAdapter(Context context, List<DataSchedules> list, ScheduleFragment scheduleFragment) {
        this.list = list;
        this.context = context;
        this.parrent = scheduleFragment;
    }

    @NonNull
    @Override
    public SchedulesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_schedule, parent, false);
        return new SchedulesHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchedulesHolder holder, int position) {
        holder.tvTimer.setText(list.get(position).getTimer());
        holder.tvTitle.setText(list.get(position).getMoodtrackName());
        holder.tvDayOfWeek.setText(context.getResources().
                getStringArray(R.array.days_array)
                [Integer.parseInt(list.get(position).getDayOfWeek())]);

        if (list.get(position).getStatus().matches("1")) {
            holder.tvStatus.setText(R.string.label_schedule_on);
        } else {
            holder.tvStatus.setText(R.string.label_schedule_off);
        }

        if (list.get(position).getRepeat().matches("1")) {
            holder.tvRepeat.setText(R.string.label_repeat);
        }

        String url = Constants.URL_IMAGE_MOODTRACK + list.get(position).getMoodtrackImage();
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        ColorDrawable cd = new ColorDrawable(color);
        loadImagesMoodFromURL(context, holder.imgCover, url, cd);

        holder.imgOptions.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                showOptions(holder.imgOptions,list.get(position).getId(),
                        list.get(position).getSoundtrackId(),list.get(position).getDayOfWeek(),list.get(position).getTimer(),
                        list.get(position).getRepeat(),list.get(position).getStoreId(),
                        list.get(position).getUserId(), list.get(position).getMoodtrackName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SchedulesHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_cover)
        ImageView imgCover;
        @BindView(R.id.img_options)
        ImageView imgOptions;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_day_of_week)
        TextView tvDayOfWeek;
        @BindView(R.id.tv_timer)
        TextView tvTimer;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.ll_desc)
        LinearLayout llDesc;
        @BindView(R.id.cardView)
        CardView cardView;
        @BindView(R.id.tv_repeat)
        TextView tvRepeat;

        SchedulesHolder(View childView) {
            super(childView);
            ButterKnife.bind(this, childView);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showOptions(ImageView btnOptions, String scheduleId, String idMood, String dayOfWeek,
                             String timer, String repeat, String storeId,
                             String userId, String name) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.menu_schedule);
        popup.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.remove) {
                confirmation(scheduleId, idMood, dayOfWeek, storeId,userId, timer, repeat, name);
            } else if (item.getItemId() == R.id.edit_schedule) {
                edit_schedule(scheduleId,idMood,storeId,userId, dayOfWeek, timer, repeat, name);
            }
            return false;
        });
        makePopForceShowIcon(popup);
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            Objects.requireNonNull(mPopup).setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void confirmation(String idSchedule, String idMood, String storeId, String userId, String dayOfWeek,
                              String timer, String repeat, String name) {
        String msg = context.getResources().getString(R.string.label_delete)+" '"+name+"' "+
                context.getResources().getString(R.string.msg_confirm_deleted_schedule);
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_warning_sign);
        alertDialog.setTitle(context.getResources().getString(R.string.label_confirmation_delete));
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.button_ok),
                (dialog, which) -> {
                    dialog.dismiss();
                    parrent.removeSchedule(idSchedule, idMood, dayOfWeek, storeId, userId, timer, repeat);
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getResources().getString(R.string.label_cancel),
                (dialog, which) -> dialog.dismiss());
        Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;

        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void edit_schedule(String idSchedule,String idMood, String storeId, String userId, String dayOfWeek,
                               String timer, String repeat, String name) {

        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.form_add_to_schedule);
        dialog.setCancelable(false);
        NumberPicker numberPicker = dialog.findViewById(R.id.np_day_of_week);
        Button save = dialog.findViewById(R.id.btn_save);
        Button cancel = dialog.findViewById(R.id.btn_cancel);
        etTimer = dialog.findViewById(R.id.et_timer);
        etTimer.setText(timer);
        CheckBox cbRepeat = dialog.findViewById(R.id.cb_repeat);
        //kalau repeatnya 1= true
        cbRepeat.setChecked(repeat.contains("1"));
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(context.getResources().getStringArray(R.array.days_array).length-1);
        numberPicker.setValue(Integer.parseInt(dayOfWeek));
        numberPicker.setDisplayedValues(context.getResources().getStringArray(R.array.days_array));

        dialog.show();

        etTimer.setOnClickListener(v -> showTimeDialog());

        cancel.setOnClickListener(v -> dialog.dismiss());

        save.setOnClickListener(view -> {
            if (etTimer.getText().toString().matches("")) {
                Toast.makeText(context,context.
                        getString(R.string.err_field_required),Toast.LENGTH_SHORT).show();
            } else {
                dialog.dismiss();
                parrent.editSchedulerMoodtrack(context,idSchedule,idMood,
                        String.valueOf(numberPicker.getValue()),etTimer.getText().toString(),
                        timer,cbRepeat.isChecked(),repeat);
            }
        });
    }

    private void showTimeDialog() {

        Calendar calendar = Calendar.getInstance();

        @SuppressLint({"SetTextI18n", "DefaultLocale"}) TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> etTimer.setText(String.format("%02d:%02d", hourOfDay, minute)),
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(context));
        Objects.requireNonNull(timePickerDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;

        timePickerDialog.show();
    }
}
