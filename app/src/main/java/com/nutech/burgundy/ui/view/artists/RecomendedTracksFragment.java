package com.nutech.burgundy.ui.view.artists;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.presenter.ArtistsPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class RecomendedTracksFragment extends BaseFragment implements
        ArtistsView {

    private static final String KEY_ID_ARTIST = "key_id_artist";
    private static final String KEY_ID_GENRE = "key_id_genre";

    @BindView(R.id.rv_tracks_artists)
    RecyclerView rvTracksArtists;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private Unbinder unbinder;
    private ArtistsPresenter presenter;
    private String idArtists;
    private String genreId;
    private TracksAdapter adapter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;

    public RecomendedTracksFragment newInstance(String id, String genreId) {
        RecomendedTracksFragment fragment = new RecomendedTracksFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_ID_ARTIST, id);
        bundle.putString(KEY_ID_GENRE,genreId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
//        presenter.getTracksRecomended(idArtists, genreId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        idArtists = getArguments().getString(KEY_ID_ARTIST);
        genreId = getArguments().getString(KEY_ID_GENRE);

        presenter = new ArtistsPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        dmApplication = (DmApplication) getActivity().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_artist_tracks, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadingIndicatorView.smoothToShow();
        presenter.getTracksRecomended(idArtists, "0");
        System.out.println("------- Get Recomwndede _------");
        return view;
    }

    @Override
    public void showAllArtists(DataArtistList artistsList) {
        System.out.println("------- showAllArtists _------");
    }

    @Override
    public void showGenre(List<DataGenre> genreList) {
        System.out.println("------- showGenre _------");
    }

    @Override
    public void showArtistsTracks(final List<DataTracks> tracksList) {

        System.out.println("------- showArtistsTracks _------"+tracksList);

        loadingIndicatorView.smoothToHide();
        rvTracksArtists.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracksArtists.setItemAnimator(new DefaultItemAnimator());
        rvTracksArtists.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        adapter = new TracksAdapter(getActivity(), tracksList, navigation, "", new TracksAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(DataTracks dataTracks) {

            }

            @Override
            public void onItemUncheck(DataTracks dataTracks) {

            }
        });
        rvTracksArtists.setAdapter(adapter);

        rvTracksArtists.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTracksArtists,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

    }
    @Override
    public void showArtistsAlbums(List<DataAlbums> albumsList) {
        System.out.println("------- showArtistsAlbums _------");
    }

    @Override
    public void showArtistsSearch(List<DataArtists> artistsList) {

    }

    @Override
    public void showErrorSearch(String error) {

    }

    @Override
    public void showError(String error) {
        Log.e("Error", error);
        System.out.println("------- error _------"+error    );
        loadingIndicatorView.smoothToHide();

    }


}
