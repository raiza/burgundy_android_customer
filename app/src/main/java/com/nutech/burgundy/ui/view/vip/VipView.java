package com.nutech.burgundy.ui.view.vip;

import com.nutech.burgundy.data.model.DataSubscribe;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public interface VipView extends BaseView {

    void showPriceAll(List<DataSubscribePrice> subscribePriceList);

    void showDetailPrice(DataSubscribePrice subscribePrice);

    void showDataSubscribe(DataSubscribe dataSubscribe);

    void showError(String error);
}
