package com.nutech.burgundy.ui.view.splash;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.intro.MainIntrosActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Arif Setiawan on 12/4/17.
 * Xeranta Mobile Solutions
 */

public class BlockedActivity extends BaseAppCompatActivity {

    @OnClick(R.id.bt_close)
    public void closeApp(){
        sessionManager.logout();
        finishAffinity();
        System.exit(0);
    }

    @OnClick(R.id.bt_relogin)
    public void reLoginApp(){
        sessionManager.logout();
        Intent intent = new Intent(this, MainIntrosActivity.class);
        startActivity(intent);
    }

    private SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked);
        ButterKnife.bind(this);
        sessionManager = getSessionManager();
    }

    private void showWarning(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Peringatan");
        alertDialog.setMessage("Login ulang "+ "\n" +
                "Anda tidak bisa mengakses aplikasi diperangkat ini.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Mengerti",
                (dialog, which) -> {
                    dialog.dismiss();

                    finishAffinity();
                    System.exit(0);
                });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
