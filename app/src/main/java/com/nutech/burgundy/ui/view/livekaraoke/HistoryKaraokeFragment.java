package com.nutech.burgundy.ui.view.livekaraoke;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.SegmentedButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public class HistoryKaraokeFragment extends BaseFragment {

    @BindView(R.id.segmented)
    SegmentedButton buttons;
    @BindView(R.id.frame)
    FrameLayout mainFrame;

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_karaoke, container, false);
        unbinder = ButterKnife.bind(this, view);

        setUpTabLayout();
        replaceFragment(new HistoryByCustomerFragment());
        return view;
    }

    private void setUpTabLayout() {

        buttons.clearButtons();
        buttons.addButtons(
//                getString(R.string.live),
                getString(R.string.your_history),
                getString(R.string.all_history));

        buttons.setPushedButtonIndex(0);

        buttons.setOnClickListener(new SegmentedButton.OnClickListenerSegmentedButton() {
            @Override
            public void onClick(int index) {

                if (index == 0) {
                    replaceFragment(new HistoryByCustomerFragment());
                } else {
                    replaceFragment(new HistoryByAllFragment());
                }

                /*
                if (index == 0) {
                    replaceFragment(new HistoryLiveKaraokeFragment());
                } else if (index == 1){
                    replaceFragment(new HistoryByCustomerFragment());
                } else {
                    replaceFragment(new HistoryByAllFragment());
                }
                */
            }
        });

    }

    private void replaceFragment(Fragment fragment) {

        if (mainFrame != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(mainFrame.getId(), fragment).commit();
        }
    }


}
