package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataOfficial;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 13/02/18.
 * Xeranta Mobile Solutions
 */

public class OfficialAdapter extends RecyclerView.Adapter<OfficialAdapter.OfficialHolder> {

    private Context context;
    private List<DataOfficial> list;

    public OfficialAdapter (
            Context context, List<DataOfficial> list) {
        this.context = context;
        this.list = list;
    }

    class OfficialHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_official_name)
        TextView tvOfficial;
        @BindView(R.id.iv_official)
        ImageView ivOfficial;
        View childView;

        OfficialHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public OfficialAdapter.OfficialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_official, parent, false);
        return new OfficialAdapter.OfficialHolder(view);
    }

    @Override
    public void onBindViewHolder(OfficialAdapter.OfficialHolder holder, int position) {

        holder.tvOfficial.setText(list.get(position).getMedsos_name());


        String ImageUrl = Constants.URL_IMAGE_OFFICIAL+list.get(position).getMedsos_image();

        Picasso.with(context.getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivOfficial);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
