package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.utils.Helper;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NearestStoreAdapter extends BaseAdapter<NearestStoreAdapter.NearestStoreHolder> {

    private Context context;
    private List<DataStore> storeList;

    public NearestStoreAdapter(Context context, List<DataStore> storeList) {
        this.context = context;
        this.storeList = storeList;
    }

    class NearestStoreHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_store_logo)
        ImageView ivStoreLogo;
        @BindView(R.id.tv_store_name)
        TextView tvStoreName;
        @BindView(R.id.tv_distance)
        TextView tvDistance;

        NearestStoreHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public NearestStoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_nearest_store, parent, false);
        return new NearestStoreHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NearestStoreHolder holder, int position) {
        String url = Constants.URL_STORE_LOGO + storeList.get(position).getLogo();
        String distance = Helper.getDistance(storeList.get(position).getDistance());

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        ColorDrawable cd = new ColorDrawable(color);

        holder.tvDistance.setText(distance);
        holder.tvStoreName.setText(storeList.get(position).getSite_name());

        loadImagesMoodFromURL(context, holder.ivStoreLogo, url, cd);


    }

    @Override
    public int getItemCount() {
        return storeList.size();
    }
}
