package com.nutech.burgundy.ui.view.search;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;

import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackFragment;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataSearch;
import com.nutech.burgundy.presenter.SearchPresenter;
import com.nutech.burgundy.ui.adapter.SearchAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.album.DetailAlbumFragment;
import com.nutech.burgundy.ui.view.artists.TabArtistFragment;
import com.nutech.burgundy.ui.view.playlist.DetailPlaylistFragment;
import com.nutech.burgundy.ui.view.track.DetailTracksFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class SearchFragment extends BaseFragment implements SearchView {

    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.recycle_search)
    RecyclerView rvSearch;
    @BindView(R.id.loading)
    AVLoadingIndicatorView avLoadingIndicatorView;

    private Unbinder unbinder;
    private SearchPresenter presenter;
    private Timer timer;
    private final long DELAY = 2000; //--- on milisecond

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SearchPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();

        return view;
    }

    @Override
    public void showResultSearch(final List<DataSearch> dataSearchList) {

        if (dataSearchList.size() > 0) {
            rvSearch.setVisibility(View.VISIBLE);
            avLoadingIndicatorView.setVisibility(View.GONE);
        } else {
            rvSearch.setVisibility(View.GONE);
            avLoadingIndicatorView.setVisibility(View.GONE);
        }

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_animation_fall_down);
        rvSearch.setLayoutAnimation(animation);
        rvSearch.setAdapter(new SearchAdapter(getActivity(), dataSearchList));
        rvSearch.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvSearch,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                        Helper.hideSoftKeyBoard(Objects.requireNonNull(getActivity()));

                        Log.e("data_type", dataSearchList.get(position).getData_type());

                        if (dataSearchList.get(position).getData_type().contains("Artist")){
                            navigation.pushFragment(
                                    new TabArtistFragment(
                                            dataSearchList.get(position).getId(),
                                            dataSearchList.get(position).getImage(),
                                            "0"),
                                    dataSearchList.get(position).getName(),
                                    true);
                        } else if (dataSearchList.get(position).getData_type().contains("Track")){

                            navigation.pushFragment(
                                    new DetailTracksFragment(
                                            dataSearchList.get(position).getId(),
                                            dataSearchList.get(position).getImage()),
                                    dataSearchList.get(position).getName(),
                                    true);

                        } else if (dataSearchList.get(position).getData_type().contains("Playlist")){
                            navigation.pushFragment(
                                    new DetailPlaylistFragment(
                                            dataSearchList.get(position).getId(),
                                            dataSearchList.get(position).getImage(),
                                            Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_PLAYLIST),
                                    dataSearchList.get(position).getName(),
                                    true);
                        } else if (dataSearchList.get(position).getData_type().contains("Album")){
                            navigation.pushFragment(
                                    new DetailAlbumFragment(
                                            dataSearchList.get(position).getId(),
                                            dataSearchList.get(position).getImage()),
                                    dataSearchList.get(position).getName(),
                                    true);
                        } else if (dataSearchList.get(position).getData_type().contains("Soundtracks")){
                            navigation.pushFragment(
                                    new DetailMoodtrackFragment(
                                            dataSearchList.get(position).getId(),false),
                                    dataSearchList.get(position).getName(),
                                    true);
                        }
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showError(String error) {

    }

    private void initView() {

        rvSearch.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSearch.setItemAnimator(new DefaultItemAnimator());
        rvSearch.addItemDecoration(new DividerItemDecoration(
                Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL));

        rvSearch.setVisibility(View.GONE);
        avLoadingIndicatorView.setVisibility(View.GONE);
    }

    private void initEvent() {

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                rvSearch.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(final Editable s) {

                if (s.length() > 3) {
                    avLoadingIndicatorView.setVisibility(View.VISIBLE);
                    Map<String,String> params = new HashMap<>();
                    params.put("name", s.toString());
                    params.put("user_type",getSessionManager().getUserType());
                    if (getSessionManager().getUserType().contains(Constants.UserType.USER_STORE)) {
                        params.put("id", getSessionManager().getUserStore().getStore_id());
                    } else {
                        params.put("id", getSessionManager().getDataMember().getId());
                    }
//                    String param = s.toString();
                    presenter.getSearchByName(params);
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
