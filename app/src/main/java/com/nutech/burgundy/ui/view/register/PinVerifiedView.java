package com.nutech.burgundy.ui.view.register;

import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataOperator;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 12/3/17.
 * Xeranta Mobile Solutions
 */

public interface PinVerifiedView extends BaseView {

    void showSuccess(DataMember dataMember);

    void showError(String Error);

    void successResend(String succes);
    void errorResend(String error);

    void showOperator(List<DataOperator> dataOperator);
    void showErrorOperator(String error);
}
