package com.nutech.burgundy.ui.view.stores;

import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

public interface StoresView extends BaseView {
    void ShowStores(List<DataStore> storeList);

    void ShowStoresError(String error);
}
