package com.nutech.burgundy.ui.view.vip;

import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.data.model.melon.ResponseTransaction;
import com.nutech.burgundy.data.model.melon.Token;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 12/6/17.
 * Xeranta Mobile Solutions
 */

public interface DetailPriceView extends BaseView {

    void showDataSubscribePrice(DataSubscribePrice dataSubscribePrice);

    void showSuccess(DataUserSubscribe dataSubscribe);

    void showSuccessFromMelon(DataUserSubscribe dataSubscribe);

    void showSuccesTransactionToken(Token token);

    void showPayTransaction(ResponseTransaction responseTransaction);

    void showChargeTransaction(ResponseTransaction responseTransaction);

    void showErrorChargeTransaction(String error);

    void showErrorPayTransaction(String error);

    void showErrorTransactionToken(String error);

    void showError(String error);

    void showErrorFromMelon(String error);
}
