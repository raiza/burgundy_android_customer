package com.nutech.burgundy.ui.view.follow;

import com.nutech.burgundy.data.model.DataOfficial;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public interface OfficialView extends BaseView {

    void showDataOfficial(List<DataOfficial> listDataOfficial);

    void showError(String error);

}
