package com.nutech.burgundy.ui.view.setting;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.view.about.AboutFragment;
import com.nutech.burgundy.ui.view.register.StoreLoginActivity;
import com.nutech.burgundy.utils.Helper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

//import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * @author Arif Setiawan on 18/05/19.
 * @company NuTech
 */

public class SettingFragment extends BaseFragment implements SettingView {

    @BindView(R.id.iv_member)
    CircleImageView ivMember;
    @BindView(R.id.tv_name_member)
    TextView tvNameMember;
    @BindView(R.id.tv_type_member)
    TextView tvTypeMember;
    @BindView(R.id.tv_point)
    TextView tvPoint;
    @BindView(R.id.cardView)
    CardView cardView;
    @BindView(R.id.ll_about)
    LinearLayout llAbout;
    @BindView(R.id.btn_logout)
    Button btnLogout;

    @OnClick(R.id.btn_logout)
    public void logout() {

        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        sessionManager.logout();
        Intent intent = new Intent(getActivity(), StoreLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @OnClick(R.id.ll_about)
    public void moveToAbout() {
        navigation.pushFragment(new AboutFragment(),getString(R.string.about_menu),true);
    }

    private Unbinder unbinder;
    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);

        initEvent();
        initView();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @SuppressLint("SetTextI18n")
    private void initView() {

        if (sessionManager.getUserType().equals(Constants.UserType.USER_PUBLIC)) {
            DataMember dataMember = sessionManager.getDataMember();
                    tvNameMember.setText(dataMember.getName());

        if (sessionManager.getStatusPackage().equals("1")) {
            tvTypeMember.setText(getString(R.string.label_subscribe));
        } else {
            tvTypeMember.setText(getString(R.string.label_not_subscribe));
        }

        tvPoint.setText(dataMember.getPoint() + " Point");

        String url = Constants.URL_IMAGE_MEMBER + dataMember.getImage();
        Picasso.with(context)
                .load(url)
                .error(R.drawable.default_user)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivMember);
        } else {
            DataUserStore dataUserStore = sessionManager.getUserStore();
            tvNameMember.setText(dataUserStore.getName());

            if (sessionManager.getStatusPackage().equals("1")) {
                tvTypeMember.setText(getString(R.string.label_subscribe));
            } else {
                tvTypeMember.setText(getString(R.string.label_not_subscribe));
            }

            String url = Constants.URL_IMAGE_MEMBER + dataUserStore.getImage();
            Picasso.with(context)
                    .load(url)
                    .error(R.drawable.default_user)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(ivMember);
        }
    }

    private void initEvent() {

    }
}
