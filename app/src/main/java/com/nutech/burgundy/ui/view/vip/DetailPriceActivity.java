package com.nutech.burgundy.ui.view.vip;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.data.model.melon.ResponseTransaction;
import com.nutech.burgundy.data.model.melon.Token;
import com.nutech.burgundy.presenter.DetailPricePresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

//import com.anjlab.android.iab.v3.BillingProcessor;
//import com.anjlab.android.iab.v3.TransactionDetails;

/**
 * Created by Arif Setiawan on 12/6/17.
 * Xeranta Mobile Solutions
 */

/**
 * Changed activity because onActivityResult fragment not working for purchase billing.
 */

public class DetailPriceActivity extends BaseAppCompatActivity implements DetailPriceView,
        PurchasesUpdatedListener {

    static final String TAG = "DetailPriceActivity";
    private static final String LOG_TAG = "DangdutAsik";

    private String PRODUCT_ID = "";
    private static final String SUBSCRIPTION_ID = "com.anjlab.test.iab.subs1";
    private static final String LICENSE_KEY = Constants.Key.KEY_RSA;
    private static final String MERCHANT_ID = Constants.Key.MERCHANT_ID;

//    private BillingProcessor bp;
    private boolean readyToPurchase = false;


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bt_submit)
    Button btBuy;
    @BindView(R.id.tv_subscribe_name)
    TextView tvSubscribeName;
    @BindView(R.id.tv_subscribe_desc)
    TextView tvSubscribeDesc;
    @BindView(R.id.tv_subscribe_price)
    TextView tvSubscribePrice;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    @OnClick(R.id.bt_consume)
    public void consume(){
        //Only for testing!
//        Boolean consumed = bp.consumePurchase(PRODUCT_ID);
//        if (consumed)
//            showToast("Successfully consumed");
        Log.e(TAG,"Konsumsi berhasil & Selesai");
    }

    private Unbinder unbinder;
    private DetailPricePresenter presenter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;

    private Intent intent;
    private String id;
    private DataSubscribePrice subscribePrice;
    private BottomDialog bottomDialogChoice, bottomDialogMelon;
    private LinearLayout llMainView, llProccessView, llErrorView, llSuccessView;
//    private BillingManager mBillingManager;
//    private BillingProvider mBillingProvider;
    private BillingClient mBillingClient;
    private Activity activity;
    private Button btnAction;
    private TextView tvErrorMessageMelon, tvSuccessMessage;
    private String transactionId = "";

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new DetailPricePresenter(getDataManager(), getNetworkHelper(), getSessionManager() ,getApplicationContext());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        dmApplication = (DmApplication) getApplicationContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_detail_price);
        ButterKnife.bind(this);
        activity = this;
        mBillingClient = BillingClient.newBuilder(this).setListener(this).build();
        intent = getIntent();
        presenter = new DetailPricePresenter(getDataManager(), getNetworkHelper(), getSessionManager(), getApplicationContext());
        presenter.attachView(this);
        sessionManager = getSessionManager();

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(intent.getStringExtra("name"));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        handleManagerAndUiReady();

        id =  intent.getStringExtra("id");

        getData();
        toConsume();
        initPayBilling();

        btBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                toPurchase();
//                buyViaMelon();
                String payType = intent.getStringExtra("paymentType");
                if (payType.equals("all")) {
                    choicePayment();
                } else if (payType.equals("google_play")) {
                    toPurchase();
                } else if (payType.equals("melon")) {
                    confirmPurchaseMelon();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void toPurchase(){
//        toConsume();
//        bp.purchase(this, PRODUCT_ID);
        Log.e(TAG, "startPurchaseFlow");
        startPurchaseFlow(PRODUCT_ID,
                "subs");

        Log.e("SKU", PRODUCT_ID);
    }

    private void toConsume(){
        try {
//            Boolean consumed = bp.consumePurchase(PRODUCT_ID);
//            if (consumed)
//            showToast("Successfully consumed");
                Log.e(TAG,"Konsumsi berhasil & Selesai");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getData(){
        presenter.getDetailPrice(id);
    }

    private void initPayBilling() {

//        if(!BillingProcessor.isIabServiceAvailable(this)) {
////            showToast("In-app billing service is unavailable, please upgrade Android Market/Play to version >= 3.9.16");
//        }
//
//        bp = new BillingProcessor(this, LICENSE_KEY, MERCHANT_ID, new BillingProcessor.IBillingHandler() {
//            @Override
//            public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
//                Log.e(TAG,"orderId = "+details.orderId);
//                Log.e(TAG,"productId = "+details.productId);
//                Log.e(TAG,"purchaseToken = "+details.purchaseToken);
//                Log.e(TAG,"purchaseInfo = "+details.purchaseInfo);
//                Log.e(TAG,"purchaseTime = "+details.purchaseTime);
//
//            }
//            @Override
//            public void onBillingError(int errorCode, @Nullable Throwable error) {
//            }
//            @Override
//            public void onBillingInitialized() {
////                showToast("onBillingInitialized");
//                readyToPurchase = true;
//            }
//            @Override
//            public void onPurchaseHistoryRestored() {
////                showToast("onPurchaseHistoryRestored");
//                for(String sku : bp.listOwnedProducts())
//                    Log.d(LOG_TAG, "Owned Managed Product: " + sku);
//                for(String sku : bp.listOwnedSubscriptions())
//                    Log.d(LOG_TAG, "Owned Subscription: " + sku);
//            }
//        });

    }

    @Override
    public void showDataSubscribePrice(DataSubscribePrice dataSubscribePrice) {
        btBuy.setVisibility(View.VISIBLE);
        this.subscribePrice = dataSubscribePrice;

        //Testing Only
//        PRODUCT_ID = "android.test.purchased";

        PRODUCT_ID = subscribePrice.getProduct_id();

        Log.e(TAG, "PRODUCT_ID"+PRODUCT_ID);

        String price = "IDR "+Helper.formatedPrice(subscribePrice.getPrice());

        tvSubscribeName.setText(subscribePrice.getName());
        tvSubscribeDesc.setText(subscribePrice.getDescription());
        tvSubscribePrice.setText(price);
    }

    @Override
    public void showSuccess(DataUserSubscribe dataSubscribe) {
        loadingIndicatorView.smoothToHide();
        confirmSuccess();
    }

    @Override
    public void showSuccessFromMelon(DataUserSubscribe dataSubscribe) {
        loadingIndicatorView.smoothToHide();
        visibleSuccessView();
    }

    @Override
    public void showSuccesTransactionToken(Token token) {
        Log.e("Token", token.getToken());
        sessionManager.setToken(token);
        payTransaction();
    }

    @Override
    public void showPayTransaction(ResponseTransaction responseTransaction) {
        Log.e("respon Code", responseTransaction.getResponse_code());
        Log.e("Status", responseTransaction.getTransaction_status());

        if (responseTransaction.getResponse_code().equals("0")){
            subscribeCustomerFromMelon(responseTransaction);
        } else if (responseTransaction.getResponse_code().equals("1")){
            subscribeCustomerFromMelon(responseTransaction);
        } else {
            visibleErrorView(responseTransaction.getResponse_code());
        }
    }

    @Override
    public void showChargeTransaction(ResponseTransaction responseTransaction) {
        Log.e("respon Code", responseTransaction.getResponse_code());
        Log.e("Status", responseTransaction.getTransaction_status());
        if (responseTransaction.getResponse_code().equals("0")){
        } else if (responseTransaction.getResponse_code().equals("1")){
        } else {
            visibleErrorView(responseTransaction.getResponse_code());
        }
    }

    @Override
    public void showErrorChargeTransaction(String error) {

    }

    @Override
    public void showErrorPayTransaction(String error) {
    }

    @Override
    public void showErrorTransactionToken(String error) {
    }

    @Override
    public void showError(String error) {
//        btBuy.setVisibility(View.GONE);
    loadingIndicatorView.smoothToHide();

    }

    @Override
    public void showErrorFromMelon(String error) {
        visibleSuccessView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onDestroy() {

        presenter.detachView();
//        if (bp != null)
//            bp.release();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("***************", "result Code "+resultCode);
//        if (!bp.handleActivityResult(requestCode, resultCode, data))
//            super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == -1){
//            subscribeCustomer();
//        }
    }


//    private void showToast(String message) {
//        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
//    }

    public void subscribeCustomerFromGoogle(String orderID, String pckgName, String purchaseToken,
                                  String signature, String sku, Date currentDate, String price) {

        String customerId = sessionManager.getDataMember().getId();

        Map<String, String> fields = new HashMap<>();
        fields.put("subscribe_id", sku);
        fields.put("customer_id", customerId);
        fields.put("pay_with", "0");
        fields.put("pay_amount", price);
        fields.put("pay_date", Helper.todayDate());
        fields.put("payment_status", "1");
        fields.put("payment_type", Constants.PAYTIPE.GOOGLE);
        fields.put("order_id", orderID);
        fields.put("order_date", Helper.formatOrderDate(currentDate));
        fields.put("status", Constants.Status.SUCCESS);
        fields.put("token", purchaseToken);
        fields.put("signature", signature);
        fields.put("start_date", Helper.formatOrderDate(currentDate));

        System.out.println("Param : "+fields);
        presenter.subscribeCustomer(fields);
    }

    public void subscribeCustomerFromMelon(ResponseTransaction responseTransaction) {

        String customerId = sessionManager.getDataMember().getId();

        Map<String, String> fields = new HashMap<>();
        fields.put("subscribe_id", intent.getStringExtra("itemCode"));
        fields.put("customer_id", customerId);
        fields.put("pay_with", "0");
        fields.put("pay_amount", intent.getStringExtra("price"));
        fields.put("pay_date", Helper.todayDate());
        fields.put("payment_status", "0");
        fields.put("payment_type", Constants.PAYTIPE.MELON);
        fields.put("order_id", transactionId);
        fields.put("status", Constants.Status.PENDING);

        System.out.println("Param : "+fields);

        presenter.subscribeCustomer(fields);
    }

    private void getSKUDetails() {
        SkuDetailsResponseListener responseListener = new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode,
                                             List<SkuDetails> skuDetailsList) {
                // If we successfully got SKUs, add a header in front of it
                if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
                    // Repacking the result for an adapter
                    for (SkuDetails details : skuDetailsList) {
                        Log.e(TAG, "Found sku: " + details);
//                        listSKUDetails = skuDetailsList;
//
//                        System.out.println("listSKUDetails = "+ listSKUDetails.size());
                    }
                }
            }
        };
    }

    private void choicePayment() {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.choice_purchase_via, null);

        ImageView imgGooglePlay = (ImageView) customView.findViewById(R.id.iv_google_play);
        ImageView imgMelon = (ImageView) customView.findViewById(R.id.iv_melon);

        imgGooglePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialogChoice.dismiss();
                toPurchase();
            }
        });

        imgMelon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomDialogChoice.dismiss();
                confirmPurchaseMelon();
            }
        });

        bottomDialogChoice = new BottomDialog.Builder(this)
                .setTitle("Choose your payment method")
                .setCustomView(customView)
                .show();
    }

    private void confirmPurchaseMelon() {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.confirm_melon_payment_layout, null);

        TextView tvProductName = (TextView) customView.findViewById(R.id.product_name);
        TextView tvProductDesc = (TextView) customView.findViewById(R.id.product_description);
        TextView tvProductPrice = (TextView) customView.findViewById(R.id.product_price);
        TextView tvPriceDesc = (TextView) customView.findViewById(R.id.price_description);
        TextView tvOperator = (TextView) customView.findViewById(R.id.operator_name);
        btnAction = (Button) customView.findViewById(R.id.btn_action);
        tvErrorMessageMelon = (TextView) customView.findViewById(R.id.tv_error_melon);
        tvSuccessMessage = (TextView) customView.findViewById(R.id.tvmsg_success);
        llMainView = (LinearLayout) customView.findViewById(R.id.ll_main);
        llProccessView = (LinearLayout) customView.findViewById(R.id.ll_proccess);
        llErrorView = (LinearLayout) customView.findViewById(R.id.ll_error);
        llSuccessView = (LinearLayout) customView.findViewById(R.id.ll_success);

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnAction.getText().equals(getResources().getString(R.string.label_subscribe))) {
                    requestTokenTransaction();
                } else {
                    bottomDialogMelon.dismiss();
                }
            }
        });

        tvProductName.setText(intent.getStringExtra("name"));
        tvProductDesc.setText(intent.getStringExtra("description"));
        tvProductPrice.setText("IDR "+intent.getStringExtra("price"));

        String operator = sessionManager.getOperator().getFullName();
        tvOperator.setText(operator);

        bottomDialogMelon = new BottomDialog.Builder(this)
                .setTitle(getResources().getString(R.string.label_melon_payment))
                .setCustomView(customView)
                .setCancelable(true)
                .show();
    }

    private void visibleMainView() {
        llMainView.setVisibility(View.VISIBLE);
        llProccessView.setVisibility(View.GONE);
        llErrorView.setVisibility(View.GONE);
        llSuccessView.setVisibility(View.GONE);
    }

    private void visibleProccessView() {
        llMainView.setVisibility(View.GONE);
        llProccessView.setVisibility(View.VISIBLE);
        llErrorView.setVisibility(View.GONE);
        llSuccessView.setVisibility(View.GONE);

        btnAction.setText(getResources().getString(R.string.button_ok));
    }

    private void visibleErrorView(String responseCode) {
        llMainView.setVisibility(View.GONE);
        llProccessView.setVisibility(View.GONE);
        llErrorView.setVisibility(View.VISIBLE);
        llSuccessView.setVisibility(View.GONE);

        String msg = "";
        if (responseCode.equals("103")) {
            msg = getResources().getString(R.string.msg_error_transaction_melon);
        } else if (responseCode.equals("104")) {
            msg = getResources().getString(R.string.msg_error_transaction_melon);
        }
        tvErrorMessageMelon.setText(msg);
        btnAction.setText(getResources().getString(R.string.button_ok));
    }

    private void visibleSuccessView() {
        llMainView.setVisibility(View.GONE);
        llProccessView.setVisibility(View.GONE);
        llErrorView.setVisibility(View.GONE);
        llSuccessView.setVisibility(View.VISIBLE);

        btnAction.setText(getResources().getString(R.string.button_ok));
    }

    /**
     * Executes query for SKU details at the background thread
     */
    private void handleManagerAndUiReady() {
        SkuDetailsResponseListener responseListener = new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode,
                                             List<SkuDetails> skuDetailsList) {
                // If we successfully got SKUs, add a header in front of it
                if (responseCode == BillingClient.BillingResponse.OK && skuDetailsList != null) {
                    // Repacking the result for an adapter
                    for (SkuDetails details : skuDetailsList) {
                        Log.e(TAG, "Found sku: " + details);
                    }
                }
            }
        };
    }

    private void confirmSuccess() {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.title_confirmation));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getString(R.string.msg_succes_subscriber));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(DetailPriceActivity.this, PrimaryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }
                });
        alertDialog.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        Log.e(TAG, "onPurchasesUpdated() response: " + responseCode);
        getSKUDetails();
        String orderID = "";
        String pckgName = "";
        String purchaseToken = "";
        String signature = "";
        String sku = "";
        long date = 0;
//        String price = "";
        Date currentDate = null;

        if (responseCode == 0) {
            Log.e(TAG, "purchases size: " + purchases.size());
            for (int i = 0; i < purchases.size(); i++) {

                orderID = purchases.get(i).getOrderId();
                pckgName = purchases.get(i).getPackageName();
                purchaseToken = purchases.get(i).getPurchaseToken();
                signature = purchases.get(i).getSignature();
                sku = purchases.get(i).getSku();
                date = purchases.get(i).getPurchaseTime();
            }

            currentDate = new Date(date);

//            for (int i = 0; i <listSKUDetails.size(); i++) {
//
//                if (sku.equals(listSKUDetails.get(i).getSku())) {
//                    price = listSKUDetails.get(i).getPrice();
//                }
//            }

            subscribeCustomerFromGoogle(orderID, pckgName, purchaseToken, signature, sku, currentDate, intent.getStringExtra("price"));

        } else {

        }

        System.out.println("Response Purchase "+"\n"+
                "Order ID: " + "\n" +
                orderID + "\n" +
                "Package Name: " + "\n" +
                pckgName + "\n" +
                "purchase Token: " + "\n" +
                purchaseToken + "\n" +
                "Signature: " + "\n" +
                signature + "\n" +
                "SKU: " + "\n" +
                sku + "\n" +
                "Date: " + "\n" +
                currentDate);
    }

    public void startPurchaseFlow(final String skuId, final String billingType) {
        // Specify a runnable to start when connection to Billing client is established
        Runnable executeOnConnectedService = new Runnable() {
            @Override
            public void run() {
                BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                        .setType(billingType)
                        .setSku(skuId)
                        .build();
                mBillingClient.launchBillingFlow(activity, billingFlowParams);
            }
        };

        // If Billing client was disconnected, we retry 1 time and if success, execute the query
        startServiceConnectionIfNeeded(executeOnConnectedService);
    }

    /**
     * Trying to restart service connection if it's needed or just execute a request.
     * <p>Note: It's just a primitive example - it's up to you to implement a real retry-policy.</p>
     *
     * @param executeOnSuccess This runnable will be executed once the connection to the Billing
     *                         service is restored.
     */
    private void startServiceConnectionIfNeeded(final Runnable executeOnSuccess) {
        if (mBillingClient.isReady()) {
            if (executeOnSuccess != null) {
                executeOnSuccess.run();
            }
        } else {
            mBillingClient.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponse) {
                    if (billingResponse == BillingClient.BillingResponse.OK) {
                        Log.i(TAG, "onBillingSetupFinished() response: " + billingResponse);
                        if (executeOnSuccess != null) {
                            executeOnSuccess.run();
                        }
                    } else {
                        Log.w(TAG, "onBillingSetupFinished() error code: " + billingResponse);
                    }
                }

                @Override
                public void onBillingServiceDisconnected() {
                    Log.w(TAG, "onBillingServiceDisconnected()");
                }
            });
        }
    }

    private void requestTokenTransaction() {

        visibleProccessView();

        String orderId = "DA-"+Helper.generateCode();
        HashMap<String, String> params = new HashMap<>();
        params.put("order_id", orderId);
        params.put("item", intent.getStringExtra("itemCode"));
        params.put("merchant_id", intent.getStringExtra("merchantId"));
        params.put("name", intent.getStringExtra("merchantName"));
        params.put("callback_url", Constants.CMS);
        params.put("success_url", Constants.CMS);
        params.put("cancel_url", Constants.CMS);

        transactionId = orderId;
        presenter.requestToken(params);
    }

    private void payTransaction() {

        String paymentType = "";
        if (sessionManager.getOperator().getShortName().equals("TSEL")) {
            paymentType = Constants.PAYMENT_CHANNEL.TELKOMSEL;
        } else if (sessionManager.getOperator().getShortName().equals("3")) {
            paymentType = Constants.PAYMENT_CHANNEL.THREE;
        } else if (sessionManager.getOperator().getShortName().equals("XL")) {
            paymentType = Constants.PAYMENT_CHANNEL.XL;
        } else if (sessionManager.getOperator().getShortName().equals("SM")) {
            paymentType = Constants.PAYMENT_CHANNEL.SMARTFREN;
        } else if (sessionManager.getOperator().getShortName().equals("ISAT")) {
            paymentType = Constants.PAYMENT_CHANNEL.INDOSAT;
        } else {

        }

        HashMap<String, String> params = new HashMap<>();
        params.put("payment_type", paymentType);
        params.put("phone_number", sessionManager.getDataMember().getMobile_phone());
        params.put("transaction_id", sessionManager.getToken().getToken());

        presenter.payTransaction(params);
    }

    private void chargeTransaction() {
        String paymentType = "";
        if (sessionManager.getOperator().getShortName().equals("TSEL")) {
            paymentType = Constants.PAYMENT_CHANNEL.TELKOMSEL;
        } else if (sessionManager.getOperator().getShortName().equals("3")) {
            paymentType = Constants.PAYMENT_CHANNEL.THREE;
        } else if (sessionManager.getOperator().getShortName().equals("XL")) {
            paymentType = Constants.PAYMENT_CHANNEL.XL;
        } else if (sessionManager.getOperator().getShortName().equals("SM")) {
            paymentType = Constants.PAYMENT_CHANNEL.SMARTFREN;
        } else if (sessionManager.getOperator().getShortName().equals("ISAT")) {
            paymentType = Constants.PAYMENT_CHANNEL.INDOSAT;
        } else {

        }
        HashMap<String, String> params = new HashMap<>();
        params.put("payment_type", paymentType);
        params.put("phone_number", sessionManager.getDataMember().getMobile_phone());
        params.put("transaction_id", sessionManager.getToken().getToken());

        presenter.chargeTransaction(params);
    }
}
