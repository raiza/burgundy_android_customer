package com.nutech.burgundy.ui.view.register;

import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 9/27/17.
 * Xeranta Mobile Solutions
 */

public interface RegisterView extends BaseView {

    void showRegisterSuccess(String success);

    void showRegisterFailed(String error);
}
