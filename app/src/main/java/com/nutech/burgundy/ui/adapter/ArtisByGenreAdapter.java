package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataGenre;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class ArtisByGenreAdapter extends RecyclerView.Adapter<ArtisByGenreAdapter.TimelineHolder> {

    private Context context;
    private List<DataGenre> list;

    public ArtisByGenreAdapter(
            Context context, List<DataGenre> list) {
        this.context = context;
        this.list = list;
    }

    class TimelineHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_genre_name)
        TextView tvGenre;
        View childView;

        TimelineHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public TimelineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_artisbygenre, parent, false);
        return new TimelineHolder(view);
    }

    @Override
    public void onBindViewHolder(TimelineHolder holder, int position) {

        holder.tvGenre.setText(list.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
