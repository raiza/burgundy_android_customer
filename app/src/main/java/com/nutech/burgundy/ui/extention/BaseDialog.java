package com.nutech.burgundy.ui.extention;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.utils.Helper;

public class BaseDialog extends Dialog implements View.OnClickListener {

    private ImageView ivIcon;
    private ImageView ivClose;
    private TextView tvMessage;
    private TextView tvTitle;
    private Button btnOK;
    private LinearLayout container;
    private LinearLayout layoutDialog;

    private String message;
    private String title;
    private String buttonString;
    private String tipe;
    private @DrawableRes
    int iconResource;
    private View.OnClickListener listener;

    private int timeToDismiss;
    private boolean noContainer = false;
    private boolean autoDismiss = false;
    private boolean noClose = false;
    private boolean hasPaddingVertical = false;
    private boolean hasPaddingHorizontal= false;
    private boolean bigIcon = false;
    private OnDismissListener dismissListener;

    private View additionalView;
    private int layout_id;

    private Typeface fonts;
    private Context context;

    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_base);
        context = getContext();

        layoutDialog = findViewById(R.id.layout_dialog);
        ivIcon = findViewById(R.id.iv_icon);
        ivClose = findViewById(R.id.iv_close);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvMessage = findViewById(R.id.tv_message);
        tvTitle = findViewById(R.id.tv_title);
        btnOK = findViewById(R.id.btn_ok);
        container = findViewById(R.id.container);


        if ((autoDismiss && timeToDismiss > 0) || (noClose)){
            ivClose.setVisibility(View.INVISIBLE);
        }

        if(noContainer){
            container.setVisibility(View.GONE);
        }

        if(tipe != null){
            if(tipe == "small"){
                ViewGroup.LayoutParams marginParamsIcon = (ViewGroup.LayoutParams) layoutDialog.getLayoutParams();
                marginParamsIcon.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                marginParamsIcon.width = Helper.getScreenWidth() * 2 / 3;
            }else if(tipe == "medium"){
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((260),
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutDialog.setLayoutParams(lp);
            }else if(tipe == "large"){
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams((280),
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutDialog.setLayoutParams(lp);
            }
        }
        super.setOnDismissListener(dismissListener);

        try {
            if (message != null) {
                boolean isPin = message.contains("PIN berhasil diubah");
                tvMessage.setText(message);
                if(isPin){
                    tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);tvMessage.setTypeface(null, Typeface.BOLD);
                }
            }else{
                tvMessage.setVisibility(View.GONE);
            }



            if (buttonString != null) {
                btnOK.setText(buttonString);
                btnOK.setVisibility(View.VISIBLE);
                btnOK.setOnClickListener(this);
            }

            if (iconResource != 0) {
                ivIcon.setVisibility(View.VISIBLE);
                ivIcon.setImageResource(iconResource);

                if(hasPaddingHorizontal){
                    if(bigIcon){
                        ivIcon.getLayoutParams().width = Helper.getScreenWidth() * 2 / 4;
                        ivIcon.getLayoutParams().height = Helper.getScreenWidth() * 2 / 3;
                    }else{
                        ivIcon.getLayoutParams().width = Helper.getScreenWidth() / 3;
                        ivIcon.getLayoutParams().height = Helper.getScreenWidth() / 4;
                    }
                }else if(hasPaddingVertical){
                    if(bigIcon) {
                        ivIcon.getLayoutParams().width = Helper.getScreenWidth() * 2 / 3;
                        ivIcon.getLayoutParams().height = Helper.getScreenWidth() * 2 / 4;
                    }else{
                        ivIcon.getLayoutParams().width = Helper.getScreenWidth() / 2;
                        ivIcon.getLayoutParams().height = Helper.getScreenWidth() / 3;
                    }
                }else{
                    if(bigIcon) {
                        ivIcon.getLayoutParams().width = Helper.getScreenWidth() * 2 / 3;
                        ivIcon.getLayoutParams().height = Helper.getScreenWidth() * 2 / 3;
                    }else{
                        if (message != null) {
                            boolean verify = message.toLowerCase().contains("nomor berhasil");
                            if(verify){
                                ivIcon.getLayoutParams().width = Helper.getScreenWidth() / 6;
                                ivIcon.getLayoutParams().height = Helper.getScreenWidth() / 6;
                            }else {
                                ivIcon.getLayoutParams().width = Helper.getScreenWidth() / 2;
                                ivIcon.getLayoutParams().height = Helper.getScreenWidth() / 2;
                            }
                        }
                    }

                }
            }

            if(bigIcon){
                ViewGroup.MarginLayoutParams marginParamsIcon = (ViewGroup.MarginLayoutParams) ivIcon.getLayoutParams();
                marginParamsIcon.setMargins(0, Helper.getDp(-20), 0, 0);

                ViewGroup.MarginLayoutParams marginParamsTitle = (ViewGroup.MarginLayoutParams) tvTitle.getLayoutParams();
                marginParamsTitle.setMargins(0, Helper.getDp(-20), 0, 0);
            }

            if (additionalView != null) {
                container.addView(additionalView);
            }

            if (title != null) {
                tvTitle.setText(title);
                tvTitle.setVisibility(View.VISIBLE);
            }

            if (message != null) {
                boolean isOTP = message.contains("Untuk input OTP kelima kalinya");
                tvMessage.setText(message);
                if(isOTP){
                    tvMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float) 14);
                    tvMessage.setLineSpacing(TypedValue.COMPLEX_UNIT_PX, (float)1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void show() {
        super.show();
        Window window = this.getWindow();
        window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        if (autoDismiss && timeToDismiss > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    /*if (dismissListener != null) {
                        dismissListener.onDismiss(null);
                    }*/
                    dismiss();
                }
            }, timeToDismiss);
        }
    }

    /**
     * @param message as a dialog message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public void setHasPaddingVertical(Boolean hasPaddingVertical) {
        this.hasPaddingVertical= hasPaddingVertical;
    }

    public void setHasPaddingHorizontal(Boolean hasPaddingHorizontal) {
        this.hasPaddingHorizontal = hasPaddingHorizontal;
    }

    public void setButtonString(String buttonString) {
        this.buttonString = buttonString;
    }

    public void setButtonEnable(boolean enable) {
        if (btnOK != null) {
            btnOK.setEnabled(enable);
        }
    }

    public void setTipe(String tipe) { this.tipe = tipe; }

    public void setNoContainer(boolean noContainer) {
        this.noContainer = noContainer;
    }

    public void setIconResource(@DrawableRes int iconResource) {
        this.iconResource = iconResource;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setAutoDismiss(boolean autoDismiss) {
        this.autoDismiss = autoDismiss;
    }

    public void setNoClose(boolean NoClose) {
        this.noClose = NoClose;
    }

    public void setTimeToDismiss(int timeToDismiss) {
        this.timeToDismiss = timeToDismiss;
    }

    public void setDismissListener(OnDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    public void setAdditionalView(View additionalView) {
        this.additionalView = additionalView;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLayout(int layout_id) {this.layout_id= layout_id;}

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_ok) {
            if (listener != null) {
                listener.onClick(view);
            }
            dismiss();
        }
    }

    public void setBigIcon(boolean bigIcon){
        this.bigIcon = bigIcon;
    }
}
