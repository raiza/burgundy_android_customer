package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.TrackPresenter;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class TrackListAdapter extends ArrayAdapter<DataTracks> {

    private Context context;
    private List<DataTracks> list;
    private LayoutInflater layoutInflater;
    private View view;

    public TrackListAdapter(@NonNull Context context, int resource, List<DataTracks> list) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position,convertView,parent);
        label.setTextColor(Color.BLACK);
            label.setText(list.get(position).getName()+" - "+list.get(position).getArtistnames());

        return label;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position,convertView,parent);
        label.setTextColor(Color.BLACK);
        label.setText(list.get(position).getName()+" - "+list.get(position).getArtistnames());

        return label;
    }
}
