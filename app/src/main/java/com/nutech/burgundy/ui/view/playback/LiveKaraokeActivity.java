package com.nutech.burgundy.ui.view.playback;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.presenter.livekaraoke.RecordPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.ui.view.livekaraoke.record.RecordView;
import com.nutech.burgundy.utils.AutoFitTextureView;
import com.nutech.burgundy.utils.CountdownTimerAnimation;
import com.nutech.burgundy.utils.Helper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LiveKaraokeActivity extends BaseAppCompatActivity implements RecordView,
        JcPlayerView.JcPlayerViewServiceListener, CountdownTimerAnimation.CountDownListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "-- LiveKaraoke -- ";
    public static final int REQUEST_CODE_READ_EXTERNAL_MEMORY = 100;

    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    private static final int REQUEST_VIDEO_PERMISSIONS = 1;
    private static final String FRAGMENT_DIALOG = "dialog";

    private static final String[] VIDEO_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    @BindView(R.id.tv_video_timer)
    TextView tvVideoTimer;
    @BindView(R.id.ll_video_timer)
    LinearLayout llVideoTimer;
    @BindView(R.id.tv_dump_timer)
    TextView tvDumpTimer;

    @BindView(R.id.bt_submit)
    Button btSubmit;
    @BindView(R.id.ib_stop)
    ImageButton btStop;
    @BindView(R.id.rl_close)
    RelativeLayout rlClose;
    @BindView(R.id.primary_jcplayer)
    JcPlayerView jcPlayerView;

    @BindView(R.id.txt_lirick_passed)
    TextView txtLiricksPassed;
    @BindView(R.id.txt_lirick_focused)
    TextView txtLiricksFocused;
    @BindView(R.id.txt_lirick_next)
    TextView txtLiricksNext;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.indicator_live)
    AVLoadingIndicatorView indicatorLive;
    @BindView(R.id.tv_track_name)
    TextView tvTrackName;
    @BindView(R.id.tv_artists_name)
    TextView tvArtistName;
    @BindView(R.id.rl_countdown_timer)
    RelativeLayout rlCountDownTimer;
    @BindView(R.id.tv_countdown_timer)
    TextView tvCountDownTimer;
    @BindView(R.id.tv_msg_loading)
    TextView tvMsgLoading;

    @BindView(R.id.texture)
    AutoFitTextureView mTextureView;
    @BindView(R.id.ll_loading_upload)
    RelativeLayout llLoadingUpload;
    @BindView(R.id.progress_indicator)
    ProgressBar progressBarIndicator;
    @BindView(R.id.tv_msg_loading_indicator)
    TextView tvMsgLoadingIndicator;
    @BindView(R.id.tv_msg_loading_upload)
    TextView tvMsgLoadingUpload;

    private Timer timer;
    private Intent intent;
    private Boolean isRecording;
    private String roomId;
    private int elapsedTime;
    private String sourceTrack;
    private String paramPathMusic = "";
    private String paramPathVideo = "";
    private boolean isDownloaded = false;

    private RecordPresenter presenter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;
    private List<DataKaraoke> tempListData = new ArrayList<>();
    private List<JcAudio> jcAudios = new ArrayList<JcAudio>();
    private List<DataLiricks> mDataLiricks = new ArrayList<>();
    private String tempRoomId;
    private AlertDialog alertDialogNotSubs;
    private AlertDialog alertDialogCompleted;
    private AlertDialog alertDialogExit;
    private AlertDialog alertDialogPermission;
    private AlertDialog alertDialogUpload;

    //    private ILiveVideoBroadcaster liveVideoBroadcaster;
    private ContentLoadingProgressBar progressBar;
    private CountdownTimerAnimation countdownTimerAnimation;
    private CountDownTimer countDownTimer;
    private Handler handler = new Handler();
    private Context context;

    private CameraDevice mCameraDevice;

    /**
     * A reference to the current {@link CameraCaptureSession} for
     * preview.
     */
    private CameraCaptureSession mPreviewSession;

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture,
                                              int width, int height) {
            System.out.println(TAG + "onSurfaceTextureAvailable");
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture,
                                                int width, int height) {
            System.out.println(TAG + "onSurfaceTextureSizeChanged");
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            System.out.println(TAG + "onSurfaceTextureDestroyed");
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

    };

    /**
     * The {@link Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * The {@link Size} of video recording.
     */
    private Size mVideoSize;

    /**
     * MediaRecorder
     */
    private MediaRecorder mMediaRecorder;

    /**
     * Whether the app is recording video now
     */
    private boolean mIsRecordingVideo;

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its status.
     */
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {

            System.out.println(TAG + "onOpened");
            mCameraDevice = cameraDevice;

            startPreview();
            mCameraOpenCloseLock.release();
            if (null != mTextureView) {
                configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            System.out.println(TAG + "onDisconnected");
            mCameraOpenCloseLock.release();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cameraDevice.close();
            }
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            System.out.println(TAG + "onError");
            mCameraOpenCloseLock.release();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cameraDevice.close();
            }
            mCameraDevice = null;
            Activity activity = LiveKaraokeActivity.this;
            if (null != activity) {
                activity.finish();
            }
        }

    };
    private Integer mSensorOrientation;
    private String mNextVideoAbsolutePath;
    private CaptureRequest.Builder mPreviewBuilder;


    /**
     * In this sample, we choose a video size with 3x4 aspect ratio. Also, we don't use sizes
     * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
     *
     * @param choices The list of available sizes
     * @return The video size
     */
    private static Size chooseVideoSize(Size[] choices) {
        System.out.println(TAG + "chooseVideoSize");
        for (Size size : choices) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
                    return size;
                }
            }
        }
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        System.out.println(TAG + "chooseOptimalSize");
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        int w = 0;
        int h = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            w = aspectRatio.getWidth();
            h = aspectRatio.getHeight();
            for (Size option : choices) {
                if (option.getHeight() == option.getWidth() * h / w &&
                        option.getWidth() >= width && option.getHeight() >= height) {
                    bigEnough.add(option);
                }
            }
        }


        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_karaoke);
        ButterKnife.bind(this);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        intent = getIntent();
        presenter = new RecordPresenter(getDataManager(), getNetworkHelper(), getApplicationContext());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        dmApplication = (DmApplication) getApplicationContext();
        context = LiveKaraokeActivity.this;

        jcPlayerView.registerServiceListener(this);

//        glSurface.setEGLContextClientVersion(2);

        System.out.println(TAG + "onCreate");

        setupView();
        initView();
        initEvent();
//        dialogInfo2();
        dialogInfo();

    }

    @Override
    public void onResume() {
        System.out.println(TAG + "onResume");
        System.out.println(TAG + " mTextureView "+mTextureView.isAvailable());
        super.onResume();
        startBackgroundThread();
        if (mTextureView.isAvailable()) {
            System.out.println(TAG + " Masuk if");
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            System.out.println(TAG + " Masuk else");
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
//        bindService(intentService, connection, 0);
    }

    @Override
    public void onPause() {
        System.out.println(TAG + "onPause");
        closeCamera();
        stopBackgroundThread();
        super.onPause();
//        if (liveVideoBroadcaster != null) {
//            liveVideoBroadcaster.pause();
//        }
        if (alertDialogUpload != null) {
            alertDialogUpload.dismiss(); }
    }

    @Override
    public void onStop() {
        System.out.println(TAG + "onStop");
        super.onStop();
//        unbindService(connection);
        if (alertDialogUpload != null) {
            alertDialogUpload.dismiss(); }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        System.out.println(TAG + "onConfigurationChanged");
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
                || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            liveVideoBroadcaster.setDisplayOrientation();
        }
    }

    @Override
    public void startBroadcast(String roomId) {

    }

    @Override
    public void resultBroadcast(Boolean result) {

    }

    @Override
    public void showDataKaraoke(DataKaraoke dataKaraoke) {


        sourceTrack = dataKaraoke.getUrl() + dataKaraoke.getSource();

        tempListData.add(dataKaraoke);

        new DownloadTrack().execute(sourceTrack);


//        llLoading.setVisibility(View.GONE);

    }

    @Override
    public void showErrorKaraoke(String error) {

        Helper.showToastNetworkError(getApplicationContext());

        dialogInfo();

    }

    @Override
    public void showError(String error) {

        llLoading.setVisibility(View.GONE);

    }

    @Override
    public void showLiricks(List<DataLiricks> dataLiricksList) {

        mDataLiricks.clear();
        mDataLiricks = dataLiricksList;

        getDataKaraoke();
    }

    @Override
    public void showErrorLiricks(String error) {
        getDataKaraoke();
    }

    @Override
    public void showRoom(String succes) {
        llLoading.setVisibility(View.GONE);

        presenter.send(intent.getStringExtra("track_id"));
    }

    @Override
    public void uploadVideoDone(String data) {

        llLoadingUpload.setVisibility(View.GONE);
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("room_code", tempRoomId);
            params.put("status", "1");
        } catch (Exception e) {
            e.printStackTrace();
        }

        presenter.updateStatusUploadVideo(params);
        dialogUploadDone(getString(R.string.msg_upload_video_3));

    }

    @Override
    public void showErrorRoom(String error) {
        llLoading.setVisibility(View.GONE);
        presenter.send(intent.getStringExtra("track_id"));

    }

    @Override
    public void showUpdateRoom(String succes) {
//        dialogCompleted();
    }

    @Override
    public void showErrorUpdateRoom(String error) {
//        dialogCompleted();
    }

    @Override
    public void showUpdateRoomBeforeExit(String succes) {

//        File file = new File(mNextVideoAbsolutePath);
//        RequestBody reqFile = RequestBody.create(MediaType.parse("video/*"), file);
//        MultipartBody.Part body = MultipartBody.Part.create(file);


//        Map<String, String> params = new HashMap<String, String>();
//
//        try {
//            params.put("userFile", mNextVideoAbsolutePath);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("Params Upload Video" +params.toString());


        dialogBeforeUpload(paramPathVideo, paramPathMusic);



//        exitPage();
    }

    @Override
    public void showErrorUpdateRoomBeforeExit(String error) {

//        File file = new File(mNextVideoAbsolutePath);
//        RequestBody reqFile = RequestBody.create(MediaType.parse("video/*"), file);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("userFile", file.getName(), reqFile);


//        Map<String, String> params = new HashMap<String, String>();
//
//        try {
//            params.put("userFile", mNextVideoAbsolutePath);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("Params Upload Video" +params.toString());


        dialogBeforeUpload(paramPathVideo, paramPathMusic);



//        exitPage();
    }

    @Override
    public void errorUploadVideo(String error) {

        llLoading.setVisibility(View.GONE);
        dialogUploadFailed(getString(R.string.msg_upload_video_failed));

    }

    @Override
    public void showUpdateRoomOnCompletedKaraoke(String succes) {

        dialogBeforeUpload(paramPathVideo, paramPathMusic);

    }

    @Override
    public void showErrorUpdateRoomOnCompletedKaraoke(String error) {

        dialogBeforeUpload(paramPathVideo, paramPathMusic);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (alertDialogExit != null && alertDialogExit.isShowing()) {
            alertDialogExit.dismiss();
        }
        if (alertDialogCompleted != null && alertDialogCompleted.isShowing()) {
            alertDialogCompleted.dismiss();
        }
        if (alertDialogNotSubs != null && alertDialogNotSubs.isShowing()) {
            alertDialogNotSubs.dismiss();
        }
        presenter.detachView();
    }

    private void addRoom() {

        llLoading.setVisibility(View.VISIBLE);

        Map<String, String> params = new HashMap<String, String>();


        Log.i("**** isWiredHeadsetOn ", ""+Helper.isWiredHeadsetOn(getApplicationContext()));

        int useHeadPhone = 0;
        if (Helper.isWiredHeadsetOn(getApplicationContext())){
            useHeadPhone = 1;
        } else {
            useHeadPhone = 0;
        }

        try {
            params.put("customer_id", sessionManager.getDataMember().getId());
            params.put("room_code", sessionManager.getDataMember().getMobile_phone()+"_"+
                    sessionManager.getDataMember().getId() +"_"+ Helper.generateCode());
            params.put("track_id", intent.getStringExtra("track_id"));
            params.put("live", Constants.Status.LIVE_TRUE);
            params.put("use_headphone", String.valueOf(useHeadPhone));
            params.put("karaoke_track_id", intent.getStringExtra("karaoke_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        tempRoomId = params.get("room_code");
        presenter.addRomm(params);
    }

    private void removeRoom() {

        Map<String, String> params = new HashMap<String, String>();

        try {
            params.put("room_code", tempRoomId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        presenter.removeRoom(params);
    }

    private void getDataKaraoke() {
        llLoading.setVisibility(View.VISIBLE);
        presenter.getDetailKaraoke(intent.getStringExtra("karaoke_id"));
    }

    private void initView() {

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(VIDEO_PERMISSIONS,
                        REQUEST_CODE_READ_EXTERNAL_MEMORY);
            }
        } else {
//            dispatchTakePictureIntent();
        }

//        sourceTrack = intent.getStringExtra("url_track");
        mDataLiricks = dmApplication.getDataLiricksList();

        System.out.println("Lirik :" + mDataLiricks.toString());


        alertDialogNotSubs = new AlertDialog.Builder(this).create();
        alertDialogCompleted = new AlertDialog.Builder(this).create();
        alertDialogExit = new AlertDialog.Builder(this).create();
        alertDialogPermission = new AlertDialog.Builder(this).create();
        alertDialogUpload = new AlertDialog.Builder(this).create();

        isRecording = false;

        progressBar = new ContentLoadingProgressBar(this);
        progressBar.setIndeterminate(true);

        btStop.setVisibility(View.GONE);
        llVideoTimer.setVisibility(View.GONE);
    }

    private void initEvent() {

        countdownTimerAnimation = new CountdownTimerAnimation(tvCountDownTimer, getStartCount());
        countdownTimerAnimation.setCountDownListener(this);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btStop.setVisibility(View.VISIBLE);
                llVideoTimer.setVisibility(View.VISIBLE);
                btSubmit.setVisibility(View.GONE);

                startCountDownAnimation();
            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit();
            }
        });
        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogExit();
            }
        });
    }

    private void triggerStopRecording() {

        if (jcPlayerView != null){
            jcPlayerView.kill();
        }

        stopRecordingVideo();

        updateStatus();

        if (isRecording) {


//            liveVideoBroadcaster.stopBroadcasting();
        }

        isRecording = false;

        stopTimer();

        dialogNotSubscribe();

    }

    private void triggerStopRecordingAfterKaraoke() {

        stopRecordingVideo();

        if (isRecording) {

            stopTimer();
//            liveVideoBroadcaster.stopBroadcasting();
        }
        stopTimer();

        updateStatusOnCompletedKaraoke();

        isRecording = false;

    }

    private void triggerStopRecordingBeforeExit() {

        stopRecordingVideo();

        if (isRecording) {

            stopTimer();
//            liveVideoBroadcaster.stopBroadcasting();
        }

        stopTimer();

        isRecording = false;

        updateStatusBeforeExit();

    }

    private void triggerStopRecordingWithoutSave() {

        if (isRecording) {

            stopTimer();
//            liveVideoBroadcaster.stopBroadcasting();
        }

        isRecording = false;

        stopTimer();

        exitPage();

    }

    private void startTimer() {

        indicatorLive.smoothToShow();

        Log.i(TAG, "startTimer: ");

        if (timer == null) {
            timer = new Timer();
        }

        elapsedTime = 0;
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        elapsedTime += 1;
                        tvVideoTimer.setText(Helper.getDurationString(elapsedTime));

//                        if (liveVideoBroadcaster == null || !liveVideoBroadcaster.isConnected()) {
//                            //-- Step Lama
////                            triggerStopRecording(sessionManager.getStatusPackage());
//                            triggerStopRecording(sessionManager.getMemberCheck().getStatus());
//                            showErrorAdsList("Connection Lost");
//                        }
                    }
                });
            }
        }, 0, 1000);
    }

    private void stopTimer() {

        indicatorLive.smoothToHide();

        Log.i(TAG, "stopTimer: ");

        if (timer != null) {
            this.timer.cancel();
        }

        this.timer = null;
        this.elapsedTime = 0;
    }

    @Override
    public void onBackPressed() {
        dialogExit();
    }

    public void setPlaylist(List<DataKaraoke> dataKaraokes) {

        String tempSourcePath = "";

        if (dataKaraokes.size() > 0) {
            jcAudios.clear();
            for (int i = 0; i < dataKaraokes.size(); i++) {

                if (isDownloaded) {

                    System.out.println("======== Download true");

                    if (!TextUtils.isEmpty(paramPathMusic)) {

                        System.out.println("======== paramPathMusic tidak kosong");
                        tempSourcePath = paramPathMusic;

                        jcAudios.add(JcAudio.createFromFilePath(dataKaraokes.get(i).getName(),
                                tempSourcePath,
                                dataKaraokes.get(i).getImage(), intent.getStringExtra("track_id"),
                                dataKaraokes.get(i).getArtists()));
                        jcPlayerView.initPlaylist(jcAudios);
                    } else {

                        System.out.println("========= paramPathMusic Kosong");
                        tempSourcePath = dataKaraokes.get(i).getUrl() + dataKaraokes.get(i).getSource();

                        jcAudios.add(JcAudio.createFromURL(dataKaraokes.get(i).getId(),
                                "1",dataKaraokes.get(i).getName(),
                                tempSourcePath,
                                dataKaraokes.get(i).getImage(), intent.getStringExtra("track_id"),
                                dataKaraokes.get(i).getArtists()));
                        jcPlayerView.initPlaylist(jcAudios);
                    }
                } else {

                    System.out.println("======== Download false");
                    tempSourcePath = dataKaraokes.get(i).getUrl() + dataKaraokes.get(i).getSource();

                    jcAudios.add(JcAudio.createFromURL(dataKaraokes.get(i).getId(),"1",dataKaraokes.get(i).getName(),
                            tempSourcePath,
                            dataKaraokes.get(i).getImage(), intent.getStringExtra("track_id"),
                            dataKaraokes.get(i).getArtists()));
                    jcPlayerView.initPlaylist(jcAudios);
                }

            }
        }

        System.out.println("==== Tracks "+tempSourcePath);
    }

    private void playingBackSound() {

        System.out.println(TAG + "playingBackSound");

        jcPlayerView.registerServiceListener(this);
        jcPlayerView.playAudio(jcPlayerView.getMyPlaylist().get(0));
//        jcPlayerView.createNotification();
    }

    private void dialogCompleted() {

        CharSequence[] listEvent = {getString(R.string.msg_save), getString(R.string.msg_remove)};

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveKaraokeActivity.this);
        builder.setTitle(getString(R.string.title_karaoke_completed));
//        builder.setMessage(getString(R.string.msg_done_karaoke));
        builder.setCancelable(false);
        builder.setSingleChoiceItems(listEvent, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        dialog.dismiss();
                        triggerStopRecordingAfterKaraoke();
                        break;
                    case 1:
                        removeRoom();
                        dialog.dismiss();
                        exitPage();
                        break;
                }
                alertDialogCompleted.dismiss();
            }
        });
        alertDialogCompleted = builder.create();
        alertDialogCompleted.show();

    }

    private void dialogNotSubscribe() {

        alertDialogNotSubs.setTitle(getString(R.string.label_info));
        alertDialogNotSubs.setMessage(getString(R.string.msg_full_fiture_subs_umb));
        alertDialogNotSubs.setCancelable(false);

//** Step Lama, jangan dihapus! **//
//        alertDialogNotSubs.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.label_subscribe),
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        deletedLocalVideo();
//                        deletedLocalTrack();
//                        dialog.dismiss();
//                        jcPlayerView.kill();
//                        finish();
//                        dmApplication.getNavigation()
//                                .pushFragment(new VipFragment(), getString(R.string.label_subscribe), true);
//
//                    }
//                });

        alertDialogNotSubs.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        exitPage();

                    }
                });

        alertDialogNotSubs.show();
    }

    private void dialogExit() {

        CharSequence[] listEvent = {getString(R.string.msg_exit), getString(R.string.msg_calcel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(LiveKaraokeActivity.this);
        builder.setTitle(getString(R.string.message_to_exit_karaoke));
//        builder.setMessage(getString(R.string.msg_done_karaoke));
        builder.setCancelable(false);
        builder.setSingleChoiceItems(listEvent, -1, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        removeRoom();
                        dialog.dismiss();
                        jcPlayerView.kill();
                        //-- Step Lama
//                        triggerStopRecordingWithoutSave();
                        triggerStopRecordingWithoutSave();
                        break;
                    case 1:
                        dialog.dismiss();
                        break;
                }
                alertDialogExit.dismiss();
            }
        });
        alertDialogExit = builder.create();
        alertDialogExit.show();
    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {

    }

    @Override
    public void onCompletedAudio() {

        jcPlayerView.kill();
        //-- Step Lama
//        triggerStopRecordingAfterKaraoke(sessionManager.getStatusPackage());

        try {
            if(!((Activity) context).isFinishing())
            {
                //show dialog
                dialogCompleted();
            } else {
                showBottomDialogCompleted();
            }
        } catch (Exception e){
            showBottomDialogCompleted();
        }
    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onContinueAudio() {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void addLogAds(String id,String trackId) {

    }

    @Override
    public void onTimeChanged(long currentTime) {
        final int newCurrentTime = (int) currentTime / 1000;

        int minutes = (int) (newCurrentTime / 60);
        int seconds = (int) (newCurrentTime % 60);
        final String sMinutes = minutes < 10 ? "0" + minutes : minutes + "";
        final String sSeconds = seconds < 10 ? "0" + seconds : seconds + "";

//        String startOriginalLyrics = String.valueOf(sMinutes + ":" + sSeconds);
        tvDumpTimer.post(new Runnable() {
            @Override
            public void run() {
                tvDumpTimer.setText(String.valueOf(sMinutes + ":" + sSeconds));
                String startOriginalLyrics = String.valueOf(sMinutes + ":" + sSeconds);

//                if (newCurrentTime > sessionManager.getTimer()) {
//                    if (!sessionManager.getStatusPackage().equals("1")) {
//                        triggerStopRecording();
//                    }
//                }
                cangesLyrics(startOriginalLyrics);
            }
        });
    }

    @Override
    public void updateTitle(final String title, final String artisName) {

        tvTrackName.post(new Runnable() {
            @Override
            public void run() {
                tvTrackName.setText(title);
            }
        });

        tvArtistName.post(new Runnable() {
            @Override
            public void run() {
                tvArtistName.setText(artisName);
            }
        });

    }

    @Override
    public void updateCover(String cover) {

    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {

    }

    @Override
    public void removeNotification() {

    }

    private void cangesLyrics(String startOriginalLyrics) {

        for (int i = 0; i < mDataLiricks.size(); i++) {

            if (startOriginalLyrics.equals(mDataLiricks.get(i).getStart_at())) {
                final int finalI = i;
                txtLiricksFocused.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            txtLiricksPassed.setText(mDataLiricks.get(finalI - 1).getText());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        txtLiricksFocused.setText(mDataLiricks.get(finalI).getText());

                        try {
                            txtLiricksNext.setText(mDataLiricks.get(finalI + 1).getText());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

//            if (startOriginalLyrics > mDataLiricks.get(i).getStart_at())
        }
    }

    private void updateStatus() {
        Map<String, String> params = new HashMap<String, String>();

        int useHeadPhone = 0;
        if (Helper.isWiredHeadsetOn(getApplicationContext())){
            useHeadPhone = 1;
        } else {
            useHeadPhone = 0;
        }
        try {
            params.put("customer_id", sessionManager.getDataMember().getId());
            params.put("room_code", tempRoomId);
            params.put("track_id", intent.getStringExtra("track_id"));
            params.put("live", Constants.Status.LIVE_FALSE);
            params.put("use_headphone", String.valueOf(useHeadPhone));
            params.put("karaoke_track_id", intent.getStringExtra("karaoke_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        presenter.updateStatusVideo(params);
    }

    private void updateStatusOnCompletedKaraoke() {
        Map<String, String> params = new HashMap<String, String>();

        int useHeadPhone = 0;
        if (Helper.isWiredHeadsetOn(getApplicationContext())){
            useHeadPhone = 1;
        } else {
            useHeadPhone = 0;
        }
        try {
            params.put("customer_id", sessionManager.getDataMember().getId());
            params.put("room_code", tempRoomId);
            params.put("track_id", intent.getStringExtra("track_id"));
            params.put("live", Constants.Status.LIVE_FALSE);
            params.put("use_headphone", String.valueOf(useHeadPhone));
            params.put("karaoke_track_id", intent.getStringExtra("karaoke_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        presenter.updateStatusVideoOnCompletedKaraoke(params);
    }

    private void updateStatusBeforeExit() {

        Map<String, String> params = new HashMap<String, String>();
        int useHeadPhone = 0;
        if (Helper.isWiredHeadsetOn(getApplicationContext())){
            useHeadPhone = 1;
        } else {
            useHeadPhone = 0;
        }

        try {
            params.put("customer_id", sessionManager.getDataMember().getId());
            params.put("room_code", tempRoomId);
            params.put("track_id", intent.getStringExtra("track_id"));
            params.put("live", Constants.Status.LIVE_FALSE);
            params.put("use_headphone", String.valueOf(useHeadPhone));
            params.put("karaoke_track_id", intent.getStringExtra("karaoke_id"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Params "+params.toString());

        presenter.updateStatusVideoBeforeExit(params);
    }

    private void exitPage() {
        deletedLocalVideo();
        deletedLocalTrack();
        Intent intent = new Intent(LiveKaraokeActivity.this, PrimaryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
//        finish();
    }

    private void setupView() {

//        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
//                .getDefaultDisplay();
//
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;
//
//        ViewGroup.LayoutParams params = glSurface.getLayoutParams();
//        params.width = width;
//        params.height = height;
//        glSurface.setLayoutParams(params);
    }

    private void dialogInfo() {

        AlertDialog dialogInfo = new AlertDialog.Builder(this).create();
        dialogInfo.setIcon(R.drawable.ic_headphones);
        dialogInfo.setTitle(getString(R.string.title_headphone_recommended));
        dialogInfo.setMessage(getString(R.string.msg_headphone_recommended));
        dialogInfo.setCancelable(false);
        dialogInfo.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        addRoom();
                    }
                });
        dialogInfo.show();
    }

    private void dialogInfo2() {

        AlertDialog dialogInfo2 = new AlertDialog.Builder(this).create();
        dialogInfo2.setTitle(getString(R.string.title_network_information));
        dialogInfo2.setMessage(getString(R.string.msg_volume_information));
        dialogInfo2.setCancelable(false);
        dialogInfo2.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialogInfo2.show();
    }


    private void dialogInfoFailed() {

        AlertDialog dialogInfo = new AlertDialog.Builder(this).create();
        dialogInfo.setTitle(getString(R.string.title_network_information));
        dialogInfo.setMessage(getString(R.string.msg_network_information));
        dialogInfo.setCancelable(true);
        dialogInfo.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        addRoom();
                    }
                });
        dialogInfo.show();
    }

    private int getStartCount() {
        return Integer.parseInt(Constants.DefaultTimer.timerCountdown);
    }

    @Override
    public void onCountDownEnd(CountdownTimerAnimation animation) {
        rlCountDownTimer.setVisibility(View.GONE);
        btSubmit.setVisibility(View.GONE);
        startTimer();
        playingBackSound();
        startRecordingVideo();
        countDownTimer = new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {


            }
        }.start();
    }

    private void startCountDownAnimation() {

        rlCountDownTimer.setVisibility(View.VISIBLE);

        Animation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        countdownTimerAnimation.setAnimation(animationSet);

        // Customizable start count
        countdownTimerAnimation.setStartCount(getStartCount());
        countdownTimerAnimation.start();
    }

    class DownloadTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            llLoading.setVisibility(View.VISIBLE);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);


                // Output stream to write file
                OutputStream output = null;

                output = new FileOutputStream(getAudioFilePath(getApplicationContext()));

//                File f = new File(Environment.getExternalStorageDirectory() + "/dangdutasik");
//                if (f.isDirectory()) {
//                    output = new FileOutputStream(getAudioFilePath(this));
//
//                } else {
//                    File dir = new File("/sdcard/dangdutasik");
//                    try {
//                        if (dir.mkdir()) {
//                            output = new FileOutputStream("/sdcard/dangdutasik/dangdutasik_file_" + tempRoomId + ".mp3");
//                        } else {
//                            output = new FileOutputStream("/sdcard/dangdutasik_file_" + tempRoomId + ".mp3");
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
                isDownloaded = false;
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
//            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {

            File folder = new File(Environment.getExternalStorageDirectory() + "/dangdutasik");
            paramPathMusic = getAudioFilePath(getApplicationContext());;

            System.out.println("path music "+paramPathMusic);

            isDownloaded = true;

            setPlaylist(tempListData);
            llLoading.setVisibility(View.GONE);

            btSubmit.setBackgroundColor(getResources().getColor(R.color.bt_accent));
            btSubmit.setTextColor(getResources().getColor(R.color.primary_text));
            btSubmit.setEnabled(true);
        }

    }

    private void deletedLocalTrack() {
        try {
            File file = new File(paramPathMusic);
            if (file.exists()) {

                boolean result = file.delete();
                if (result){
                    Log.i(TAG, "Music Dihapus");
                } else {
                    Log.i(TAG, "Music Gagal Dihapus");
                }
//                 file.delete();
            } else {
            }
        } catch (Exception e) {
            Log.e("App", "Exception while deleting file " + e.getMessage());
        }
    }

    private void deletedLocalVideo() {
        try {
            if (!TextUtils.isEmpty(paramPathVideo)){
                File file = new File(paramPathVideo);
                if (file.exists()) {

                    boolean result = file.delete();

                    if (result){
                        Log.i(TAG, "Video Dihapus");
                    } else {
                        Log.i(TAG, "Video Gagal Dihapus");
                    }
//                 file.delete();
                } else {
                }
            }
        } catch (Exception e) {
            Log.e("App", "Exception while deleting file " + e.getMessage());
        }
    }

    private void startBackgroundThread() {
        System.out.println(TAG + "startBackgroundThread");
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        System.out.println(TAG + "stopBackgroundThread");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mBackgroundThread.quitSafely();
        }
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets whether you should show UI with rationale for requesting permissions.
     *
     * @param permissions The permissions your app wants to request.
     * @return Whether you can show permission rationale UI.
     */
    private boolean shouldShowRequestPermissionRationale(String[] permissions) {
        System.out.println(TAG + "shouldShowRequestPermissionRationale");
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Requests permissions needed for recording video.
     */
    private void requestVideoPermissions() {
        System.out.println(TAG + "requestVideoPermissions");
        if (shouldShowRequestPermissionRationale(VIDEO_PERMISSIONS)) {
//            new ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
            dialogPermission();
        } else {
            ActivityCompat.requestPermissions(this, VIDEO_PERMISSIONS, REQUEST_VIDEO_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult");
        if (requestCode == REQUEST_VIDEO_PERMISSIONS) {
            if (grantResults.length == VIDEO_PERMISSIONS.length) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
//                        ErrorDialog.newInstance(getString(R.string.permission_request))
//                                .show(this, FRAGMENT_DIALOG);
                        break;
                    }
                }
            } else {
//                ErrorDialog.newInstance(getString(R.string.permission_request))
//                        .show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        } else if (requestCode == REQUEST_CODE_READ_EXTERNAL_MEMORY) {
            System.out.println("*** grantResults " + grantResults.length);
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean hasPermissionsGranted(String[] permissions) {
        System.out.println(TAG + "hasPermissionsGranted");
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(LiveKaraokeActivity.this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("MissingPermission")
    private void openCamera(int width, int height) {

        System.out.println(TAG + "OpenCamera");

        if (!hasPermissionsGranted(VIDEO_PERMISSIONS)) {
            requestVideoPermissions();
            return;
        }
        final Activity activity = LiveKaraokeActivity.this;
        if (null == activity || activity.isFinishing()) {
            return;
        }
        CameraManager manager = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        }
        try {
            Log.d(TAG, "tryAcquire");
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            String cameraId = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                cameraId = manager.getCameraIdList()[0];
                cameraId = String.valueOf(Camera.CameraInfo.CAMERA_FACING_FRONT);
            }

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                characteristics = manager.getCameraCharacteristics(cameraId);
            }
            StreamConfigurationMap map = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                map = characteristics
                        .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            }
            if (map == null) {
                throw new RuntimeException("Cannot get available preview/video sizes");
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        width, height, mVideoSize);
            }

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mTextureView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                    mTextureView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
                }
            } else {

            }
            configureTransform(width, height);
            mMediaRecorder = new MediaRecorder();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                manager.openCamera(cameraId, mStateCallback, null);
            }
        } catch (@SuppressLint("NewApi") CameraAccessException e) {
            Toast.makeText(activity, "Cannot access the camera.", Toast.LENGTH_SHORT).show();
            activity.finish();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
//            ErrorDialog.newInstance(getString(R.string.camera_error))
//                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.");
        }
    }

    private void closeCamera() {
        System.out.println(TAG + "closeCamera");
        try {
            mCameraOpenCloseLock.acquire();
            closePreviewSession();
            if (null != mCameraDevice) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mCameraDevice.close();
                }
                mCameraDevice = null;
            }
            if (null != mMediaRecorder) {
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.");
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Start the camera preview.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void startPreview() {
        System.out.println(TAG + "startPreview");
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            }

            Surface previewSurface = new Surface(texture);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPreviewBuilder.addTarget(previewSurface);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mCameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                        new CameraCaptureSession.StateCallback() {

                            @Override
                            public void onConfigured(@NonNull CameraCaptureSession session) {
                                mPreviewSession = session;
                                updatePreview();
                            }

                            @Override
                            public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                                Activity activity = LiveKaraokeActivity.this;
                                if (null != activity) {
                                    Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the camera preview. {@link #startPreview()} needs to be called in advance.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void updatePreview() {
        System.out.println(TAG + "updatePreview");
        if (null == mCameraDevice) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
            HandlerThread thread = new HandlerThread("CameraPreview");
            thread.start();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        System.out.println(TAG + "setUpCaptureRequestBuilder");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        }
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `mTextureView`.
     * This method should not to be called until the camera preview size is determined in
     * openCamera, or until the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        System.out.println(TAG + "configureTransform");
        Activity activity = LiveKaraokeActivity.this;
        if (null == mTextureView || null == mPreviewSize || null == activity) {
            return;
        }

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        }
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                scale = Math.max(
                        (float) viewHeight / mPreviewSize.getHeight(),
                        (float) viewWidth / mPreviewSize.getWidth());
            }
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mTextureView.setTransform(matrix);

    }

    private void setUpMediaRecorder() throws IOException {
        System.out.println(TAG + "setUpMediaRecorder");
        final Activity activity = LiveKaraokeActivity.this;
        if (null == activity) {
            return;
        }

//        Log.e("size width", String.valueOf(mVideoSize.getWidth()));
//        Log.e("size heigth", String.valueOf(mVideoSize.getHeight()));


//        Log.e("==== isWiredHeadsetOn ", ""+Helper.isWiredHeadsetOn(getApplicationContext()));
//        if (Helper.isWiredHeadsetOn(getApplicationContext())){
////            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
//            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

//            System.out.println("******* setAudioSource VOICE_CALL *****");
//        } else {
//            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

//            System.out.println("******* setAudioSource MIC *****");
//        }

        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath.isEmpty()) {
            mNextVideoAbsolutePath = getVideoFilePath(this);
        }

        paramPathVideo = mNextVideoAbsolutePath;
        mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
//        mMediaRecorder.setVideoEncodingBitRate(10000000);
        mMediaRecorder.setVideoEncodingBitRate(600000);
        mMediaRecorder.setVideoFrameRate(30);
//        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoSize(640, 480);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        switch (mSensorOrientation) {
            case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                break;
            case SENSOR_ORIENTATION_INVERSE_DEGREES:
                mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                break;
        }
        mMediaRecorder.prepare();
    }

    private String getVideoFilePath(Context context) {
        System.out.println(TAG + "getVideoFilePath");
        final File dir = context.getExternalFilesDir(null);

        String path = "";
        if (!TextUtils.isEmpty(path)){
            path = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                    + tempRoomId + ".mp4";
        } else {
            path = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                    + tempRoomId + ".mp4";
        }

        return path;
    }

    private String getAudioFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);

        String path = "";
        if (!TextUtils.isEmpty(path)) {
            path = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                    + tempRoomId + ".mp3";
        } else {
            path = (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                    + tempRoomId + ".mp3";
        }

        return path;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void startRecordingVideo() {

        System.out.println(TAG + "startRecordingVideo");
        System.out.println(TAG + "mCameraDevice = " +mCameraDevice);
        System.out.println(TAG + "mTextureView = "+mTextureView.isAvailable());
        System.out.println(TAG + "mPreviewSize = " +mPreviewSize);
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            System.out.println(TAG + "Masuk 1");
            return;
        }
        if (Build.VERSION.SDK_INT >= Constants.currentVersion.minSDK21) {

            System.out.println(TAG + "Masuk 2");
            try {
                closePreviewSession();
                setUpMediaRecorder();
                SurfaceTexture texture = mTextureView.getSurfaceTexture();
                assert texture != null;
                texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
                List<Surface> surfaces = new ArrayList<>();

                // Set up Surface for the camera preview
                Surface previewSurface = new Surface(texture);
                surfaces.add(previewSurface);
                mPreviewBuilder.addTarget(previewSurface);

                // Set up Surface for the MediaRecorder
                Surface recorderSurface = mMediaRecorder.getSurface();
                surfaces.add(recorderSurface);
                mPreviewBuilder.addTarget(recorderSurface);

                // Start a capture session
                // Once the session starts, we can update the UI and start recording

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            mPreviewSession = cameraCaptureSession;
                            updatePreview();
                            LiveKaraokeActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // UI
                                    mIsRecordingVideo = true;

                                    // Start recording
                                    mMediaRecorder.start();
                                }
                            });
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                            Activity activity = LiveKaraokeActivity.this;
                            if (null != activity) {
                                Toast.makeText(activity, "Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, mBackgroundHandler);
                }

            } catch (CameraAccessException | IOException e) {
                System.out.println(TAG + "CameraAccessException");
                e.printStackTrace();
            }
        } else {

        }

    }

    private void closePreviewSession() {
        System.out.println(TAG + "closePreviewSession");
        if (mPreviewSession != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPreviewSession.close();
            }
            mPreviewSession = null;
        }
    }

    private void stopRecordingVideo() {

        try {
            // UI
            mIsRecordingVideo = false;
            // Stop recording
            mMediaRecorder.stop();
            mMediaRecorder.reset();

            Activity activity = LiveKaraokeActivity.this;
            if (null != activity) {
                Log.d(TAG, "Video saved: " + mNextVideoAbsolutePath);
            }
            mNextVideoAbsolutePath = null;
            startPreview();
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            int value = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                value = Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                        (long) rhs.getWidth() * rhs.getHeight());

            }
            return value;
        }
    }

    private void dialogPermission() {

        alertDialogPermission.setTitle(getString(R.string.label_info));
        alertDialogPermission.setMessage(getString(R.string.permission_request));
        alertDialogPermission.setCancelable(false);

        alertDialogPermission.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(LiveKaraokeActivity.this, VIDEO_PERMISSIONS,
                                REQUEST_VIDEO_PERMISSIONS);

                    }
                });

        alertDialogPermission.show();
    }

    private void dialogBeforeUpload(final String pathVideo, final String paramPathMusic) {

        alertDialogUpload.setTitle(getString(R.string.label_info));
        alertDialogUpload.setMessage(getString(R.string.msg_upload_video_done_1)+"\n\n" +
                getString(R.string.msg_upload_video_done_2));
        alertDialogUpload.setCancelable(false);

        alertDialogUpload.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.label_upload),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        int useHeadPhone = 0;
                        if (Helper.isWiredHeadsetOn(getApplicationContext())){
                            useHeadPhone = 1;
                        } else {
                            useHeadPhone = 0;
                        }

                        llLoadingUpload.setVisibility(View.VISIBLE);
                        presenter.uploadFile(pathVideo, paramPathMusic, tempRoomId, progressBarIndicator, tvMsgLoadingIndicator, String.valueOf(useHeadPhone));
                    }
                });

        alertDialogUpload.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        removeRoom();
                        exitPage();
                    }
                });


        alertDialogUpload.show();
    }

    private void dialogUploadDone(String msg) {
        AlertDialog dialog = new AlertDialog.Builder(this).create();

        dialog.setTitle(getString(R.string.label_info));
        dialog.setMessage(msg);
        dialog.setCancelable(false);

        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        exitPage();
                    }
                });

        dialog.show();
    }

    private void dialogUploadFailed(String msg) {
        AlertDialog dialog = new AlertDialog.Builder(this).create();

        dialog.setTitle(getString(R.string.label_info));
        dialog.setMessage(msg);
        dialog.setCancelable(false);

        dialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.label_upload_again),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int useHeadPhone = 0;
                        if (Helper.isWiredHeadsetOn(getApplicationContext())){
                            useHeadPhone = 1;
                        } else {
                            useHeadPhone = 0;
                        }
                        llLoadingUpload.setVisibility(View.VISIBLE);
                        presenter.uploadFile(paramPathVideo, paramPathMusic, tempRoomId, progressBarIndicator, tvMsgLoadingIndicator, String.valueOf(useHeadPhone));
                    }
                });

        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.label_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        exitPage();
                    }
                });

        dialog.show();
    }

    private void showBottomDialogCompleted() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.custom_option_completed, null);

        LinearLayout llSave = (LinearLayout) customView.findViewById(R.id.ll_save);
        LinearLayout llRemove = (LinearLayout) customView.findViewById(R.id.ll_remove);

        final BottomDialog dialog = new BottomDialog.Builder(this)
                .setCustomView(customView)
                .setCancelable(false)
                .build();

        llSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                triggerStopRecordingAfterKaraoke();
            }
        });

        llRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeRoom();
                dialog.dismiss();
                exitPage();
            }
        });

        dialog.show();
    }

    public static class ConfirmationDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new android.app.AlertDialog.Builder(getActivity())
                    .setMessage(R.string.permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions( VIDEO_PERMISSIONS,
                                        REQUEST_VIDEO_PERMISSIONS);
                            }

                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    parent.getActivity().finish();
                                }
                            })
                    .create();
        }

    }


}
