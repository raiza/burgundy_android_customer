package com.nutech.burgundy.ui.view.vip;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataSubscribe;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.presenter.VipPresenter;
import com.nutech.burgundy.ui.adapter.VipAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class VipFragment extends BaseFragment implements VipView {

    @BindView(R.id.image_backdrop)
    ImageView imageBackdrop;
    @BindView(R.id.rv_vip)
    RecyclerView rvVip;

    private Unbinder unbinder;
    private VipPresenter presenter;
    private DmApplication dmApplication;
    private SessionManager sessionManager;

    private VipAdapter adapter;
    private List<DataSubscribePrice> priceList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new VipPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        dmApplication = (DmApplication) getActivity().getApplicationContext();
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vip, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();

        presenter.getSubscribePriceAll();

        return view;
    }

    @Override
    public void showPriceAll(List<DataSubscribePrice> subscribePriceList) {

        if (priceList.size() > 0) {
            priceList.clear();
        }

        priceList.addAll(subscribePriceList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDetailPrice(DataSubscribePrice subscribePrice) {

    }

    @Override
    public void showDataSubscribe(DataSubscribe dataSubscribe) {

    }

    @Override
    public void showError(String error) {

        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void initView() {
        priceList = new ArrayList<>();
        adapter = new VipAdapter(priceList);

        rvVip.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvVip.setItemAnimator(new DefaultItemAnimator());
        rvVip.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvVip.setAdapter(adapter);
    }

    private void initEvent() {

        rvVip.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvVip, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                String id = priceList.get(position).getId();
                String name = priceList.get(position).getName();
                String description = priceList.get(position).getDescription();
                String price = priceList.get(position).getPrice();
                String itemCode = priceList.get(position).getItem_code();
                String itemName = priceList.get(position).getItem_name();
                String merchantId = priceList.get(position).getMerchant_id();
                String merchantName = priceList.get(position).getMerchant_name();
                String productName = priceList.get(position).getProduct_name();
                String paymentType = priceList.get(position).getPayment_type();

//                navigation.pushFragment(
//                        new DetailPriceActivity().newInstance(id), "Detail", true);
                dmApplication.setNavigation(navigation);
                Intent intent = new Intent(getActivity(), DetailPriceActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                intent.putExtra("description", description);
                intent.putExtra("price", price);
                intent.putExtra("itemCode", itemCode);
                intent.putExtra("itemName", itemName);
                intent.putExtra("merchantId", merchantId);
                intent.putExtra("merchantName", merchantName);
                intent.putExtra("productName", productName);
                intent.putExtra("paymentType", paymentType);
                startActivity(intent);

//                Intent intent = new Intent(getActivity(), GamePlayActivity.class);
//                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

}
