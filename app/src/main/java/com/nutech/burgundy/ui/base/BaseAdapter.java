package com.nutech.burgundy.ui.base;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.api.AntMediaService;
import com.nutech.burgundy.data.api.AntMediaServiceFactory;
import com.nutech.burgundy.data.api.DangdutAsikService;
import com.nutech.burgundy.data.api.DangdutAsikServiceFactory;
import com.nutech.burgundy.data.api.MelonService;
import com.nutech.burgundy.data.api.MelonServiceFactory;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

/**
 * @author Arif Setiawan on 27/5/19
 * NuTech Development
 */

public abstract class BaseAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected Context context;
    private DangdutAsikService dangdutAsikService;
    private AntMediaService antMediaService;
    private MelonService melonService;
    private DataManager dataManager;
    private NetworkHelper networkHelper;
    private SessionManager sessionManager;

    public void setContext(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public DangdutAsikService getDangdutAsikService() {
        if (dangdutAsikService == null) {
            dangdutAsikService = DangdutAsikServiceFactory.create();
        }
        return dangdutAsikService;
    }

    public AntMediaService getAntMediaService(){
        if(antMediaService == null){
            antMediaService = AntMediaServiceFactory.create();
        }
        return antMediaService;
    }

    public MelonService getMelonService(){
        if(melonService == null){
            melonService = MelonServiceFactory.create();
        }
        return melonService;
    }


    public SessionManager getSessionManager() {
        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(getContext()));
        }
        return sessionManager;
    }

    public DataManager getDataManager() {
        if (dataManager == null) {
            dataManager = new DataManager(getDangdutAsikService(),getAntMediaService(), getMelonService());
        }
        return dataManager;
    }

    public NetworkHelper getNetworkHelper() {
        if (networkHelper == null) {
            networkHelper = new NetworkHelper(getContext(), getSessionManager());
        }
        return networkHelper;
    }

    protected void loadImagesFromURL(Context context, ImageView view, String url) {

        if (TextUtils.isEmpty(url)) {
            view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.placeholder));
            return;
        }

//        String auth = Helper.digestAuth("arie@xeranta.com|210",
//                "46f6b3a71d4522ac5c291bc555221a6f",Helper.generateCode(),
//                Helper.generateCode(),Helper.generateCode(), Constants.method.GET,
//                DangdutAsikService.GET_ARTISTSALL);

//        GlideUrl glideUrl = new GlideUrl(url,
//                new LazyHeaders.Builder().addHeader("Authorization",auth).build());

        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(view);
    }

    protected void loadImageAvatarFromURL(Context context, ImageView view, String url) {

        if (TextUtils.isEmpty(url)) {
            view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.image_default_error_black));
            return;
        }

        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.image_default_error_black)
                .error(R.drawable.image_default_error_black)
                .into(view);

    }

    protected void loadImagesMoodFromURL(Context context, ImageView view, String url, ColorDrawable cd) {

        if (TextUtils.isEmpty(url)) {
            view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.placeholder));
            return;
        }

        Glide.with(context)
                .load(url)
                .placeholder(cd)
                .error(cd)
                .into(view);
    }
}
