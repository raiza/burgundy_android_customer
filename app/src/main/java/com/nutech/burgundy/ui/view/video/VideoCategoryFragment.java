package com.nutech.burgundy.ui.view.video;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataVideos;
import com.nutech.burgundy.presenter.CategoryVideoPresenter;
import com.nutech.burgundy.ui.adapter.CategoryVideoAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/27/17.
 * Xeranta Mobile Solutions
 */

public class VideoCategoryFragment extends BaseFragment implements CategoryVideoView {

    @BindView(R.id.recycle_video)
    RecyclerView rvVideo;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.main_error_layout)
    RelativeLayout rlMainError;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private CategoryVideoPresenter presenter;
    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new CategoryVideoPresenter(getDataManager(), getNetworkHelper(), getActivity());
        presenter.attachView(this);

        sessionManager = getSessionManager();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_category, container, false);
        ButterKnife.bind(this, view);

        initEvent();
        getData();
        return view;
    }

    private void initEvent(){
        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(true);
            getData();
        });

        btnRetry.setOnClickListener((View view)-> {
            swipeRefreshLayout.setRefreshing(true);
            getData();
        });
    }

    private void getData(){
        loadingIndicatorView.smoothToShow();
        presenter.getVideosList();
    }

    @Override
    public void showVideos(final List<DataVideos> videosList) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
        rlMainError.setVisibility(View.GONE);
        rvVideo.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvVideo.setItemAnimator(new DefaultItemAnimator());
        rvVideo.setAdapter(new CategoryVideoAdapter(getActivity(), videosList));
        rvVideo.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvVideo,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                        navigation.stopMusic();
                        navigation.updateControll(true);

                        Intent intent = new Intent(getActivity(), VideoPlayingActivity.class);
                        intent.putExtra("id", videosList.get(position).getId());
                        intent.putExtra("name", videosList.get(position).getName());
                        intent.putExtra("videourl", videosList.get(position).getUrl() +
                                videosList.get(position).getSource());
                        startActivity(intent);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showError(String error) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
            rlMainError.setVisibility(View.VISIBLE);
    }
}
