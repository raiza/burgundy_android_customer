package com.nutech.burgundy.ui.adapter.soundtrack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.soundtrack.Sound;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.soundtrack.AddNewSoundtrackFragment;
import com.nutech.burgundy.ui.view.soundtrack.EditMoodtrackActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 2019-06-28.
 * NuTech (Nusantara Teknologi)
 */
public class SoundAdapter extends BaseAdapter<SoundAdapter.EnergyHolder> {

    private Context context;
    private List<Sound> list;
    private List<String> ids = new ArrayList<String>();
    private AddNewSoundtrackFragment parrent;
    private EditMoodtrackActivity activity;
    private String soundIds;
    private String tag;

    public SoundAdapter(Context context, List<Sound> energyList, AddNewSoundtrackFragment addNewSoundtrackFragment, String tag) {
        this.context = context;
        this.list = energyList;
        this.parrent = addNewSoundtrackFragment;
        this.tag = tag;
    }

    public SoundAdapter(EditMoodtrackActivity context, List<Sound> dataSound, EditMoodtrackActivity editMoodtrackActivity,
                        String soundIds, String tag) {
        this.context = context;
        this.list = dataSound;
        this.activity = editMoodtrackActivity;
        this.soundIds = soundIds;
        this.tag = tag;
    }

    class EnergyHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cp_energy)
        Chip cpEnergy;
        @BindView(R.id.cg_energy)
        ChipGroup cgEnergy;
        View childView;

        EnergyHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public SoundAdapter.EnergyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tags_row, parent, false);
        return new EnergyHolder(view);
    }

    @Override
    public void onBindViewHolder(EnergyHolder holder, int position) {

        holder.cpEnergy.setText(list.get(position).getName());
        if (tag.equals(Constants.Key.EDIT_MOOD)){
            String[] items = soundIds.split(",");
            for (String item : items)
            {
                if (list.get(position).getId().equals(item)) {
                    holder.cpEnergy.setChecked(true);
                    ids.add(list.get(position).getId());
                    if (activity != null) {
                        activity.setSoundsParam(ids);
                    }
                }
            }
        }

        holder.cpEnergy.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                ids.add(list.get(position).getId());
            } else {
                try {
                    String idRemove = (list.get(position).getId());
                    for (int i = 0; i < ids.size(); i++) {
                        if (ids.get(i).equals(idRemove)) {
                            ids.remove(i);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (tag.equals(Constants.Key.EDIT_MOOD)) {
                if (activity != null) {
                    activity.setSoundsParam(ids);
                }
            } else if (tag.equals(Constants.Key.ADD_MOOD)) {
                if (parrent != null) {
                    parrent.setSoundsParam(ids);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
