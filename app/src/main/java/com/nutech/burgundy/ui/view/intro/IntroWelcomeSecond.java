package com.nutech.burgundy.ui.view.intro;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nutech.burgundy.R;
import com.nutech.burgundy.ui.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroWelcomeSecond extends BaseFragment {


    public IntroWelcomeSecond() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_intro_welcome_second, container, false);


        return rootView;
    }

}
