package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.presenter.MyMusicPresenter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playlist.DetailPlaylistDeleteFragment;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class PlaylistMemberAdapter extends RecyclerView.Adapter<PlaylistMemberAdapter.PlaylistHolder> {

    private Context context;
    private List<DataPlaylistMember> list;
    private MyMusicPresenter myMusicPresenter;
    private SessionManager sessionManager;
    private RecyclerView recyclerView;
    private DmApplication dmApplication;
    private FragmentNavigation fragmentNavigation;

    public PlaylistMemberAdapter(
            Context context, List<DataPlaylistMember> list, MyMusicPresenter presenter,
            SessionManager sessionManager, RecyclerView rvMyPlaylist, FragmentNavigation navigation) {
        this.context = context;
        this.list = list;
        this.myMusicPresenter = presenter;
        this.sessionManager = sessionManager;
        this.recyclerView = rvMyPlaylist;
        this.fragmentNavigation = navigation;
        this.dmApplication = (DmApplication) context.getApplicationContext();
    }

    class PlaylistHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_playlist_name)
        TextView tvPlaylistName;
        @BindView(R.id.tv_count_tracks)
        TextView tvCountTracks;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.image_playlist)
        ImageView ivPlaylist;
        @BindView(R.id.iv_option)
        ImageView ivOptions;
        @BindView(R.id.rl_more)
        RelativeLayout rlMore;
        @BindView(R.id.ll_playlist)
        LinearLayout llPlaylist;
        View childView;

        PlaylistHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public PlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_myplaylist, parent, false);
        return new PlaylistHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaylistHolder holder, final int position) {

        holder.tvPlaylistName.setText(list.get(position).getName());
        holder.tvCountTracks.setText(list.get(position).getCount_tracks());
        holder.tvLikes.setText(list.get(position).getLikes());

        String ImageUrl = Constants.URL_IMAGE_PLAYLIST_WS + list.get(position).getImage();
        Picasso.with(context.getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivPlaylist);


        holder.rlMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(context, holder.ivOptions);
                popup.inflate(R.menu.myplaylist_options);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(android.view.MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.remove:
                                String memberId = sessionManager.getDataMember().getId();
                                Map<String, String> params = new HashMap<String, String>();
                                try {
                                    params.put("customer_id", memberId);
                                    params.put("playlist_id", list.get(position).getId());
                                } catch (Exception e) {

                                }
//                                myMusicPresenter.removePlaylistMember(params);
                                remove(position, list);
                                break;
                            case R.id.menu2:
                                //handle menu2 click
                                break;
                            case R.id.menu3:
                                //handle menu3 click
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
            }
        });

        holder.ivPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dmApplication.setTitle(list.get(position).getName());

                fragmentNavigation.pushFragment(
                        new DetailPlaylistDeleteFragment(
                                list.get(position).getId(),
                                list.get(position).getImage(),
                                Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_MYMUSIC),
                        list.get(position).getName(),
                        true);
            }
        });

        holder.llPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dmApplication.setTitle(list.get(position).getName());
                fragmentNavigation.pushFragment(
                        new DetailPlaylistDeleteFragment(
                                list.get(position).getId(),
                                list.get(position).getImage(),
                                Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_MYMUSIC),
                        list.get(position).getName(),
                        true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void remove(int position, List<DataPlaylistMember> list) {
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(true);

        System.out.println("position: "+position);
        list.remove(position);
        notifyItemRemoved(position);

        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

}
