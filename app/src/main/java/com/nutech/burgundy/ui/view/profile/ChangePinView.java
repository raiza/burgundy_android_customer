package com.nutech.burgundy.ui.view.profile;

import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 07/05/18.
 * Xeranta Mobile Solutions
 */
public interface ChangePinView extends BaseView {

    void changeSucess(DataMember dataCustomer);

    void changeError(String error);

}
