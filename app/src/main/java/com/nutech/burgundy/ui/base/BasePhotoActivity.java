package com.nutech.burgundy.ui.base;

import android.net.Uri;

import androidx.annotation.NonNull;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.utils.PermissionManager;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.List;

/**
 * Created by Arif Setiawan on 15/03/19.
 * Xeranta Mobile Solutions
 */
public abstract class BasePhotoActivity extends BaseActivity {

    private PermissionManager.PermissionListener permissionListener = new PermissionManager.PermissionListener() {
        @Override
        public void onPermissionsGranted(List<String> perms) {
            startPickImageActivity();
        }

        @Override
        public void onPermissionsDenied(List<String> perms) {

        }

        @Override
        public void onPermissionRequestRejected() {

        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.onRequestPermissionsResult(context, permissionListener, requestCode, permissions, grantResults);
    }

    protected void showDialogChoice() {
        checkingPermission();
    }

    protected void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setMinCropResultSize(100, 100)
                .setMaxCropResultSize(1000, 1000)
                .setAspectRatio(1, 1)
                .start(this);
    }

    protected void startCropImage(Uri imageUri) {
        CropImage.activity(imageUri)
                .setMinCropResultSize(100, 100)
                .setOutputCompressQuality(60)
                .start(this);
    }

    protected void startCropImageMoodtrackActivity(Uri imageUri) {
        File file = new File(imageUri.getPath());
        if (file.length() > 0){
            CropImage.activity(imageUri)
                    .setMinCropResultSize(100, 100)
                    .setAspectRatio(16, 9)
                    .setOutputCompressQuality(20)
                    .start(this);
        } else {
            CropImage.activity(imageUri)
                    .setMinCropResultSize(100, 100)
                    .setAspectRatio(16, 9)
                    .setOutputCompressQuality(50)
                    .start(this);
        }
    }

    private void startPickImageActivity() {
        CropImage.startPickImageActivity(this);
    }

    private void checkingPermission() {

        if (PermissionManager.hasPermissions(context, Constants.PERMISSIONS_PHOTO)) {
            startPickImageActivity();
            return;
        }

        PermissionManager.requestPermissions(
                context, permissionListener,
                "Please Enable Permission", Constants.PERMISSIONS_PHOTO);
    }
}
