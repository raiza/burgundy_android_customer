package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.promo.DetailPromoFragment;

import java.util.ArrayList;

/**
 * Created by Arif Setiawan on 9/2/17.
 * Xeranta Mobile Solutions
 */

public class SlidePromoAdapter extends PagerAdapter {

    private ArrayList<String> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private ViewPager viewPager;
    private FragmentNavigation navigation;
    private ArrayList<String> idS;

    public SlidePromoAdapter(FragmentActivity activity, ArrayList<String> imagesArray, String tag,
                             ViewPager vpPromo, FragmentNavigation nav, ArrayList<String> ids) {
        this.IMAGES = imagesArray;
        this.inflater = LayoutInflater.from(activity);
        this.context = activity;
        this.viewPager = vpPromo;
        this.navigation = nav;
        this.idS = ids;
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View imageLayout = inflater.inflate(R.layout.custom_layout_slider, container, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);


            String imageUrl = Constants.URL_IMAGE_PROMOTIONS + IMAGES.get(position);

            Picasso.with(context.getApplicationContext())
                    .load(imageUrl)
                    .error(R.drawable.image_dm_error)
                    .fit()
                    .into(imageView);

            container.addView(imageLayout, 0);

        imageView.setOnClickListener(view -> navigation.pushFragment(new DetailPromoFragment(idS.get(position)), "Detail Promo", true));

        return imageLayout;
    }
}
