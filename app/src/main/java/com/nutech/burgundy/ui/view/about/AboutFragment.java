package com.nutech.burgundy.ui.view.about;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataAbout;
import com.nutech.burgundy.presenter.AboutPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class AboutFragment extends BaseFragment implements AboutView {

    @BindView(R.id.tv_aboutus)
    TextView tvAboutUs;
    @BindView(R.id.tv_term_conditions)
    TextView tvTermConditions;
    @BindView(R.id.tv_telpon)
    TextView tvTelpon;
    @BindView(R.id.tv_email)
    TextView tvEmail;
    @BindView(R.id.tv_web)
    TextView tvWebsite;
    @BindView(R.id.iv_logo)
    CircleImageView ivLogo;
    @BindView(R.id.tv_sitename)
    TextView tvSiteName;
    @BindView(R.id.tv_app_version)
    TextView tvVersion;

    private Unbinder unbinder;
    private AboutPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new AboutPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter.getAboutUs();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showData(DataAbout dataAbout) {

        tvSiteName.setText(dataAbout.getSite_name());
        tvAboutUs.setText(dataAbout.getAbout_us());
        tvTermConditions.setText(dataAbout.getTerm_conditions());
        tvTelpon.setText(getString(R.string.label_telpon)+" "+dataAbout.getTel());
        tvEmail.setText("Email: "+dataAbout.getEmail());
        tvWebsite.setText("Website: "+dataAbout.getWebsite());

        String appVersion = BuildConfig.VERSION_NAME;
        tvVersion.setText(getString(R.string.label_version_app)+" "+appVersion);

        String ImageUrl = Constants.URL_IMAGE_LOGO+dataAbout.getLogo();
        loadImagesAvatarFromURL(context, ivLogo, ImageUrl);

    }

    @Override
    public void showError(String error) {

    }
}
