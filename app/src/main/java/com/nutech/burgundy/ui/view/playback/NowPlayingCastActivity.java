package com.nutech.burgundy.ui.view.playback;

import android.os.Bundle;

import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import com.nutech.burgundy.R;

public class NowPlayingCastActivity extends AppCompatActivity {

    private PlaybackLocation playbackLocation;
    private CastSession castSession;
    private SessionManagerListener<CastSession> sessionManagerListener;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_playing_cast);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

}
