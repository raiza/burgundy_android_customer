package com.nutech.burgundy.ui.view.playlist;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.adapter.PlaylistCustomerAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 11/15/17.
 * Xeranta Mobile Solutions
 */

public class PlaylistCustomerFragment extends BaseFragment implements PlaylistView {

    @BindView(R.id.recycle_playlist_customer)
    RecyclerView rvPlaylistCustomer;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private PlaylistPresenter presenter;
    private SessionManager sessionManager;
    private Unbinder unbinder;
    private String state = "";

    public PlaylistCustomerFragment(String states) {
        this.state = states;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist_customer, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvPlaylistCustomer.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPlaylistCustomer.setItemAnimator(new DefaultItemAnimator());
        rvPlaylistCustomer.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });

        getData();


        return view;
    }

    private void getData() {
        loadingIndicatorView.smoothToShow();
        String memberId = sessionManager.getDataMember().getId();
        presenter.getPlaylistCustomer(memberId);
    }

    @Override
    public void showTracks(List<DataTracks> tracksList) {

    }

    @Override
    public void showAllPlaylist(final List<DataPlaylist> dataPlaylists) {

        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
        rvPlaylistCustomer.setAdapter(new PlaylistCustomerAdapter(getActivity(), dataPlaylists, navigation,
                presenter, getSessionManager(), state));
        rvPlaylistCustomer.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvPlaylistCustomer,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showErrorAllPlaylist(String error) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showErrorPlaylist(String error) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(false);
        rvPlaylistCustomer.removeAllViews();
    }

    @Override
    public void showAddSucces(String data) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(true);
        getData();
        Toast.makeText(getActivity(), "Success add to playlist.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(true);
        getData();
//        Toast.makeText(getActivity(), "Success add to playlist.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRemoveSucces(String data) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(true);
        Toast.makeText(getActivity(), "Playlist Deleted.", Toast.LENGTH_SHORT).show();
        getData();
    }

    @Override
    public void showRemoveError(String error) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(true);
//        Toast.makeText(getActivity(), "Playlist Deleted.", Toast.LENGTH_SHORT).show();
        getData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
