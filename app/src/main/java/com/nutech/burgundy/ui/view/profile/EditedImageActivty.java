package com.nutech.burgundy.ui.view.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.theartofdev.edmodo.cropper.CropImageView;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.presenter.UpdateProfilePresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 26/02/18.
 * Xeranta Mobile Solutions
 */

public class EditedImageActivty extends BaseAppCompatActivity implements UpdateProfileView {

    @BindView(R.id.cropImageView)
    CropImageView cropImageView;
    @BindView(R.id.ib_close)
    ImageButton ibClose;
    @BindView(R.id.ib_done)
    ImageButton ibDone;
    @BindView(R.id.loading)
    AVLoadingIndicatorView avLoadingIndicatorView;
    @BindView(R.id.ib_rotate)
    ImageButton ibRotate;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;

    private Intent intent;
    private UpdateProfilePresenter presenter;
    private SessionManager sessionManager;

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new UpdateProfilePresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edited_image);
        ButterKnife.bind(this);

        initViews();
        initEvents();

    }

    private void initViews(){

        intent = getIntent();
        cropImageView.setAutoZoomEnabled(false);
        cropImageView.setFixedAspectRatio(true);
        cropImageView.setImageBitmap(Constants.ParamValue.B_IMAGE);

    }

    private void initEvents(){

        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ibDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap cropped = cropImageView.getCroppedImage();
                Constants.ParamValue.B_IMAGE = cropped;
                uploadToServer(cropped);
            }
        });

        ibRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptions(ibRotate);
            }
        });
    }

    private void uploadToServer(Bitmap imageBitmap){


        //Update Image & Upload to server]

        String memberId = sessionManager.getDataMember().getId();
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("id", memberId);
            params.put("image", Helper.convertToBase64(imageBitmap));
        } catch (Exception e) {

        }
//        System.out.println("** Param ** "+ params);

        llLoading.setVisibility(View.VISIBLE);
        presenter.uploadImageProfile(params);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    public void showDataMember(DataMember dataMember) {

    }

    @Override
    public void errorDataMember(String error) {

    }

    @Override
    public void succesUpdate(String success) {
        llLoading.setVisibility(View.GONE);
        onBackPressed();
    }

    @Override
    public void error(String error) {
        llLoading.setVisibility(View.GONE);
        onBackPressed();
    }

    private void showOptions(ImageButton btnOptions) {

        PopupMenu popup = new PopupMenu(this, btnOptions);
        popup.inflate(R.menu.rotation_options);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(android.view.MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.r_45:
                        cropImageView.rotateImage(45);
                        break;
                    case R.id.r_90:
                        cropImageView.rotateImage(90);
                        break;
                    case R.id.r_180:
                        cropImageView.rotateImage(180);
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

}
