package com.nutech.burgundy.ui.view.promo;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;


public class DialogNotificationActivity extends BaseAppCompatActivity {
    private View mControlsView;
    private boolean mVisible;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_notification);
        extras = getIntent().getExtras();
        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        ImageView imageView = (ImageView) findViewById(R.id.fullscreen_content);
        ImageButton btnClose = (ImageButton) findViewById(R.id.button_close);

        hide();

        String body = extras.getString("body");

        System.out.println("--- downloading image --- "+body);
        Picasso.with(DialogNotificationActivity.this)
                .load(body)
                .placeholder(R.drawable.image_dm_error)
                .error(R.drawable.image_dm_error)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(imageView);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
