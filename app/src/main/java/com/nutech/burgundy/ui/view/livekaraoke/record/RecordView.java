package com.nutech.burgundy.ui.view.livekaraoke.record;

import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * @author Dafian on 12/5/17
 */

public interface RecordView extends BaseView {

    void startBroadcast(String roomId);

    void resultBroadcast(Boolean result);

    void showDataKaraoke(DataKaraoke dataKaraoke);

    void showErrorKaraoke(String error);

    void showError(String error);

    void showLiricks(List<DataLiricks> dataLiricksList);

    void showErrorLiricks(String error);

    void showRoom(String succes);

    void uploadVideoDone(String data);

    void showErrorRoom(String error);

    void showUpdateRoom(String succes);

    void showErrorUpdateRoom(String error);

    void showUpdateRoomBeforeExit(String succes);

    void showErrorUpdateRoomBeforeExit(String error);

    void errorUploadVideo(String error);

    void showUpdateRoomOnCompletedKaraoke(String succes);

    void showErrorUpdateRoomOnCompletedKaraoke(String error);
}
