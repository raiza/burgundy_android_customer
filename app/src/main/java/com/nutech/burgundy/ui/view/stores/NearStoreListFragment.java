package com.nutech.burgundy.ui.view.stores;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.lib.gps.GPSTracker;
import com.nutech.burgundy.lib.gps.IOnlocationChanged;
import com.nutech.burgundy.presenter.store.StorePresenter;
import com.nutech.burgundy.ui.adapter.NearStoreListAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NearStoreListFragment extends BaseFragment implements StoresView, IOnlocationChanged {
    @BindView(R.id.rv_store_list)
    RecyclerView rvStoreList;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.img_nodata)
    ImageView imgNodata;
    @BindView(R.id.tv_msg_error)
    TextView tvMsgError;
    @BindView(R.id.main_error_layout)
    RelativeLayout mainErrorLayout;
    private double lat;
    private double lng;
    private List<DataStore> storeList;
    private Unbinder unbinder;
    private StorePresenter storePresenter;
    private GPSTracker gps;

    public NearStoreListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();
        }

        storePresenter = new StorePresenter(getDataManager(), getNetworkHelper());
        storePresenter.attachView(this);

        gps = new GPSTracker(context,this);
        if (gps.canGetLocation()) {
            lat = gps.getLatitude(); // returns latitude
            lng = gps.getLongitude();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        initEvent();
        initView();


        return view;
    }

    private void initEvent() {

        swipeRefresh.setOnRefreshListener(this::loadingData);

        btnRetry.setOnClickListener((View v) -> {
            swipeRefresh.setRefreshing(true);
            loadingData();
        });
    }

    private void initView() {
        loadingData();
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        rvStoreList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvStoreList.setItemAnimator(new DefaultItemAnimator());
    }

    private void loadingData() {
        swipeRefresh.setRefreshing(true);
        storePresenter.getNearStoreList(lat, lng);
    }

    private void checkPermission() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.RequestCode.GPS_NEAR_STORE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public void moveToDetail(int position) {
        if (storeList.size() > 0) {
            navigation.pushFragment(new DetailStoreFragment(storeList.get(position)),storeList.get(position).getSite_name(),true);
        }
    }

    @Override
    public void ShowStores(List<DataStore> storeList) {
        swipeRefresh.setRefreshing(false);
        if (storeList.size() > 0) {
            mainErrorLayout.setVisibility(View.VISIBLE);
            this.storeList = storeList;
            NearStoreListAdapter storeAdapter = new NearStoreListAdapter(context, storeList, this);
            rvStoreList.setAdapter(storeAdapter);
        } else {
            mainErrorLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void ShowStoresError(String error) {
        swipeRefresh.setRefreshing(false);
        mainErrorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (lat == 0.0 && lng == 0.0) {
            lat = location.getLatitude();
            lng = location.getLongitude();
            gps.stopUsingGPS();
            loadingData();
        }
    }
}
