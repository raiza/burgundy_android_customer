package com.nutech.burgundy.ui.view.playlist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * @author Arif Setiawan on 19/05/19.
 * @company NuTech
 */

public class DetailPlaylistFragment extends BaseFragment implements PlaylistView {

    @BindView(R.id.rv_track_list)
    RecyclerView rvTrackList;
    @BindView(R.id.image_backdrop)
    ImageView imageBackdrop;
    @BindView(R.id.image_playlist)
    ImageView imagePlaylist;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.image_fav)
    ImageView imageFav;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @OnClick(R.id.image_fav)
    public void addToFavorite(){
        String memberId = sessionManager.getDataMember().getId();
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("customer_id", memberId);
            params.put("playlist_id", playlistId);
        } catch (Exception e) {

        }
        System.out.println("** Param ** "+ params);
        presenter.sendPlaylist(params);
    }

    private Unbinder unbinder;
    private PlaylistPresenter presenter;
    private SessionManager sessionManager;

    //---- Params ----//
    private String playlistId = "";
    private String playlistImage = "";
    private String key = "";

    public DetailPlaylistFragment(String id, String image, String keyDetailPlaylist) {
        this.playlistId = id;
        this.playlistImage = image;
        this.key = keyDetailPlaylist;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        initEvents();
        getData();
        return view;
    }

    private void getData() {
        loadingIndicatorView.smoothToShow();
        presenter.getTracksByPLaylist(playlistId);
    }

    public void initView() {

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        if (key.equals(Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_MYMUSIC)){
            String ImageUrl = Constants.URL_IMAGE_PLAYLIST_WS + playlistImage;
            loadImagesAvatarFromURL(context, imagePlaylist, ImageUrl);
            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_detail_error)
                    .transform(new BlurTransformation(getActivity()))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(imageBackdrop);
        } else {

            String ImageUrl = Constants.URL_IMAGE_PLAYLIST + playlistImage;
            loadImagesAvatarFromURL(context, imagePlaylist, ImageUrl);
            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_detail_error)
                    .transform(new BlurTransformation(getActivity()))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .into(imageBackdrop);
        }
    }

    private void initEvents() {
        swipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) () -> {
            swipeRefreshLayout.setRefreshing(true);
            getData();
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showTracks(final List<DataTracks> tracksList) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
        rvTrackList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTrackList.setItemAnimator(new DefaultItemAnimator());
        rvTrackList.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        rvTrackList.setAdapter(new TracksAdapter(getActivity(), tracksList, navigation, "",
                new TracksAdapter.OnItemCheckListener() {
                    @Override
                    public void onItemCheck(DataTracks dataTracks) {

                    }

                    @Override
                    public void onItemUncheck(DataTracks dataTracks) {

                    }
                }));

        rvTrackList.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTrackList,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showAllPlaylist(List<DataPlaylist> dataPlaylists) {

    }

    @Override
    public void showErrorAllPlaylist(String error) {

    }

    @Override
    public void showErrorPlaylist(String error) {

    }

    @Override
    public void showAddSucces(String data) {
    }

    @Override
    public void showError(String error) {

        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
    }

    @Override
    public void showRemoveSucces(String data) {

    }

    @Override
    public void showRemoveError(String error) {

    }
}
