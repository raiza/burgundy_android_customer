package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataRoom;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 23/01/18.
 * Xeranta Mobile Solutions
 */

public class TopKaraokeAdapter extends RecyclerView.Adapter<TopKaraokeAdapter.TopKaraokeHolder> {

    private Context context;
    private List<DataRoom> tracks;

    public TopKaraokeAdapter(Context context, List<DataRoom> tracks) {
        this.context = context;
        this.tracks = tracks;
    }

    class TopKaraokeHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_number)
        TextView tvNumber;
        @BindView(R.id.tv_artists_name)
        TextView tvArtistsName;
        @BindView(R.id.tv_track_name)
        TextView tvTrackName;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.tv_viewers)
        TextView tvViewers;

        TopKaraokeHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public TopKaraokeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_top_karaoke, parent, false);
        return new TopKaraokeHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopKaraokeHolder holder, int position) {

        Integer index = position + 1;
        holder.tvNumber.setText(String.valueOf(index));
        holder.tvArtistsName.setText(tracks.get(position).getCustomername());
        holder.tvTrackName.setText(tracks.get(position).getTrackname());
        holder.tvLikes.setText(tracks.get(position).getLikes());
        holder.tvViewers.setText(tracks.get(position).getView());
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }
}
