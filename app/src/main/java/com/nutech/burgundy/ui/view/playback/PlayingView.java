package com.nutech.burgundy.ui.view.playback;

import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 10/17/17.
 * Xeranta Mobile Solutions
 */

public interface PlayingView extends BaseView {

    void showSucces(String success);

    void showError(String error);
}
