package com.nutech.burgundy.ui.view.artists;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.presenter.ArtistsPresenter;
import com.nutech.burgundy.ui.adapter.ArtistsAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;

/**
 * @author Arif Setiawan on 29/05/17.
 * Nusantara Teknologi
 */

public class ArtistsFragment extends BaseFragment implements ArtistsView {

    private final String TAG = "ArtistsFragment -- ";
    private int PER_PAGE = 20;
    private int PAGE = 0;

    @BindView(R.id.recycle_artist)
    IndexFastScrollRecyclerView rvAllArtists;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private DmApplication dmApplication;
    private ArtistsPresenter presenter;
    private Unbinder unbinder;
    private ArtistsAdapter adapter;
    private boolean isLoading = false;
    private boolean isLoadMore = false;
    private boolean isLastPage = false;
    private Handler handler = new Handler();
    private DataArtistList dataArtistsList;
    private LinearLayoutManager linearLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dmApplication = (DmApplication) getActivity().getApplicationContext();
        presenter = new ArtistsPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_artists, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews();
        initEvent();
        loadingData();

        return view;
    }

    private void loadingData() {
        presenter.getAllArtistsWithPaging(PAGE, PER_PAGE);
//        new CountDownTimer(2000, 1000) {
//
//            public void onTick(long millisUntilFinished) {
//
//            }
//
//            public void onFinish() {
//                presenter.getArtistAndGenre();
//            }
//        }.start();
    }

    private void initViews() {

        if (dataArtistsList == null) {
            dataArtistsList = new DataArtistList();
        }
//        dataArtistsList = new DataArtistList();
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvAllArtists.setLayoutManager(linearLayoutManager);
        rvAllArtists.setItemAnimator(new DefaultItemAnimator());
        rvAllArtists.addItemDecoration(new DividerItemDecoration(
                Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL));

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);


        rvAllArtists.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        isLoading = true;
    }

    private void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        isLoading = false;
    }

    private void initEvent() {

        swipeRefreshLayout.setOnRefreshListener(() -> {
            swipeRefreshLayout.setRefreshing(true);
            loadingData();
        });

        rvAllArtists.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvAllArtists,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        dmApplication.setTitle(dataArtistsList.getArtists().get(position).getName());
                        navigation.pushFragment(
                                new TabArtistFragment(
                                        dataArtistsList.getArtists().get(position).getId(),
                                        dataArtistsList.getArtists().get(position).getImage(),
                                        dataArtistsList.getArtists().get(position).getGenre_id()),
                                dataArtistsList.getArtists().get(position).getName(),
                                true);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        rvAllArtists.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0 && totalItemCount >= PER_PAGE) {
                        showProgress();
                        loadingData();
                    }
                }
            }
        });
    }

    @Override
    public void showAllArtists(DataArtistList artistsList) {

        new Thread(() -> handler.post(() -> {
            try {
                hideProgress();
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                rvAllArtists.setVisibility(View.VISIBLE);
            } catch (Exception ignored) {

            }
        })
        ).start();

        if (dataArtistsList.getArtists() == null && artistsList != null && artistsList.getArtists().size() > 0) {
            Log.e("test","1");
            dataArtistsList.setArtists(artistsList.getArtists());
            adapter = new ArtistsAdapter(getActivity(), dataArtistsList);
            rvAllArtists.setAdapter(adapter);
            rvAllArtists.setIndexBarColor(R.color.colorAccent);
            rvAllArtists.setIndexBarTextColor(R.color.black);
            rvAllArtists.setIndexBarStrokeVisibility(false);
        } else if (dataArtistsList.getArtists() != null && artistsList != null && artistsList.getArtists().size() > 0) {
           Log.e("test","2");

            dataArtistsList.addArtistsAll(artistsList.getArtists());

            if (adapter == null) {
                Log.e("test","3");
                adapter = new ArtistsAdapter(getActivity(), dataArtistsList);
                rvAllArtists.setAdapter(adapter);
                rvAllArtists.setIndexBarColor(R.color.colorAccent);
                rvAllArtists.setIndexBarTextColor(R.color.black);
                rvAllArtists.setIndexBarStrokeVisibility(false);
            }

            if (rvAllArtists.getAdapter() == null) {

                rvAllArtists.setVisibility(View.VISIBLE);
                rvAllArtists.setAdapter(adapter);
                rvAllArtists.setIndexBarColor(R.color.colorAccent);
                rvAllArtists.setIndexBarTextColor(R.color.black);
                rvAllArtists.setIndexBarStrokeVisibility(false);
            }
        }
        if (dataArtistsList.getArtists() != null) {
            if (dataArtistsList.getArtists().size() >= PAGE * PER_PAGE) {
                PAGE = PAGE + 1;
            } else {
                isLastPage = true;
                rvAllArtists.setPadding(0,0,0,0);
            }
        }
        if (adapter != null ) {
            adapter.notifyDataSetChanged();
        }
//        adapter.notifyDataSetChanged();
    }

    @Override
    public void showGenre(List<DataGenre> genreList) {

    }

    @Override
    public void showArtistsTracks(List<DataTracks> tracksList) {

    }

    @Override
    public void showArtistsAlbums(List<DataAlbums> albumsList) {

    }

    @Override
    public void showArtistsSearch(final List<DataArtists> artistsList) {
    }

    @Override
    public void showErrorSearch(String error) {
        showErrorToast(getString(R.string.msg_artist_not_found));
    }

    @Override
    public void showError(String error) {
        if (dataArtistsList.getArtists() != null) {
            if (dataArtistsList.getArtists().size() > 0) {

                if (rvAllArtists.getAdapter() == null) {

                    rvAllArtists.setVisibility(View.VISIBLE);
                    rvAllArtists.setAdapter(adapter);
                    rvAllArtists.setIndexBarColor(R.color.colorAccent);
                    rvAllArtists.setIndexBarTextColor(R.color.black);
                    rvAllArtists.setIndexBarStrokeVisibility(false);
                }

                swipeRefreshLayout.setRefreshing(false);
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);

                hideProgress();
                isLastPage = true;
                rvAllArtists.setPadding(0,0,0,0);
            } else {
                swipeRefreshLayout.setRefreshing(false);
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                rvAllArtists.setVisibility(View.GONE);
            }
        } else {
            swipeRefreshLayout.setRefreshing(false);
            mShimmerViewContainer.stopShimmer();
            mShimmerViewContainer.setVisibility(View.GONE);
            rvAllArtists.setVisibility(View.GONE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onPause() {
//        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void showErrorToast(String msg) {
        Toast.makeText(dmApplication, msg, Toast.LENGTH_SHORT).show();
    }

}
