package com.nutech.burgundy.ui.view.soundtrack;

import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Sound;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 2019-07-02.
 * NuTech (Nusantara Teknologi)
 */
public interface SoundtrackView extends BaseView {

    void showEnergyList(List<Energy> dataEnergy);

    void showErrorEnergy(String error);

    void showSoundList(List<Sound> dataSound);

    void showErrorSound(String error);

    void showGenreList(List<DataGenre> genres);

    void showErrorGenre(String error);

    void showDecadeList(List<Decade> decades);

    void showErrorDecade(String error);

    void showSuccessAdd(String success);
    void showErrorAdd(String error);
}
