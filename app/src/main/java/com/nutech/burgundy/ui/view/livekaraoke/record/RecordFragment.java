package com.nutech.burgundy.ui.view.livekaraoke.record;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.presenter.livekaraoke.RecordPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.antmedia.android.broadcaster.ILiveVideoBroadcaster;
import io.antmedia.android.broadcaster.LiveVideoBroadcaster;

/**
 * @author Dafian on 12/5/17
 */

public class RecordFragment extends BaseFragment implements RecordView {

    private static final String TAG = "Record";

    @BindView(R.id.gl_surface)
    GLSurfaceView glSurface;

    @BindView(R.id.tv_video_timer)
    TextView tvVideoTimer;
    @BindView(R.id.ll_video_timer)
    LinearLayout llVideoTimer;

    @BindView(R.id.iv_user_profile)
    CircleImageView ivUserProfile;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.tv_user_join_date)
    TextView tvUserJoinDate;
    @BindView(R.id.et_room_id)
    EditText etRoomId;

    @BindView(R.id.ll_video_detail)
    LinearLayout llVideoDetail;

    @BindView(R.id.bt_submit)
    Button btSubmit;
    @BindView(R.id.bt_stop)
    Button btStop;

    private Timer timer;
    private Intent intentService;
    private Boolean isRecording;
    private String roomId;
    private int elapsedTime;

    private Unbinder unbinder;
    private RecordPresenter presenter;

    private ILiveVideoBroadcaster liveVideoBroadcaster;
    private ContentLoadingProgressBar progressBar;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            LiveVideoBroadcaster.LocalBinder binder = (LiveVideoBroadcaster.LocalBinder) iBinder;
            if (liveVideoBroadcaster == null) {
                liveVideoBroadcaster = binder.getService();
                liveVideoBroadcaster.init(getActivity(), glSurface);
                liveVideoBroadcaster.setAdaptiveStreaming(true);
            }
            liveVideoBroadcaster.openCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            liveVideoBroadcaster = null;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        intentService = new Intent(getActivity(), LiveVideoBroadcaster.class);
        getActivity().startService(intentService);

        presenter = new RecordPresenter(getDataManager(), getNetworkHelper(), getActivity().getApplicationContext());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_record, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();

        glSurface.setEGLContextClientVersion(2);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(intentService, connection, 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (liveVideoBroadcaster != null) {
            liveVideoBroadcaster.pause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unbindService(connection);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
                || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            liveVideoBroadcaster.setDisplayOrientation();
        }
    }

    @Override
    public void startBroadcast(String roomId) {

        Log.i(TAG, "startBroadcast: " + roomId);

        this.roomId = roomId;

        if (!isRecording) {

            Log.d(TAG, "!isRecording: ");

            if (liveVideoBroadcaster != null) {

                Log.d(TAG, "liveVideoBroadcaster != null: ");

                if (!liveVideoBroadcaster.isConnected()) {

                    Log.d(TAG, "liveVideoBroadcaster now is running: ");

                    // You can insert some logic to create notification in here
                    triggerStartRecording();

                } else {
                    showError(getString(R.string.main_error_streaming_not_finished));
                }

            } else {
                showError(getString(R.string.main_error_streaming_unknown));
            }

        } else {
            triggerStopRecording();
        }
    }

    @Override
    public void resultBroadcast(Boolean result) {

        Log.i(TAG, "resultBroadcast: " + result);

        isRecording = result;
        progressBar.hide();

        if (result) {
            // Start Timer
            btStop.setVisibility(View.VISIBLE);
            llVideoDetail.setVisibility(View.GONE);
            llVideoTimer.setVisibility(View.VISIBLE);
            startTimer();

        } else {
            // Stop Record & Update status Video
//            presenter.updateStatusVideo(roomId);
            triggerStopRecording();
        }

        btSubmit.setEnabled(true);
        btSubmit.setBackgroundColor(
                ContextCompat.getColor(getActivity(), R.color.colorAccent));
    }

    @Override
    public void showDataKaraoke(DataKaraoke dataKaraoke) {

    }

    @Override
    public void showErrorKaraoke(String error) {

    }

    @Override
    public void showError(String error) {

        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();

        btSubmit.setEnabled(true);
        btSubmit.setBackgroundColor(
                ContextCompat.getColor(getActivity(), R.color.colorAccent));
    }

    @Override
    public void showLiricks(List<DataLiricks> dataLiricksList) {

    }

    @Override
    public void showErrorLiricks(String error) {

    }

    @Override
    public void showRoom(String succes) {

    }

    @Override
    public void uploadVideoDone(String data) {

    }

    @Override
    public void showErrorRoom(String error) {

    }

    @Override
    public void showUpdateRoom(String succes) {

    }

    @Override
    public void showErrorUpdateRoom(String error) {

    }

    @Override
    public void showUpdateRoomBeforeExit(String succes) {

    }

    @Override
    public void showErrorUpdateRoomBeforeExit(String error) {

    }

    @Override
    public void errorUploadVideo(String error) {

    }

    @Override
    public void showUpdateRoomOnCompletedKaraoke(String succes) {

    }

    @Override
    public void showErrorUpdateRoomOnCompletedKaraoke(String error) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void initView() {

        isRecording = false;

        progressBar = new ContentLoadingProgressBar(getActivity());
        progressBar.setIndeterminate(true);

        btStop.setVisibility(View.GONE);
        llVideoDetail.setVisibility(View.VISIBLE);
        llVideoTimer.setVisibility(View.GONE);
    }

    private void initEvent() {

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendDataToServer();
            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                presenter.updateStatusVideo(roomId);
                triggerStopRecording();
            }
        });
    }

    private void sendDataToServer() {

        btSubmit.setEnabled(false);
        btSubmit.setBackgroundColor(
                ContextCompat.getColor(getActivity(), R.color.grey_600));

        // Login for sending data live karaoke to server

        String roomId = etRoomId.getText().toString();
        if (TextUtils.isEmpty(roomId)) {
            showError("Room Id Not Allow Null");
        }

        presenter.sendDataToServer(roomId);
    }

    private void triggerStartRecording() {

        presenter.startLiveStreaming(liveVideoBroadcaster, roomId);
        progressBar.show();
    }

    private void triggerStopRecording() {

        Log.i(TAG, "triggerStopRecording: ");

        if (isRecording) {

            stopTimer();
            liveVideoBroadcaster.stopBroadcasting();
        }

        isRecording = false;
    }

    private void startTimer() {

        Log.i(TAG, "startTimer: ");

        if (timer == null) {
            timer = new Timer();
        }

        elapsedTime = 0;
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        elapsedTime += 1;
                        tvVideoTimer.setText(Helper.getDurationString(elapsedTime));

                        if (liveVideoBroadcaster == null || !liveVideoBroadcaster.isConnected()) {
                            triggerStopRecording();
                            showError("Connection Lost");
                        }
                    }
                });
            }
        }, 0, 1000);
    }

    private void stopTimer() {

        Log.i(TAG, "stopTimer: ");

        if (timer != null) {
            this.timer.cancel();
        }

        this.timer = null;
        this.elapsedTime = 0;
    }

}
