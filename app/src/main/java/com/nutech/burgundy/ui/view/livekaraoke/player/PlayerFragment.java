package com.nutech.burgundy.ui.view.livekaraoke.player;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.RtmpDataSource;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.api.AntMediaService;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.lib.extractor.DefaultExtractorsFactoryForFLV;
import com.nutech.burgundy.presenter.livekaraoke.PlayerPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Dafian on 12/5/17
 */

@SuppressLint("ValidFragment")
public class PlayerFragment extends BaseFragment
        implements PlayerView, ExoPlayer.EventListener, PlaybackControlView.VisibilityListener {

    public static final String KEY_ROOM_ID = "key_room_id";
    public static final String PREFER_EXTENSION_DECODERS = "prefer_extension_decoders";
    private static final String TAG = "Player";
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();
    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    @BindView(R.id.pv_exo_player)
    SimpleExoPlayerView pvExoPlayer;
    @BindView(R.id.bt_play)
    Button btPlay;
    //    @BindView(R.id.tv_video_status)
//    TextView tvVideoStatus;
    @BindView(R.id.et_room_id)
    EditText etRoomId;
    @BindView(R.id.iv_user_profile)
    CircleImageView ivUserProfile;
    @BindView(R.id.tv_user_name)
    TextView tvUserName;
    @BindView(R.id.ll_video_detail)
    LinearLayout llVideoDetail;
    @BindView(R.id.image_like)
    ShineButton imgLikes;
    @BindView(R.id.image_report)
    ShineButton imgReport;
    @BindView(R.id.indicator_live)
    AVLoadingIndicatorView indicatorLive;
    private Unbinder unbinder;
    private PlayerPresenter presenter;
    private SessionManager sessionManager;
    private AntMediaService antMediaService;
    private Boolean isAutoPlay;
    private Boolean isRetrySource;
    private String userAgent;
    private String roomId;
    private String roomStatus;
    private int resumeWindow;
    private long resumePosition;

    private DataSource.Factory mediaDataSourceFactory;
    private RtmpDataSource.RtmpDataSourceFactory rtmpDataSourceFactory;

    private SimpleExoPlayer exoPlayer;
    private DefaultTrackSelector trackSelector;
    private Handler handler;
    private String Id;
    private String tempRoomId;
    private String tempTrackId;
    private String customerId;
    private String isLive;


    @SuppressLint("ValidFragment")
    public PlayerFragment(String id, String roomId, String tracksId, String custId, String live) {
        this.Id = id;
        this.tempRoomId = roomId;
        this.tempTrackId = tracksId;
        this.customerId = custId;
        this.isLive = live;
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {

        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }

        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }

        return false;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userAgent = Util.getUserAgent(getContext(), getString(R.string.app_name));
        isAutoPlay = true;

        clearResumePosition();

        mediaDataSourceFactory = buildDataSourceFactory(true);
        rtmpDataSourceFactory = new RtmpDataSource.RtmpDataSourceFactory();

        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }

        presenter = new PlayerPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_player, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    public void showRoomVideo(String roomId) {

    }

    @Override
    public void showVideoNotExist() {
        Toast.makeText(getActivity(), "Video Uploading",Toast.LENGTH_LONG).show();
        Log.e(TAG, " Video Uploading");
    }

    @Override
    public void showError(String error) {

        Log.i(TAG, "showErrorAdsList: " + error);
        Snackbar.make(ivUserProfile, error, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showDataKaraoke(DataKaraoke dataKaraoke) {

    }

    @Override
    public void successReport(String dataTimer) {

    }

    @Override
    public void errorReport(String error) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

        // Do Nothing
        Log.e(TAG, "onTimelineChanged: ");
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        Log.d(TAG, "onTracksChanged: ");

        MappingTrackSelector.MappedTrackInfo mappedTrackInfo =
                trackSelector.getCurrentMappedTrackInfo();

        if (mappedTrackInfo != null) {

            if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO)
                    == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                showError(getString(R.string.main_error_unsupported_video));
            }

            if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO)
                    == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                showError(getString(R.string.main_error_unsupported_audio));
            }
        }
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

        // Do Nothing
        Log.d(TAG, "onLoadingChanged: ");
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        Log.d(TAG, "onPlayerStateChanged: playWhenReady => "
                + playWhenReady + "| playbackState => " + playbackState);
    }

    @Override
    public void onPlayerError(ExoPlaybackException e) {

        Log.e(TAG, "onPlayerError: " + e.getMessage(), e);

        btPlay.setVisibility(View.VISIBLE);
        String errorMessage = null;

        if (e.type == ExoPlaybackException.TYPE_RENDERER) {

            Exception cause = e.getRendererException();

            if (cause instanceof MediaCodecRenderer.DecoderInitializationException) {
                // Special case for decoder initialization failures.
                MediaCodecRenderer.DecoderInitializationException decoderInitializationException =
                        (MediaCodecRenderer.DecoderInitializationException) cause;
                if (decoderInitializationException.decoderName == null) {
                    if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        errorMessage = getString(R.string.main_error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        errorMessage = getString(R.string.main_error_no_secure_decoder,
                                decoderInitializationException.mimeType);
                    } else {
                        errorMessage = getString(R.string.main_error_no_decoder,
                                decoderInitializationException.mimeType);
                    }
                } else {
                    errorMessage = getString(R.string.main_error_instantiating_decoder,
                            decoderInitializationException.decoderName);
                }
            }
        }

        if (errorMessage != null) {
            showError(errorMessage);
        }

        isRetrySource = true;
        if (isBehindLiveWindow(e)) {
            clearResumePosition();
            playVideo();
        } else {
            updateResumePosition();
        }
    }

    @Override
    public void onPositionDiscontinuity() {

        if (isRetrySource) {
            updateResumePosition();
        }

        Log.d(TAG, "onPositionDiscontinuity: ");
    }

    @Override
    public void onVisibilityChange(int visibility) {

        Log.d(TAG, "onVisibilityChange: ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void initView() {

        if (isLive.equals("1")) {
            indicatorLive.smoothToShow();
//            tvVideoStatus.setVisibility(View.VISIBLE);
        } else {
            indicatorLive.smoothToHide();
//            tvVideoStatus.setVisibility(View.GONE);
        }

        if (customerId.equals(sessionManager.getDataMember().getId())) {
            imgReport.setVisibility(View.GONE);
        }

        roomStatus = "1";

        handler = new Handler();

        pvExoPlayer.setControllerVisibilityListener(this);
        pvExoPlayer.requestFocus();

        Map<String, String> param = new HashMap<String, String>();
        try {
            param.put("id", Id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        presenter.updateViewers(param);
    }

    private void initEvent() {

        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (TextUtils.isEmpty(etRoomId.getText())) {
//                    showErrorAdsList("Not Allow Null Room Id");
//                    return;
//                }

//                roomId = etRoomId.getText().toString();
                roomId = tempRoomId;
                playVideo();
            }
        });

        imgLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> param = new HashMap<String, String>();
                try {
                    param.put("id", Id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                presenter.update(param, imgLikes);
            }
        });

        imgReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ReportActivity.class);
                intent.putExtra("room_code", tempRoomId);
                intent.putExtra("trackId", tempTrackId);
                intent.putExtra("customerId", customerId);
                startActivity(intent);
            }
        });
    }

    @Override
    public void initPlayer(String rtmpUrl) {

        Log.d(TAG, "initPlayer: " + rtmpUrl);

        Intent intent = getActivity().getIntent();
        boolean isNeedNewPlayer = exoPlayer == null;

        if (isNeedNewPlayer) {

            boolean preferExtensionDecoders = intent.getBooleanExtra(PREFER_EXTENSION_DECODERS, false);
            @SimpleExoPlayer.ExtensionRendererMode int extensionRendererMode =
                    useExtensionRenders()
                            ? (preferExtensionDecoders ? SimpleExoPlayer.EXTENSION_RENDERER_MODE_PREFER
                            : SimpleExoPlayer.EXTENSION_RENDERER_MODE_ON)
                            : SimpleExoPlayer.EXTENSION_RENDERER_MODE_OFF;
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(BANDWIDTH_METER);
            trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            exoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector, new DefaultLoadControl(),
                    null, extensionRendererMode);
            exoPlayer.addListener(this);

            pvExoPlayer.setPlayer(exoPlayer);
            exoPlayer.setPlayWhenReady(isAutoPlay);
        }

        if (isNeedNewPlayer || isRetrySource) {

            Uri[] uris;
            String[] extensions;

            uris = new Uri[1];
            uris[0] = Uri.parse(rtmpUrl);
            extensions = new String[1];
            extensions[0] = "";

            if (Util.maybeRequestReadExternalStoragePermission(getActivity(), uris)) {
                // The player will be reinitialized if the permission is granted.
                return;
            }

            MediaSource[] mediaSources = new MediaSource[uris.length];
            for (int i = 0; i < uris.length; i++) {
                mediaSources[i] = buildMediaSource(uris[i], extensions[i]);
            }

            MediaSource mediaSource = mediaSources.length == 1 ? mediaSources[0]
                    : new ConcatenatingMediaSource(mediaSources);
            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
            if (haveResumePosition) {
                exoPlayer.seekTo(resumeWindow, resumePosition);
            }

            exoPlayer.prepare(mediaSource, !haveResumePosition, false);
            isRetrySource = false;
        }
    }

    @Override
    public void showErrorKaraoke(String rtmpUrl) {

    }

    private void playVideo() {

        Log.d(TAG, "playVideo: ");

        String rtmpUrl;

        if (isLive.equals("1")) {
            indicatorLive.smoothToShow();
//            tvVideoStatus.setVisibility(View.VISIBLE);
            rtmpUrl = Constants.URL_RTMP + roomId + " live=" + isLive;

        } else {
            indicatorLive.smoothToHide();
//            tvVideoStatus.setVisibility(View.GONE);
            rtmpUrl = Constants.URL_RTMP_ENDED + roomId + ".mp4";

        }

        presenter.checkVideoExist(roomId, rtmpUrl);
        Log.e(TAG, "rtmpUrl: " + rtmpUrl);
//        initPlayer(rtmpUrl);
        btPlay.setVisibility(View.GONE);
    }

    private void releasePlayer() {

        Log.d(TAG, "releasePlayer: ");

        if (exoPlayer != null) {
            isAutoPlay = exoPlayer.getPlayWhenReady();
            updateResumePosition();
            exoPlayer.release();
            exoPlayer = null;
            trackSelector = null;
        }
    }

    private void updateResumePosition() {

        Log.d(TAG, "updateResumePosition: ");

        resumeWindow = exoPlayer.getCurrentWindowIndex();
        resumePosition = exoPlayer.isCurrentWindowSeekable() ? Math.max(0, exoPlayer.getCurrentPosition()) :
                C.TIME_UNSET;
    }

    private void clearResumePosition() {

        Log.d(TAG, "clearResumePosition: ");

        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }

    @NonNull
    private MediaSource buildMediaSource(Uri uri, String overrideExtension) {

        int type = TextUtils.isEmpty(overrideExtension) ? Util.inferContentType(uri)
                : Util.inferContentType("." + overrideExtension);

        switch (type) {

            case C.TYPE_SS:
                return new SsMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultSsChunkSource.Factory(mediaDataSourceFactory), handler, null);

            case C.TYPE_DASH:
                return new DashMediaSource(uri, buildDataSourceFactory(false),
                        new DefaultDashChunkSource.Factory(mediaDataSourceFactory), handler, null);

            case C.TYPE_HLS:
                return new HlsMediaSource(uri, mediaDataSourceFactory, handler, null);

            case C.TYPE_OTHER:
                if (uri.getScheme().equals("rtmp")) {
                    return new ExtractorMediaSource(uri, rtmpDataSourceFactory, new DefaultExtractorsFactoryForFLV(),
                            handler, null);
                } else {
                    return new ExtractorMediaSource(uri, mediaDataSourceFactory, new DefaultExtractorsFactory(),
                            handler, null);
                }

            default: {
                throw new IllegalStateException("Unsupported type: " + type);

            }
        }
    }

    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        return buildDataSourceFactory(useBandwidthMeter ? BANDWIDTH_METER : null);
    }

    private DataSource.Factory buildDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultDataSourceFactory(getActivity(), bandwidthMeter,
                buildHttpDataSourceFactory(bandwidthMeter));
    }

    private HttpDataSource.Factory buildHttpDataSourceFactory(DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter);
    }

    private boolean useExtensionRenders() {
        return BuildConfig.FLAVOR.equals("withExtensions");
    }
}
