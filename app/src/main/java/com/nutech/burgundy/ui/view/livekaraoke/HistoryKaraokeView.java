package com.nutech.burgundy.ui.view.livekaraoke;

import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public interface HistoryKaraokeView extends BaseView {

    void showDataHistory(List<DataRoom> dataRoomList);

    void showError(String error);

    void showRemoveError(String error);

    void showRemoveSuccess(String success);



}
