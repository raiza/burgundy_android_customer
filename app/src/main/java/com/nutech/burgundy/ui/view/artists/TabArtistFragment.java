package com.nutech.burgundy.ui.view.artists;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.ui.adapter.GeneralPagerAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.SegmentedButton;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * @author Arif Setiawan on 14/05/19.
 * @company NuTech
 */

public class TabArtistFragment extends BaseFragment {

    private String TAG = "TabArtistFragment";

    private static final int INDEX_TRACK = 0;
    private static final int INDEX_ALBUM = 1;

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.segmented_artist)
    SegmentedButton segmentedButton;
    @BindView(R.id.frame_artist)
    FrameLayout frameLayout;

    @BindView(R.id.iv_backdrop)
    ImageView ivBackdrop;
    @BindView(R.id.iv_photo_artists)
    CircleImageView ivPhotoArtists;
    @BindView(R.id.tv_followers)
    TextView tvFollowers;

    @BindView(R.id.pager_artist)
    ViewPager pagerArtist;

    private Unbinder unbinder;
//    private DataArtists dataArtists;
    private String idArtists = "";
    private String ImageArtists = "";
    private String genreId = "";



    public TabArtistFragment(String id, String image, String genre_id) {
        this.idArtists = id;
        this.ImageArtists = image;
        this.genreId = genre_id;
    }

/*    public TabArtistFragment newInstance(DataArtists artists) {
        TabArtistFragment fragment = new TabArtistFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.ParamKey.KEY_ARTISTS, artists);
        fragment.setArguments(bundle);
        return fragment;
    }*/

    @Override
    public void onStart() {
        super.onStart();
        initViews();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        dataArtists = getArguments().getParcelable(Constants.ParamKey.KEY_ARTISTS);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_artist, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initViews() {

        setupViewPager(pagerArtist);
        tabs.setupWithViewPager(pagerArtist);


        String ImageUrl = Constants.URL_IMAGE_ARTISTS + ImageArtists;

        Log.e(TAG+" ++ image url", ImageUrl);
        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.default_user)
                .fit()
                .into(ivPhotoArtists);

        loadImagesFromURL(context, ivPhotoArtists, ImageUrl);

        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.placeholder)
                .transform(new BlurTransformation(getActivity()))
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(ivBackdrop);

    }

    private void setupViewPager(ViewPager viewPager) {

        segmentedButton.clearButtons();
        segmentedButton.addButtons(
                getString(R.string.label_tracks),
                getString(R.string.label_album));

        // First button is selected
        segmentedButton.setPushedButtonIndex(0);
        replaceFragment(new DetailArtisTracksFragment().newInstance(idArtists),
                Constants.ParamKey.ARTIST_TRACK);

        // Some example click handlers. Note the click won't get executed
        // if the segmented button is already selected (dark blue)
        segmentedButton.setOnClickListener((SegmentedButton.OnClickListenerSegmentedButton) index -> {
            if (index == 0) {
                replaceFragment(new DetailArtisTracksFragment().newInstance(idArtists),
                        Constants.ParamKey.ARTIST_TRACK);
            } else if (index == 1){
                replaceFragment(new DetailArtisAlbumFragment(idArtists),
                        Constants.ParamKey.ARTIST_ALBUM);
            }
        });


        GeneralPagerAdapter adapter = new GeneralPagerAdapter(getFragmentManager());

        adapter.addFragment(new DetailArtisTracksFragment().newInstance(idArtists), "Tracks");
        adapter.addFragment(new DetailArtisAlbumFragment(idArtists), "Album");

        viewPager.setAdapter(adapter);
    }

    private void replaceFragment(Fragment fragment,String nameFragment) {

        if (frameLayout != null) {
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                    .replace(frameLayout.getId(), fragment, nameFragment).commit();
        }
    }
}
