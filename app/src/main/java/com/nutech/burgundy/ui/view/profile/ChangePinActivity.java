package com.nutech.burgundy.ui.view.profile;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.presenter.ChangePinPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePinActivity extends BaseAppCompatActivity implements ChangePinView {

    @BindView(R.id.et_enter_pin)
    EditText etEnterPin;
    @BindView(R.id.et_re_enter_pin)
    EditText etReEnterPinxx;
    @BindView(R.id.et_old_pin)
    EditText etOldEnterPin;

    @OnClick(R.id.button_submit)
    public void nextToSubmit(){

        //-- Digunakan Apabila pin diinvisible
        if (TextUtils.isEmpty(etEnterPin.getText().toString())){
            etEnterPin.setError(getString(R.string.hint_enter_pin));
        } else if (TextUtils.isEmpty(etOldEnterPin.getText().toString())){
            etOldEnterPin.setError(getString(R.string.hint_enter_old_pin));
        }else {
            if(!etOldEnterPin.getText().toString().equals(etEnterPin.getText().toString())){
                Map<String, String> fields = new HashMap<>();
                fields.put("mobile_phone", sessionManager.getDataMember().getMobile_phone());
                fields.put("activation_code", etOldEnterPin.getText().toString());
                fields.put("new_activation_code", etEnterPin.getText().toString());
                presenter.changePin(fields);
            } else {
                Toast.makeText(this, "PIN tidak boleh sama", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private ChangePinPresenter presenter;
    private SessionManager sessionManager;

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new ChangePinPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pin);
        ButterKnife.bind(this);

        initEvent();
    }

    private void initEvent(){

        etEnterPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() != 6){
                    etEnterPin.setError("PIN harus 6 digit");
                }
            }
        });

        etOldEnterPin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() != 6){
                    etOldEnterPin.setError("PIN harus 6 digit");
                }
            }
        });
    }

    private void alertShowing(String title, String message, final boolean status){

        AlertDialog dialogInfo = new AlertDialog.Builder(this).create();
        dialogInfo.setTitle(title);
        dialogInfo.setMessage(message);
        dialogInfo.setCancelable(false);
        dialogInfo.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (status){
                            finish();
                        }
                        dialog.dismiss();
                    }
                });
        dialogInfo.show();
    }

    @Override
    public void changeSucess(DataMember dataCustomer) {
        alertShowing(getString(R.string.label_info), getString(R.string.msg_sucess_change_pin), true);
    }

    @Override
    public void changeError(String error) {
        System.out.println("******** eroor code "+error);
        if (error.equals("200")){
            alertShowing(getString(R.string.label_info), getString(R.string.msg_sucess_change_pin), true);
        } else {
            alertShowing(getString(R.string.label_info), getString(R.string.msg_failed_change_pin), false);
        }
    }

    public void btnBackClicked(View view) {
        finish();
    }
}
