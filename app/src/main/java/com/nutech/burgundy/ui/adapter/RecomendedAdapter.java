package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataTracks;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 23/01/18.
 * Xeranta Mobile Solutions
 */

public class RecomendedAdapter extends RecyclerView.Adapter<RecomendedAdapter.TopChartHolder> {

    private Context context;
    private List<DataTracks> tracks;

    public RecomendedAdapter(Context context, List<DataTracks> tracks) {
        this.context = context;
        this.tracks = tracks;
    }

    class TopChartHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_number)
        TextView tvNumber;
        @BindView(R.id.tv_artists_name)
        TextView tvArtistsName;
        @BindView(R.id.tv_artists_song)
        TextView tvArtistsSong;

        TopChartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public TopChartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recomended_tracks, parent, false);
        return new TopChartHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopChartHolder holder, int position) {

        Integer index = position + 1;
        holder.tvNumber.setText(String.valueOf(index));
        holder.tvArtistsName.setText(tracks.get(position).getArtistname());
        holder.tvArtistsSong.setText(tracks.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }
}
