package com.nutech.burgundy.ui.view;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.lib.musicplayerview.MusicPlayerView;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public interface FragmentNavigation {

    /**
     * Call when Fragment move into another Fragment
     *
     * @param fragment Object with extend Fragment
     * @param title title of page
     * @param withBackArrow boolean of arrow
     */
    void pushFragment(Fragment fragment, String title, Boolean withBackArrow);

    void playingMusic(int position, List<DataTracks> dataTracksList);

    void updatePositionTracks(List<DataTracks> dataTracksList);

    void setWaitingNext(int waitNext);
    void setWaitingPrevious(int waitPrev);

    void updatePlayer(MusicPlayerView mpvMusic, ImageButton btnPlay, ImageButton btnNext,
                      ImageButton btnPrev, AVLoadingIndicatorView loading, ImageView llBackdrop,
                      SeekBar seekBarDuration, TextView currentProgress, TextView maxprogress,
                      TextView txtLiricksPassed, TextView txtLiricksFocussed, TextView txtLiricksNext,
                      LinearLayout llLiricks, TextView tvTrackName, Button btKaraoke, String includeKaraoke,
                      String karaokeId, String urlTrack,
                      ImageView ivShufflePlaying, ImageButton btnListTrack);

    void updateControll(boolean status);

    void updateProfile();

    void stopMusic();

    List<DataTracks> getDataTracks();


    /**
     * Call when Fragment back to Fragment/Activity previously
     */
    void backFragment();

    void updateTitle(String title);
}
