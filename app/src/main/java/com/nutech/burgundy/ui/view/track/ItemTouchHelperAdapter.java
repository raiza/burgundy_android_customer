package com.nutech.burgundy.ui.view.track;

public interface ItemTouchHelperAdapter {
    boolean onItemMove(int fromPosition, int toPosition);

    void onItemSelectedChange();

}
