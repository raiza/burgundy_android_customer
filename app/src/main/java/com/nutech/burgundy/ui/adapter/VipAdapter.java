package com.nutech.burgundy.ui.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.utils.Helper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Dafian on 10/16/17
 */

public class VipAdapter extends RecyclerView.Adapter<VipAdapter.VipHolder> {

    private List<DataSubscribePrice> subscribePriceList;

    public VipAdapter(List<DataSubscribePrice> subscribePriceList) {
        this.subscribePriceList = subscribePriceList;
    }

    class VipHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_subscribe_name)
        TextView tvSubscribeName;
        @BindView(R.id.tv_subscribe_desc)
        TextView tvSubscribeDesc;
        @BindView(R.id.tv_subscribe_price)
        TextView tvSubscribePrice;

        VipHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public VipHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_subscribe_price, parent, false);
        return new VipHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VipHolder holder, int position) {

        String price = "IDR "+ Helper.formatedPrice(subscribePriceList.get(position).getPrice());
        holder.tvSubscribeName.setText(subscribePriceList.get(position).getName());
        holder.tvSubscribeDesc.setText(subscribePriceList.get(position).getDescription());
        holder.tvSubscribePrice.setText(price);
    }

    @Override
    public int getItemCount() {
        return subscribePriceList.size();
    }
}
