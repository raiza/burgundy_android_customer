package com.nutech.burgundy.ui.view.profile;

import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * @author Dafian on 10/17/17
 */

public interface MyProfileView extends BaseView {

    void showCustomer(DataMember member);

    void showSubscribeStatus(DataUserSubscribe userSubscribe);

    void showSuccess(String success);

    void showErrorCustomer(String error);

    void showErrorSubscribeStatus(String error);

    void showError(String error);
}
