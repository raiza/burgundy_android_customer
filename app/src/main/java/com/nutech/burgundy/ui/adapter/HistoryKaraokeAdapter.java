package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.presenter.livekaraoke.HistoryKaraokePresenter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.livekaraoke.PlayingKaraokeActivity;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public class HistoryKaraokeAdapter extends RecyclerView.Adapter<HistoryKaraokeAdapter.HistoryHolder> {

    private List<DataRoom> dataRoomList;
    private FragmentNavigation nav;
    private Context context;
    private SessionManager session;
    private HistoryKaraokePresenter historyKaraokePresenter;
    private RecyclerView rvHistory;

    public HistoryKaraokeAdapter(List<DataRoom> historyRoom, FragmentNavigation navigation, FragmentActivity activity,
                                 SessionManager sessionManager, HistoryKaraokePresenter presenter, RecyclerView rvHistory) {
        this.dataRoomList = historyRoom;
        this.nav = navigation;
        this.context = activity;
        this.session = sessionManager;
        this.historyKaraokePresenter = presenter;
        this.rvHistory = rvHistory;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history_karaoke_customer, parent, false);
        return new HistoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HistoryHolder holder, final int position) {

        String date = Helper.formatedMonth(dataRoomList.get(position).getCreatedate());
        String vocalName = dataRoomList.get(position).getCustomername();

        holder.tvVocalName.setText(context.getString(R.string.vocal_by) + " " + vocalName);
        holder.tvRoomCode.setText(context.getString(R.string.singing_on) + " " + date);
        holder.tvTitleTrack.setText(dataRoomList.get(position).getTrackname());
        holder.tvLikes.setText(dataRoomList.get(position).getLikes());
        holder.tvViewers.setText(dataRoomList.get(position).getView());

        String ImageUrl = Constants.URL_IMAGE_MEMBER+dataRoomList.get(position).getImage();
        Log.e("++Image",ImageUrl);

        Picasso.with(context.getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.default_user)
                .fit()
                .into(holder.imgUser);

        holder.btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Constants.DataRoomTemporary.dataRoom = dataRoomList.get(position);
                Intent intent = new Intent(context.getApplicationContext(), PlayingKaraokeActivity.class);
                context.startActivity(intent);
//                nav.pushFragment(
//                        new PlayerFragment(
//                                dataRoomList.get(position).getId(),
//                                dataRoomList.get(position).getRoomcode(),
//                                dataRoomList.get(position).getTrackid(),
//                                dataRoomList.get(position).getCustomerid(),
//                                dataRoomList.get(position).getLive()),
//                        dataRoomList.get(position).getTrackname(), true);
//                Log.e("btDelete clicked","on btPlay");

            }
        });

        holder.btProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadDialog();
            }
        });

        if (dataRoomList.get(position).getCustomerid().equals(session.getDataMember().getId())) {

            holder.btDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    confirmDelete(
                            dataRoomList.get(position).getCustomerid(),
                            dataRoomList.get(position).getRoomcode(),
                            dataRoomList, position);
                }
            });

//            Log.e("cust id " + position,dataRoomList.get(position).getCustomerid());
//            Log.e("member id " + position,session.getDataMember().getId());
        } else {
            holder.btDelete.setVisibility(View.GONE);
//            Log.e("cust id " + position,dataRoomList.get(position).getCustomerid());
//            Log.e("member id " + position,session.getDataMember().getId());
        }

        holder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataRoomList.get(position).getCustomerid().equals(session.getDataMember().getId())){
                    showOptions(holder.imgMore, position, v);
                } else {
                    showOptions2(holder.imgMore, position, v);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataRoomList.size();
    }

    private void confirmDelete(final String customer_id, final String room_code, final List<DataRoom> dataRoomList, final int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Delete This Karaoke?");
        alertDialog.setMessage("The karaoke you want to delete can not be reappeared");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Map<String, String> params = new HashMap<String, String>();
                        try {
                            params.put("customer_id", customer_id);
                            params.put("room_code", room_code);
                        } catch (Exception e) {

                        }
                        System.out.println("** Param ** " + params);
                        historyKaraokePresenter.removeHistoryByCustomer(params);
                        remove(position, dataRoomList);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void uploadDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(context.getString(R.string.label_info));
        alertDialog.setMessage("Video Sedang diupload");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    class HistoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_room_code)
        TextView tvRoomCode;
        @BindView(R.id.tv_title_track)
        TextView tvTitleTrack;
        @BindView(R.id.tv_vocal_name)
        TextView tvVocalName;
        @BindView(R.id.bt_play)
        Button btPlay;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.tv_viewers)
        TextView tvViewers;
        @BindView(R.id.bt_delete)
        Button btDelete;
        @BindView(R.id.bt_progress)
        Button btProgress;
        @BindView(R.id.img_more)
        ImageView imgMore;
        @BindView(R.id.img_user)
        ImageView imgUser;


        HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void remove(int position, List<DataRoom> list) {
        ((SimpleItemAnimator) rvHistory.getItemAnimator()).setSupportsChangeAnimations(true);

        System.out.println("position: " + position);
        list.remove(position);
        notifyItemRemoved(position);

        ((SimpleItemAnimator) rvHistory.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private void showOptions(ImageView btnOptions, final int position, View view) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.karaoke_options);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(android.view.MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.play:
                        Constants.DataRoomTemporary.dataRoom = dataRoomList.get(position);
                        Intent intent = new Intent(context.getApplicationContext(), PlayingKaraokeActivity.class);
                        context.startActivity(intent);
                        break;
                    case R.id.delete:
                        confirmDelete(
                                dataRoomList.get(position).getCustomerid(),
                                dataRoomList.get(position).getRoomcode(),
                                dataRoomList, position);
                        break;
                }
                return false;
            }
        });
        makePopForceShowIcon(popup);
    }

    private void showOptions2(ImageView btnOptions, final int position, View view) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.karaoke_options_2);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(android.view.MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.play:
                        Constants.DataRoomTemporary.dataRoom = dataRoomList.get(position);
                        Intent intent = new Intent(context.getApplicationContext(), PlayingKaraokeActivity.class);
                        context.startActivity(intent);
                        break;
                }
                return false;
            }
        });
        makePopForceShowIcon(popup);
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }
}
