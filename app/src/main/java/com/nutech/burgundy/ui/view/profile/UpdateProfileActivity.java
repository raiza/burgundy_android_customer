package com.nutech.burgundy.ui.view.profile;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.presenter.UpdateProfilePresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 12/20/17.
 * Xeranta Mobile Solutions
 */

public class UpdateProfileActivity extends BaseAppCompatActivity implements UpdateProfileView{

    @BindView(R.id.et_nickname)
    EditText etNickName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_birtdate)
    TextView etBirtDate;

    @OnClick(R.id.bt_save)
    public void save(){

        if (TextUtils.isEmpty(etNickName.getText().toString())) {
            etNickName.setError("Fields Required");
        }else if (TextUtils.isEmpty(etEmail.getText().toString())){
            etEmail.setError("Fields Required");
        } else {


            Map<String, String> params = new HashMap<String, String>();
            try {
                params.put("id", sessionManager.getDataMember().getId());
                params.put("name", etNickName.getText().toString());
                params.put("email", etEmail.getText().toString());
                params.put("birth_date", etBirtDate.getText().toString());
            } catch (Exception e) {
            }

            presenter.updateDataProfile(params);
        }
    }

    private Unbinder unbinder;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private SimpleDateFormat simpleDateFormat;
    private UpdateProfilePresenter presenter;
    private SessionManager sessionManager;

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new UpdateProfilePresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        ButterKnife.bind(this);
        sessionManager = getSessionManager();

        initView();
        initEvent();

    }

    private void initView() {

        etNickName.setText(sessionManager.getDataMember().getName());
        etEmail.setText(sessionManager.getDataMember().getEmail());
        etBirtDate.setText(sessionManager.getDataMember().getBirth_date());

        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etBirtDate.setText(simpleDateFormat.format(newDate.getTime()));
            }

        },calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

    }

    private void initEvent(){

        etBirtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showDataMember(DataMember dataMember) {
        onBackPressed();
    }

    @Override
    public void errorDataMember(String error) {

    }

    @Override
    public void succesUpdate(String success) {

    }

    @Override
    public void error(String error) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
