package com.nutech.burgundy.ui.view.profile;

import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 11/28/17.
 * Xeranta Mobile Solutions
 */

public interface UpdateProfileView extends BaseView {

    void showDataMember(DataMember dataMember);

    void errorDataMember(String error);

    void succesUpdate(String success);

    void error(String error);

}
