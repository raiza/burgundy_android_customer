package com.nutech.burgundy.ui.view.history;

import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 11/26/17.
 * Xeranta Mobile Solutions
 */

public interface HistoryView extends BaseView {

    void showHistory(List<DataHistoryExchange> dataHistoryExchange);

    void showError(String error);
}
