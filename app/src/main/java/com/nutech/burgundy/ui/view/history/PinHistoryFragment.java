package com.nutech.burgundy.ui.view.history;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.presenter.PinHistoryPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 10/17/17
 */

public class PinHistoryFragment extends BaseFragment implements PinHistoryView {

    private static final String KEY_HISTORY_ID = "key_history_id";

    @BindView(R.id.txt_pin_entry)
    PinEntryEditText pvPin;
    @BindView(R.id.bt_submit)
    Button btSubmit;
    @BindView(R.id.tv_reward_name)
    TextView tvRewardName;
    @BindView(R.id.tv_exchange_date)
    TextView tvExchangeDate;
    @BindView(R.id.tv_expired_date)
    TextView tvExpiredDate;
    @BindView(R.id.tv_status)
    TextView tvStatus;

    private Unbinder unbinder;
    private PinHistoryPresenter presenter;
    private SessionManager sessionManager;

    private String id;
    private String activCode;
    private String pin;

    public PinHistoryFragment newInstance(String id) {
        PinHistoryFragment fragment = new PinHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_HISTORY_ID, id);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getArguments().getString(KEY_HISTORY_ID);
        presenter = new PinHistoryPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pin_history, container, false);
        unbinder = ButterKnife.bind(this, view);

        presenter.getDetailHistpry(id);


        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pin.length() < 4){
                    Toast.makeText(getActivity(), "Masukan kode verifikasi", Toast.LENGTH_SHORT).show();
                }else {
                    updateStatusPointExchange();
                }

            }
        });

        pvPin.setOnPinEnteredListener(str -> pin = str.toString());

        return view;
    }

    @Override
    public void showDetailHistory(DataHistoryExchange dataHistory) {

        activCode = dataHistory.getActivationcode();

        tvRewardName.setText(dataHistory.getRewardname());
        tvStatus.setText(dataHistory.getExchangestatus());
        tvExchangeDate.setText("Expired Date:");
        tvExpiredDate.setText(Helper.formatedDate(dataHistory.getExpireddate()));
        sendSMS();

    }

    @Override
    public void showSuccess(String success) {

        Toast.makeText(getActivity(), success, Toast.LENGTH_LONG).show();

        navigation.backFragment();
    }

    @Override
    public void showSuccessUpdate(String success) {
        navigation.backFragment();
    }

    @Override
    public void showSuccessUsed(String success) {
        alertSuccess();
    }

    @Override
    public void showError(String error) {

        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorUpdate(String error) {
        navigation.backFragment();
    }

    @Override
    public void showErrorUsed(String error) {

        Log.e("error code", error);

        if (error.equals("200")){
            alertSuccess();
        } else if (error.equals("401")){
            Toast.makeText(getActivity(), "Kode verifikasi tidak sesuai", Toast.LENGTH_LONG).show();
        } else {
            alertSuccess();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void updateStatusPointExchange(){

        System.out.println("Status = "+tvStatus.getText().toString());

        Map<String, String> fields = new HashMap<>();

        if (tvStatus.getText().toString().equals(Constants.Status.INACTIVE)){
            fields.put("id", id);
            fields.put("exchange_status", Constants.Status.ACTIVE);
            fields.put("activation_code", Helper.generateCode());
            presenter.updateStatusPointExchange(fields);
        } else if (tvStatus.getText().toString().equals(Constants.Status.ACTIVE)){
            fields.put("id", id);
            fields.put("exchange_status", Constants.Status.USED);
            fields.put("activation_code", pin);
            presenter.usePointExchange(fields);
        }
    }

    private void alertSuccess() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Sukses");
        alertDialog.setMessage("Selamat menikmati hadiah Anda" + "\n" + "Terima Kasih.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Tutup",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        navigation.backFragment();
                    }
                });
        alertDialog.show();
    }

    private void sendSMS(){
        String numberPhone = "0"+sessionManager.getDataMember().getMobile_phone();
        String textContent = getString(R.string.msg_sms_login)+
                " "+ activCode;
        presenter.sendSms(numberPhone, textContent);
    }

}
