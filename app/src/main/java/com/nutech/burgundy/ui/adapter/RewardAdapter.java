package com.nutech.burgundy.ui.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataReward;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Dafian on 11/13/17
 */

public class RewardAdapter extends RecyclerView.Adapter<RewardAdapter.RewardHolder> {

    private List<DataReward> rewardList;

    public RewardAdapter(List<DataReward> rewardList) {
        this.rewardList = rewardList;
    }

    class RewardHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_point)
        TextView tvPoint;

        RewardHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public RewardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_reward, parent, false);
        return new RewardHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RewardHolder holder, int position) {

        String required = "Required " + rewardList.get(position).getPointRequired() + " Points";

        holder.tvName.setText(rewardList.get(position).getName());
        holder.tvPoint.setText(required);
    }

    @Override
    public int getItemCount() {
        return rewardList.size();
    }
}
