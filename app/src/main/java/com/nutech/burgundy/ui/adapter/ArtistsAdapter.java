package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.ui.base.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 31/05/19.
 * Nusantara Teknologi (NuTech)
 */

public class ArtistsAdapter extends BaseAdapter<ArtistsAdapter.artistsVH> implements SectionIndexer {

    private DataArtistList artistsList;
    private ArrayList<Integer> mSectionPositions;
    private Context context;

    public ArtistsAdapter(
            Context context, DataArtistList list) {
        this.context = context;
        this.artistsList = list;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = artistsList.getArtists().size(); i < size; i++) {
            String section = String.valueOf(artistsList.getArtists().get(i).getName().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }

    class artistsVH extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_artists_name)
        TextView tvartistsName;
        @BindView(R.id.tv_sum_tracks)
        TextView tvSumTracks;
        @BindView(R.id.iv_artists)
        ImageView ivArtists;
        View childView;

        artistsVH(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public artistsVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_artists, parent, false);
        return new artistsVH(itemView);
    }

    @Override
    public void onBindViewHolder(artistsVH holder, int position) {

        final artistsVH artistsVH = (artistsVH) holder;

        artistsVH.tvartistsName.setText(artistsList.getArtists().get(position).getName());

        if (!TextUtils.isEmpty(artistsList.getArtists().get(position).getSumTracks())){
            artistsVH.tvSumTracks.setText(artistsList.getArtists().get(position).getSumTracks());
        }

        String ImageUrl = Constants.URL_IMAGE_ARTISTS+artistsList.getArtists().get(position).getImage();

        loadImagesFromURL(context, artistsVH.ivArtists, ImageUrl);
    }

    @Override
    public int getItemCount() {
        return artistsList.getArtists().size();
    }

}
