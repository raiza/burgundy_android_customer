package com.nutech.burgundy.ui.view.recomended;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 1/4/18.
 * Xeranta Mobile Solutions
 */

public interface RecomendedView extends BaseView {

    void showRecomendedData(List<DataTracks> listDataTracks);

    void showError(String error);
}
