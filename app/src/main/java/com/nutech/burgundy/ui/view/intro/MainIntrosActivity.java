package com.nutech.burgundy.ui.view.intro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.ui.adapter.IntroAdapter;
import com.nutech.burgundy.ui.base.BaseFragmentActivity;
import com.nutech.burgundy.ui.view.register.PhoneAuthActivity;
import com.nutech.burgundy.ui.view.register.StoreLoginActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.List;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class MainIntrosActivity extends BaseFragmentActivity {

    @BindView(R.id.viewpager_intro)
    ViewPager viewpager_intro;
    @BindView(R.id.indicator_intro)
    CircleIndicator indicator_intro;
    @BindView(R.id.button_startnow)
    Button button_startNow;

    private IntroAdapter introAdapter;
    private SessionManager sessionManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_intros);
        ButterKnife.bind(this);
        Helper.hideSoftKeyBoard(this);
        initEvent();

        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(this));
        }

        List<Fragment> fragments = new Vector<Fragment>();
        fragments.add(Fragment.instantiate(this, IntroWelcomeFirst.class.getName()));
        fragments.add(Fragment.instantiate(this, IntroWelcomeSecond.class.getName()));
        fragments.add(Fragment.instantiate(this, IntroWelcomeThird.class.getName()));
        introAdapter = new IntroAdapter(getSupportFragmentManager(), fragments);
        viewpager_intro.setAdapter(introAdapter);

        indicator_intro.setViewPager(viewpager_intro);
        indicator_intro.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    indicator_intro.setVisibility(View.VISIBLE);

                } else if (position == 1) {
                    indicator_intro.setVisibility(View.VISIBLE);

                } else if (position == 2) {
                    indicator_intro.setVisibility(View.VISIBLE);

                } else if (position == 3) {
                    indicator_intro.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initEvent() {

        button_startNow.setOnClickListener(view -> {
            sessionManager.setIsIntro(true);
            Intent intent = new Intent(MainIntrosActivity.this, StoreLoginActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        System.exit(0);
    }

}
