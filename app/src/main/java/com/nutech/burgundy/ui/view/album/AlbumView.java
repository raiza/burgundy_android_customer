package com.nutech.burgundy.ui.view.album;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 10/10/17.
 * Xeranta Mobile Solutions
 */

public interface AlbumView extends BaseView {

    void showDataAlbumsTracks(List<DataTracks> dataTracksList);

    void showError(String error);
}
