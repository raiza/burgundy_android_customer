package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.FavoriteTracksPresenter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.home.FavoriteTracksFragment;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class TracksMemberAdapter extends RecyclerView.Adapter<TracksMemberAdapter.TracksHolder> {

    private FavoriteTracksFragment context;
    private List<DataTracks> list;
    private FavoriteTracksPresenter presenter;
    private SessionManager sessionManager;
    private RecyclerView recyclerView;
    private FragmentNavigation fragmentNavigation;

    public TracksMemberAdapter(FavoriteTracksFragment activity, List<DataTracks> dataTracks,
                               FavoriteTracksPresenter presenter, SessionManager sessionManager,
                               RecyclerView rvMyTracks, FragmentNavigation navigation) {
        this.context = activity;
        this.list = dataTracks;
        this.presenter = presenter;
        this.sessionManager = sessionManager;
        this.recyclerView = rvMyTracks;
        this.fragmentNavigation = navigation;

    }

    class TracksHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_tracks)
        TextView tvTitleTracks;
        @BindView(R.id.tv_title_album)
        TextView tvTitleAlbum;
        @BindView(R.id.btn_options)
        ImageView btnOptions;
        @BindView(R.id.rl_options)
        RelativeLayout rlOptions;
        @BindView(R.id.ll_play)
        LinearLayout llPlay;

        View childView;

        TracksHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public TracksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_tracks_member, parent, false);
        return new TracksHolder(view);
    }

    @Override
    public void onBindViewHolder(final TracksHolder holder, final int position) {

        holder.tvTitleTracks.setText(list.get(position).getName());
        holder.tvTitleAlbum.setText(list.get(position).getAlbum_name());

        holder.rlOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions(holder.btnOptions, position);
            }
        });

        holder.llPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentNavigation.playingMusic(position, list);
                context.toDoNowPlaying(
                        list.get(position).getId(),
                        list.get(position).getInclude_karaoke(),
                        list.get(position).getKaraoke_id(),
                        list.get(position).getUrl()+list.get(position).getSource());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void showOptions(ImageView btnOptions, final int position) {

        PopupMenu popup = new PopupMenu(context.getActivity(), btnOptions);
        popup.inflate(R.menu.tracks_member_options);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(android.view.MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.remove:

                        String memberId = sessionManager.getDataMember().getId();
                        Map<String, String> params = new HashMap<String, String>();
                        try {
                            params.put("customer_id", memberId);
                            params.put("track_id", list.get(position).getId());
                        } catch (Exception e) {

                        }
                        System.out.println("** Param ** "+ params);
                        presenter.removeTracksMember(params);
                        remove(position, list);

                        break;
                    case R.id.share:
                        String applicationName = context.getResources().getString(R.string.app_name);
                        String content = context.getResources().getString(R.string.content_share)+" "+
                                list.get(position).getName()+"." +
                                context.getResources().getString(R.string.content_share_only_this_app)+" "+
                                applicationName+" "+context.getResources().getString(R.string.content_share_only_this_app)+"\n"+
                                context.getResources().getString(R.string.content_share_download_app);
                        Helper.shareContent(context.getContext(), content, applicationName, sessionManager.getDataMember().getId(), Constants.ActivityPoint.SHARE_SOSMED);
                        break;
                }
                return false;
            }
        });
        makePopForceShowIcon(popup);
    }

    private void remove(int position, List<DataTracks> list) {
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(true);

        list.remove(position);
        notifyItemRemoved(position);

        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }

}
