package com.nutech.burgundy.ui.view.search;

import com.nutech.burgundy.data.model.DataSearch;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public interface SearchView extends BaseView {

    void showResultSearch(List<DataSearch> dataSearchList);

    void showError(String error);
}
