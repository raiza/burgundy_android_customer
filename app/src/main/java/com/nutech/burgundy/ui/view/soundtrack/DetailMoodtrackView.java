package com.nutech.burgundy.ui.view.soundtrack;

import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 2019-07-17.
 * NuTech (Nusantara Teknologi)
 */
public interface DetailMoodtrackView extends BaseView {

    void showMoodtracks(Moodtrack dataMoodtracks);

    void showErrorMoodtracks(String error);

    void showSuccessUpdatePosition(String success);

    void showErrorUpdatePosition(String error);

}
