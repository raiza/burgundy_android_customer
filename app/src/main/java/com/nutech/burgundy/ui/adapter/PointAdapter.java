package com.nutech.burgundy.ui.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataPoint;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Dafian on 11/13/17
 */

public class PointAdapter extends RecyclerView.Adapter<PointAdapter.PointHolder> {

    private List<DataPoint> pointList;

    public PointAdapter(List<DataPoint> pointList) {
        this.pointList = pointList;
    }

    class PointHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_point)
        TextView tvPoint;

        PointHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public PointHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_point, parent, false);
        return new PointHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PointHolder holder, int position) {

        String points = pointList.get(position).getPoint() + " Points";

        holder.tvName.setText(pointList.get(position).getName());
        holder.tvPoint.setText(points);
    }

    @Override
    public int getItemCount() {
        return pointList.size();
    }
}
