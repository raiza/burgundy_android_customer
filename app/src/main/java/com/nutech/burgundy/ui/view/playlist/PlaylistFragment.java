package com.nutech.burgundy.ui.view.playlist;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.adapter.PlaylistAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Arif Setiawan on 19/05/19.
 * @company NuTech
 */

public class PlaylistFragment extends BaseFragment implements PlaylistView {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycle_playlist)
    RecyclerView rvPlaylist;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private Unbinder unbinder;
    private PlaylistPresenter presenter;
    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews();
        initEvents();
        getData("ADMIN");

        return view;
    }

    private void initViews() {

        rvPlaylist.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPlaylist.setItemAnimator(new DefaultItemAnimator());
        rvPlaylist.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    private void initEvents() {
        swipeRefreshLayout.setOnRefreshListener((SwipeRefreshLayout.OnRefreshListener) () -> {
            swipeRefreshLayout.setRefreshing(true);
            getData("ADMIN");
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.playlist_short, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.short_by_admin:
                getData("ADMIN");
                return true;
            case R.id.short_by_user:
                getData("USER");
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getData(String created) {
        loadingIndicatorView.smoothToShow();
        String memberId = sessionManager.getDataMember().getId();
        presenter.getPlaylistAll(memberId, created);
    }

    @Override
    public void showTracks(List<DataTracks> tracksList) {

    }

    @Override
    public void showAllPlaylist(final List<DataPlaylist> dataPlaylist) {

        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
        rvPlaylist.setAdapter(new PlaylistAdapter(getActivity(), dataPlaylist, navigation, presenter, getSessionManager()));
        rvPlaylist.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvPlaylist,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

//                        navigation.pushFragment(
//                                new DetailPlaylistFragment().newInstance(dataPlaylist.get(position)),
//                                dataPlaylist.get(position).getName(),
//                                true);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showErrorAllPlaylist(String error) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorPlaylist(String error) {

    }

    public void showAddSucces(String data) {
        Toast.makeText(getActivity(), "Playlist Added", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showRemoveSucces(String data) {

    }

    @Override
    public void showRemoveError(String error) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

}
