package com.nutech.burgundy.ui.view.point;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataReward;
import com.nutech.burgundy.presenter.RewardPresenter;
import com.nutech.burgundy.ui.adapter.RewardAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.utils.Helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 11/10/17
 */

public class RewardFragment extends BaseFragment implements RewardView {

    private static final String KEY_POINT = "key_point";

    @BindView(R.id.rv_reward)
    RecyclerView rvReward;

    private Unbinder unbinder;
    private RewardPresenter presenter;

    private List<DataReward> rewardList;
    private RewardAdapter adapter;
    private SessionManager sessionManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RewardPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reward, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();
        loadingData();

        return view;
    }

    @Override
    public void showRewardAll(List<DataReward> dataRewardList) {

        this.rewardList.clear();
        this.rewardList.addAll(dataRewardList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void successAddPointExchnge(String succes) {
        confirmSuccessAddPoint();
    }

    @Override
    public void errorAddPointExchange(String error) {
        confirmSuccessAddPoint();
    }

    @Override
    public void showError(String error) {

        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void initView() {

        rewardList = new ArrayList<>();
        adapter = new RewardAdapter(rewardList);

        rvReward.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvReward.setItemAnimator(new DefaultItemAnimator());
        rvReward.setAdapter(adapter);
    }

    private void initEvent() {

        rvReward.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvReward, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                if (!TextUtils.isEmpty(sessionManager.getDataMember().getPoint())) {

                    int cusPoint = Integer.parseInt(sessionManager.getDataMember().getPoint());
                    int rewardPoint = Integer.parseInt(rewardList.get(position).getPointRequired());
                    if (cusPoint < rewardPoint){
                        Toast.makeText(getActivity(), "Point Anda tidak cukup", Toast.LENGTH_SHORT).show();
                    } else {
                        confirmAddPoint(rewardList.get(position).getId());
                    }
                } else {
                    Toast.makeText(getActivity(), "Point Anda tidak cukup", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void loadingData() {

        presenter.getRewardAll();
    }

    private void confirmAddPoint(final String rewardId) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Konfirmasi");
        alertDialog.setMessage("Poin yang sudah ditukar tidak bisa dikembalikan lagi, Yakin ingin melanjutkan?");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Lanjutkan",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Map<String, String> params = new HashMap<String, String>();
                        try {
                            params.put("customer_id", sessionManager.getDataMember().getId());
                            params.put("reward_id", rewardId);
                            params.put("exchange_status", Constants.Status.INACTIVE);
                            params.put("activation_code", Helper.generateCode());
                        } catch (Exception e) {

                        }
                        System.out.println("** Param ** "+ params);
                        presenter.addPointExchange(params);

                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Batal",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void confirmSuccessAddPoint() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Konfirmasi");
        alertDialog.setMessage("Segera aktifkan reward anda pada halaman history, kode aktivasi akan dikirim melalui SMS");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Mengerti",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });
        alertDialog.show();
    }

}
