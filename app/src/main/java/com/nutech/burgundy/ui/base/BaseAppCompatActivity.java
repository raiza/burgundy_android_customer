package com.nutech.burgundy.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.api.AntMediaService;
import com.nutech.burgundy.data.api.AntMediaServiceFactory;
import com.nutech.burgundy.data.api.DangdutAsikService;
import com.nutech.burgundy.data.api.DangdutAsikServiceFactory;
import com.nutech.burgundy.data.api.MelonService;
import com.nutech.burgundy.data.api.MelonServiceFactory;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by mactc08 on 7/14/17.
 */

public abstract class BaseAppCompatActivity extends AppCompatActivity {

    protected Context context;
    private DangdutAsikService dangdutAsikService;
    private AntMediaService antMediaService;
    private MelonService melonService;

    private SessionManager sessionManager;
    private DataManager dataManager;
    private NetworkHelper networkHelper;


    private static final int DELAY_MILLIS = 1000;

    private CastContext mCastContext;

    private final FragmentManager.OnBackStackChangedListener mBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                }
            };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        int playServicesAvailable =
                GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);

        if (playServicesAvailable == ConnectionResult.SUCCESS) {
            mCastContext = CastContext.getSharedInstance(this);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        try {
            this.context = newBase;
            super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DangdutAsikService getDangdutAsikService() {
        if (dangdutAsikService == null) {
            dangdutAsikService = DangdutAsikServiceFactory.create();
        }
        return dangdutAsikService;
    }

    public AntMediaService getAntMediaService() {
        if (antMediaService == null){
            antMediaService = AntMediaServiceFactory.create();
        }
        return antMediaService;
    }

    public MelonService getMelonService() {
        if (melonService == null){
            melonService = MelonServiceFactory.create();
        }
        return melonService;
    }

    public SessionManager getSessionManager() {
        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(this));
        }
        return sessionManager;
    }

    public DataManager getDataManager() {
        if (dataManager == null) {
            dataManager = new DataManager(getDangdutAsikService(), getAntMediaService(), getMelonService());
        }
        return dataManager;
    }

    public NetworkHelper getNetworkHelper() {
        if (networkHelper == null) {
            networkHelper = new NetworkHelper(this, getSessionManager());
        }
        return networkHelper;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
