package com.nutech.burgundy.ui.view.soundtrack;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Sound;
import com.nutech.burgundy.presenter.soundtrack.AddSoundtrackPresenter;
import com.nutech.burgundy.ui.adapter.soundtrack.DecadeAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.EnergyAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.GenresAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.SoundAdapter;
import com.nutech.burgundy.ui.base.BasePhotoFragment;
import com.nutech.burgundy.utils.Helper;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;

/**
 * Created by Arif Setiawan on 2019-06-28.
 * NuTech (Nusantara Teknologi)
 */
public class AddNewSoundtrackFragment extends BasePhotoFragment implements SoundtrackView {

    public static final int REQUEST_CODE_CAMERA = 100;
    private static final String[] PERMISSIONS_LIST = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @BindView(R.id.rv_energy_tag)
    RecyclerView rvEnergyTag;
    @BindView(R.id.rv_sound_tag)
    RecyclerView rvSoundTag;
    @BindView(R.id.rv_genre_tag)
    RecyclerView rvGenreTag;
    @BindView(R.id.rv_decade_tag)
    RecyclerView rvDecadeTag;
    @BindView(R.id.button_submit)
    Button btSubmit;
    @BindView(R.id.button_retry)
    Button btRetry;
    @BindView(R.id.shimmer_energy)
    ShimmerFrameLayout shimmerEnergy;
    @BindView(R.id.shimmer_sound)
    ShimmerFrameLayout shimmerSound;
    @BindView(R.id.shimmer_genre)
    ShimmerFrameLayout shimmerGenre;
    @BindView(R.id.shimmer_decade)
    ShimmerFrameLayout shimmerDecade;
    @BindView(R.id.layout_error_energy)
    RelativeLayout errorLayoutEnergy;
    @BindView(R.id.layout_error_sound)
    RelativeLayout errorLayoutSound;
    @BindView(R.id.layout_error_genre)
    RelativeLayout errorLayoutGenre;
    @BindView(R.id.layout_error_decade)
    RelativeLayout errorLayoutDecade;
    @BindView(R.id._switch)
    Switch shareSwitch;
    @BindView(R.id.iv_info)
    ImageView ivInfo;
    @BindView(R.id.et_title_mood)
    TextInputEditText etTitleMood;
    @BindView(R.id.et_description_mood)
    TextInputEditText etDescriptionMood;
    @BindView(R.id.image_cover)
    RoundedImageView imgCover;
    @BindView(R.id.iv_info_generate)
    ImageView ivInfoGenerate;
    @BindView(R.id._switch_generate)
    Switch SwitchGenerate;

    private Unbinder unbinder;
    private AddSoundtrackPresenter presenter;
    private EnergyAdapter energyAdapter;
    private SoundAdapter soundAdapter;
    private GenresAdapter genresAdapter;
    private DecadeAdapter decadeAdapter;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private ChipsLayoutManager chipsLayoutManager;
    private List<String> energyListParam = new ArrayList<>();
    private List<String> soundListParam = new ArrayList<>();
    private List<String> genreListParam = new ArrayList<>();
    private List<String> decadeListParam = new ArrayList<>();
    private String typeMood = "0";
    private String isGenerate = "0";
    private ProgressDialog pDialog;
    private Uri cropImageUri;
    private String baseImage = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new AddSoundtrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadData();
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerEnergy.startShimmer();
        shimmerSound.startShimmer();
        shimmerGenre.startShimmer();
        shimmerDecade.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fregment_addnewsoundtrack, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews();
        initEvents();
        return view;
    }

    private void initViews() {

        initPermission();

        rvEnergyTag.setVisibility(View.GONE);
    }

    private void initEvents() {

        btSubmit.setOnClickListener(view -> {

            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            if (energyListParam.size() == 0) {
//                Toasty.custom(getActivity(), R.string.msg_choose_energy,
//                        getResources().getDrawable(R.drawable.ic_info_outline_white_24dp),
//                        android.R.color.darker_gray,
//                        android.R.color.black,
//                        Toasty.LENGTH_SHORT,
//                        true,
//                        true).show();
                Toasty.info(Objects.requireNonNull(getActivity()), getString(R.string.msg_choose_energy), Toast.LENGTH_SHORT, true).show();
            } else if (soundListParam.size() == 0) {
                Toasty.info(Objects.requireNonNull(getActivity()), getString(R.string.msg_choose_sound), Toast.LENGTH_SHORT, true).show();
            } else if (genreListParam.size() == 0) {
                Toasty.info(Objects.requireNonNull(getActivity()), getString(R.string.msg_choose_genre), Toast.LENGTH_SHORT, true).show();
            } else if (decadeListParam.size() == 0) {
                Toasty.info(Objects.requireNonNull(getActivity()), getString(R.string.msg_choose_decade), Toast.LENGTH_SHORT, true).show();
            } else if (TextUtils.isEmpty(etTitleMood.getText())) {
                Toasty.info(Objects.requireNonNull(getActivity()), getString(R.string.msg_title_mood), Toast.LENGTH_SHORT, true).show();
            } else {
                showProgressDialog();
                String memberId = getSessionManager().getDataMember().getId();

                Map<String, String> params = new HashMap<String, String>();
                try {
                    params.put("customer_id", memberId);
                    params.put("name", etTitleMood.getText().toString());
                    params.put("description", Objects.requireNonNull(etDescriptionMood.getText()).toString());
                    params.put("public", typeMood);
                    params.put("generate", isGenerate);
                    params.put("tags", etTitleMood.getText().toString() + etDescriptionMood.getText().toString() + typeMood);
                    params.put("image", baseImage);
                    params.put("energy_ids", Helper.convertStringArrayToString(energyListParam, ","));
                    params.put("sound_ids", Helper.convertStringArrayToString(soundListParam, ","));
                    params.put("genre_ids", Helper.convertStringArrayToString(genreListParam, ","));
                    params.put("decade_ids", Helper.convertStringArrayToString(decadeListParam, ","));

                    if (getSessionManager().getUserType().equals(Constants.UserType.USER_STORE)) {
                        Log.e("type", getSessionManager().getUserType());
                        params.put("store_id", getSessionManager().getUserStore().getStore_id());
                    }
                } catch (Exception ignored) {

                }
                System.out.println("** Param ** " + params);
                presenter.saveNewMoodtrack(params);
            }
        });

        btRetry.setOnClickListener(view -> {
            loadData();
        });

        shareSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                // The toggle is enabled
                typeMood = "1";
            } else {
                // The toggle is disabled
                typeMood = "0";
            }
        });

        SwitchGenerate.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                // The toggle is enabled
                isGenerate = "1";
            } else {
                // The toggle is disabled
                isGenerate = "0";
            }
        });

        ivInfo.setOnClickListener(view -> {
            AlertDialog alertDialog = new AlertDialog.Builder(Objects.requireNonNull(getActivity())).create();
            alertDialog.setTitle(getResources().getString(R.string.label_info));
            alertDialog.setMessage(getString(R.string.msg_visible_moodtrack));
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.button_ok),
                    (dialog, which) -> dialog.dismiss());
            alertDialog.show();
        });

        imgCover.setOnClickListener(view -> {
            showDialogChoice();
        });
    }

    void initPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS_LIST,
                        REQUEST_CODE_CAMERA);
            }
        } else {
//            dispatchTakePictureIntent();
        }

    }

    private void loadData() {
        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                presenter.getEnergy();
                presenter.getSound();
                presenter.getGenresAll();
                presenter.getDecadeAll();
            }
        }.start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showEnergyList(List<Energy> dataEnergy) {
        shimmerEnergy.stopShimmer();
        shimmerEnergy.setVisibility(View.GONE);
        rvEnergyTag.setVisibility(View.VISIBLE);
        errorLayoutEnergy.setVisibility(View.GONE);
        btRetry.setVisibility(View.GONE);
        btSubmit.setVisibility(View.VISIBLE);
        if (dataEnergy.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvEnergyTag.addItemDecoration(new SpacingItemDecoration(
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro),
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro)));
            rvEnergyTag.setLayoutManager(chipsLayoutManager);
            energyAdapter = new EnergyAdapter(getActivity(), dataEnergy, this, Constants.Key.ADD_MOOD);
            rvEnergyTag.setAdapter(energyAdapter);
        }
    }

    @Override
    public void showErrorEnergy(String error) {
        shimmerEnergy.stopShimmer();
        shimmerEnergy.setVisibility(View.GONE);
        rvEnergyTag.setVisibility(View.GONE);
        errorLayoutEnergy.setVisibility(View.VISIBLE);
        btSubmit.setVisibility(View.GONE);
        btRetry.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataEnergy error " + error);
    }

    @Override
    public void showSoundList(List<Sound> dataSound) {
        shimmerSound.stopShimmer();
        shimmerSound.setVisibility(View.GONE);
        rvSoundTag.setVisibility(View.VISIBLE);
        errorLayoutSound.setVisibility(View.GONE);
        btRetry.setVisibility(View.GONE);
        btSubmit.setVisibility(View.VISIBLE);
        if (dataSound.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvSoundTag.addItemDecoration(new SpacingItemDecoration(
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro),
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro)));
            rvSoundTag.setLayoutManager(chipsLayoutManager);
            soundAdapter = new SoundAdapter(getActivity(), dataSound, this, Constants.Key.ADD_MOOD);
            rvSoundTag.setAdapter(soundAdapter);
        }
    }

    @Override
    public void showErrorSound(String error) {
        shimmerSound.stopShimmer();
        shimmerSound.setVisibility(View.GONE);
        rvSoundTag.setVisibility(View.GONE);
        errorLayoutSound.setVisibility(View.VISIBLE);
        btSubmit.setVisibility(View.GONE);
        btRetry.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataSound error " + error);
    }

    @Override
    public void showGenreList(List<DataGenre> genres) {
        shimmerGenre.stopShimmer();
        shimmerGenre.setVisibility(View.GONE);
        rvGenreTag.setVisibility(View.VISIBLE);
        errorLayoutGenre.setVisibility(View.GONE);
        btRetry.setVisibility(View.GONE);
        btSubmit.setVisibility(View.VISIBLE);
        if (genres.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvGenreTag.addItemDecoration(new SpacingItemDecoration(
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro),
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro)));
            rvGenreTag.setLayoutManager(chipsLayoutManager);
            genresAdapter = new GenresAdapter(getActivity(), genres, this, Constants.Key.ADD_MOOD);
            rvGenreTag.setAdapter(genresAdapter);
        }
    }

    @Override
    public void showErrorGenre(String error) {
        shimmerGenre.stopShimmer();
        shimmerGenre.setVisibility(View.GONE);
        rvGenreTag.setVisibility(View.GONE);
        errorLayoutGenre.setVisibility(View.VISIBLE);
        btSubmit.setVisibility(View.GONE);
        btRetry.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataGenre error " + error);
    }

    @Override
    public void showDecadeList(List<Decade> decades) {
        try {
            shimmerDecade.stopShimmer();
            shimmerDecade.setVisibility(View.GONE);
            rvDecadeTag.setVisibility(View.VISIBLE);
            errorLayoutDecade.setVisibility(View.GONE);
            btRetry.setVisibility(View.GONE);
            btSubmit.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (decades.size() > 0) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
            chipsLayoutManager = ChipsLayoutManager.newBuilder(context)
                    .setMaxViewsInRow(4)
                    .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_SPACE)
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .build();
            rvDecadeTag.addItemDecoration(new SpacingItemDecoration(
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro),
                    getResources().getDimensionPixelOffset(R.dimen.spacing_micro)));
            rvDecadeTag.setLayoutManager(chipsLayoutManager);
            decadeAdapter = new DecadeAdapter(getActivity(), decades, this, Constants.Key.ADD_MOOD);
            rvDecadeTag.setAdapter(decadeAdapter);
        }
    }

    @Override
    public void showErrorDecade(String error) {
        shimmerDecade.stopShimmer();
        shimmerDecade.setVisibility(View.GONE);
        rvDecadeTag.setVisibility(View.GONE);
        errorLayoutDecade.setVisibility(View.VISIBLE);
        btSubmit.setVisibility(View.GONE);
        btRetry.setVisibility(View.VISIBLE);
        System.out.println("@@@@ dataGenre error " + error);
    }

    @Override
    public void showSuccessAdd(String success) {
        hideProgressDialog();
        reEmptyData();
    }

    @Override
    public void showErrorAdd(String error) {
        hideProgressDialog();
        reEmptyData();
        System.out.println("@@@@ showErrorAdd " + error);

    }

    public void setEnergysParam(List<String> ids) {
        energyListParam = ids;
        System.out.println("***** energyListParam = " + energyListParam.toString());
    }

    public void setSoundsParam(List<String> ids) {
        soundListParam = ids;

        System.out.println("***** soundListParam = " + soundListParam.toString());
    }

    public void setGenresParam(List<String> ids) {
        genreListParam = ids;
        System.out.println("***** genreListParam = " + genreListParam.toString());
    }

    public void setDecadeParam(List<String> ids) {
        decadeListParam = ids;
        System.out.println("***** decadeListParam = " + decadeListParam.toString());
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage(getString(R.string.msg_creating));
        pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    private void reEmptyData() {
//        energyListParam.clear();
//        soundListParam.clear();
//        genreListParam.clear();
//        decadeListParam.clear();
//        etTitleMood.setText("");
//        etDescriptionMood.setText("");
//        shareSwitch.setChecked(false);
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getResources().getString(R.string.label_info));
        alertDialog.setMessage(getResources().getString(R.string.msg_success_add_moodtrack));
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.button_ok),
                (dialog, which) -> {
                    dialog.dismiss();
                    navigation.backFragment();
//                    Intent intent = new Intent(getActivity(), PrimaryActivity.class);
//                    startActivity(intent);
                }
        );
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                Uri imageUri = CropImage.getPickImageResultUri(context, data);

                if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri)) {
                    cropImageUri = imageUri;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                Constants.PERMISSIONS_PHOTO,
                                CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                    }
                } else {
                    startCropImageMoodtrackActivity(imageUri);
                }
            } else {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == Activity.RESULT_OK) {

                Uri resultImageUri = result.getUri();
                imgCover.setImageURI(resultImageUri);
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                baseImage = Helper.convertToBase64(bitmap);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (cropImageUri != null && grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCropImageMoodtrackActivity(cropImageUri);
            } else {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
