package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nutech.burgundy.ui.base.BaseAdapter;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataSearch;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class SearchAdapter extends BaseAdapter<SearchAdapter.SearchHolder> {

    private Context context;
    private List<DataSearch> list;

    public SearchAdapter(
            Context context, List<DataSearch> list) {
        this.context = context;
        this.list = list;
    }

    class SearchHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_search_name)
        TextView tvSearchName;
        @BindView(R.id.tv_search_category)
        TextView tvSearchCategory;
        @BindView(R.id.iv_search)
        ImageView ivSearch;
        View childView;

        SearchHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search, parent, false);
        return new SearchHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchHolder holder, int position) {

        holder.tvSearchName.setText(list.get(position).getName());
        holder.tvSearchCategory.setText(list.get(position).getData_type());

        if (list.get(position).getData_type().contains("Artist")) {
            String ImageUrl = Constants.URL_IMAGE_ARTISTS+list.get(position).getImage();
            Picasso.with(context.getApplicationContext())
                    .load(ImageUrl)
                    .error(R.drawable.default_user)
                    .fit()
                    .into(holder.ivSearch);
        } else if (list.get(position).getData_type().contains("Album")){
            String ImageUrl = Constants.URL_IMAGE_ALBUMS+list.get(position).getImage();
            Picasso.with(context.getApplicationContext())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(holder.ivSearch);
        } else if (list.get(position).getData_type().contains("Playlist")){
            String ImageUrl = Constants.URL_IMAGE_PLAYLIST+list.get(position).getImage();
            Picasso.with(context.getApplicationContext())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .fit()
                    .into(holder.ivSearch);
        } else if (list.get(position).getData_type().contains("Track")){
            String ImageUrl = Constants.URL_IMAGE_ARTISTS+list.get(position).getImage();
            Picasso.with(context.getApplicationContext())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .fit()
                    .into(holder.ivSearch);
        } else if (list.get(position).getData_type().contains("Soundtracks")){
            String ImageUrl = Constants.URL_IMAGE_MOODTRACK+list.get(position).getImage();
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            ColorDrawable cd = new ColorDrawable(color);
            loadImagesMoodFromURL(context, holder.ivSearch, ImageUrl, cd);
//            Picasso.with(context.getApplicationContext())
//                    .load(ImageUrl)
//                    .error(R.drawable.image_dm_error)
//                    .fit()
//                    .into(holder.ivSearch);
        } else {
            String ImageUrl = Constants.URL_IMAGE_GENRES+list.get(position).getImage();
            Picasso.with(context.getApplicationContext())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .fit()
                    .into(holder.ivSearch);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
