package com.nutech.burgundy.ui.view.playlist;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.adapter.TracksDelAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class DetailPlaylistDeleteFragment extends BaseFragment implements PlaylistView {

    String TAG = "DetailPlaylistDeleteFragment";

    @BindView(R.id.rv_track_list)
    RecyclerView rvTrackList;
    @BindView(R.id.image_backdrop)
    ImageView imageBackdrop;
    @BindView(R.id.image_playlist)
    ImageView imagePlaylist;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.image_fav)
    ImageView imageFav;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @OnClick(R.id.image_fav)
    public void addToFavorite(){
        String memberId = sessionManager.getDataMember().getId();
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("customer_id", memberId);
            params.put("playlist_id", playlistId);
        } catch (Exception e) {

        }
        System.out.println("** Param ** "+ params);
        presenter.sendPlaylist(params);
    }

    private Unbinder unbinder;
    private PlaylistPresenter presenter;
    private SessionManager sessionManager;

    //---- Params ----//
    private String playlistId = "";
    private String playlistImage = "";
    private String key = "";

    public DetailPlaylistDeleteFragment(String id, String image, String keyDetailPlaylist) {
        this.playlistId = id;
        this.playlistImage = image;
        this.key = keyDetailPlaylist;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvents();
        getData();
        return view;
    }

    public void getData() {
        swipeRefreshLayout.setRefreshing(true);
        loadingIndicatorView.smoothToShow();
        presenter.getTracksByPLaylist(playlistId);
    }

    public void initEvents(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });
    }

    public void remove(String id) {
        presenter.removePlaylistTrack(playlistId, id);
    }

    public void initView() {

        Log.e("######", TAG);

        rvTrackList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTrackList.setItemAnimator(new DefaultItemAnimator());
        rvTrackList.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));


        if (key.equals(Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_MYMUSIC)){
            String ImageUrl = Constants.URL_IMAGE_PLAYLIST_WS+playlistImage;
            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .resize(Constants.Resize.sizeWeigth, Constants.Resize.sizeHeigth)
                    .into(imagePlaylist);

            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .fit()
                    .error(R.drawable.image_detail_error)
                    .transform(new BlurTransformation(getActivity()))
                    .into(imageBackdrop);
        } else {

            String ImageUrl = Constants.URL_IMAGE_PLAYLIST+playlistImage;
            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .resize(Constants.Resize.sizeWeigth, Constants.Resize.sizeHeigth)
                    .into(imagePlaylist);

            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_detail_error)
                    .transform(new BlurTransformation(getActivity()))
                    .fit()
                    .into(imageBackdrop);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void showTracks(final List<DataTracks> tracksList) {

        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();

        rvTrackList.setAdapter(new TracksDelAdapter(getActivity(), tracksList, navigation,
                playlistId, presenter,
                DetailPlaylistDeleteFragment.this, rvTrackList));

        rvTrackList.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTrackList,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

    }

    @Override
    public void showAllPlaylist(List<DataPlaylist> dataPlaylists) {

    }

    @Override
    public void showErrorAllPlaylist(String error) {

    }

    @Override
    public void showErrorPlaylist(String error) {

    }

    @Override
    public void showAddSucces(String data) {
    }

    @Override
    public void showError(String error) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
    }

    @Override
    public void showRemoveSucces(String data) {
        getData();
    }

    @Override
    public void showRemoveError(String error) {
        getData();
    }
}
