package com.nutech.burgundy.ui.view.home;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.mediarouter.app.MediaRouteButton;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.lib.cast.SessionManagerListenerImpl;
import com.nutech.burgundy.lib.gps.GPSTracker;
import com.nutech.burgundy.lib.gps.IOnlocationChanged;
import com.nutech.burgundy.presenter.GenrePresenter;
import com.nutech.burgundy.ui.adapter.GenreAdapter;
import com.nutech.burgundy.ui.adapter.MostPlayedSongsAdapter;
import com.nutech.burgundy.ui.adapter.PopularTodayAdapter;
import com.nutech.burgundy.ui.adapter.RecomendedAdapter;
import com.nutech.burgundy.ui.adapter.SlidePromoAdapter;
import com.nutech.burgundy.ui.adapter.TopKaraokeAdapter;
import com.nutech.burgundy.ui.adapter.TopPlaylistAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.MoodtrackForYouAdapter;
import com.nutech.burgundy.ui.adapter.NearestStoreAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.artists.ArtistsFragment;
import com.nutech.burgundy.ui.view.genre.DetailGenreFragment;
import com.nutech.burgundy.ui.view.livekaraoke.HistoryKaraokeFragment;
import com.nutech.burgundy.ui.view.playlist.PlaylistFragment;
import com.nutech.burgundy.ui.view.recomended.RecomendedFragment;
import com.nutech.burgundy.ui.view.search.SearchFragment;
import com.nutech.burgundy.ui.view.soundtrack.AddNewSoundtrackFragment;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackFragment;
import com.nutech.burgundy.ui.view.stores.DetailStoreFragment;
import com.nutech.burgundy.ui.view.soundtrack.ListMoodTrackForYouFragment;
import com.nutech.burgundy.ui.view.soundtrack.ListMoodTrackPlayingStoreFragment;
import com.nutech.burgundy.ui.view.stores.NearStoreListFragment;
import com.nutech.burgundy.ui.view.topchart.TopChartFragment;
import com.nutech.burgundy.ui.view.video.VideoCategoryFragment;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class FragmentDiscover extends BaseFragment implements DiscoverView,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, IOnlocationChanged {

    @BindView(R.id.viewpager_promo)
    ViewPager vpPromo;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;
    @BindView(R.id.ll_search)
    RelativeLayout llSearch;

    @BindView(R.id.iv_menu_artists)
    CircleImageView ivMenuArtists;

    @BindView(R.id.ll_detail_top_chart)
    LinearLayout llDetailTopChart;
    @BindView(R.id.ll_detail_top_playlist)
    LinearLayout llDetailTopPlaylist;

    @BindView(R.id.iv_discover_top_chart)
    ImageView ivDiscoverTopChart;
    @BindView(R.id.bt_more_popular_today)
    ImageButton btMorePopularToday;
    @BindView(R.id.bt_more_most_played_songs)
    ImageButton btMoreMostPLayed;
    @BindView(R.id.tv_more_recomended)
    TextView tvMoreRecomended;
    @BindView(R.id.tv_more_karaoke)
    TextView tvMoreKaraoke;
    @BindView(R.id.rv_top_playlist)
    RecyclerView rvTopPlayList;
    @BindView(R.id.rv_popular_today)
    RecyclerView rvPopularToday;
    @BindView(R.id.recycle_discover_genre)
    RecyclerView rvDiscoverGenre;
    @BindView(R.id.ll_recomended)
    LinearLayout llRecomended;
    @BindView(R.id.iv_recomended)
    ImageView imgRecomended;
    @BindView(R.id.rv_recomended)
    RecyclerView rvRecomended;
    @BindView(R.id.rv_most_played_songs)
    RecyclerView rvMostPlayed;

    @BindView(R.id.ll_top_karaoke)
    LinearLayout llTopKaraoke;
    @BindView(R.id.rv_top_karaoke)
    RecyclerView rvTopKaraoke;
    @BindView(R.id.rv_store_playing_mood_track)
    RecyclerView rvStorePlayingMoodTrack;
    @BindView(R.id.ll_store_playing_mood_track)
    LinearLayout llStorePlayingMoodTrack;
    @BindView(R.id.v_genre)
    View vGenre;

    @BindView(R.id.v_nearest_stores)
    View vNearestStores;
    @BindView(R.id.rv_nearest_stores)
    RecyclerView rvNearestStores;
    @BindView(R.id.ll_nearest_stores)
    LinearLayout llNearestStores;
    @BindView(R.id.v_store_playing_mood_track)
    View vStorePlayingMoodTrack;
    @BindView(R.id.bt_more_nearest_store)
    ImageButton btMoreNearestStore;
    @BindView(R.id.bt_more_store_playing_moodtrack)
    ImageButton btMoreStorePlayingMoodtrack;
    @BindView(R.id.tv_menu_cast)
    TextView tvMenuCast;
    @BindView(R.id.rl_menu_cast)
    RelativeLayout rlMenuCast;
    @BindView(R.id.mrb_menu_cast)
    MediaRouteButton mrbMenuCast;
    @BindView(R.id.v_moodtrack_for_you)
    View vMoodtrackForYou;
    @BindView(R.id.tv_moodtrack_for_you)
    TextView tvMoodtrackForYou;
    @BindView(R.id.bt_more_moodtrack_for_you)
    ImageButton btMoreMoodtrackForYou;
    @BindView(R.id.iv_discover_moodtrack_for_you)
    ImageView ivDiscoverMoodtrackForYou;
    @BindView(R.id.rv_moodtrack_for_you)
    RecyclerView rvMoodtrackForYou;
    @BindView(R.id.ll_moodtrack_for_you)
    LinearLayout llMoodtrackForYou;

    private Unbinder unbinder;
    private GenrePresenter presenter;
    private LocationManager locationManager;
    private GoogleApiClient googleApiClient;

    private int currentPage = 0;
    private int NUM_PAGES = 0;
    private ArrayList<String> ImagesArray = new ArrayList<>();
    private ArrayList<String> ids = new ArrayList<>();

    private Handler handler;
    private Runnable runnable;
    private Timer swipeTimer;
    private GPSTracker gps;
    private double lat = 0.0;
    private double lng = 0.0;
    private SlidePromoAdapter adapter;
    private MoodtrackForYouAdapter moodtrackAdapter;

    //Cast
    private CastContext castContext;
    private CastSession castSession;
    private SessionManager sessionManager;
    private final SessionManagerListener sessionManagerListener = new SessionManagerListenerImpl();
    private CastStateListener castStateListener;
    private IntroductoryOverlay introductoryOverlay;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        castContext = CastContext.getSharedInstance(context);
//        castStateListener = newState -> {
//            if (newState == CastState.NO_DEVICES_AVAILABLE) {
//                rlMenuCast.setVisibility(View.GONE);
//                mrbMenuCast.setVisibility(View.GONE);
//            } else {
//                rlMenuCast.setVisibility(View.VISIBLE);
//                mrbMenuCast.setVisibility(View.VISIBLE);
//            }
//
//            if (castContext.getCastState() == CastState.NO_DEVICES_AVAILABLE) {
//                rlMenuCast.setVisibility(View.GONE);
//                mrbMenuCast.setVisibility(View.GONE);
//            } else {
//                rlMenuCast.setVisibility(View.VISIBLE);
//                mrbMenuCast.setVisibility(View.VISIBLE);
//            }
//
//        };
//        castContext.addCastStateListener(castStateListener);
//        CastButtonFactory.setUpMediaRouteButton(context,mrbMenuCast);
        sessionManager = getSessionManager();

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        presenter = new GenrePresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);

//        if (!Objects.requireNonNull(locationManager).isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            if (!getSessionManager().getIsNoGps()) {
//                alertGpsOff();
//            }
//        }

//        getLocation();

        handler = new Handler();
        swipeTimer = new Timer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_discover, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();
        loadingData();
        return view;
    }

    @Override
    public void onResume() {
//        castSession = sessionManager.getCurrentCastSession();
//        sessionManager.addSessionManagerListener(sessionManagerListener);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
//        sessionManager.removeSessionManagerListener(sessionManagerListener);
//        castSession = null;
    }

    private void loadingData() {
        presenter.getAllData(lat, lng);
    }

    private synchronized void getLocation() {
        gps = new GPSTracker(context, this);
        if (gps.canGetLocation()) {
            lat = gps.getLatitude(); // returns latitude
            lng = gps.getLongitude();
        }
    }

    @OnClick(R.id.ll_search)
    void moveToSearch() {
        navigation.pushFragment(new SearchFragment(), getString(R.string.label_menu_search), true);
    }

    @OnClick(R.id.iv_menu_artists)
    void moveToArtist() {
        navigation.pushFragment(new ArtistsFragment(), getString(R.string.label_menu_artists), true);
    }

    @OnClick(R.id.iv_menu_video)
    void moveToVideo() {
        navigation.pushFragment(new VideoCategoryFragment(), getString(R.string.label_menu_video), true);
    }

    private void alertGpsOff() {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        alertDialog.setTitle(getString(R.string.label_info));
        alertDialog.setMessage(getString(R.string.msg_gps_off));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.label_turn_on),
                (dialog, which) -> turnGPSOn());

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.label_no),
                (dialog, which) -> {
                    dialog.dismiss();
                    getSessionManager().setIsNoGps(true);
                });
        alertDialog.show();
    }

    private void showIntroductoryOverlay() {
        if (introductoryOverlay != null) {
            introductoryOverlay.remove();
        }
        if ((mrbMenuCast != null) && mrbMenuCast.getVisibility() == View.VISIBLE) {
            new Handler().post(() -> {
                introductoryOverlay = new IntroductoryOverlay.Builder(
                        (Activity) context, mrbMenuCast)
                        .setTitleText("Introducing Cast")
                        .setSingleTime()
                        .setOnOverlayDismissedListener(
                                new IntroductoryOverlay.OnOverlayDismissedListener() {
                                    @Override
                                    public void onOverlayDismissed() {
                                        introductoryOverlay = null;
                                    }
                                })
                        .build();
                introductoryOverlay.show();
            });
        }
    }

    private void turnGPSOn() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(Objects.requireNonNull(getContext()))
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true); //this is the key ingredient

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(result1 -> {
                final Status status = result1.getStatus();
                final LocationSettingsStates state = result1.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
// Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            startIntentSenderForResult(status.getResolution().getIntentSender(),
                                    Constants.RequestCode.ON_GPS_DISCOVER, null, 0, 0, 0, null);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
// Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.RequestCode.ON_GPS_DISCOVER) {
            if (resultCode == Activity.RESULT_OK) {
                getLocation();
            }
        }
    }

    @Override
    public void showPromo(List<DataPromoSlide> promoSlideList) {

        if (ImagesArray.size() > 0) {
            ImagesArray.clear();
            ids.clear();
        }

        for (int i = 0; i < promoSlideList.size(); i++) {
            ImagesArray.add(promoSlideList.get(i).getImage());
            ids.add(promoSlideList.get(i).getPromotion_id());
        }

        NUM_PAGES = ImagesArray.size();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showMoodtrackPlayingStore(List<Moodtrack> moodtrackList) {
        llStorePlayingMoodTrack.setVisibility(View.VISIBLE);
//        rvStorePlayingMoodTrack.setAdapter(new StorePlayingMoodtrackAdapter(getActivity(), moodtrackList));
//        rvStorePlayingMoodTrack.addOnItemTouchListener(new RecyclerTouchListener(
//                getActivity(), rvStorePlayingMoodTrack, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                navigation.pushFragment(
//                        new DetailMoodtrackFragment(moodtrackList.get(position).getId(),
//                                moodtrackList.get(position).getEnergyids(),
//                                moodtrackList.get(position).getSoundIds(),
//                                moodtrackList.get(position).getDecadeIds(),
//                                moodtrackList.get(position).getGenreIds()),
//                        moodtrackList.get(position).getName(), true);
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));
    }

    @Override
    public void showNearStores(List<DataStore> storeList) {

        llNearestStores.setVisibility(View.VISIBLE);
        rvNearestStores.setAdapter(new NearestStoreAdapter(getActivity(), storeList));
        rvNearestStores.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvNearestStores, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                       new DetailStoreFragment(storeList.get(position)),
                        storeList.get(position).getSite_name(), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void showTopChart(List<DataTracks> tracks) {

        if (tracks.size() > 0) {
            String url = Constants.URL_IMAGE_ARTISTS + tracks.get(0).getArtisimage();
            Log.e("*** image *** ", url);
            Picasso.with(getActivity())
                    .load(url)
                    .placeholder(R.drawable.image_dm_error)
                    .error(R.drawable.image_dm_error)
                    .fit()
                    .into(ivDiscoverTopChart);
        }

        llDetailTopChart.setVisibility(View.VISIBLE);
        rvPopularToday.setAdapter(new PopularTodayAdapter(getActivity(), tracks));
        rvMostPlayed.setAdapter(new MostPlayedSongsAdapter(getActivity(), tracks));
    }

    @Override
    public void showTopPlayList(List<DataPlaylist> playlist) {
        rvTopPlayList.setAdapter(new TopPlaylistAdapter(getActivity(), playlist));
    }

    @Override
    public void showRecomended(List<DataTracks> listDataTracks) {
        System.out.println("++++++++ listDataTracks +++++ " + listDataTracks.size());

        if (listDataTracks.size() > 0) {

            try {
                llRecomended.setVisibility(View.GONE);

                String url = Constants.URL_IMAGE_ARTISTS + listDataTracks.get(0).getArtisimage();
                Picasso.with(getActivity())
                        .load(url)
                        .placeholder(R.drawable.image_dm_error)
                        .error(R.drawable.image_dm_error)
                        .fit()
                        .into(imgRecomended);

                rvRecomended.setAdapter(new RecomendedAdapter(getActivity(), listDataTracks));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            llRecomended.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMoodtrackForYou(List<Moodtrack> listDataTracks) {
        llMoodtrackForYou.setVisibility(View.VISIBLE);
        moodtrackAdapter = new MoodtrackForYouAdapter(context, listDataTracks);
        rvMoodtrackForYou.setAdapter(moodtrackAdapter);
        rvMoodtrackForYou.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(),
                rvMoodtrackForYou,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        navigation.pushFragment(new DetailMoodtrackFragment(
                                        listDataTracks.get(position).getId(),false),
                                listDataTracks.get(position).getName(), true);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showTopKaraoke(List<DataRoom> listDataRoom) {
        rvTopKaraoke.setAdapter(new TopKaraokeAdapter(getActivity(), listDataRoom));
    }

    @Override
    public void showErrorTopKaraoke(String error) {

    }

    @Override
    public void showGenre(final List<DataGenre> genreList) {
        rvDiscoverGenre.setAdapter(new GenreAdapter(getActivity(), genreList));
        rvDiscoverGenre.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvDiscoverGenre, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new DetailGenreFragment(
                                genreList.get(position).getId(),
                                genreList.get(position).getImage()
                        ), genreList.get(position).getName(), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void showError(String error) {

//        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        Log.i("Discover", "showErrorAdsList: " + error);
    }

    @Override
    public void showErrorMoodtrackForYou(String error) {

    }

    @Override
    public void showNearStoresError(String error) {
        try {
            llNearestStores.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMoodtrackPlayingStoreError(String error) {
        try {
            llStorePlayingMoodTrack.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showErrorRecomended(String error) {
        try {
            llRecomended.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
        swipeTimer.purge();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
//        gps.stopUsingGPS();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();

        if (handler != null) {
            handler.removeCallbacks(runnable);
            handler = null;
        }
        swipeTimer.purge();

    }

    @SuppressLint("WrongConstant")
    private void initView() {

        tvMoreRecomended.setText(Html.fromHtml(getString(R.string.label_more_with_underline)));
        tvMoreKaraoke.setText(Html.fromHtml(getString(R.string.label_more_with_underline)));

        rvPopularToday.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvPopularToday.setItemAnimator(new DefaultItemAnimator());

        rvMoodtrackForYou.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvMoodtrackForYou.setItemAnimator(new DefaultItemAnimator());

        rvRecomended.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvRecomended.setItemAnimator(new DefaultItemAnimator());

        rvTopPlayList.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvTopPlayList.setItemAnimator(new DefaultItemAnimator());

        rvStorePlayingMoodTrack.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.HORIZONTAL, false
        ));
        rvStorePlayingMoodTrack.setItemAnimator(new DefaultItemAnimator());

        rvNearestStores.setLayoutManager(new LinearLayoutManager( getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvNearestStores.setItemAnimator(new DefaultItemAnimator());

        rvDiscoverGenre.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        rvDiscoverGenre.setItemAnimator(new DefaultItemAnimator());

        rvTopKaraoke.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.VERTICAL, false));
        rvTopKaraoke.setItemAnimator(new DefaultItemAnimator());

        rvMostPlayed.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvMostPlayed.setItemAnimator(new DefaultItemAnimator());
    }

    private void initEvent() {

//        String tag = "slide_promo";

//        adapter = new SlidePromoAdapter(getActivity(), ImagesArray, tag, vpPromo, navigation, ids);
//        vpPromo.setAdapter(adapter);
//        indicator.setViewPager(vpPromo);

//        final float density = getResources().getDisplayMetrics().density;
//        indicator.setRadius(5 * density);

        btMoreMoodtrackForYou.setOnClickListener(view -> navigation.pushFragment(new ListMoodTrackForYouFragment(), getString(R.string.label_moodtrack_for_your), true));

        // Auto start of viewpager
//        runnable = () -> {
//            if (currentPage == NUM_PAGES) {
//                currentPage = 0;
//                vpPromo.setCurrentItem(0, true);
//            } else {
//                try {
//                    vpPromo.setCurrentItem(currentPage++, true);
//                } catch (Exception e) {
//                    Log.e("*** Error Slide Promo", "");
//                }
//            }
//
//        };

        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (handler != null && runnable != null) {
                    handler.post(runnable);
                }
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        llDetailTopChart.setOnClickListener(v -> navigation.pushFragment(
                new TopChartFragment(), "Top Chart", true));

        rvPopularToday.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvPopularToday, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new TopChartFragment(), getString(R.string.label_popular_today), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        mrbMenuCast.setClickable(true);

        rvRecomended.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvRecomended, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new RecomendedFragment(), getString(R.string.label_recomended_for_you), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        ivDiscoverTopChart.setOnClickListener(v -> navigation.pushFragment(
                new TopChartFragment(), getResources().getString(R.string.label_popular_today), true));

        llDetailTopPlaylist.setOnClickListener(v -> navigation.pushFragment(
                new PlaylistFragment(), getString(R.string.label_menu_playlist), true));

        rvTopPlayList.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvTopPlayList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new PlaylistFragment(), getString(R.string.label_menu_playlist), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rvTopKaraoke.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvTopKaraoke, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new HistoryKaraokeFragment(), getString(R.string.label_top_karaoke), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        btMoreNearestStore.setOnClickListener(view -> navigation.pushFragment(
                new NearStoreListFragment(), getString(R.string.label_nearest_store), true
        ));
        btMoreStorePlayingMoodtrack.setOnClickListener(view -> navigation.pushFragment(
                new ListMoodTrackPlayingStoreFragment(), getString(R.string.label_store_playing_mood_track), true
        ));
        imgRecomended.setOnClickListener(view -> navigation.pushFragment(
                new RecomendedFragment(), getString(R.string.label_recomended_for_you), true));

        tvMoreKaraoke.setOnClickListener(v -> navigation.pushFragment(
                new HistoryKaraokeFragment(), getString(R.string.label_top_karaoke), true));

        tvMoreRecomended.setOnClickListener(v -> navigation.pushFragment(
                new RecomendedFragment(), getString(R.string.label_recomended_for_you), true));

        rvMostPlayed.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvMostPlayed, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new TopChartFragment(), getResources().getString(R.string.label_most_played_songs), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (lat == 0.0 && lng == 0.0) {
            lat = location.getLatitude();
            lng = location.getLongitude();
            gps.stopUsingGPS();
            loadingData();
        }
    }
}
