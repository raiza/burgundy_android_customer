package com.nutech.burgundy.ui.base;

import retrofit2.HttpException;

/**
 * Created by Arif-Setiawan on 8/28/17.
 */

public class BasePresenter<T extends BaseView> implements Presenter<T> {

    private T view;

    @Override
    public void attachView(T view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    public boolean isViewAttached() {
        return view != null;
    }

    public T getView() {
        return view;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new BaseViewNotAttachedException();
    }

    public static class BaseViewNotAttachedException extends RuntimeException {

        public BaseViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    protected boolean isEmptyData(Throwable throwable){

        if(throwable instanceof HttpException){
            HttpException httpException = (HttpException) throwable;
            int errorCode = httpException.code();

            return errorCode == 404;
        }

        return false;

    }

}
