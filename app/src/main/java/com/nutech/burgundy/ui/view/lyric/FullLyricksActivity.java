package com.nutech.burgundy.ui.view.lyric;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.ui.adapter.LyricksAdapter;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 10/25/17.
 * Xeranta Mobile Solutions
 */

public class FullLyricksActivity extends AppCompatActivity {

    @BindView(R.id.rv_lyricks)
    RecyclerView rvLyricks;

    private DmApplication dmApplication;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_full_lyricks);
        ButterKnife.bind(this);
        dmApplication = (DmApplication) getApplicationContext();

        initViews();
    }


    private void initViews() {

        rvLyricks.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvLyricks.setItemAnimator(new DefaultItemAnimator());
        rvLyricks.addItemDecoration(new DividerItemDecoration(
                getApplicationContext(), LinearLayoutManager.VERTICAL));
        rvLyricks.setAdapter(new LyricksAdapter(getApplicationContext(), dmApplication.getDataLiricksList()));
        rvLyricks.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvLyricks,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }
}
