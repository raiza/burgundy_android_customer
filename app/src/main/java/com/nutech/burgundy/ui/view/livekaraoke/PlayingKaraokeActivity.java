package com.nutech.burgundy.ui.view.livekaraoke;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.presenter.livekaraoke.PlayerPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.livekaraoke.player.PlayerView;
import com.nutech.burgundy.ui.view.livekaraoke.player.ReportActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 24/05/18.
 * Xeranta Mobile Solutions
 */
public class PlayingKaraokeActivity extends BaseAppCompatActivity implements
        PlayerView, JcPlayerView.JcPlayerViewServiceListener {

    @BindView(R.id.player_view)
    SimpleExoPlayerView simpleExoPlayerView;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.image_like)
    ShineButton imgLikes;
    @BindView(R.id.image_report)
    ShineButton imgReport;
    @BindView(R.id.jcplayer)
    JcPlayerView jcPlayerView;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;

    @OnClick(R.id.toolbar)
    public void back(){
        deletedLocalTrack();
        finish();
    }

    private Unbinder unbinder;
    private DataRoom dataRoom;
    private PlayerPresenter presenter;
    private SessionManager sessionManager;
    private String sourceTrack;
    private String nameTrackInPath = "";
    private String sourceTrackFromPath = "";
    private String paramPath = "";
    private boolean isDownloaded = false;
    private List<DataKaraoke> tempListData = new ArrayList<>();
    private List<JcAudio> jcAudios = new ArrayList<JcAudio>();
    private CountDownTimer countDownTimer;

    private SimpleExoPlayer player;

    private Timeline.Window window;
    private DataSource.Factory mediaDataSourceFactory;
    private DefaultTrackSelector trackSelector;
    private boolean shouldAutoPlay;
    private BandwidthMeter bandwidthMeter;
    private boolean lanscape = true;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karaoke_playing);
        ButterKnife.bind(this);

        presenter = new PlayerPresenter(getDataManager(), getNetworkHelper());
        sessionManager = getSessionManager();
        presenter.attachView(this);
        dataRoom = Constants.DataRoomTemporary.dataRoom;

        initViews();
        initEvent();
    }

    private void initViews() {

        shouldAutoPlay = true;
        bandwidthMeter = new DefaultBandwidthMeter();
        mediaDataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"), (TransferListener<? super DataSource>) bandwidthMeter);
        window = new Timeline.Window();


        if (dataRoom.getCustomerid().equals(sessionManager.getDataMember().getId())) {
            imgReport.setVisibility(View.GONE);
        }

        String urlVideo = Constants.URL_VIDEO_KARAOKE+dataRoom.getRoomcode()+".mp4";
        System.out.println("*** URL VIDEO 2 *** "+urlVideo);
        initializePlayer(urlVideo);

        Map<String, String> param = new HashMap<String, String>();
        try {
            param.put("id", dataRoom.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        presenter.updateViewers(param);
    }

    private void initEvent() {

        imgLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> param = new HashMap<String, String>();
                try {
                    param.put("id", dataRoom.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                presenter.update(param, imgLikes);
            }
        });

        imgReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PlayingKaraokeActivity.this, ReportActivity.class);
                intent.putExtra("room_code", dataRoom.getRoomcode());
                intent.putExtra("trackId", dataRoom.getTrackid());
                intent.putExtra("customerId", dataRoom.getCustomerid());
                startActivity(intent);
            }
        });
    }

    private void getDataKaraoke() {
        presenter.getDetailKaraoke(dataRoom.getKaraoke_track_id());
    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {

    }

    @Override
    public void onCompletedAudio() {

    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onContinueAudio() {

    }

    @Override
    public void onPlaying() {
    }

    @Override
    public void addLogAds(String id,String trackId) {

    }

    @Override
    public void onTimeChanged(long currentTime) {

    }

    @Override
    public void updateTitle(String title, String artisName) {

    }

    @Override
    public void updateCover(String cover) {

    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {

    }

    @Override
    public void removeNotification() {

    }

    class DownloadTrack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            llLoading.setVisibility(View.VISIBLE);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);


                // Output stream to write file
                OutputStream output = null;

                File f = new File(Environment.getExternalStorageDirectory() + "/dangdutasik");
                if (f.isDirectory()) {
                    output = new FileOutputStream("/sdcard/dangdutasik/dangdutasik_file_" + dataRoom.getRoomcode() + ".mp3");

                } else {
                    File dir = new File("/sdcard/dangdutasik");
                    try {
                        if (dir.mkdir()) {
                            output = new FileOutputStream("/sdcard/dangdutasik/dangdutasik_file_" + dataRoom.getRoomcode() + ".mp3");
                        } else {
                            output = new FileOutputStream("/sdcard/dangdutasik_file_" + dataRoom.getRoomcode() + ".mp3");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                nameTrackInPath = "dangdutasik_file_" + dataRoom.getRoomcode() + ".mp3";

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
                isDownloaded = false;
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
//            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {

            File folder = new File(Environment.getExternalStorageDirectory() + "/dangdutasik");

            sourceTrackFromPath = folder.toString() + "/" + nameTrackInPath;

            isDownloaded = true;

            setPlaylist(tempListData);
            llLoading.setVisibility(View.GONE);
            simpleExoPlayerView.setVisibility(View.VISIBLE);

            Log.e("~~~~", "Download Selesai");

        }

    }

    private void deletedLocalTrack() {
        try {
            File file = new File(sourceTrackFromPath);
            if (file.exists()) {

                boolean result = file.delete();
//                 file.delete();
            } else {
            }
        } catch (Exception e) {
            Log.e("App", "Exception while deleting file " + e.getMessage());
        }
    }

    public void setPlaylist(List<DataKaraoke> dataKaraokes) {

        String tempSourcePath = "";

        if (dataKaraokes.size() > 0) {
            jcAudios.clear();
            for (int i = 0; i < dataKaraokes.size(); i++) {

                if (isDownloaded) {

                    if (!TextUtils.isEmpty(sourceTrackFromPath)) {
                        tempSourcePath = sourceTrackFromPath;

                        jcAudios.add(JcAudio.createFromFilePath(dataKaraokes.get(i).getName(),
                                tempSourcePath,
                                dataKaraokes.get(i).getImage(), dataRoom.getTrackid(),
                                dataKaraokes.get(i).getArtists()));
                        jcPlayerView.initPlaylist(jcAudios);
                    } else {
                        tempSourcePath = dataKaraokes.get(i).getUrl() + dataKaraokes.get(i).getSource();

                        jcAudios.add(JcAudio.createFromURL(dataKaraokes.get(i).getId(),"1",dataKaraokes.get(i).getName(),
                                tempSourcePath,
                                dataKaraokes.get(i).getImage(), dataRoom.getTrackid(),
                                dataKaraokes.get(i).getArtists()));
                        jcPlayerView.initPlaylist(jcAudios);
                    }
                } else {
                    tempSourcePath = dataKaraokes.get(i).getUrl() + dataKaraokes.get(i).getSource();

                    jcAudios.add(JcAudio.createFromURL(dataKaraokes.get(i).getId(),"1",dataKaraokes.get(i).getName(),
                            tempSourcePath,
                            dataKaraokes.get(i).getImage(), dataRoom.getTrackid(),
                            dataKaraokes.get(i).getArtists()));
                    jcPlayerView.initPlaylist(jcAudios);
                }

            }
        }
    }

    private void playingBackSound() {

        jcPlayerView.registerServiceListener(this);
        jcPlayerView.playAudio(jcPlayerView.getMyPlaylist().get(0));
//        jcPlayerView.createNotification();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }


    @Override
    public void showRoomVideo(String roomId) {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showDataKaraoke(DataKaraoke dataKaraoke) {
        sourceTrack = dataKaraoke.getUrl() + dataKaraoke.getSource();

        tempListData.add(dataKaraoke);

        new DownloadTrack().execute(sourceTrack);
    }

    @Override
    public void successReport(String dataTimer) {

    }

    @Override
    public void errorReport(String error) {

    }

    @Override
    public void initPlayer(String rtmpUrl) {

    }

    @Override
    public void showErrorKaraoke(String rtmpUrl) {

    }

    @Override
    public void showVideoNotExist() {

    }
    private void initializePlayer(String urlVideo) {
        simpleExoPlayerView.requestFocus();

        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);

        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, new DefaultLoadControl());

        simpleExoPlayerView.setPlayer(player);

        player.setPlayWhenReady(shouldAutoPlay);
/*        MediaSource mediaSource = new HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null);*/

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(urlVideo),
                mediaDataSourceFactory, extractorsFactory, null, null);

        player.prepare(mediaSource);

        lanscape = true;
    }

    private void releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }
}
