package com.nutech.burgundy.ui.view.schedule;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.lib.gridRecyclerView.GridRecycleView;
import com.nutech.burgundy.presenter.SchedulesPresenter;
import com.nutech.burgundy.ui.adapter.SchedulesAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class ScheduleFragment extends BaseFragment implements ScheduleView {

    @BindView(R.id.rv_schedule)
    GridRecycleView rvSchedule;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.img_nodata)
    ImageView imgNodata;
    @BindView(R.id.tv_msg_error)
    TextView tvMsgError;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.main_error_layout)
    RelativeLayout mainErrorLayout;

    private Unbinder unbinder;
    private SchedulesPresenter presenter;
    private SchedulesAdapter adapter;
    private SessionManager sessionManager;
    private String idMood;
    private String dayOfWeek;
    private String idSchedule;
    private String timer;
    private boolean repeat;
    private String timerTemp;
    private boolean repeatTemp;
    private Context dialogContext;
    private List<DataSchedules> dataSchedules;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SchedulesPresenter(getDataManager(),getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_schedule, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews(view);
        initEvents();
        getData();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void initViews(View view) {

        unbinder = ButterKnife.bind(this, view);

    }

    private void initEvents() {

        btnRetry.setOnClickListener((View v) -> getData());
        swipeRefresh.setOnRefreshListener(this::getData);
    }

    private void getData() {
        swipeRefresh.setRefreshing(true);
        presenter.getScheduleList(sessionManager.getUserStore().getId());
    }

    public void removeSchedule(String idSchedule, String idMood, String dayOfWeek, String storeId, String userId,
                               String timer, String repeat) {
        Map<String, String> params = new HashMap<>();
        params.put("id", idSchedule);
        presenter.removeFromSchedule(params);
        Helper.cancelScheduleMoodtrack(context,Integer.valueOf(idSchedule),storeId,idMood, dayOfWeek,timer,
                repeat.equals("1"));
    }

    public void editSchedulerMoodtrack(Context dialogContext, String idSchedule, String idMood, String day_of_week, String timer,
                                       String timerTemp, boolean repeat,String repeatTemp) {
        this.dialogContext = dialogContext;

        Map<String,String> params = new HashMap<>();
        params.put("id", idSchedule);
        params.put("soundtrack_id", idMood);
        params.put("store_id",getSessionManager().getUserStore().getStore_id());
        params.put("timer", timer);
        params.put("day_of_week", day_of_week);
        params.put("repeat", repeat ? "1":"0");
        params.put("user_id", getSessionManager().getUserStore().getId());


        this.idMood = idMood;
        this.idSchedule = idSchedule;
        this.dayOfWeek = day_of_week;
        this.timer = timer;
        this.repeat = repeat;
        this.timerTemp = timerTemp;
        //jika repeatTemp 1=true
        this.repeatTemp = repeatTemp.contains("1");

        presenter.editToScheduleMoodtrack(params);
    }

    @Override
    public void showSchedules(List<DataSchedules> dataSchedules) {
        try {
            swipeRefresh.setRefreshing(false);
            mainErrorLayout.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dataSchedules.size() > 0) {
            this.dataSchedules = dataSchedules;
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false);
            rvSchedule.setLayoutManager(gridLayoutManager);
            adapter = new SchedulesAdapter(getActivity(), dataSchedules,this);
            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.grid_layout_animation_from_bottom);
            rvSchedule.setLayoutAnimation(animation);
            rvSchedule.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            mainErrorLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorSchedules(String error) {
        try {
            swipeRefresh.setRefreshing(false);
            mainErrorLayout.setVisibility(View.VISIBLE);
            dataSchedules.clear();
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showSuccessRemoveSchedule(String dataSchedules) {
        swipeRefresh.setRefreshing(false);
        mainErrorLayout.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void showErrorRemoveSchedule(String error) {
        swipeRefresh.setRefreshing(false);
        mainErrorLayout.setVisibility(View.GONE);
        getData();
    }

    @Override
    public void showSuccessSchedule(DataResult success) {
Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
     Helper.cancelScheduleMoodtrack(context,Integer.valueOf(idSchedule)
            ,getSessionManager().getUserStore().getStore_id(),idMood, dayOfWeek,timerTemp,repeatTemp);

    Helper.setSchedulerMoodtrack(context,Integer.valueOf(idSchedule)
            ,getSessionManager().getUserStore().getStore_id(),idMood, dayOfWeek,timer,repeat);

    swipeRefresh.setRefreshing(false);
    mainErrorLayout.setVisibility(View.GONE);
    getData();
});
    }

    @Override
    public void showErrorSchedule(String error) {
        Toast.makeText(dialogContext,error,Toast.LENGTH_LONG).show();
    }
}
