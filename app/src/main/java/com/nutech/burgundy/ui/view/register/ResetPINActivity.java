package com.nutech.burgundy.ui.view.register;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.ResetPIN;
import com.nutech.burgundy.presenter.ResetPINPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPINActivity extends BaseAppCompatActivity implements ResetPINView {

    @BindView(R.id.country_code)
    CountryCodePicker countryCodePicker;
    @BindView(R.id.et_phone_number)
    EditText etNumberPhone;
    @BindView(R.id.img_validity)
    ImageView imgValidity;
    @BindView(R.id.tv_validity)
    TextView tvValidity;
    @BindView(R.id.loading)
    AVLoadingIndicatorView avLoadingIndicatorView;
    @BindView(R.id.button_send)
    Button btnSend;

    @OnClick(R.id.button_send)
    public void send(){
        if (TextUtils.isEmpty(etNumberPhone.getText().toString())) {
            etNumberPhone.setError(getString(R.string.err_field_required));
        } else {

            avLoadingIndicatorView.smoothToShow();
            btnSend.setVisibility(View.GONE);
            presenter.resetPIN(countryCodePicker.getFullNumber());
        }
    }

    private ResetPINPresenter presenter;

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new ResetPINPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pin);
        ButterKnife.bind(this);

        registerCarrierEditText();
    }

    private void registerCarrierEditText() {

        countryCodePicker.registerCarrierNumberEditText(etNumberPhone);
        countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                if (isValidNumber) {
                    if (etNumberPhone.getText().toString().startsWith("0")){
                        imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                        tvValidity.setText(R.string.error_wrong_number);
                        tvValidity.setTextColor(Color.parseColor("#FF0000"));
                    } else {
                        imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_validated));
                        tvValidity.setText(R.string.label_valid_number);
                        tvValidity.setTextColor(Color.parseColor("#FFFFFF"));
                    }

                } else {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                    tvValidity.setText(R.string.label_invalid_number);
                    tvValidity.setTextColor(Color.parseColor("#FF0000"));
                }
            }
        });
    }

    @Override
    public void resetSuccess(ResetPIN dataReset) {

        avLoadingIndicatorView.smoothToHide();
        btnSend.setVisibility(View.VISIBLE);

        String msg_reset_pin_sms = getString(R.string.msg_reset_pin_sms_1) + " " +
                dataReset.getActivation_code()+" "+ getString(R.string.msg_reset_pin_sms_2);

        presenter.sendSms(countryCodePicker.getFullNumber(), msg_reset_pin_sms);
        alertShowing(getString(R.string.label_info), getString(R.string.msg_request_new_pin_success)+
                " "+countryCodePicker.getFullNumber(), true);
    }

    @Override
    public void resetFailed(String error) {

        avLoadingIndicatorView.smoothToHide();
        btnSend.setVisibility(View.VISIBLE);
        System.out.println("******** eroor code "+error);
        if (error.equals("200")){
            alertShowing(getString(R.string.label_info), getString(R.string.msg_request_new_pin_success)+
                    " "+countryCodePicker.getFullNumber(), true);
        } else {
            alertShowing(getString(R.string.label_info), getString(R.string.msg_request_new_pin_failed), false);
        }
    }

    private void alertShowing(String title, String message, final boolean status){

        AlertDialog dialogInfo = new AlertDialog.Builder(this).create();
        dialogInfo.setTitle(title);
        dialogInfo.setMessage(message);
        dialogInfo.setCancelable(false);
        dialogInfo.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (status){
                            finish();
                        }
                        dialog.dismiss();
                    }
                });
        dialogInfo.show();
    }

    public void btnBackClicked(View view) {
        finish();
    }
}
