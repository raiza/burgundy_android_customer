package com.nutech.burgundy.ui.view.playlist;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 9/29/17.
 * Xeranta Mobile Solutions
 */

public interface PlaylistDetailView extends BaseView {

    void showArtistsTracks(List<DataTracks> tracksList);

    void showError(String error);
}
