package com.nutech.burgundy.ui.view.soundtrack;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.work.WorkManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.lib.gridRecyclerView.GridRecycleView;
import com.nutech.burgundy.presenter.soundtrack.ListMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.soundtrack.MoodtrackAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class ListMoodTrackFragment extends BaseFragment implements ListMoodtrackView {

    @BindView(R.id.rv_moodtrack)
    GridRecycleView rvMoodtrack;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.main_error_layout)
    RelativeLayout rlMainError;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.fab_add_moodtrack)
    FloatingActionButton fabAddMoodtrack;

    private Unbinder unbinder;
    private ListMoodtrackPresenter presenter;
    private MoodtrackAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Moodtrack> moodtrackList;
    private WorkManager workManager;
    private String idMood;
    private String timer;
    private String dayOfWeek;
    private Context dialogContext;
    private boolean repeat;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ListMoodtrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        workManager = WorkManager.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_moodtracks, container, false);

        initViews(view);
        initEvents(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void loadingData() {
        swipeRefreshLayout.setRefreshing(true);
        if (getSessionManager().getUserType().equals(Constants.UserType.USER_STORE)) {
            presenter.getMoodtrackStore(getSessionManager().getUserStore().getStore_id());
        } else {
            presenter.getData(getSessionManager().getDataMember().getId());
        }
    }

    private void initViews(View view) {

        unbinder = ButterKnife.bind(this, view);
        loadingData();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    private void initEvents(View view) {

        if (getSessionManager().getUserType().equals(Constants.UserType.USER_STORE)) {
            if (!getSessionManager().getUserStore().getRole().equals(Constants.StoreMemberRole.SUPER_ADMIN)) {
                fabAddMoodtrack.setVisibility(View.GONE);
            } else {
                fabAddMoodtrack.setVisibility(View.VISIBLE);
            }
        }
        rvMoodtrack.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvMoodtrack,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        swipeRefreshLayout.setOnRefreshListener(this::loadingData);

        btnRetry.setOnClickListener((View v) -> {
            swipeRefreshLayout.setRefreshing(true);
            loadingData();
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fab_add_moodtrack)
    void moveToPlaylist() {
        navigation.pushFragment(new AddNewSoundtrackFragment(), getString(R.string.label_create_soundtrack), true);
    }

    public void moveToDetail(int position) {
        if (moodtrackList != null) {
            navigation.pushFragment(
                    new DetailMoodtrackFragment(
                            moodtrackList.get(position).getId(), false),
                    moodtrackList.get(position).getName(),
                    true);
        }
    }

    public void removeMoodtrack(String idMood) {
        Map<String, String> params = new HashMap<>();
        params.put("id", idMood);
        presenter.removeMood(params);
    }

    public void setSchedulerMoodtrack(Context dialogContext, String idMood,String day_of_week, String timer, boolean repeat) {
        this.dialogContext = dialogContext;

        Map<String,String> params = new HashMap<>();
        params.put("soundtrack_id", idMood);
        params.put("store_id",getSessionManager().getUserStore().getStore_id());
        params.put("timer", timer);
        params.put("day_of_week", day_of_week);
        params.put("repeat", repeat ? "1":"0");
        params.put("user_id", getSessionManager().getUserStore().getId());

        presenter.addToScheduleMoodtrack(params);

        this.idMood = idMood;
        this.dayOfWeek = day_of_week;
        this.timer = timer;
        this.repeat = repeat;

//        Helper.setSchedulerMoodtrack(context,0,getSessionManager().getUserStore().getStore_id(),idMood,timer,repeat);
    }

    @Override
    public void showMoodtracks(List<Moodtrack> dataMoodtracks) {
        try {
            swipeRefreshLayout.setRefreshing(false);
            rlMainError.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dataMoodtracks.size() > 0) {
            moodtrackList = dataMoodtracks;
            gridLayoutManager = new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false);
            rvMoodtrack.setLayoutManager(gridLayoutManager);
            adapter = new MoodtrackAdapter(getActivity(), getSessionManager(), moodtrackList, ListMoodTrackFragment.this);
            LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.grid_layout_animation_from_bottom);
            rvMoodtrack.setLayoutAnimation(animation);
            rvMoodtrack.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            rlMainError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorMoodtracks(String error) {

        try {
            swipeRefreshLayout.setRefreshing(false);
            rlMainError.setVisibility(View.VISIBLE);
            moodtrackList.clear();
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMoodtracksPlayingStore(List<Moodtrack> dataMoodtracks) {

    }

    @Override
    public void showErrorMoodtracksPlayingStore(String error) {

    }

    @Override
    public void showSuccessRemove(String success) {
        rlMainError.setVisibility(View.GONE);
        loadingData();
    }

    @Override
    public void showErrorRemove(String error) {
        rlMainError.setVisibility(View.GONE);
        loadingData();
    }

    @Override
    public void showSuccessSchedule(String success) {

        Helper.setSchedulerMoodtrack(context,Integer.valueOf(success)
                ,getSessionManager().getUserStore().getStore_id(),idMood,dayOfWeek,timer,repeat);
    }

    @Override
    public void showErrorSchedule(String error) {
        Toast.makeText(dialogContext,error,Toast.LENGTH_LONG).show();
    }
}
