package com.nutech.burgundy.ui.view.home;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.FavoriteTracksPresenter;
import com.nutech.burgundy.ui.adapter.TracksMemberAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 11/17/17.
 * Xeranta Mobile Solutions
 */

public class FavoriteTracksFragment extends BaseFragment implements FavoriteTracksView {

    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.recycle_tracks_favorite)
    RecyclerView rvFavTracks;

    private Unbinder unbinder;
    private FavoriteTracksPresenter presenter;
    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FavoriteTracksPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_tracks, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvFavTracks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFavTracks.setItemAnimator(new DefaultItemAnimator());
        rvFavTracks.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));

        getData();


        return view;
    }

    private void getData() {
        loadingIndicatorView.smoothToShow();
        String memberId = sessionManager.getDataMember().getId();
        presenter.getTracksMember(memberId);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showSuccess(List<DataTracks> dataTracks) {
        loadingIndicatorView.smoothToHide();
        rvFavTracks.setAdapter(new TracksMemberAdapter(FavoriteTracksFragment.this, dataTracks,
                presenter, getSessionManager(), rvFavTracks, navigation));
        rvFavTracks.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvFavTracks,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showSuccessRemove(String success) {
        getActivity().onBackPressed();
    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
    }

    public void toDoNowPlaying(String id, String includeKaraoke, String karaokeId, String sourceTrack) {

        navigation.pushFragment(
                new NowPlayingFragment(
                        id,
                        includeKaraoke,
                        karaokeId, sourceTrack), getString(R.string.label_now_playing),
                true);

    }

}
