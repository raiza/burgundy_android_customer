package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Arif Setiawan on 27/5/19
 * NuTech Development
 */

public class MostPlayedSongsAdapter extends BaseAdapter<MostPlayedSongsAdapter.TopChartHolder>{

    private Context context;
    private List<DataTracks> tracks;

    public MostPlayedSongsAdapter(Context context, List<DataTracks> tracks) {
        this.context = context;
        this.tracks = tracks;
    }

    class TopChartHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_description)
        TextView tvDescription;
        @BindView(R.id.img_cover)
        ImageView imgCover;

        TopChartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public TopChartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_most_played_songs, parent, false);
        return new TopChartHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopChartHolder holder, int position) {

        holder.tvTitle.setText(tracks.get(position).getName());
        holder.tvDescription.setText(tracks.get(position).getArtistnames());

        loadImagesFromURL(context, holder.imgCover, Constants.URL_IMAGE_ARTISTS + tracks.get(position).getArtisimage());
    }

    @Override
    public int getItemCount() {
        return tracks.size();
    }
}
