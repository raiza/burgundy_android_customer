package com.nutech.burgundy.ui.view.splash;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataCheckDevice;
import com.nutech.burgundy.data.model.DataTimer;
import com.nutech.burgundy.presenter.SplashPresenter;
import com.nutech.burgundy.ui.base.BaseCustomActivity;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.ui.view.intro.MainIntrosActivity;
import com.nutech.burgundy.ui.view.register.StoreLoginActivity;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Modified by Arif Setiawan on 29/5/19.
 * Nusantara Teknologi
 */
public class SplashCustomActivity extends BaseCustomActivity implements SplashView {

    @BindView(R.id.iv_logo)
    ImageView iv_logo;
    @BindView(R.id.tv_version)
    TextView tvVersion;
    @BindView(R.id.tv_mood)
    TextView tvMood;

    private Thread mSplashThread;
    private SessionManager sessionManager;
    private SplashPresenter presenter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sessionManager = getSessionManager();
        presenter = new SplashPresenter(getDataManager(), getNetworkHelper(), getSessionManager(), this);
        presenter.attachView(this);

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setStartOffset(1500);
        fadeIn.setDuration(1000);

//        iv_logo.setAnimation(fadeIn);
        tvMood.setAnimation(fadeIn);
        tvVersion.setText("version "+BuildConfig.VERSION_NAME);
        tvVersion.setAnimation(fadeIn);

        getRegisterId();
        TimeWait();

    }

    public void TimeWait() {

        mSplashThread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        // Wait given period of time or exit on touch
                        wait(3501);
                    }
                } catch (InterruptedException ignored) {
                }

                if (!sessionManager.isLoggedIn()) {
                    Intent intent;
                    if (!sessionManager.isIntro()) {
                        intent = new Intent(SplashCustomActivity.this, MainIntrosActivity.class);
                    } else {
                        intent = new Intent(SplashCustomActivity.this, StoreLoginActivity.class);
                    }
                    startActivity(intent);

                } else {
                    sessionManager.isLoggedIn();

                    @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);

                    Map<String, String> params = new HashMap<>();
                    try {
                        if (!sessionManager.getUserType().contains(Constants.UserType.USER_STORE)) {
                            params.put("mobile_phone", sessionManager.getDataMember().getMobile_phone());
                        } else {
                            params.put("email", sessionManager.getUserStore().getEmail());
                        }
                        params.put("device_id", android_id);
                    } catch (Exception ignored) {
                    }

                    System.out.println("Params : "+params);

                    if (!sessionManager.getUserType().contains(Constants.UserType.USER_STORE)) {
                        presenter.getData(params);
                    } else {
                        presenter.getDataStore(params);
                }
                }
            }
        };
        mSplashThread.start();
    }

    @Override
    public void success(DataCheckDevice dataCheckDevice) {
        Intent intent = new Intent(SplashCustomActivity.this, PrimaryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void error(String error) {

        System.out.println("Error = "+error);

        Intent intent = new Intent(SplashCustomActivity.this, BlockedActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

//        dialogWarning();

//        if (error.equals("404")){
//            Intent intent = new Intent(SplashCustomActivity.this, BlockedActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        } else if (error.equals("200")){
//            Intent intent = new Intent(SplashCustomActivity.this, PrimaryActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        } else {
//            Toast.makeText(this, "Tidak dapat terhubung dengan server, mohon coba beberapa saat lagi", Toast.LENGTH_SHORT).show();
//        }

    }

    @Override
    public void successTimer(DataTimer dataTimer) {

    }

    @Override
    public void errorTimer(String error) {

    }

    public void dialogWarning(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Peringatan");
        alertDialog.setMessage("Nomor " + sessionManager.getDataMember().getMobile_phone()+ " sudah dipakai diperangkat yang berbeda."+ "\n" +
                "Anda tidak bisa mengakses aplikasi diperangkat ini.");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Mengerti",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        finishAffinity();
                        System.exit(0);
                    }
                });
        alertDialog.show();
    }

    @SuppressLint("StaticFieldLeak")
    public void getRegisterId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    String token = FirebaseInstanceId.getInstance().getToken();
                    System.out.println("=====" + " : token :" + token);
                    if (token != null || !token.isEmpty()) {

                        sessionManager.setRegisterId(token);
                    }

                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                System.out.println("*** Prepare Registrasion ID ***");
                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }
}
