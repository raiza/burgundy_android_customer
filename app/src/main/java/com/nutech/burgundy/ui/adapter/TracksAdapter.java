package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.TrackPresenter;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class TracksAdapter extends BaseAdapter<TracksAdapter.TracksHolder> {

    private Context context;
    private List<DataTracks> list;
    private SessionManager sessionManager;
    private SparseArray<Float> progressMap = new SparseArray<>();
    private FragmentNavigation fragmentNavigation;
    private String tag;
    private boolean isCheckbox = false;
    private boolean isSelectedAll = false;
    private int longPressPosition = 0;
    private TrackPresenter presenter;

    public interface OnItemCheckListener {
        void onItemCheck(DataTracks dataTracks);
        void onItemUncheck(DataTracks dataTracks);
    }

    private OnItemCheckListener onItemClick;

    public TracksAdapter(Context context, List<DataTracks> list, FragmentNavigation navigation,
                         String tag, OnItemCheckListener onItemCheckListener) {
        this.context = context;
        this.list = list;
        this.fragmentNavigation = navigation;
        this.tag = tag;
        this.onItemClick = onItemCheckListener;

        setContext(context);
    }

    class TracksHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_tracks)
        TextView tvTitleTracks;
        @BindView(R.id.rl_toucher)
        RelativeLayout rlToucher;
        @BindView(R.id.tv_title_album)
        TextView tvTitleAlbum;
        @BindView(R.id.ll_play)
        LinearLayout llPlay;
        @BindView(R.id.btn_options)
        ImageView ivOptions;
        @BindView(R.id.bt_sing)
        Button btSing;
        @BindView(R.id.cb_select)
        CheckBox cbSelect;

        View childView;

        TracksHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
            presenter = new TrackPresenter(getDataManager(), getNetworkHelper());
        }
    }

    @Override
    public TracksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_tracks, parent, false);
        sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        return new TracksHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final TracksHolder holder, final int position) {

        if (!TextUtils.isEmpty(list.get(position).getInclude_karaoke())) {
            if (list.get(position).getInclude_karaoke().equals("1")) {
                holder.btSing.setVisibility(View.VISIBLE);
            } else {
                holder.btSing.setVisibility(View.GONE);
            }
        } else {
            holder.btSing.setVisibility(View.GONE);
        }

        if (isCheckbox) {
            holder.cbSelect.setVisibility(View.VISIBLE);
            holder.ivOptions.setVisibility(View.GONE);
        } else {
            holder.cbSelect.setVisibility(View.GONE);
            holder.ivOptions.setVisibility(View.VISIBLE);
        }

        if (isCheckbox) {
            if (isSelectedAll) {
                holder.cbSelect.setChecked(true);
//                onItemClick.onItemCheck(list.get(position));
            } else {
                holder.cbSelect.setChecked(false);
//                onItemClick.onItemUncheck(list.get(position));
            }
        }

        if (longPressPosition == position) {
            holder.cbSelect.setChecked(true);
            longPressPosition = -1;
        }

        holder.tvTitleTracks.setText(list.get(position).getName());

        holder.tvTitleAlbum.setText(
                list.get(position).getAlbum_name() + " - "
                        + list.get(position).getArtistnames());

        holder.rlToucher.setOnClickListener(view -> {

            if (isCheckbox) {
                if (!holder.cbSelect.isChecked()) {
                    holder.cbSelect.setChecked(true);

                    onItemClick.onItemCheck(list.get(position));
                } else {
                    holder.cbSelect.setChecked(false);

                    onItemClick.onItemUncheck(list.get(position));
                }
            }
//            fragmentNavigation.playingMusic(position, list);
//            toDoNowPlaying(
//                    list.get(position).getId(),
//                    list.get(position).getInclude_karaoke(),
//                    list.get(position).getKaraoke_id(),
//                    list.get(position).getName(),
//                    list.get(position).getArtistnames(),
//                    list.get(position).getUrl()+list.get(position).getSource());
        });

        holder.llPlay.setOnClickListener(view -> {
//            fragmentNavigation.playingMusic(position, list);
//            toDoNowPlaying(
//                    list.get(position).getId(),
//                    list.get(position).getInclude_karaoke(),
//                    list.get(position).getKaraoke_id(),
//                    list.get(position).getName(),
//                    list.get(position).getArtistnames(),
//                    list.get(position).getUrl() + list.get(position).getSource());
        });

        holder.ivOptions.setOnClickListener(view -> {
            showOptionsTracks(holder.ivOptions, position, view);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void showOptionsTracks(ImageView btnOptions, final int position, View view) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.tracks_options);
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.add:

                    break;
                case R.id.play:
                    Log.e("***Tracks Id = ", list.get(position).getId() + " ***");
                    fragmentNavigation.playingMusic(position, list);
                    toDoNowPlaying(
                            list.get(position).getId(),
                            list.get(position).getInclude_karaoke(),
                            list.get(position).getKaraoke_id(), list.get(position).getName(), list.get(position).getArtistnames(), list.get(position).getUrl() + list.get(position).getSource());
                    break;
                case R.id.share:
                    String applicationName = context.getResources().getString(R.string.app_name);
                    String content = context.getResources().getString(R.string.content_share) + " " +
                            list.get(position).getName() + "." +
                            context.getResources().getString(R.string.content_share_only_this_app) + " " +
                            applicationName + " " + context.getResources().getString(R.string.content_share_only_this_app) + "\n" +
                            context.getResources().getString(R.string.content_share_download_app);
                    Helper.shareContent(context, content, applicationName, sessionManager.getDataMember().getId(), Constants.ActivityPoint.SHARE_SOSMED);

                    break;
                case R.id.addtoplaylist:
                    Intent intent = new Intent(context, MoodListDialogActivity.class);
                    intent.putExtra("track_id", list.get(position).getId());
                    intent.putExtra("track_name", list.get(position).getName());
                    context.startActivity(intent);
                    break;
            }
            return false;
        });
        makePopForceShowIcon(popup);
    }

    private void toDoNowPlaying(String id, String includeKaraoke, String karaokeId, String name,
                                String artistnames, String sourceTrack) {

        fragmentNavigation.pushFragment(
                new NowPlayingFragment(id, includeKaraoke, karaokeId, sourceTrack), name,
                true);

    }

    public void selectAllItem() {
        isSelectedAll = true;
        notifyDataSetChanged();
    }

    public void unSelectAllItem() {
        isSelectedAll = false;
        notifyDataSetChanged();
    }

    public void isSelector(boolean val, int position) {
        this.isCheckbox = val;
        this.longPressPosition = position;
        notifyDataSetChanged();
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup = popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
