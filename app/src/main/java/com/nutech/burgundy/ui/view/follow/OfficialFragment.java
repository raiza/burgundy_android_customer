package com.nutech.burgundy.ui.view.follow;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataOfficial;
import com.nutech.burgundy.presenter.OfficialPresenter;
import com.nutech.burgundy.ui.adapter.OfficialAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.utils.Helper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class OfficialFragment extends BaseFragment implements OfficialView {

    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recycle_official)
    RecyclerView rvOfficial;

    private OfficialPresenter officialPresenter;
    private Unbinder unbinder;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        officialPresenter = new OfficialPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        officialPresenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_official, container, false);
        unbinder = ButterKnife.bind(this, view);

        initViews();
        initEvents();
        getData();

        return view;
    }

    private void initViews(){
        rvOfficial.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvOfficial.setItemAnimator(new DefaultItemAnimator());
        rvOfficial.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
    }

    private void initEvents(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getData();
            }
        });
    }

    private void getData(){
        loadingIndicatorView.smoothToShow();
        officialPresenter.getAllOfficial();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showDataOfficial(final List<DataOfficial> listDataOfficial) {
        loadingIndicatorView.smoothToHide();
        swipeRefreshLayout.setRefreshing(false);
        rvOfficial.setAdapter(new OfficialAdapter(getActivity(), listDataOfficial));
        rvOfficial.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvOfficial,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                        String url = listDataOfficial.get(position).getMedsos_url();

                        officialPresenter.updateStatusPoint(Constants.ActivityPoint.FOLLOW_OFFICIAL);

                        if (url.contains("facebook")){
                            Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
                            String facebookUrl = Helper.getOpenFacebook(getActivity(), url);
                            facebookIntent.setData(Uri.parse(facebookUrl));
                            startActivity(facebookIntent);

                        } else if (url.contains("instagram")){
                            Helper.getOpenInstagram(getActivity(), url);
                        } else if (url.contains("twitter")){
                            Helper.getOpenTwitter(getActivity(), url);
                        } else if (url.contains("youtube")){
                            Helper.getOpenYoutube(getActivity(), url);
                        } else {

                        }

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showError(String error) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
    }
}
