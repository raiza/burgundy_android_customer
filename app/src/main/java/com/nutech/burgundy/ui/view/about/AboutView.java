package com.nutech.burgundy.ui.view.about;

import com.nutech.burgundy.data.model.DataAbout;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public interface AboutView extends BaseView {

    void showData(DataAbout dataAbout);

    void showError(String error);

}
