package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.stores.NearStoreListFragment;
import com.nutech.burgundy.utils.Helper;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NearStoreListAdapter extends BaseAdapter<NearStoreListAdapter.StoreHolder> {

    private Context context;
    private List<DataStore> storeList;
    private NearStoreListFragment parent;

    public NearStoreListAdapter(Context context, List<DataStore> storeList, NearStoreListFragment storeListFragment) {
        this.context = context;
        this.storeList = storeList;
        this.parent = storeListFragment;
    }

    @NonNull
    @Override
    public StoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_near_store, parent, false);
        return new StoreHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreHolder holder, int position) {
        String url = Constants.URL_STORE_LOGO+storeList.get(position).getLogo();
        String distance = Helper.getDistance(storeList.get(position).getDistance());

        holder.tvStoreDistance.setText(distance);
        holder.tvStoreName.setText(storeList.get(position).getSite_name());
        holder.tvStoreAddress.setText(storeList.get(position).getAddress());

        Picasso.with(context)
                .load(url)
                .into(holder.ivStoreLogo);

        holder.tvStoreAddress.setOnClickListener(view -> {
            if (holder.tvStoreAddress.getEllipsize() == TextUtils.TruncateAt.END) {
                holder.tvStoreAddress.setMaxLines(Integer.MAX_VALUE);
                holder.tvStoreAddress.setEllipsize(null);
            } else {
                holder.tvStoreAddress.setMaxLines(3);
                holder.tvStoreAddress.setEllipsize(TextUtils.TruncateAt.END);
            }
        });

        holder.ivStoreLogo.setOnClickListener(view -> parent.moveToDetail(position));
        holder.tvStoreName.setOnClickListener(view -> parent.moveToDetail(position));

    }

    @Override
    public int getItemCount() {
        return storeList.size();
    }

    class StoreHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_store_logo)
        ImageView ivStoreLogo;
        @BindView(R.id.tv_store_name)
        TextView tvStoreName;
        @BindView(R.id.tv_store_address)
        TextView tvStoreAddress;
        @BindView(R.id.tv_store_distance)
        TextView tvStoreDistance;
        @BindView(R.id.cv_stores)
        CardView cvStores;

        StoreHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
