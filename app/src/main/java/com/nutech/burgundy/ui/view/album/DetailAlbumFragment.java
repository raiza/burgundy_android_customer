package com.nutech.burgundy.ui.view.album;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.AlbumsPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;
import com.nutech.burgundy.utils.Helper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * Created by Arif Setiawan on 10/10/17.
 * Xeranta Mobile Solutions
 */

public class DetailAlbumFragment extends BaseFragment implements AlbumView {

    @BindView(R.id.iv_photo_albums)
    CircleImageView ivPhotoAlbums;
    @BindView(R.id.iv_backdrop)
    ImageView ivBackdrop;
    @BindView(R.id.rv_tracks_albums)
    RecyclerView rvTracksAlbums;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.fab_play)
    FloatingActionButton fabPlay;

    private Unbinder unbinder;
    private String albumId, albumImage;
    private TracksAdapter adapter;
    private AlbumsPresenter presenter;
    private List<DataTracks> mTracksList;
    private List<String> selectedTrack;
    private boolean isCheckbox = false;

    public DetailAlbumFragment(String id, String image) {
        this.albumId = id;
        this.albumImage = image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new AlbumsPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_album, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadingIndicatorView.smoothToShow();
        presenter.getAlbumsTracks(albumId);
        selectedTrack = new ArrayList<>();

        initViews();
        initEvents();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_track_moodtrack,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_to_moodtrack :
                Intent intent = new Intent(context, MoodListDialogActivity.class);
                intent.putExtra("track_id", Helper.convertStringArrayToString(selectedTrack,","));
                intent.putExtra("track_name", R.string.label_tracks);
                context.startActivity(intent);
                break;
            case R.id.select_all :
                adapter.selectAllItem();
                for (DataTracks tracks : mTracksList) {
                    selectedTrack.add(tracks.getId());
                }
                break;
            case R.id.unselect_all :
                adapter.unSelectAllItem();
                selectedTrack.clear();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void initViews(){

        String ImageUrl = Constants.URL_IMAGE_ALBUMS + albumImage;
        loadImagesAvatarFromURL(context, ivPhotoAlbums, ImageUrl);

        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.placeholder)
                .transform(new BlurTransformation(getActivity()))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivBackdrop);
    }

    private void initEvents() {

        fabPlay.setOnClickListener(view1 -> {
            System.out.println("@@@@@@ === "+mTracksList.size());
            if (mTracksList.size() > 0){
                navigation.playingMusic(0, mTracksList);
                navigation.pushFragment(
                        new NowPlayingFragment(
                                mTracksList.get(0).getId(),
                                mTracksList.get(0).getInclude_karaoke(),
                                mTracksList.get(0).getKaraoke_id(),
                                mTracksList.get(0).getUrl() + mTracksList.get(0).getSource()),
                        mTracksList.get(0).getName(),
                        true);
            }
        });
    }

    public boolean isCheckbox() {
        return isCheckbox;
    }

    public void setCheckbox( boolean checkbox) {
        if (!checkbox) {
            setHasOptionsMenu(checkbox);
            adapter.isSelector(false,0);
            selectedTrack.clear();
        }

        isCheckbox = checkbox;
    }

    @Override
    public void showDataAlbumsTracks(final List<DataTracks> dataTracksList) {

        mTracksList = dataTracksList;

        loadingIndicatorView.smoothToHide();
        rvTracksAlbums.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracksAlbums.setItemAnimator(new DefaultItemAnimator());
        rvTracksAlbums.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        adapter = new TracksAdapter(getActivity(), dataTracksList, navigation, "",
                new TracksAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(DataTracks dataTracks) {
                selectedTrack.add(dataTracks.getId());
            }

            @Override
            public void onItemUncheck(DataTracks dataTracks) {
                selectedTrack.remove(dataTracks.getId());
            }
        });
        rvTracksAlbums.setAdapter(adapter);

        rvTracksAlbums.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTracksAlbums,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

//                        navigation.playingMusic(position, dataTracksList);
//                        toDoNowPlaying();

                    }

                    @Override
                    public void onLongClick(View view, int position) {
                        if (!isCheckbox) {
                            adapter.isSelector(true,position);
                            isCheckbox = true;
                            selectedTrack.add(mTracksList.get(position).getId());
                            setHasOptionsMenu(true);
                        }
                    }
                }));
    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
