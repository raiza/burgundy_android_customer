package com.nutech.burgundy.ui.extention;

/**
 * Created by Arif Setiawan on 9/24/17.
 * Xeranta Mobile Solutions
 */

public class MenuItem {

    String ItemName;
    int imgResID;

    public MenuItem(String itemName, int imgResID) {
        super();
        ItemName = itemName;
        this.imgResID = imgResID;
    }

    public String getItemName() {
        return ItemName;
    }
    public void setItemName(String itemName) {
        ItemName = itemName;
    }
    public int getImgResID() {
        return imgResID;
    }
    public void setImgResID(int imgResID) {
        this.imgResID = imgResID;
    }
}
