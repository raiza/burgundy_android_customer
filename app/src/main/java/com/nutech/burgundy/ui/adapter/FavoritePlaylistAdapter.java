package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.presenter.FavoritePlaylistPresenter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class FavoritePlaylistAdapter extends RecyclerView.Adapter<FavoritePlaylistAdapter.PlaylistHolder> {

    private Context context;
    private List<DataPlaylistMember> list;
    private FragmentNavigation fragmentNavigation;
    private FavoritePlaylistPresenter playlistPresenter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;
    private RecyclerView recyclerView;

    public FavoritePlaylistAdapter(
            Context context, List<DataPlaylistMember> list, FragmentNavigation navigation,
            FavoritePlaylistPresenter presenter, SessionManager sessionManager, RecyclerView rvFavPlaylist) {
        this.context = context;
        this.list = list;
        this.fragmentNavigation = navigation;
        this.playlistPresenter = presenter;
        this.sessionManager = sessionManager;
        this.dmApplication = (DmApplication) context.getApplicationContext();
        this.recyclerView = rvFavPlaylist;

    }
    class PlaylistHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_playlist_name)
        TextView tvPlaylistName;
        @BindView(R.id.tv_count_tracks)
        TextView tvCountTracks;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.iv_options)
        ImageView ivOptions;
        @BindView(R.id.iv_playlist)
        ImageView ivPlaylist;
        @BindView(R.id.rl_more)
        RelativeLayout rlMore;
        @BindView(R.id.ll_playlist)
        LinearLayout llPlaylist;
        View childView;

        PlaylistHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public PlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_playlist_favorite, parent, false);
        return new PlaylistHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaylistHolder holder, final int position) {

        holder.tvPlaylistName.setText(list.get(position).getName());
        holder.tvCountTracks.setText(list.get(position).getCount_tracks());
        holder.tvLikes.setText(list.get(position).getLikes());

        String ImageUrl = Constants.URL_IMAGE_PLAYLIST + list.get(position).getImage();
        Picasso.with(context.getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivPlaylist);

        holder.ivOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(context, holder.ivOptions);
                popup.inflate(R.menu.fav_playlist_options);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(android.view.MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.remove:
                                String memberId = sessionManager.getDataMember().getId();
                                Map<String, String> params = new HashMap<String, String>();
                                try {
                                    params.put("customer_id", memberId);
                                    params.put("playlist_id", list.get(position).getId());
                                } catch (Exception e) {

                                }
                                System.out.println("** Param ** "+ params);
                                playlistPresenter.removePlaylistMember(params);
                                remove(position, list);
                                break;
                            case R.id.share:
                                String applicationName = context.getResources().getString(R.string.app_name);
                                String content = context.getResources().getString(R.string.content_share)+" "+
                                        list.get(position).getName()+"." +
                                        context.getResources().getString(R.string.content_share_only_this_app)+" "+
                                        applicationName+" "+context.getResources().getString(R.string.content_share_only_this_app)+"\n"+
                                        context.getResources().getString(R.string.content_share_download_app);
                                Helper.shareContent(context, content, applicationName, sessionManager.getDataMember().getId(), Constants.ActivityPoint.SHARE_SOSMED);
                                playlistPresenter.updateStatusPoint(
                                        sessionManager.getDataMember().getId(),
                                        Constants.ActivityPoint.SHARE_SOSMED);

                                break;
                        }
                        return false;
                    }
                });
                makePopForceShowIcon(popup);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }

    private void remove(int position, List<DataPlaylistMember> list) {
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(true);

        System.out.println("position: "+position);
        list.remove(position);
        notifyItemRemoved(position);

        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }
}
