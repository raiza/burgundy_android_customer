package com.nutech.burgundy.ui.view.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.presenter.MyMusicPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.SegmentedButton;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.ui.view.schedule.ScheduleFragment;
import com.nutech.burgundy.ui.view.soundtrack.ListMoodTrackFragment;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 5/29/19.
 * Nusantara Teknologi Indonesia
 */

public class FragmentMyMusic extends BaseFragment implements MyMusicView {

    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.segmented_tabs)
    SegmentedButton segmentedButton;
    @BindView(R.id.main_frame)
    FrameLayout mainFrame;

    private Unbinder unbinder;
    private MyMusicPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MyMusicPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mymusic, container, false);
        initViews(view);
        initEvents(view);

        return view;
    }

    private void initViews(View view) {
        unbinder = ButterKnife.bind(this, view);
        setupTabsLayout();
    }

    private void initEvents(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
//        view.setOnKeyListener((v, keyCode, event) -> {
//            Log.e("masuk","asik");
//            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
//                Intent intent = new Intent(getActivity(), PrimaryActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                return true;
//            }
//            return false;
//        });
    }

    private void setupTabsLayout() {
        segmentedButton.clearButtons();
        segmentedButton.addButtons(
                getResources().getString(R.string.label_mood_track),
                getResources().getString(R.string.label_schedule));

        segmentedButton.setPushedButtonIndex(0);
        try {
            replaceFragment(new ListMoodTrackFragment());
        } catch (Exception e) {
            e.printStackTrace();
        }

        segmentedButton.setOnClickListener((SegmentedButton.OnClickListenerSegmentedButton) index -> {
            Log.e("segment",String.valueOf(index));
            if (index == 0) {
                replaceFragment(new ListMoodTrackFragment());
            } else if (index == 1) {
                replaceFragment(new ScheduleFragment());
            }
        });
    }

    private void replaceFragment(Fragment fragment) {
        try {
            if (mainFrame != null) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .replace(mainFrame.getId(), fragment).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showListPlaylistRegistered(List<DataPlaylistMember> dataPlaylistMembers) {
         loadingIndicatorView.smoothToHide();
    }

    @Override
    public void showSuccesRemove(String data) {

        Toast.makeText(getActivity(), "Removed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {

        try {
            loadingIndicatorView.smoothToHide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
