package com.nutech.burgundy.ui.view.track;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.presenter.soundtrack.DetailMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.JcAudioAdapter;
import com.nutech.burgundy.ui.adapter.SimpleItemTouchHelperCallback;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackView;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Arie Gunawan 17 Desember 2019
 * Nusantara Teknologi Indonesia
 */
public class ListTrackMoodtrackFragment extends BaseFragment implements
        DetailMoodtrackView,
        JcPlayerView.JcPlayerViewServiceListener {

    @BindView(R.id.rv_tracks)
    RecyclerView rvTracks;

    private List<JcAudio> audioList;
    private Unbinder unbinder;
    private JcAudioAdapter adapter;
    private JcPlayerView jcPlayerView;
    private DetailMoodtrackPresenter presenter;
    private SessionManager sessionManager;

    public ListTrackMoodtrackFragment(List<JcAudio> audioList, JcPlayerView jcPlayerView) {
        this.audioList = audioList;
        this.jcPlayerView = jcPlayerView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new DetailMoodtrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_track_moodtrack, container, false);
        unbinder = ButterKnife.bind(this, view);

        initEvents();
        initViews();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_track_moodtrack, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    void initViews() {
        rvTracks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracks.setItemAnimator(new DefaultItemAnimator());
        rvTracks.addItemDecoration(new DividerItemDecoration(
                Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL));
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);
        rvTracks.setLayoutAnimation(animation);
        adapter = new JcAudioAdapter(context, audioList, this, navigation, jcPlayerView);
        rvTracks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracks.addItemDecoration(new DividerItemDecoration(
                Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL));
        rvTracks.setAdapter(adapter);
        jcPlayerView.registerServiceListener(this);


        if (getSessionManager().getUserStore().getRole().equals(Constants.StoreMemberRole.SUPER_ADMIN)) {
            ItemTouchHelper.Callback callback =
                    new SimpleItemTouchHelperCallback(adapter);
            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
            touchHelper.attachToRecyclerView(rvTracks);
        }

    }

    void initEvents() {
    }

    public void updatePositionTracks(JcAudio track,
                                     int fromPosition, int toPosition) {
        presenter.updatePositionMoodtrackTracks(sessionManager.getCurrentMoodTrack(),
                String.valueOf(track.getId()),
                String.valueOf(fromPosition), String.valueOf(toPosition));
    }

    @Override
    public void showMoodtracks(Moodtrack dataMoodtracks) {

    }

    @Override
    public void showErrorMoodtracks(String error) {

    }

    @Override
    public void showSuccessUpdatePosition(String success) {

    }

    @Override
    public void showErrorUpdatePosition(String error) {

    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onCompletedAudio() {

    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onContinueAudio() {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void addLogAds(String id, String trackId) {

    }

    @Override
    public void onTimeChanged(long currentTime) {

    }

    @Override
    public void updateTitle(String title, String artisName) {

    }

    @Override
    public void updateCover(String cover) {

    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {

    }

    @Override
    public void removeNotification() {

    }
}
