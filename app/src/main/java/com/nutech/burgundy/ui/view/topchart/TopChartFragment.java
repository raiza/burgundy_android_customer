package com.nutech.burgundy.ui.view.topchart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.TopChartPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * @author Dafian on 10/4/17
 */

public class TopChartFragment extends BaseFragment implements TopChartView {

    @BindView(R.id.image_backdrop)
    ImageView imageBackdrop;
    @BindView(R.id.image_playlist)
    CircleImageView imagePlaylist;
    @BindView(R.id.rv_top_chart)
    RecyclerView rvTopChart;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private Unbinder unbinder;
    private TopChartPresenter presenter;
    private DmApplication dmApplication;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new TopChartPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        dmApplication = (DmApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_chart, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvTopChart.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTopChart.setItemAnimator(new DefaultItemAnimator());
        rvTopChart.addItemDecoration(new DividerItemDecoration(
                Objects.requireNonNull(getActivity()), DividerItemDecoration.VERTICAL));

        getData();

        return view;
    }

    private void getData(){
        loadingIndicatorView.smoothToShow();
        presenter.getTopChart(10);
    }

    @Override
    public void showTopChart(final List<DataTracks> tracks) {

        loadingIndicatorView.smoothToHide();
        if (tracks.size() > 0) {

            String ImageUrl = Constants.URL_IMAGE_ALBUMS + tracks.get(0).getAlbumImage();

            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .resize(Constants.Resize.sizeWeigth, Constants.Resize.sizeHeigth)
                    .into(imagePlaylist);

            Picasso.with(getActivity())
                    .load(ImageUrl)
                    .error(R.drawable.image_dm_error)
                    .transform(new BlurTransformation(Objects.requireNonNull(getActivity())))
                    .fit()
                    .into(imageBackdrop);
        }

        rvTopChart.setAdapter(new TracksAdapter(getActivity(), tracks, navigation, "",
                new TracksAdapter.OnItemCheckListener() {
                    @Override
                    public void onItemCheck(DataTracks dataTracks) {

                    }

                    @Override
                    public void onItemUncheck(DataTracks dataTracks) {

                    }
                }));
        rvTopChart.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTopChart,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showError(String error) {

        loadingIndicatorView.smoothToHide();

//        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
