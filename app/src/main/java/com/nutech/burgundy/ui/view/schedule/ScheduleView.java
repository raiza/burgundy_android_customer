package com.nutech.burgundy.ui.view.schedule;

import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

public interface ScheduleView extends BaseView {

    void showSchedules(List<DataSchedules> dataSchedules);

    void showErrorSchedules(String error);

    void showSuccessRemoveSchedule(String dataSchedules);

    void showErrorRemoveSchedule(String error);

    void showSuccessSchedule(DataResult success);

    void showErrorSchedule(String error);
}
