package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.utils.Helper;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StorePlayingMoodtrackAdapter extends BaseAdapter<StorePlayingMoodtrackAdapter.StorePlayingMoodtrackHolder> {

    private Context context;
    private List<Moodtrack> moodtrackList;

    public StorePlayingMoodtrackAdapter(Context context, List<Moodtrack> moodtrackList) {
        this.context = context;
        this.moodtrackList = moodtrackList;
    }

    class StorePlayingMoodtrackHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_moodtrack_cover)
        ImageView ivMoodtrackCover;
        @BindView(R.id.tv_moodtrack_name)
        TextView tvMoodtrackName;
        @BindView(R.id.tv_store_name)
        TextView tvStoreName;
        @BindView(R.id.tv_distance)
        TextView tvDistance;

        StorePlayingMoodtrackHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public StorePlayingMoodtrackHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_store_playing_mood_track, parent, false);
        return new StorePlayingMoodtrackHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull StorePlayingMoodtrackHolder holder, int position) {
        String url = Constants.URL_IMAGE_MOODTRACK+moodtrackList.get(position).getImage();
        String distance = Helper.getDistance(moodtrackList.get(position).getDistance());

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        ColorDrawable cd = new ColorDrawable(color);

        holder.tvDistance.setText(distance);
        holder.tvMoodtrackName.setText(moodtrackList.get(position).getName());
        holder.tvStoreName.setText(moodtrackList.get(position).getSiteName());

        loadImagesMoodFromURL(context, holder.ivMoodtrackCover, url, cd);


    }

    @Override
    public int getItemCount() {
        return moodtrackList.size();
    }
}
