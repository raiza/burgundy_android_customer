package com.nutech.burgundy.ui.view.register;

import com.nutech.burgundy.data.model.ResetPIN;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 07/05/18.
 * Xeranta Mobile Solutions
 */
public interface ResetPINView extends BaseView {

    void resetSuccess(ResetPIN dataReset);

    void resetFailed(String error);
}
