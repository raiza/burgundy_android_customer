package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataGenre;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.TimelineHolder> {

    private Context context;
    private List<DataGenre> list;

    public GenreAdapter(
            Context context, List<DataGenre> list) {
        this.context = context;
        this.list = list;
    }

    class TimelineHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_genre_name)
        TextView tvGenre;
        @BindView(R.id.image_genre)
        ImageView ivGenre;
        View childView;

        TimelineHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public TimelineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_genre, parent, false);
        return new TimelineHolder(view);
    }

    @Override
    public void onBindViewHolder(TimelineHolder holder, int position) {

        holder.tvGenre.setText(list.get(position).getName().replace("Dangdut",""));

        String ImageUrl = Constants.URL_IMAGE_GENRES+list.get(position).getImage();
        Picasso.with(context.getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivGenre);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
