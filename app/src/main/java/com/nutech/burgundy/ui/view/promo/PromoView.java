package com.nutech.burgundy.ui.view.promo;

import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 12/29/17.
 * Xeranta Mobile Solutions
 */

public interface PromoView extends BaseView {

    void showPromoDetail(DataPromoSlide dataPromoSlide);

    void showError(String error);
}
