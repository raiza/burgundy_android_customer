package com.nutech.burgundy.ui.view.home;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.ui.adapter.GeneralPagerAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class TabHomeFragment extends BaseFragment {

    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager_home)
    ViewPager pagerHome;

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        setupViewPager(pagerHome);
        tabs.setupWithViewPager(pagerHome);
        setupTabIcons();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setupViewPager(ViewPager viewPager) {

        GeneralPagerAdapter adapter = new GeneralPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new FragmentMyMusic(), "My Music");
        adapter.addFragment(new FragmentDiscover(), "Discover");
        adapter.addFragment(new FragmentDiscover(), "Streaming");

        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {

        int[] iconList = {
                R.drawable.ic_action_mymusic, R.drawable.ic_action_home, R.drawable.ic_action_live};

        for (int i = 0; i < iconList.length; i++) {

            LinearLayout ll = (LinearLayout) LayoutInflater.from(getActivity())
                    .inflate(R.layout.custom_tabs, pagerHome, false);
            ImageView ivIcon = (ImageView) ll.findViewById(R.id.iv_icon_tabs);
            ivIcon.setImageDrawable(ContextCompat.getDrawable(getActivity(), iconList[i]));
            tabs.getTabAt(i).setCustomView(ivIcon);
        }
    }
}
