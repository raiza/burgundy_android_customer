package com.nutech.burgundy.ui.view.playback;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;

import java.util.ArrayList;
import java.util.List;

public class InitPlaylistAsync extends AsyncTask<List<DataTracks>,Void ,List<JcAudio>> {
    private List<JcAudio> jcAudios = new ArrayList<>();
    private PlaylistView listener;
    private String status;
    @SuppressLint("StaticFieldLeak")
    private JcPlayerView jcPlayerView;

    public InitPlaylistAsync(Context context, JcPlayerView jcPlayerView,String status){
        listener = (PlaylistView) context;
        this.jcPlayerView = jcPlayerView;
        this.status = status;
    }

    @SuppressLint("WrongThread")
    @SafeVarargs
    @Override
    protected final List<JcAudio> doInBackground(List<DataTracks>... lists) {
        List<DataTracks> dataTracksList = lists[0];

        if (!isCancelled()) {
            if (dataTracksList.size() > 0) {
                jcAudios.clear();
                for (int i = 0; i < dataTracksList.size(); i++) {
                    Log.e("initial", String.valueOf(i));
                    jcAudios.add(JcAudio.createFromURL(dataTracksList.get(i).getId(),
                            dataTracksList.get(i).getPosition(),dataTracksList.get(i).getName(),
                            dataTracksList.get(i).getUrl() + dataTracksList.get(i).getSource(),
                            Constants.URL_IMAGE_ALBUMS+dataTracksList.get(i).getAlbumImage(), dataTracksList.get(i).getId(),
                            dataTracksList.get(i).getArtistnames()));

                    if (i == 0) {
                        jcPlayerView.initPlaylist(jcAudios);
                    }
                }
            }
            return jcAudios;
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<JcAudio> jcAudios) {
        jcPlayerView.initPlaylist(jcAudios);
        if (status.equals(Constants.InitPlayistAsyncStatus.PLAY)) {
            listener.onFinishedInitPlaylist(jcAudios);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}