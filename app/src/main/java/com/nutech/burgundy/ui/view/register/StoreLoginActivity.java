package com.nutech.burgundy.ui.view.register;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.presenter.LoginPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.extention.PinView;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.utils.Helper;
import com.roger.gifloadinglibrary.GifLoadingView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressCustom;
import cc.cloudist.acplibrary.ACProgressPie;

public class StoreLoginActivity extends BaseAppCompatActivity implements LoginView {

    private static final String TAG = "StoreLoginActivity";
    public static final int REQUEST_CODE = 100;

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    @BindView(R.id.country_code)
    CountryCodePicker countryCodePicker;
    @BindView(R.id.img_validity)
    ImageView imgValidity;
    @BindView(R.id.tv_validity)
    TextView tvValidity;
    @BindView(R.id.pv_pin_login)
    PinView pvPinLogin;
    @BindView(R.id.tv_other_login)
    TextView tvOtherLogin;
    @BindView(R.id.button_login)
    Button buttonLogin;
    @BindView(R.id.phoneAuthFields)
    LinearLayout phoneAuthFields;
    @BindView(R.id.main_layout)
    LinearLayout mainLayout;
    @BindView(R.id.tiet_email)
    TextInputEditText tietEmail;
    @BindView(R.id.til_email)
    TextInputLayout tilEmail;
    @BindView(R.id.tiet_password)
    TextInputEditText tietPassword;
    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private EditText mPhoneNumberField;

    private String android_id = "";
    private LoginPresenter presenter;
    private SessionManager sessionManager;
    private ACProgressCustom  pDialog;
    private String idMood = "";
    private String storeId = "";
    private String status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_login);
        ButterKnife.bind(this);
        presenter = new LoginPresenter(getDataManager(), getNetworkHelper(), getSessionManager(), getApplicationContext());
        sessionManager = getSessionManager();
        presenter.attachView(this);
        pDialog = new ACProgressCustom.Builder(this)
                .speed(5.0f)
                .useImages(R.drawable.image_default_error,R.drawable.image_default_error_black,R.drawable.image_default_error,R.drawable.image_default_error_black)
                .build();

        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            idMood = intent.getStringExtra("moodtrack_id");
            storeId = intent.getStringExtra("store_id");
            status = intent.getStringExtra("status");
            Log.e(TAG,idMood);
        }

        if (sessionManager.isLoggedIn()) {
            sucessLoginStore();
        }

        // Restore instance state
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        // Assign views

        mPhoneNumberField = findViewById(R.id.fieldPhoneNumber);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermssion();
        }
        getRegisterId();
        countryCodePicker.setDefaultCountryUsingNameCode("ID");
        registerCarrierEditText();

        if (BuildConfig.DEBUG) {
            mPhoneNumberField.setText("85319315391");
        }

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                nextTo();

            }
        };
        // [END phone_auth_callbacks]
    }

    @SuppressLint("HardwareIds")
    @OnClick(R.id.button_login)
    public void toDoNext() {

        String imei0 = Helper.getIMEI(this, 0);

        Helper.hideSoftKeyBoard(this);

        if (TextUtils.isEmpty(Objects.requireNonNull(tietEmail.getText()).toString()) || TextUtils.isEmpty(tietPassword.getText().toString())) {
            if (TextUtils.isEmpty(tietEmail.getText().toString())) {
                tilEmail.setError(getString(R.string.err_field_required));
            } else {
                tilPassword.setError(getString(R.string.err_field_required));
            }
        } else {
            pDialog.setCancelable(false);
            pDialog.show();

            android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            Map<String, String> params = new HashMap<>();

            try {
//                    params.put("country_code", countryCodePicker.getSelectedCountryCodeWithPlus());
                params.put("email", tietEmail.getText().toString());
                params.put("password", tietPassword.getText().toString());
                params.put("device_id", android_id);
                params.put("register_id", sessionManager.getRegisterId());
//                params.put("activation_code", Helper.generateCode());
                params.put("imei", imei0);
                params.put("brand_device", Build.MANUFACTURER);
                params.put("model_device", Build.MODEL);
            } catch (Exception ignored) {

            }

            System.out.println("Params : " + params);
            presenter.checkStoreLogin(params);
        }
    }

    @OnClick(R.id.tv_other_login)
    public void storeLogin() {
        Intent intent = new Intent(this, PhoneAuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void registerCarrierEditText() {
        countryCodePicker.registerCarrierNumberEditText(mPhoneNumberField);
        countryCodePicker.setPhoneNumberValidityChangeListener(isValidNumber -> {
            if (isValidNumber) {
                if (mPhoneNumberField.getText().toString().startsWith("0")) {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                    tvValidity.setText(R.string.error_wrong_number);
                    tvValidity.setTextColor(Color.parseColor("#FF0000"));
                } else {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_validated));
                    tvValidity.setText(R.string.label_valid_number);
                    tvValidity.setTextColor(Color.parseColor("#FFFFFF"));
                }

            } else {
                imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                tvValidity.setText(R.string.label_invalid_number);
                tvValidity.setTextColor(Color.parseColor("#FF0000"));
            }
        });
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.

//        // [START_EXCLUDE]
//        if (mVerificationInProgress && validatePhoneNumber()) {
//            startPhoneNumberVerification(countryCodePicker.getFullNumber());
//        }
        // [END_EXCLUDE]
    }
    // [END on_start_check_user]

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        mAuth.signOut();

                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }

                        Helper.hideSoftKeyBoard(this);
                        Intent intent = new Intent(this, PrimaryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    } else {
                        // Sign in failed, display a message and update the UI
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        }

                    }
                });
    }
    // [END sign_in_with_phone]

    private boolean validatePhoneNumber() {
        String phoneNumber = countryCodePicker.getFullNumber();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    @Override
    public void showLoginSuccess(MemberCheck data) {

    }

    @Override
    public void showLoginFailed(String error) {

    }

    @Override
    public void sucessLoginStore() {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }

        Intent intent = new Intent(this, PrimaryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("moodtrack_id",idMood);
        intent.putExtra("store_id",storeId);
        intent.putExtra("status",status);
        Log.e(TAG,idMood);
        startActivity(intent);
    }

    @Override
    public void sucessCheckMember(MemberCheck data, DataMember dataMember) {

    }

    @Override
    public void errorCheckMember(String error) {
        if (pDialog.isShowing()) {
            pDialog.dismiss();
        }

        if (error.contains("404")) {
            Toast.makeText(this, R.string.err_msg_email_password_mismatch, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.err_msg_server_down, Toast.LENGTH_SHORT).show();
        }
    }

    private void nextTo() {

        Intent intent = new Intent(this, PinVerificationActivity.class);
        intent.putExtra("mobile_phone", countryCodePicker.getFullNumber());
        intent.putExtra("device_id", android_id);
        intent.putExtra("register_id", sessionManager.getRegisterId());
        intent.putExtra("activation_code", "");
        intent.putExtra("verification_id", mVerificationId);
        intent.putExtra("resend_token", mResendToken);
        startActivity(intent);
    }

    @SuppressLint("StaticFieldLeak")
    public void getRegisterId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
//                    String token = FirebaseInstanceId.getInstance().getToken();
                    String token = FirebaseInstanceId.getInstance().getToken(Constants.Key.FCM_SENDERID, "FCM");
                    System.out.println("=====" + " : token :" + token);
                    if (token != null || !token.isEmpty()) {

                        sessionManager.setRegisterId(token);
                    }

                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermssion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE :
                if (grantResults.length > 0) {
                    boolean phone = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean location = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (phone && location) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setCancelable(false);
                        builder.setTitle(R.string.title_autostart_permission);
                        builder.setMessage(R.string.message_autostart_permission);
                        builder.setPositiveButton(R.string.bt_yes,
                                (dialog, which) -> {
                                    if (Build.VERSION.SDK_INT >= 23) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                                        startActivity(intent);
                                    }
                                });

                        builder.setNegativeButton(R.string.label_no, (dialogInterface, i) -> dialogInterface.dismiss());
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        dialog.getButton(dialog.BUTTON_POSITIVE).setPadding(10,10,10,10);
                        dialog.getButton(dialog.BUTTON_NEGATIVE).setPadding(10,10,10,10);
                        dialog.getButton(dialog.BUTTON_NEGATIVE).setBackgroundColor(getResources().getColor(R.color.black_transparent0));
                        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                        dialog.getButton(dialog.BUTTON_POSITIVE).setBackgroundColor(getResources().getColor(R.color.colorAccent));
                        dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            checkPermssion();
                        }
                    }
                }
        }
    }


}
