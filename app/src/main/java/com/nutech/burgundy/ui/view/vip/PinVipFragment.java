package com.nutech.burgundy.ui.view.vip;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.presenter.PinVipPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Dafian on 10/17/17
 */

public class PinVipFragment extends BaseAppCompatActivity implements PinVipView {

    @BindView(R.id.txt_pin_entry)
    PinEntryEditText pvPin;
    @BindView(R.id.bt_submit)
    Button btSubmit;
    @BindView(R.id.tv_subscribe_name)
    TextView tvSubscribeName;
    @BindView(R.id.tv_subscribe_desc)
    TextView tvSubscribeDesc;
    @BindView(R.id.tv_subscribe_price)
    TextView tvSubscribePrice;
    @BindView(R.id.tv_resend_sms)
    TextView tvResendSMS;
    @BindView(R.id.bt_resend)
    Button btnResend;

    @OnClick(R.id.bt_resend)
    public void resendSms(){
        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activation_code", Helper.generateCode());
        tempActivationCode = fields.get("activation_code");
        presenter.updateSubscribeActivationCode(fields);
    }

    private Unbinder unbinder;
    private PinVipPresenter presenter;
    private SessionManager sessionManager;
    private Intent intent;

    private String id;
    private String customerId;
    private DataSubscribePrice subscribePrice;
    private CountDownTimer countDownTimer;
    private String tempActivationCode;
    private String pin;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pin_vip);
        ButterKnife.bind(this);

        intent = getIntent();
        id = intent.getStringExtra("id");
        customerId = intent.getStringExtra("customer_id");
        presenter = new PinVipPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();

        presenter.getDetailPrice(id);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Helper.hideSoftKeyBoard(PinVipFragment.this);
                if (pin.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Masukan kode verifikasi", Toast.LENGTH_SHORT).show();
                } else {
                    subscribeCustomer();
                }

            }
        });

        pvPin.setOnPinEnteredListener(str -> pin = str.toString());

        waitingResend();
    }

    @Override
    public void showDetailPrice(DataSubscribePrice subscribePrice) {

        this.subscribePrice = subscribePrice;

        String price = "IDR "+Helper.formatedPrice(subscribePrice.getPrice());

        tvSubscribeName.setText(subscribePrice.getName());
        tvSubscribeDesc.setText(subscribePrice.getDescription());
        tvSubscribePrice.setText(price);
    }

    @Override
    public void showSuccess(String success) {
        String customerId = sessionManager.getDataMember().getId();
        presenter.updateStatusPoint(customerId, Constants.ActivityPoint.SUBSCRIBE);

    }

    @Override
    public void showSuccessUpdate(String success) {

        getDetailSubscriber();
    }

    @Override
    public void showSuccessUpdateCode(String success) {
        btnResend.setVisibility(View.GONE);
        sendSMS();
        waitingResend();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, getString(R.string.err_msg_conection_server), Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showErrorUpdate(String error) {
//        dialog();
        getDetailSubscriber();
    }

    @Override
    public void showErrorUpdateCode(String error) {

    }

    @Override
    public void showSubscribeStatus(DataUserSubscribe dataUserSubscribe) {
        sessionManager.setStatusPackage(dataUserSubscribe.getStatus());
        sessionManager.setPackage(dataUserSubscribe.getPackageName());
        sessionManager.setPackageCode(dataUserSubscribe.getSubscribeCode());
        dialog();
    }

    @Override
    public void showErrorSubscribeStatus(String error) {
        sessionManager.setStatusPackage("0");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    public void subscribeCustomer() {

        String customerId = sessionManager.getDataMember().getId();

        Map<String, String> fields = new HashMap<>();
        fields.put("subscribe_id", id);
        fields.put("customer_id", customerId);
        fields.put("activation_code", pin);
        fields.put("customer_id", customerId);

        presenter.updateSubscribeCustomer(fields);
    }

    private void dialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.title_confirmation));
        alertDialog.setCancelable(false);
        alertDialog.setMessage(getString(R.string.msg_succes_subscriber));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(PinVipFragment.this, PrimaryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }
                });
        alertDialog.show();
    }

    private void waitingResend(){
        countDownTimer = new CountDownTimer(Constants.DefaultTimer.s60000, 1000) {
            public void onTick(long millisUntilFinished) {

                try {
                    tvResendSMS.setText(getString(R.string.msg_resend_sms)+" "+String.valueOf(millisUntilFinished / 1000));
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            public void onFinish() {
                try {
                    if (btnResend.getVisibility() == View.GONE)
                        btnResend.setVisibility(View.VISIBLE);
                    tvResendSMS.setVisibility(View.GONE);
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
        }.start();

    }

    private void sendSMS() {
        String numberPhone = "0"+sessionManager.getDataMember().getMobile_phone();
        String textContent = getString(R.string.msg_sms_login)+
                " "+ tempActivationCode;
        presenter.sendSms(numberPhone, textContent);
    }

    private void getDetailSubscriber() {
        presenter.getSubscribeCustomerStatus(sessionManager.getDataMember().getId());
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Toast.makeText(this, R.string.msg_activation_not_complete, Toast.LENGTH_SHORT).show();
    }
}
