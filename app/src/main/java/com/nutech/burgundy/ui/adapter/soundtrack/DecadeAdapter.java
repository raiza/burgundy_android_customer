package com.nutech.burgundy.ui.adapter.soundtrack;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.soundtrack.AddNewSoundtrackFragment;
import com.nutech.burgundy.ui.view.soundtrack.EditMoodtrackActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 2019-07-11.
 * NuTech (Nusantara Teknologi)
 */
public class DecadeAdapter extends BaseAdapter<DecadeAdapter.DecadeHolder> {

private Context context;
private List<Decade> list;
private List<String> ids = new ArrayList<String>();
private AddNewSoundtrackFragment parrent;
private int lastSelectedPosition;
private EditMoodtrackActivity activity;
private String decade;
private String tag;

public DecadeAdapter(Context context, List<Decade> decadeList, AddNewSoundtrackFragment addNewSoundtrackFragment, String tag) {
        this.context = context;
        this.list = decadeList;
        this.parrent = addNewSoundtrackFragment;
        this.tag = tag;
        }

    public DecadeAdapter(EditMoodtrackActivity context, List<Decade> decades, EditMoodtrackActivity editMoodtrackActivity,
                         String decade, String tag) {
        this.context = context;
        this.list = decades;
        this.activity = editMoodtrackActivity;
        this.decade = decade;
        this.tag = tag;
    }

    class DecadeHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.cp_energy)
    Chip cpEnergy;
    @BindView(R.id.cg_energy)
    ChipGroup cgEnergy;
    View childView;

    DecadeHolder(View childView) {
        super(childView);
        this.childView = childView;
        ButterKnife.bind(this, childView);
    }
}

    @Override
    public DecadeAdapter.DecadeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_decade, parent, false);
        return new DecadeHolder(view);
    }

    @Override
    public void onBindViewHolder(DecadeHolder holder, int position) {

        holder.cpEnergy.setText(list.get(position).getYear()+"s");

        if (tag.equals(Constants.Key.EDIT_MOOD)) {
            String[] items = decade.split(",");
            for (String item : items)
            {
                if (list.get(position).getYear().equals(item)) {
                    holder.cpEnergy.setChecked(true);
                    ids.add(list.get(position).getYear());
                    if (activity != null) {
                        activity.setDecadeParam(ids);
                    }
                }
            }
        }

        holder.cpEnergy.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (isChecked) {
                ids.add(list.get(position).getYear());
            } else {
                try {
                    String idRemove = (list.get(position).getYear());
                    for (int i = 0; i < ids.size(); i++) {
                        if (ids.get(i).equals(idRemove)) {
                            ids.remove(i);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (tag.equals(Constants.Key.EDIT_MOOD)) {
                if (activity != null) {
                    activity.setDecadeParam(ids);
                }
            } else if (tag.equals(Constants.Key.ADD_MOOD)) {
                if (parrent != null) {
                    parrent.setDecadeParam(ids);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
