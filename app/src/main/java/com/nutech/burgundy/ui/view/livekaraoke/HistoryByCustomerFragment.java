package com.nutech.burgundy.ui.view.livekaraoke;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.presenter.livekaraoke.HistoryKaraokePresenter;
import com.nutech.burgundy.ui.adapter.HistoryKaraokeAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public class HistoryByCustomerFragment extends BaseFragment implements HistoryKaraokeView {

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.recycler_history)
    RecyclerView rvHistory;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private Unbinder unbinder;
    private HistoryKaraokePresenter presenter;
    private SessionManager sessionManager;

    private HistoryKaraokeAdapter adapter;
    private List<DataRoom> historyRoom;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new HistoryKaraokePresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_by_customer, container, false);
        unbinder = ButterKnife.bind(this, view);

        getData();
        initView();
        initEvent();

        return view;
    }

    private void getData(){
        loadingIndicatorView.smoothToShow();
        presenter.getHistoryByCustomer(sessionManager.getDataMember().getId());
    }

    private void initView() {

        historyRoom = new ArrayList<>();
        adapter = new HistoryKaraokeAdapter(historyRoom, navigation, getActivity(), sessionManager, presenter, rvHistory);

        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.setItemAnimator(new DefaultItemAnimator());
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvHistory.setAdapter(adapter);

    }

    private void initEvent() {

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
                getData();
            }
        });

        rvHistory.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvHistory, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void showDataHistory(List<DataRoom> dataRoomList) {
        loadingIndicatorView.smoothToHide();
        swipeRefresh.setRefreshing(false);
        if (historyRoom.size() > 0) {
            historyRoom.clear();
        }

        historyRoom.addAll(dataRoomList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String error) {
        swipeRefresh.setRefreshing(false);
        loadingIndicatorView.smoothToHide();
    }

    @Override
    public void showRemoveError(String error) {
        swipeRefresh.setRefreshing(true);
        getData();
    }

    @Override
    public void showRemoveSuccess(String success) {
        swipeRefresh.setRefreshing(true);
        getData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

}
