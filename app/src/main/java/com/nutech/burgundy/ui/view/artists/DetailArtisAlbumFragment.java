package com.nutech.burgundy.ui.view.artists;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.presenter.ArtistsPresenter;
import com.nutech.burgundy.ui.adapter.AlbumsAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.album.DetailAlbumFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class DetailArtisAlbumFragment extends BaseFragment implements ArtistsView {

    @BindView(R.id.rv_tracks_albums)
    RecyclerView rvTracksAlbums;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private Unbinder unbinder;
    private ArtistsPresenter presenter;
    private String artistId = "";

    public DetailArtisAlbumFragment(String idArtists) {
        this.artistId = idArtists;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ArtistsPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_artist_albums, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadingIndicatorView.smoothToShow();
        presenter.getAlbumsArtist(artistId);

        return view;
    }


    @Override
    public void showAllArtists(DataArtistList artistsList) {

    }

    @Override
    public void showGenre(List<DataGenre> genreList) {

    }

    @Override
    public void showArtistsTracks(List<DataTracks> tracksList) {

    }

    @Override
    public void showArtistsAlbums(final List<DataAlbums> albumsList) {
        loadingIndicatorView.smoothToHide();
        rvTracksAlbums.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracksAlbums.setItemAnimator(new DefaultItemAnimator());
        rvTracksAlbums.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        rvTracksAlbums.setAdapter(new AlbumsAdapter(getActivity(), albumsList));

        rvTracksAlbums.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTracksAlbums,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        navigation.pushFragment(
                                new DetailAlbumFragment(
                                        albumsList.get(position).getId(),
                                        albumsList.get(position).getImage()
                                ), albumsList.get(position).getName(),
                                true);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showArtistsSearch(List<DataArtists> artistsList) {

    }

    @Override
    public void showErrorSearch(String error) {

    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
        Log.e("Error", error);

    }
}
