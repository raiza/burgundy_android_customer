package com.nutech.burgundy.ui.base;


import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.api.AntMediaService;
import com.nutech.burgundy.data.api.AntMediaServiceFactory;
import com.nutech.burgundy.data.api.DangdutAsikService;
import com.nutech.burgundy.data.api.DangdutAsikServiceFactory;
import com.nutech.burgundy.data.api.MelonService;
import com.nutech.burgundy.data.api.MelonServiceFactory;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

/**
 * @author Dafian on 7/15/17
 */

public abstract class BaseFragment extends Fragment {

    protected Context context;
    private DangdutAsikService dangdutAsikService;
    private AntMediaService antMediaService;
    private MelonService melonService;

    private SessionManager sessionManager;
    private DataManager dataManager;
    private NetworkHelper networkHelper;

    public FragmentNavigation navigation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof FragmentNavigation) {
            navigation = (FragmentNavigation) context;
        }
    }

    public DangdutAsikService getDangdutAsikService() {
        if (dangdutAsikService == null) {
            dangdutAsikService = DangdutAsikServiceFactory.create();
        }
        return dangdutAsikService;
    }

    public AntMediaService getAntMediaService(){
        if(antMediaService == null){
            antMediaService = AntMediaServiceFactory.create();
        }
        return antMediaService;
    }

    public MelonService getMelonService(){
        if(melonService == null){
            melonService = MelonServiceFactory.create();
        }
        return melonService;
    }


    public SessionManager getSessionManager() {
        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(getActivity()));
        }
        return sessionManager;
    }

    public DataManager getDataManager() {
        if (dataManager == null) {
            dataManager = new DataManager(getDangdutAsikService(),getAntMediaService(), getMelonService());
        }
        return dataManager;
    }

    public NetworkHelper getNetworkHelper() {
        if (networkHelper == null) {
            networkHelper = new NetworkHelper(getActivity(), getSessionManager());
        }
        return networkHelper;
    }

    protected void loadImagesFromURL(Context context, ImageView view, String url) {

        if (TextUtils.isEmpty(url)) {
            view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.placeholder));
            return;
        }

        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(view);
    }

    protected void loadImagesAvatarFromURL(Context context, ImageView view, String url) {

        if (TextUtils.isEmpty(url)) {
            view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.default_user));
            return;
        }

        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.default_user)
                .error(R.drawable.default_user)
                .into(view);
    }
}
