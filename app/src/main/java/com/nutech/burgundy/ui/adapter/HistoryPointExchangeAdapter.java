package com.nutech.burgundy.ui.adapter;

import android.content.DialogInterface;
import android.graphics.Color;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.history.PinHistoryFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.nutech.burgundy.R.id;

/**
 * @author Dafian on 10/16/17
 */

public class HistoryPointExchangeAdapter extends RecyclerView.Adapter<HistoryPointExchangeAdapter.VipHolder> {

    private List<DataHistoryExchange> dataHistoryExchanges;
    private FragmentNavigation navigation;
    private FragmentActivity context;

    public HistoryPointExchangeAdapter(List<DataHistoryExchange> historyExchangeList,
                                       FragmentNavigation fNavigation, FragmentActivity activity) {
        this.dataHistoryExchanges = historyExchangeList;
        this.navigation = fNavigation;
        this.context = activity;
    }

    class VipHolder extends RecyclerView.ViewHolder {

        @BindView(id.tv_reward_name)
        TextView tvRewardName;
        @BindView(id.tv_activation_code)
        TextView tvActivationCode;
        @BindView(id.tv_status)
        TextView tvStatus;
        @BindView(id.btn_activation)
        Button btnActivation;

        VipHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public VipHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history_point_exchange, parent, false);
        return new VipHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VipHolder holder, final int position) {

        if (dataHistoryExchanges.get(position).getExchangestatus().equals(Constants.Status.USED)) {
            holder.btnActivation.setEnabled(false);
            holder.btnActivation.setText("Digunakan");
            holder.btnActivation.setTextColor(Color.parseColor("#BDBDBD"));
        } else if (dataHistoryExchanges.get(position).getExchangestatus().equals(Constants.Status.ACTIVE)) {
            holder.btnActivation.setEnabled(true);
            holder.btnActivation.setText("Gunakan");
        }else if (dataHistoryExchanges.get(position).getExchangestatus().equals(Constants.Status.INACTIVE)) {
            holder.btnActivation.setEnabled(true);
            holder.btnActivation.setText("Aktifkan");
        }

        holder.tvRewardName.setText(dataHistoryExchanges.get(position).getRewardname());
        holder.tvActivationCode.setText(dataHistoryExchanges.get(position).getActivationcode());
        holder.tvStatus.setText(dataHistoryExchanges.get(position).getExchangestatus());

        Log.e("Reward Name",dataHistoryExchanges.get(position).getRewardname() );


        holder.btnActivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                alertDialog.setTitle("Konfirmasi");
                alertDialog.setMessage("Kode Verifikasi akan dikirimkan melalui SMS" + "\n" +
                        "Mohon Tunggu.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Mengerti",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                navigation.pushFragment(
                                        new PinHistoryFragment().newInstance(
                                                dataHistoryExchanges.get(position).getId()),
                                        "Detail", true);
                            }
                        });
                alertDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataHistoryExchanges.size();
    }
}
