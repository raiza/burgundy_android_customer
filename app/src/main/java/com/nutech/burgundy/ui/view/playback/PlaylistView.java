package com.nutech.burgundy.ui.view.playback;

import com.nutech.burgundy.lib.jcplayer.JcAudio;

import java.util.List;

public interface PlaylistView {
    void onFinishedInitPlaylist(List<JcAudio> jcAudios);
}
