package com.nutech.burgundy.ui.view.home;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 11/17/17.
 * Xeranta Mobile Solutions
 */

public interface FavoriteTracksView extends BaseView {

    void showSuccess(List<DataTracks> dataTracks);
    void showSuccessRemove(String success);
    void showError(String error);
}
