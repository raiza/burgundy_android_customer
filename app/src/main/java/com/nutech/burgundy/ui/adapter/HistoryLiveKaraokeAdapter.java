package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.livekaraoke.player.PlayerFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public class HistoryLiveKaraokeAdapter extends RecyclerView.Adapter<HistoryLiveKaraokeAdapter.HistoryHolder> {

    private List<DataRoom> dataRoomList;
    private FragmentNavigation nav;
    private Context context;

    public HistoryLiveKaraokeAdapter(List<DataRoom> historyRoom, FragmentNavigation navigation, FragmentActivity activity) {
        this.dataRoomList = historyRoom;
        this.nav = navigation;
        this.context = activity;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history_live_karaoke, parent, false);
        return new HistoryHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, final int position) {

        String date = Helper.formatedMonth(dataRoomList.get(position).getCreatedate());
        String vocalName = dataRoomList.get(position).getCustomername();

        holder.tvVocalName.setText(context.getString(R.string.vocal_by)+ " " + vocalName);
        holder.tvRoomCode.setText(context.getString(R.string.singing_on)+" "+date);
        holder.tvTitleTrack.setText(dataRoomList.get(position).getTrackname());

        holder.indicatorLive.smoothToShow();

        holder.btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nav.pushFragment(
                        new PlayerFragment(
                                dataRoomList.get(position).getId(),
                                dataRoomList.get(position).getRoomcode(),
                                dataRoomList.get(position).getTrackid(),
                                dataRoomList.get(position).getCustomerid(),
                                dataRoomList.get(position).getLive()),
                        dataRoomList.get(position).getTrackname(), true);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataRoomList.size();
    }

    class HistoryHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_room_code)
        TextView tvRoomCode;
        @BindView(R.id.tv_title_track)
        TextView tvTitleTrack;
        @BindView(R.id.tv_vocal_name)
        TextView tvVocalName;
        @BindView(R.id.bt_play)
        Button btPlay;
        @BindView(R.id.indicator_live)
        AVLoadingIndicatorView indicatorLive;

        HistoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
