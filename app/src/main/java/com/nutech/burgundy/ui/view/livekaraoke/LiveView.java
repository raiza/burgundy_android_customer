package com.nutech.burgundy.ui.view.livekaraoke;

import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 12/19/17.
 * Xeranta Mobile Solutions
 */

public interface LiveView extends BaseView {

    void showDataLive(List<DataRoom> dataRoomList);

    void showError(String error);
}
