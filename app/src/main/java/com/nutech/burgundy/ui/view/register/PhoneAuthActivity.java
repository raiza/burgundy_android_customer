package com.nutech.burgundy.ui.view.register;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.presenter.LoginPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.utils.Helper;
import com.roger.gifloadinglibrary.GifLoadingView;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhoneAuthActivity extends BaseAppCompatActivity implements LoginView {

    private static final String TAG = "PhoneAuthActivity";
    public static final int REQUEST_CODE = 100;

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    @BindView(R.id.country_code)
    CountryCodePicker countryCodePicker;
    @BindView(R.id.img_validity)
    ImageView imgValidity;
    @BindView(R.id.tv_validity)
    TextView tvValidity;
    @BindView(R.id.tv_other_login)
    TextView tvOtherLogin;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private ViewGroup mPhoneNumberViews;
    private EditText mPhoneNumberField;

    private String android_id = "";
    private LoginPresenter presenter;
    private SessionManager sessionManager;
    private GifLoadingView mGifLoadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        ButterKnife.bind(this);
        presenter = new LoginPresenter(getDataManager(), getNetworkHelper(), getSessionManager(), getApplicationContext());
        sessionManager = getSessionManager();
        presenter.attachView(this);
        mGifLoadingView = new GifLoadingView();

        // Restore instance state
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        // Assign views
        mPhoneNumberViews = findViewById(R.id.phoneAuthFields);

        mPhoneNumberField = findViewById(R.id.fieldPhoneNumber);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermssion();
        }
        getRegisterId();
        countryCodePicker.setDefaultCountryUsingNameCode("ID");
        registerCarrierEditText();

        if (BuildConfig.DEBUG) {
            mPhoneNumberField.setText("85319315391");
        }

        // [START initialize_auth]
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Helper.dismissLoading(mGifLoadingView);
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                nextTo();

            }
        };
        // [END phone_auth_callbacks]
    }

    @SuppressLint("HardwareIds")
    @OnClick(R.id.button_login)
    public void toDoNext() {

        String imei0 = Helper.getIMEI(this, 0);
        String imei1 = Helper.getIMEI(this, 1);
        String uniqueDeviceId = Helper.getDeviceUniqueID(this);

        Helper.hideSoftKeyBoard(this);

        if (TextUtils.isEmpty(mPhoneNumberField.getText().toString())) {
            mPhoneNumberField.setError(getString(R.string.err_field_required));
        } else {
            Helper.showLoading(getFragmentManager(), mGifLoadingView);

            android_id = Settings.Secure.getString(getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            Map<String, String> params = new HashMap<String, String>();

            try {
//                    params.put("country_code", countryCodePicker.getSelectedCountryCodeWithPlus());
                params.put("mobile_phone", countryCodePicker.getFullNumber());
                params.put("device_id", android_id);
                params.put("register_id", sessionManager.getRegisterId());
                params.put("activation_code", Helper.generateCode());
                params.put("imei", imei0);
            } catch (Exception e) {

            }

            System.out.println("Params : " + params);
            presenter.checkMemberToIndosis(params);
        }
    }

    @OnClick(R.id.tv_other_login)
    public void storeLogin() {
        Intent intent = new Intent(this, StoreLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void registerCarrierEditText() {
        countryCodePicker.registerCarrierNumberEditText(mPhoneNumberField);
        countryCodePicker.setPhoneNumberValidityChangeListener(isValidNumber -> {
            if (isValidNumber) {
                if (mPhoneNumberField.getText().toString().startsWith("0")) {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                    tvValidity.setText(R.string.error_wrong_number);
                    tvValidity.setTextColor(Color.parseColor("#FF0000"));
                } else {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_validated));
                    tvValidity.setText(R.string.label_valid_number);
                    tvValidity.setTextColor(Color.parseColor("#FFFFFF"));
                }

            } else {
                imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                tvValidity.setText(R.string.label_invalid_number);
                tvValidity.setTextColor(Color.parseColor("#FF0000"));
            }
        });
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

//        // [START_EXCLUDE]
//        if (mVerificationInProgress && validatePhoneNumber()) {
//            startPhoneNumberVerification(countryCodePicker.getFullNumber());
//        }
        // [END_EXCLUDE]
    }
    // [END on_start_check_user]

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        mAuth.signOut();

                        Helper.dismissLoading(mGifLoadingView);

                        Helper.hideSoftKeyBoard(this);
                        Intent intent = new Intent(this, PrimaryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    } else {
                        // Sign in failed, display a message and update the UI
                        Helper.dismissLoading(mGifLoadingView);
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        }

                    }
                });
    }
    // [END sign_in_with_phone]

    private boolean validatePhoneNumber() {
        String phoneNumber = countryCodePicker.getFullNumber();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    @Override
    public void showLoginSuccess(MemberCheck data) {

    }

    @Override
    public void showLoginFailed(String error) {

    }

    @Override
    public void sucessCheckMember(MemberCheck data, DataMember dataMember) {
        startPhoneNumberVerification(countryCodePicker.getFullNumber());
    }

    @Override
    public void sucessLoginStore() {

    }

    @Override
    public void errorCheckMember(String error) {
        Helper.dismissLoading(mGifLoadingView);
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }

    private void nextTo() {

        Intent intent = new Intent(this, PinVerificationActivity.class);
        intent.putExtra("mobile_phone", countryCodePicker.getFullNumber());
        intent.putExtra("device_id", android_id);
        intent.putExtra("register_id", sessionManager.getRegisterId());
        intent.putExtra("activation_code", "");
        intent.putExtra("verification_id", mVerificationId);
        intent.putExtra("resend_token", mResendToken);
        startActivity(intent);
    }

    @SuppressLint("StaticFieldLeak")
    public void getRegisterId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
//                    String token = FirebaseInstanceId.getInstance().getToken();
                    String token = FirebaseInstanceId.getInstance().getToken(Constants.Key.FCM_SENDERID, "FCM");
                    System.out.println("=====" + " : token :" + token);
                    if (token != null || !token.isEmpty()) {

                        sessionManager.setRegisterId(token);
                    }

                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermssion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }


}
