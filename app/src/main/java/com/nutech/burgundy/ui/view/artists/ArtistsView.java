package com.nutech.burgundy.ui.view.artists;

import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 8/31/17.
 * Xeranta Mobile Solutions
 */

public interface ArtistsView extends BaseView {

    void showAllArtists(DataArtistList artistsList);

    void showGenre(List<DataGenre> genreList);

    void showArtistsTracks(List<DataTracks> tracksList);

    void showArtistsAlbums(List<DataAlbums> albumsList);

    void showArtistsSearch(List<DataArtists> artistsList);

    void showErrorSearch(String error);

    void showError(String error);

}
