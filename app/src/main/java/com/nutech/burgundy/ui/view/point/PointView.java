package com.nutech.burgundy.ui.view.point;

import com.nutech.burgundy.data.model.DataPoint;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * @author Dafian on 11/13/17
 */

public interface PointView extends BaseView {

    void showPointAll(List<DataPoint> dataPointList);

    void showError(String error);
}
