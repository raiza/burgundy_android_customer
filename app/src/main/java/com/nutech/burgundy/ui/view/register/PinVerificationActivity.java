package com.nutech.burgundy.ui.view.register;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataOperator;
import com.nutech.burgundy.presenter.PinVerifiedPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.utils.Helper;
import com.roger.gifloadinglibrary.GifLoadingView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Arif Setiawan on 11/30/17.
 * Xeranta Mobile Solutions
 */

public class PinVerificationActivity extends BaseAppCompatActivity implements PinVerifiedView {

    private String TAG = "PinVerificationActivity = ";

    @BindView(R.id.txt_pin_entry)
    PinEntryEditText pinVerification;
    @BindView(R.id.tv_resend_sms)
    TextView tvResendSMS;
    @BindView(R.id.bt_resend)
    Button btnResend;

    @OnClick(R.id.bt_submit)
    public void sendVerification() {

        if (pin.length() < 6){
            Toast.makeText(getApplicationContext(), "Masukan kode verifikasi", Toast.LENGTH_SHORT).show();
        } else {
            Helper.showLoading(getFragmentManager(), mGifLoadingView);

//            Map<String, String> params = new HashMap<String, String>();
//            try {
//                params.put("mobile_phone", intent.getStringExtra("mobile_phone"));
//                params.put("activation_code", pin);
//            } catch (Exception e) {
//            }
//            System.out.println("Params = "+params);
//            presenter.otpVerification(params);

            verifyPhoneNumberWithCode(intent.getStringExtra("verification_id"), pin);
        }
    }

    private Intent intent;
    private PinVerifiedPresenter presenter;
    private CountDownTimer countDownTimer;
    private String tempActivationCode = "";
    private SessionManager sessionManager;
    private String pin;
    private FirebaseAuth mAuth;
    private GifLoadingView mGifLoadingView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_verification);
        ButterKnife.bind(this);
        presenter = new PinVerifiedPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
        intent = getIntent();
        sessionManager = getSessionManager();
        mAuth = FirebaseAuth.getInstance();
        mGifLoadingView = new GifLoadingView();

        if (pinVerification != null) {
            pinVerification.setOnPinEnteredListener(str -> {
                pin = str.toString();
            });
        }

        btnResend.setOnClickListener(view -> {
            btnResend.setVisibility(View.GONE);
            Helper.showLoading(getFragmentManager(), mGifLoadingView);

        });

        waitingResend();
        getOperator();

    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        Helper.dismissLoading(mGifLoadingView);
                        sessionManager.login();

                        mAuth.signOut();

                        Helper.hideSoftKeyBoard(this);
                        Intent intent = new Intent(this, PrimaryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    } else {
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
//                            mVerificationField.setError("Invalid code.");
                            Toast.makeText(getApplicationContext(), "Kode Verifikasi tidak cocok atau sudah kadarluasa", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void showSuccess(DataMember dataMember) {
        Helper.dismissLoading(mGifLoadingView);
        Helper.hideSoftKeyBoard(this);
        Intent intent = new Intent(this, PrimaryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @Override
    public void showError(String error) {
        Helper.dismissLoading(mGifLoadingView);

        if (error.equals("404")){
            Toast.makeText(getApplicationContext(), "Kode Verifikasi tidak cocok atau sudah kadarluasa", Toast.LENGTH_SHORT).show();
        } else if (error.equals("200")){
            Helper.hideSoftKeyBoard(this);
            Intent intent = new Intent(this, PrimaryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Tidak dapat terhubung dengan server, coba beberapa saat lagi", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void successResend(String succes) {
        Helper.dismissLoading(mGifLoadingView);
        nextTo();
    }

    @Override
    public void errorResend(String error) {
        Helper.dismissLoading(mGifLoadingView);
        if (error.equals("500")){
            nextTo();
        } else if (error.equals("200")){
            nextTo();
        }else {
            Toast.makeText(getApplicationContext(), getString(R.string.err_msg_conection_server), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showOperator(List<DataOperator> dataOperator) {
        DataOperator operator = new DataOperator();
        for (int i = 0; i <dataOperator.size(); i++) {
            operator.setId(dataOperator.get(i).getId());
            operator.setShortName(dataOperator.get(i).getShortName());
            operator.setFullName(dataOperator.get(i).getFullName());
            operator.setNumberList(dataOperator.get(i).getNumberList());
            operator.setCreate_date(dataOperator.get(i).getCreate_date());
        }
        sessionManager.setOperator(operator);
    }

    @Override
    public void showErrorOperator(String error) {

    }

    private void waitingResend(){
        countDownTimer = new CountDownTimer(Constants.DefaultTimer.s60000, 1000) {
            public void onTick(long millisUntilFinished) {

                tvResendSMS.setText(getString(R.string.msg_resend_sms)+" "+String.valueOf(millisUntilFinished / 1000));
            }

            public void onFinish() {
                if (btnResend.getVisibility() == View.GONE)
                btnResend.setVisibility(View.VISIBLE);
                tvResendSMS.setVisibility(View.GONE);
            }
        }.start();

    }

    private void getOperator() {

        String number = "";
        if (intent.getStringExtra("mobile_phone").contains("+")) {
            number = intent.getStringExtra("mobile_phone").replace("+62", "0");
        } else {
            number = intent.getStringExtra("mobile_phone").replace("62", "0");
        }
        String afterString = number.substring(0, 4);
        Log.e("Number", afterString);
        presenter.getOperator(afterString);
    }

    private void nextTo() {
        String numberPhone = "0"+intent.getStringExtra("mobile_phone");
        String textContent = getString(R.string.msg_sms_login)+
                " "+ tempActivationCode;
        presenter.sendSms(numberPhone, textContent);

        startActivity(intent);
    }
}
