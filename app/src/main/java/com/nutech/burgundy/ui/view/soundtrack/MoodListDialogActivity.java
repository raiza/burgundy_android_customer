package com.nutech.burgundy.ui.view.soundtrack;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.presenter.soundtrack.ListMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.soundtrack.AddTrackAdapter;
import com.nutech.burgundy.ui.base.BaseActivity;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Arif Setiawan on 9/6/17.
 * NuTech (Nusantara Teknologi)
 */


public class MoodListDialogActivity extends BaseActivity implements ListMoodtrackView {

    @BindView(R.id.iv_close)
    ImageView iVClose;
    @BindView(R.id.rv_moodtrack)
    RecyclerView rvMood;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.img_nodata)
    ImageView imgNodata;
    @BindView(R.id.tv_msg_error)
    TextView tvMsgError;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.main_error_layout)
    RelativeLayout mainErrorLayout;

    private Intent intent;
    private ListMoodtrackPresenter presenter;
    private AddTrackAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Moodtrack> moodtrackList;
    private ProgressDialog pDialog;
    public String trackId;
    public String trackName;
    public String moodName;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mood_list_dialog);
        ButterKnife.bind(this);
        presenter = new ListMoodtrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        intent = getIntent();

        initViews();
        initEvents();
    }

    @OnClick(R.id.btn_retry)
    public void onClick() {
        loadingData();
    }

    private void loadingData() {
        showProgressDialog();
        swipeRefreshLayout.setRefreshing(true);
        presenter.getData(getSessionManager().getUserStore().getStore_id());
    }

    private void initViews() {
        trackId = intent.getStringExtra("track_id");
        trackName = intent.getStringExtra("track_name");
        loadingData();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    private void initEvents() {
        rvMood.addOnItemTouchListener(new RecyclerTouchListener(this, rvMood,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        swipeRefreshLayout.setOnRefreshListener(this::loadingData);

        iVClose.setOnClickListener(view1 -> finish());
    }

    public void addTrackToMood(String moodId, String moodName) {

        this.moodName = moodName;
        showProgressDialog();
        HashMap<String, String> params = new HashMap<>();
        try {
            params.put("mood_id", moodId);
            params.put("track_id", trackId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("** Param ** " + params.toString());
        presenter.addTrackToMood(params, this);
    }

    @Override
    public void showMoodtracks(List<Moodtrack> dataMoodtracks) {
        hideProgressDialog();
        swipeRefreshLayout.setRefreshing(false);
        if (dataMoodtracks.size() > 0) {
            mainErrorLayout.setVisibility(View.GONE);

            moodtrackList = dataMoodtracks;
            gridLayoutManager = new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false);
            rvMood.setLayoutManager(gridLayoutManager);
            adapter = new AddTrackAdapter(this, dataMoodtracks, this);
            rvMood.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void showErrorMoodtracks(String error) {
        hideProgressDialog();
        mainErrorLayout.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showMoodtracksPlayingStore(List<Moodtrack> dataMoodtracks) {

    }

    @Override
    public void showErrorMoodtracksPlayingStore(String error) {

    }

    @Override
    public void showSuccessRemove(String success) {

    }

    @Override
    public void showErrorRemove(String error) {

    }

    @Override
    public void showSuccessSchedule(String success) {

    }

    @Override
    public void showErrorSchedule(String error) {

    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage(getString(R.string.msg_creating));
        pDialog.show();
    }

    public void hideProgressDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }
}
