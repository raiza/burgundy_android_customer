package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.ui.base.BaseAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class AlbumsAdapter extends BaseAdapter<AlbumsAdapter.AlbumsHolder> {

    private Context context;
    private List<DataAlbums> list;

    public AlbumsAdapter(
            Context context, List<DataAlbums> list) {
        this.context = context;
        this.list = list;
    }

    class AlbumsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_album_name)
        TextView tvTitleAlbums;
        @BindView(R.id.iv_albums)
        ImageView ivAlbums;

        View childView;

        AlbumsHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public AlbumsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_album, parent, false);
        return new AlbumsHolder(view);
    }

    @Override
    public void onBindViewHolder(AlbumsHolder holder, int position) {

        holder.tvTitleAlbums.setText(list.get(position).getName());

        String ImageUrl = Constants.URL_IMAGE_ALBUMS+list.get(position).getImage();
        loadImagesFromURL(context, holder.ivAlbums, ImageUrl);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
