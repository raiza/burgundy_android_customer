package com.nutech.burgundy.ui.view.home;

import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public interface MyMusicView extends BaseView {

    void showListPlaylistRegistered(List<DataPlaylistMember> dataPlaylistMembers);

    void showSuccesRemove(String data);

    void showError(String error);
}
