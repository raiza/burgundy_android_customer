package com.nutech.burgundy.ui.base;

import android.app.Dialog;
import android.view.View;

public interface DialogClickListener {

    void onClick(Dialog dialog, View view);
}
