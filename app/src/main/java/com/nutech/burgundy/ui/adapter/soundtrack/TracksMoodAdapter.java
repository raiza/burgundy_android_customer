package com.nutech.burgundy.ui.adapter.soundtrack;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.TrackPresenter;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackFragment;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;
import com.nutech.burgundy.ui.view.track.ItemTouchHelperAdapter;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * NuTech (Nusantara Teknologi)
 */

public class TracksMoodAdapter extends BaseAdapter<TracksMoodAdapter.TracksHolder>
        implements ItemTouchHelperAdapter {

    private Context context;
    private List<DataTracks> list;
    private SessionManager sessionManager;
    private SparseArray<Float> progressMap = new SparseArray<>();
    private FragmentNavigation fragmentNavigation;
    private String tag;
    private TrackPresenter presenter;
    private DetailMoodtrackFragment fragment;
    private boolean myMoodtrack;
    private boolean editable = false;

    public TracksMoodAdapter(Context context, List<DataTracks> list, FragmentNavigation navigation, String tag, DetailMoodtrackFragment fragment, boolean myMoodtrack) {
        this.context = context;
        this.list = list;
        this.fragmentNavigation = navigation;
        this.tag = tag;
        this.fragment = fragment;
        this.myMoodtrack = myMoodtrack;

        setContext(context);
    }

    class TracksHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_tracks)
        TextView tvTitleTracks;
        @BindView(R.id.rl_toucher)
        RelativeLayout rlToucher;
        @BindView(R.id.tv_title_album)
        TextView tvTitleAlbum;
        @BindView(R.id.ll_play)
        LinearLayout llPlay;
        @BindView(R.id.btn_options)
        ImageView ivOptions;
        @BindView(R.id.bt_sing)
        Button btSing;

        View childView;

        TracksHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
            presenter = new TrackPresenter(getDataManager(), getNetworkHelper());
        }
    }

    @Override
    public TracksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_tracks, parent, false);
        sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        return new TracksHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final TracksHolder holder, final int position) {

        if (!TextUtils.isEmpty(list.get(position).getInclude_karaoke())) {
            if (list.get(position).getInclude_karaoke().equals("1")) {
                holder.btSing.setVisibility(View.VISIBLE);
            } else {
                holder.btSing.setVisibility(View.GONE);
            }
        } else {
            holder.btSing.setVisibility(View.GONE);
        }

        if (editable) {
            holder.ivOptions.setVisibility(View.GONE);
        }else {
            holder.ivOptions.setVisibility(View.VISIBLE);
        }

        holder.tvTitleTracks.setText(list.get(position).getName());

        holder.tvTitleAlbum.setText(
                list.get(position).getAlbum_name() + " - "
                        + list.get(position).getArtistnames());

        holder.rlToucher.setOnClickListener(view -> {
//            fragmentNavigation.playingMusic(position, list);
//            toDoNowPlaying(
//                    list.get(position).getId(),
//                    list.get(position).getInclude_karaoke(),
//                    list.get(position).getKaraoke_id(),
//                    list.get(position).getName(),
//                    list.get(position).getArtistnames(),
//                    list.get(position).getUrl()+list.get(position).getSource());
        });

        holder.llPlay.setOnClickListener(view -> {
//            fragmentNavigation.playingMusic(position, list);
//            toDoNowPlaying(
//                    list.get(position).getId(),
//                    list.get(position).getInclude_karaoke(),
//                    list.get(position).getKaraoke_id(),
//                    list.get(position).getName(),
//                    list.get(position).getArtistnames(),
//                    list.get(position).getUrl() + list.get(position).getSource());
        });

        holder.ivOptions.setOnClickListener(view -> {
                showOptionsMoodTracks(holder.ivOptions, list.get(position).getId(),
                        list.get(position).getName(),list.get(position).getPosition());
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void showOptionsMoodTracks(ImageView btnOptions, String trackId,
                                       String trackName,String position) {

        HashMap<String, String> params = new HashMap<String, String>();

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.mood_tracks_options);
        Menu menu = popup.getMenu();

        if (!myMoodtrack) {
            menu.findItem(R.id.remove).setVisible(false);
        }

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.add:
                    Intent intent = new Intent(context, MoodListDialogActivity.class);
                    intent.putExtra("track_id", trackId);
                    intent.putExtra("track_name", trackName);
                    context.startActivity(intent);
                    break;
                case R.id.remove:
                    String msgRemove = "'"+trackName +"' "+ context.getResources().getString(R.string.label_remove_from_mood);
                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    alertDialog.setTitle(context.getResources().getString(R.string.label_info));
                    alertDialog.setMessage(msgRemove);
                    alertDialog.setCancelable(true);
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.label_delete),
                            (dialog, which) -> {
                                dialog.dismiss();
                                try {
                                    params.put("soundtrack_id", Constants.ParamValue.moodId);
                                    params.put("track_id", trackId);
                                    params.put("position",position);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                System.out.println("** Param ** "+ params);
                                presenter.removeTrackFromMood(params, fragment);
                            }
                    );
                    alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                    alertDialog.show();
                    break;
            }
            return false;
        });
        makePopForceShowIcon(popup);
    }

    public void toDoNowPlaying(String id, String includeKaraoke, String karaokeId, String name,
                               String artistnames, String sourceTrack) {

        fragmentNavigation.pushFragment(
                new NowPlayingFragment(id, includeKaraoke, karaokeId, sourceTrack), name,
                true);

    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup = popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(list, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(list, i, i - 1);
            }
        }

        list.get(fromPosition).setPosition(String.valueOf(fromPosition+1));
        list.get(toPosition).setPosition(String.valueOf(toPosition+1));
        fragment.updatePositionTracks(list,list.get(fromPosition), toPosition+1,fromPosition+1);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemSelectedChange() {
        this.notifyDataSetChanged();
    }

}
