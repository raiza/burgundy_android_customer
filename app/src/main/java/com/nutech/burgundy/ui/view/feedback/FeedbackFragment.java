package com.nutech.burgundy.ui.view.feedback;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.presenter.FeedbackPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public class FeedbackFragment extends BaseFragment implements FeedbackView {

    @BindView(R.id.et_content_feedback)
    EditText etContentFeedback;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    @OnClick(R.id.btn_send_feedback)
    public void sendFeedback() {

        if (!TextUtils.isEmpty(etContentFeedback.getText())){
            Map<String, String> params = new HashMap<String, String>();
            try {
//                params.put("customer_id", sessionManager.getDataMember().getId());
                params.put("customer_id", sessionManager.getUserStore().getId());
                params.put("content", etContentFeedback.getText().toString());
                params.put("created_date", Helper.todayDateFull());
            } catch (Exception e) {

            }
            System.out.println("params "+params.toString());
            loadingIndicatorView.smoothToShow();
            presenter.sendFeedback(params);
        }
    }

    private Unbinder unbinder;
    private FeedbackPresenter presenter;
    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FeedbackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showSuccess(String success) {
        loadingIndicatorView.smoothToHide();
        Toast.makeText(getActivity(),
                getString(R.string.msg_send_feedback),
                Toast.LENGTH_SHORT).show();
        etContentFeedback.setText("");
    }

    @Override
    public void showError(String Error) {
        loadingIndicatorView.smoothToHide();
        Toast.makeText(getActivity(),
                getString(R.string.msg_send_feedback),
                Toast.LENGTH_SHORT).show();
        etContentFeedback.setText("");
    }
}
