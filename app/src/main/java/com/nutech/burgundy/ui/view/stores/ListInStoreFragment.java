package com.nutech.burgundy.ui.view.stores;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.presenter.soundtrack.ListMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.soundtrack.MoodtrackAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.MoodtrackInStoreAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.MoodtrackPlayingStoreAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackFragment;
import com.nutech.burgundy.ui.view.soundtrack.ListMoodtrackView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ListInStoreFragment extends BaseFragment implements ListMoodtrackView {
    @BindView(R.id.v_promo)
    View vPromo;
    @BindView(R.id.textView_chart)
    TextView textViewChart;
    @BindView(R.id.bt_more_promo)
    ImageButton btMorePromo;
    @BindView(R.id.rv_promo)
    RecyclerView rvPromo;
    @BindView(R.id.v_moodtrack)
    View vMoodtrack;
    @BindView(R.id.tv_moodtrack)
    TextView tvMoodtrack;
    @BindView(R.id.bt_more_moodtrack)
    ImageButton btMoreMoodtrack;
    @BindView(R.id.rv_moodtrack)
    RecyclerView rvMoodtrack;
    private String store_id;
    private ListMoodtrackPresenter listMoodtrackPresenter;
    private MoodtrackInStoreAdapter moodtrackPlayingStoreAdapter;
    private Unbinder unbinder;
    private List<Moodtrack> moodtrackList;

    public ListInStoreFragment(String store_id) {
        this.store_id = store_id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listMoodtrackPresenter = new ListMoodtrackPresenter(getDataManager(), getNetworkHelper());
        listMoodtrackPresenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_in_store, container, false);
        unbinder = ButterKnife.bind(this, view);

        initEvent();
        initView();

        return view;
    }

    private void initEvent() {

    }

    private void initView() {
        listMoodtrackPresenter.getMoodtrackByStoreId(store_id);

        rvMoodtrack.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.HORIZONTAL, false
        ));
        rvMoodtrack.setItemAnimator(new DefaultItemAnimator());
    }

    public void moveToDetail(int position) {
        if (moodtrackList != null) {
            navigation.pushFragment(
                    new DetailMoodtrackFragment(
                            moodtrackList.get(position).getId(),false),
                    moodtrackList.get(position).getName(),
                    true);
        }
    }

    @Override
    public void showMoodtracks(List<Moodtrack> dataMoodtracks) {
        moodtrackPlayingStoreAdapter = new MoodtrackInStoreAdapter(context,dataMoodtracks,this);
        rvMoodtrack.setAdapter(moodtrackPlayingStoreAdapter);
        rvMoodtrack.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvMoodtrack, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                navigation.pushFragment(
                        new DetailMoodtrackFragment(
                                dataMoodtracks.get(position).getId(),false),
                        dataMoodtracks.get(position).getName(), true);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void showErrorMoodtracks(String error) {

    }

    @Override
    public void showMoodtracksPlayingStore(List<Moodtrack> dataMoodtracks) {

    }

    @Override
    public void showErrorMoodtracksPlayingStore(String error) {

    }

    @Override
    public void showSuccessRemove(String success) {

    }

    @Override
    public void showErrorRemove(String error) {

    }

    @Override
    public void showSuccessSchedule(String success) {

    }

    @Override
    public void showErrorSchedule(String error) {

    }
}
