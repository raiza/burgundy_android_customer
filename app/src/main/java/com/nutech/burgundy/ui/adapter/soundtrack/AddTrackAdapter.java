package com.nutech.burgundy.ui.adapter.soundtrack;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.chip.Chip;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class AddTrackAdapter extends BaseAdapter<AddTrackAdapter.Holder> {

    private Context context;
    private List<Moodtrack> list;
    private MoodListDialogActivity parrent;

    public AddTrackAdapter(Context context, List<Moodtrack> moodtrackList, MoodListDialogActivity moodListDialogActivity) {
        this.context = context;
        this.list = moodtrackList;
        this.parrent = moodListDialogActivity;
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_cover)
        ImageView imgCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.img_options)
        ImageView imgOptions;
        @BindView(R.id.ll_desc)
        RelativeLayout llDesc;
        View childView;
        @BindView(R.id.chip_add)
        Chip chipAdd;

        Holder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public AddTrackAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_moodtrack_add, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.tvTitle.setText(list.get(position).getName());
        String url = Constants.URL_IMAGE_MOODTRACK + list.get(position).getImage();
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        ColorDrawable cd = new ColorDrawable(color);
        loadImagesMoodFromURL(context, holder.imgCover, url, cd);

        holder.imgOptions.setOnClickListener(view -> {
            showOptions(holder.imgOptions, list.get(position).getId(), list.get(position).getName());
        });

        holder.llDesc.setOnClickListener(view -> {
            parrent.addTrackToMood(list.get(position).getId(), list.get(position).getName());
        });

        holder.chipAdd.setOnClickListener(view -> {
            parrent.addTrackToMood(list.get(position).getId(), list.get(position).getName());
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void showOptions(ImageView btnOptions, String idMood, String name) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.mood_options_del);
        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.remove:
                    confirmation(idMood, name);
            }
            return false;
        });
        makePopForceShowIcon(popup);
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }

    private void confirmation(String idMood, String name) {
        String msg = context.getResources().getString(R.string.label_delete)+" '"+name+"' "+
                    context.getResources().getString(R.string.msg_confirm_deleted_mood);
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_warning_sign);
        alertDialog.setTitle(context.getResources().getString(R.string.label_confirmation_delete));
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.button_ok),
                (dialog, which) -> {
                    dialog.dismiss();

                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getResources().getString(R.string.label_cancel),
                (dialog, which) -> {
                    dialog.dismiss();
                });
        alertDialog.show();
    }
}