package com.nutech.burgundy.ui.view.track;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 12/28/17.
 * Xeranta Mobile Solutions
 */

public interface TracksView extends BaseView {

    void showTracks(DataTracks dataTracks);

    void showError(String error);
}
