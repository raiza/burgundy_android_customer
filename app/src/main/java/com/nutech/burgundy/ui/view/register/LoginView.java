package com.nutech.burgundy.ui.view.register;

import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 9/27/17.
 * Xeranta Mobile Solutions
 */

public interface LoginView extends BaseView {

    void showLoginSuccess(MemberCheck data);

    void showLoginFailed(String error);

    void sucessCheckMember(MemberCheck data, DataMember dataMember);
    void sucessLoginStore();

    void errorCheckMember(String error);
}
