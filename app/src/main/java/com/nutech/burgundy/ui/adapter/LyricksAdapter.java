package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataLiricks;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class LyricksAdapter extends RecyclerView.Adapter<LyricksAdapter.LyricksHolder> {

    private Context context;
    private List<DataLiricks> list;

    public LyricksAdapter(
            Context context, List<DataLiricks> list) {
        this.context = context;
        this.list = list;
    }

    class LyricksHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_lyricks)
        TextView tvLyricks;

        View childView;

        LyricksHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public LyricksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_lyricks, parent, false);
        return new LyricksHolder(view);
    }

    @Override
    public void onBindViewHolder(LyricksHolder holder, int position) {

        holder.tvLyricks.setText(list.get(position).getText());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
