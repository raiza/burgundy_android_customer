package com.nutech.burgundy.ui.view.point;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.presenter.MyProfilePresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.SegmentedButton;
import com.nutech.burgundy.ui.view.profile.MyProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * @author Dafian on 11/10/17
 */

public class MyPointFragment extends BaseFragment implements MyProfileView {

    @BindView(R.id.tv_user_point)
    TextView tvUserPoint;
    @BindView(R.id.segmented)
    SegmentedButton buttons;
    @BindView(R.id.frame)
    FrameLayout mainFrame;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;

    private Unbinder unbinder;

    private SessionManager sessionManager;
    private MyProfilePresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = getSessionManager();
        presenter = new MyProfilePresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_point, container, false);
        unbinder = ButterKnife.bind(this, view);

        replaceFragment(new RewardFragment());
        setUpTabLayout();

        String customerId = sessionManager.getDataMember().getId();
        presenter.getMemberById(customerId);

        return view;
    }

    private void setUpTabLayout() {

        buttons.clearButtons();
        buttons.addButtons(
                getString(R.string.label_reward),
                getString(R.string.label_point));

        // First button is selected
        buttons.setPushedButtonIndex(0);

        // Some example click handlers. Note the click won't get executed
        // if the segmented button is already selected (dark blue)
        buttons.setOnClickListener((SegmentedButton.OnClickListenerSegmentedButton) index -> {
            if (index == 0) {
                replaceFragment(new RewardFragment());
            } else {
                replaceFragment(new PointFragment());
            }
        });

    }

    private void replaceFragment(Fragment fragment) {

        if (mainFrame != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(mainFrame.getId(), fragment).commit();
        }
    }

    @Override
    public void showCustomer(DataMember member) {

        sessionManager.setMemberPoint(member.getPoint());

        if (!TextUtils.isEmpty(member.getPoint())) {
            String point = member.getPoint() + " Point";
            tvUserPoint.setText(point);
        }else {
            String point = "0" + " Point";
            tvUserPoint.setText(point);
        }

        String ImageUrl = Constants.URL_IMAGE_MEMBER + member.getImage();
        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.default_user)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivProfile);

    }

    @Override
    public void showSubscribeStatus(DataUserSubscribe userSubscribe) {

    }

    @Override
    public void showSuccess(String success) {

    }

    @Override
    public void showErrorCustomer(String error) {
    }

    @Override
    public void showErrorSubscribeStatus(String error) {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
