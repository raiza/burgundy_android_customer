package com.nutech.burgundy.ui.view.soundtrack;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.chip.Chip;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.presenter.soundtrack.DetailMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.SimpleItemTouchHelperCallback;
import com.nutech.burgundy.ui.adapter.soundtrack.TracksMoodAdapter;
import com.nutech.burgundy.ui.base.BasePhotoFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class DetailMoodtrackFragment extends BasePhotoFragment implements DetailMoodtrackView {

    private int REQUEST_CODE_EDIT = 100;

    @BindView(R.id.img_cover)
    RoundedImageView imgCover;
    @BindView(R.id.chip_play)
    Chip play;
    @BindView(R.id.chip_edit)
    Chip edit;
    @BindView(R.id.rv_tracks)
    RecyclerView rvTracks;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_all_songs)
    TextView tvAllSongs;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;

    private Unbinder unbinder;
    private DetailMoodtrackPresenter presenter;
    private String idMood;
    private boolean autoPlay;
    private String energies;
    private String sounds;
    private String decades;
    private String genres;
    private int PER_PAGE = 10;
    private List<DataTracks> tempPageTrackList;
    private boolean myMoodtrack = false;
    private TracksMoodAdapter adapter;
    private List<DataTracks> mTracksList;
    private ProgressDialog pDialog;
    private Handler handler = new Handler();
    private DetailMoodtrackFragment fragment;
    private LinearLayoutManager linearLayoutManager;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("detail", Objects.requireNonNull(intent.getExtras()).toString());
        }
    };

    public DetailMoodtrackFragment(String id, boolean autoPlay) {

        idMood = id;
        this.autoPlay = autoPlay;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new DetailMoodtrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_moodtrack, container, false);

        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(broadcastReceiver,new IntentFilter("moodtrack_controls"));
        initViews(view);
        initEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    private void initViews(View view) {

        unbinder = ButterKnife.bind(this, view);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        Constants.ParamValue.moodId = idMood;
        fragment = this;
                loadData();
    }

    private void initEvents() {
        play.setOnClickListener(view1 -> {
            playMoodtrack();
        });

        edit.setOnClickListener(view1 -> {

            Intent intent = new Intent(getActivity(), EditMoodtrackActivity.class);
            intent.putExtra("id", idMood);
            intent.putExtra("genre_ids", genres);
            intent.putExtra("energy_ids", energies);
            intent.putExtra("sound_ids", sounds);
            intent.putExtra("decade", decades);

            startActivityForResult(intent, REQUEST_CODE_EDIT);
        });

            rvTracks.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItem = linearLayoutManager.getChildCount();
                    int total = linearLayoutManager.getItemCount();
                    int pastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                    if ((visibleItem+pastVisibleItem) >= total) {
                        if (total < mTracksList.size()) {
                            for (int i = total; i < Math.min(total+PER_PAGE,mTracksList.size());i++) {
                                tempPageTrackList.add(mTracksList.get(i));
                            }
                            recyclerView.post(() -> adapter.notifyDataSetChanged());
                        }
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this::loadData);

    }

    private void playMoodtrack() {
        if (mTracksList.size() > 0){
            getSessionManager().setCurrentMoodTrack(idMood);
            navigation.playingMusic(0, mTracksList);
            navigation.pushFragment(
                    new NowPlayingFragment(
                            mTracksList.get(0).getId(),
                            mTracksList.get(0).getInclude_karaoke(),
                            mTracksList.get(0).getKaraoke_id(),
                            mTracksList.get(0).getUrl() +
                                    mTracksList.get(0).getSource()),
                    getString(R.string.label_now_playing),
                    true);
        }
    }

    public void loadData() {
//        showProgressDialog();
        Map<String, String> params = new HashMap<>();

        try {
            params.put("id", idMood);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("** Param ** "+ params);
        presenter.getData(params);
    }

    public void updatePositionTracks(List<DataTracks> tracks, DataTracks track,
                                     int fromPosition, int toPosition) {
        mTracksList = tracks;
        navigation.updatePositionTracks(tracks);
        presenter.updatePositionMoodtrackTracks(idMood,track.getId(),
                String.valueOf(fromPosition),String.valueOf(toPosition));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMoodtracks(Moodtrack dataMoodtracks) {

        new Thread(() -> handler.post(() -> {
            hideProgressDialog();
            if (dataMoodtracks != null) {

                navigation.updateTitle(dataMoodtracks.getName());
                genres = dataMoodtracks.getGenreIds();
                energies = dataMoodtracks.getEnergyids();
                decades = dataMoodtracks.getDecadeIds();
                sounds = dataMoodtracks.getSoundIds();

                try {
                    mShimmerViewContainer.stopShimmer();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    rvTracks.setVisibility(View.VISIBLE);
                    if (getSessionManager().getUserType().contains(Constants.UserType.USER_STORE)) {
                        if (dataMoodtracks.getStoreId() != null) {
                            if (dataMoodtracks.getStoreId().equals(getSessionManager().getUserStore().getStore_id())) {
                                if (!getSessionManager().getUserStore().getRole().equals(Constants.StoreMemberRole.SUPER_ADMIN)) {
                                    edit.setVisibility(View.GONE);
                                } else {
                                    edit.setVisibility(View.VISIBLE);
                                }
                                play.setVisibility(View.VISIBLE);
                                myMoodtrack = true;
                            } else {
                                play.setVisibility(View.VISIBLE);
                                myMoodtrack = false;
                            }
                        } else {
                            play.setVisibility(View.VISIBLE);
                            myMoodtrack = false;
                        }
                    } else if (dataMoodtracks.getCustomerId().contains(getSessionManager().getDataMember().getId())) {
                        edit.setVisibility(View.VISIBLE);
                        play.setVisibility(View.VISIBLE);
                    }

                    tvAllSongs.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);

                    mTracksList = dataMoodtracks.getDataTracks();
                    if (!autoPlay) {
                        tempPageTrackList = new ArrayList<>();
                        if (dataMoodtracks.getDataTracks().size() >= 10) {
                            for (int i = 0;i< 10;i++) {
                                tempPageTrackList.add(mTracksList.get(i));
                            }
                        } else {
                            tempPageTrackList.addAll(mTracksList);
                        }

                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        rvTracks.setLayoutManager(linearLayoutManager);
                        rvTracks.setItemAnimator(new DefaultItemAnimator());
                        rvTracks.addItemDecoration(new DividerItemDecoration(
                                Objects.requireNonNull(getActivity()), LinearLayoutManager.VERTICAL));
                        adapter = new TracksMoodAdapter(getActivity(), tempPageTrackList, navigation, "", fragment,myMoodtrack);
                        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_animation_fall_down);
                        rvTracks.setLayoutAnimation(animation);
                        rvTracks.setAdapter(adapter);

                        if (getSessionManager().getUserStore().getRole().equals(Constants.StoreMemberRole.SUPER_ADMIN)) {
                            ItemTouchHelper.Callback callback =
                                    new SimpleItemTouchHelperCallback(adapter);
                            ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                            touchHelper.attachToRecyclerView(rvTracks);
                        }


                        loadImagesFromURL(getActivity(), imgCover, Constants.URL_IMAGE_MOODTRACK + dataMoodtracks.getImage());
                    } else {
                        autoPlay = false;
                        playMoodtrack();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        })).start();
    }

    @Override
    public void showErrorMoodtracks(String error) {
        swipeRefreshLayout.setRefreshing(false);
        hideProgressDialog();
    }

    @Override
    public void showSuccessUpdatePosition(String success) {

    }

    @Override
    public void showErrorUpdatePosition(String error) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_EDIT) {
            loadData();
        }
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage(getString(R.string.loading));
        pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }

}