package com.nutech.burgundy.ui.view.playlist;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.presenter.NewPlaylistPresenter;
import com.nutech.burgundy.ui.base.BasePhotoFragment;
import com.nutech.burgundy.utils.Helper;
import com.theartofdev.edmodo.cropper.CropImage;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 11/14/17.
 * Xeranta Mobile Solutions
 */

public class NewPlaylistFragment extends BasePhotoFragment implements NewPlaylistView {

    public static final int REQUEST_CODE_CAMERA = 100;
    private static final String[] PERMISSIONS_LIST = {
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @OnClick(R.id.btn_cancel)
    public void cancel(){
    }

    @OnClick(R.id.btn_save)
    public void save() {
        if (!TextUtils.isEmpty(etName.getText())){

            loadingIndicatorView.smoothToShow();
            showProgressDialog();
            String memberId = sessionManager.getDataMember().getId();
            Map<String, String> params = new HashMap<String, String>();

            try {
                params.put("customer_id", memberId);
                params.put("name", etName.getText().toString());
                params.put("description", etDescription.getText().toString());
                params.put("public", pType);
                params.put("tags", "");
                params.put("image",baseImage);
            } catch (Exception e) {

            }
//            System.out.println("** Param ** "+ params);
            presenter.saveNewPlaylist(params);
        }
    }

    @OnClick(R.id.rl_cover)
    public void uploadCover() {
        showDialogChoice();
//        showDialog();
    }

    @BindView(R.id.switch_type)
    Switch switch_type;
    @BindView(R.id.imageView)
    ImageView imageType;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_description)
    EditText etDescription;
    @BindView(R.id.image_cover)
    ImageView imageCover;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private NewPlaylistPresenter presenter;
    private SessionManager sessionManager;
    private Unbinder unbinder;

    private int SELECT_IMAGE = 2;
    private int REQUEST_IMAGE_CAPTURE = 1;
    private String baseImage = "";
    private String pType = "0";
    private Uri cropImageUri;
    private int compress = 0;
    private ProgressDialog pDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new NewPlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);

        initPermission();

        switch_type.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    pType = "1";
                    switch_type.setText("Public Playlist");
                    imageType.setBackgroundResource(R.drawable.ic_action_private);
                }else {
                    pType = "0";
                    switch_type.setText("Private Playlist");
                    imageType.setBackgroundResource(R.drawable.ic_action_public);
                }
            }
        });

        return view;
    }

    void initPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS_LIST,
                        REQUEST_CODE_CAMERA);
            }
        } else {
//            dispatchTakePictureIntent();
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void success(String success) {
        loadingIndicatorView.smoothToHide();
        hideProgressDialog();
        getActivity().onBackPressed();

    }

    @Override
    public void error(String error) {
        loadingIndicatorView.smoothToHide();
        hideProgressDialog();
        getActivity().onBackPressed();

    }

    private void showDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_choice_update_photo, null);

        RelativeLayout rlTakePhoto = (RelativeLayout) customView.findViewById(R.id.rl_take_photo);
        RelativeLayout rlSelectGallery = (RelativeLayout) customView.findViewById(R.id.rl_take_photo_gallery);
        RelativeLayout rlCancel = (RelativeLayout) customView.findViewById(R.id.rl_cancel);

        final BottomDialog dialog = new BottomDialog.Builder(getActivity())
                .setTitle("Upload Image")
                .setCustomView(customView)
                .build();

        rlTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                dispatchTakePictureIntent();
                showDialogChoice();

                dialog.dismiss();
            }
        });

        rlSelectGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_PICK,
//                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                intent.setType("image/*");
//                startActivityForResult(intent, SELECT_IMAGE );

                showDialogChoice();

                dialog.dismiss();
            }
        });

        rlCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void dispatchTakePictureIntent() {
        loadingIndicatorView.smoothToShow();
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

//    public void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == SELECT_IMAGE)
//        {
//            if (resultCode == Activity.RESULT_OK)
//            {
//                if (data != null)
//                {
//                    try
//                    {
//                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
//                        imageCover.setImageBitmap(bitmap);
//                        converToBase64(bitmap);
//
//                    } catch (IOException e)
//                    {
//                        e.printStackTrace();
//                    }
//
//                }
//            } else if (resultCode == Activity.RESULT_CANCELED)
//            {
//            }
//        }
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
//            imageCover.setImageBitmap(imageBitmap);
//            converToBase64(imageBitmap);
//        }
//
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE) {
            compress = 100;

            if (resultCode == Activity.RESULT_OK) {

                Uri imageUri = CropImage.getPickImageResultUri(context, data);

                if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri)) {
                    cropImageUri = imageUri;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                                Constants.PERMISSIONS_PHOTO,
                                CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                    }
                } else {
                    startCropImageMoodtrackActivity(imageUri);
                }
            } else {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            compress = 25;

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == Activity.RESULT_OK) {

                Uri resultImageUri = result.getUri();

//                imageCover.setImageURI(null);
                imageCover.setImageURI(resultImageUri);
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), resultImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                baseImage = Helper.convertToBase64(bitmap);

                System.out.println("#### baseImage "+baseImage);


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(context, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (cropImageUri != null && grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCropImageMoodtrackActivity(cropImageUri);
            } else {
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Uploading...");
        pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }
}
