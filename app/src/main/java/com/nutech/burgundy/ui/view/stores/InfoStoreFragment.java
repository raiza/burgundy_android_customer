package com.nutech.burgundy.ui.view.stores;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class InfoStoreFragment extends BaseFragment {
    @BindView(R.id.v_promo)
    View vPromo;
    @BindView(R.id.textView_chart)
    TextView textViewChart;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.iv_button)
    ImageView ivButton;
    @BindView(R.id.tv_open_map)
    TextView tvOpenMap;
    private DataStore store;
    private Unbinder unbinder;


    public InfoStoreFragment(DataStore store) {
        this.store = store;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_store, container, false);
        unbinder = ButterKnife.bind(this, view);

        initEvent();
        initView();
        return view;
    }

    private void initView() {
        tvAddress.setText(store.getAddress());
    }

    private void initEvent() {
        tvAddress.setOnClickListener(view -> {
            if (tvAddress.getEllipsize() == TextUtils.TruncateAt.END) {
                tvAddress.setMaxLines(Integer.MAX_VALUE);
                tvAddress.setEllipsize(null);
            } else {
                tvAddress.setMaxLines(3);
                tvAddress.setEllipsize(TextUtils.TruncateAt.END);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder = unbinder;
    }
}
