package com.nutech.burgundy.ui.base;

import android.os.Bundle;
import androidx.fragment.app.Fragment;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.about.AboutFragment;
import com.nutech.burgundy.ui.view.artists.ArtistsFragment;
import com.nutech.burgundy.ui.view.artists.TabArtistFragment;
import com.nutech.burgundy.ui.view.feedback.FeedbackFragment;
import com.nutech.burgundy.ui.view.follow.OfficialFragment;
import com.nutech.burgundy.ui.view.genre.DetailGenreFragment;
import com.nutech.burgundy.ui.view.history.HistoryExchangeFragment;
import com.nutech.burgundy.ui.view.home.FragmentDiscover;
import com.nutech.burgundy.ui.view.home.FragmentMyMusic;
import com.nutech.burgundy.ui.view.livekaraoke.HistoryKaraokeFragment;
import com.nutech.burgundy.ui.view.livekaraoke.player.PlayerFragment;
import com.nutech.burgundy.ui.view.livekaraoke.record.RecordFragment;
import com.nutech.burgundy.ui.view.playback.NowPlayingActivity;
import com.nutech.burgundy.ui.view.playlist.DetailPlaylistFragment;
import com.nutech.burgundy.ui.view.playlist.PlaylistFragment;
import com.nutech.burgundy.ui.view.point.MyPointFragment;
import com.nutech.burgundy.ui.view.profile.MyProfileFragment;
import com.nutech.burgundy.ui.view.recomended.RecomendedFragment;
import com.nutech.burgundy.ui.view.search.SearchFragment;
import com.nutech.burgundy.ui.view.setting.SettingFragment;
import com.nutech.burgundy.ui.view.topchart.TopChartFragment;
import com.nutech.burgundy.ui.view.vip.VipFragment;

/*
 * Base activity for activities that need to show a playback control fragment when media is playing.
 */

/**
 * @author Arif-Setiawan on 8/24/17
 */

public abstract class BaseActivity extends BaseAppCompatActivity {

    private DmApplication dmApplication;

    public FragmentNavigation navigation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dmApplication = (DmApplication) getApplicationContext();
    }

    @Override
    public void onAttachFragment(android.app.Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof FragmentNavigation) {
            navigation = (FragmentNavigation) fragment;
        }
    }


    protected void setFragmentTitle(Fragment fragmentTitle) {

        if (getSupportActionBar() == null) {
            return;
        }

        if (fragmentTitle instanceof FragmentDiscover) {
            getSupportActionBar().setTitle(R.string.label_home);

        } else if (fragmentTitle instanceof FragmentMyMusic) {
            getSupportActionBar().setTitle(R.string.my_moodtrack);

        } else if (fragmentTitle instanceof VipFragment) {
            getSupportActionBar().setTitle(R.string.vip_menu);

        } else if (fragmentTitle instanceof MyPointFragment) {
            getSupportActionBar().setTitle(R.string.label_my_point);

        } else if (fragmentTitle instanceof OfficialFragment) {
            getSupportActionBar().setTitle(R.string.follow_menu);

        } else if (fragmentTitle instanceof FeedbackFragment) {
            getSupportActionBar().setTitle(R.string.feedback_menu);

        } else if (fragmentTitle instanceof AboutFragment) {
            getSupportActionBar().setTitle(R.string.about_menu);

        } else if (fragmentTitle instanceof SettingFragment) {
            getSupportActionBar().setTitle(R.string.setting_menu);

        } else if (fragmentTitle instanceof ArtistsFragment) {
            getSupportActionBar().setTitle(R.string.label_menu_artists);

        } else if (fragmentTitle instanceof TabArtistFragment) {
            getSupportActionBar().setTitle(dmApplication.getTitle());

        } else if (fragmentTitle instanceof PlaylistFragment) {
            getSupportActionBar().setTitle(R.string.label_menu_playlist);

        } else if (fragmentTitle instanceof DetailPlaylistFragment) {
            getSupportActionBar().setTitle(dmApplication.getTitle());

        } else if (fragmentTitle instanceof SearchFragment) {
            getSupportActionBar().setTitle(R.string.search_menu);

        } else if (fragmentTitle instanceof DetailGenreFragment) {
            getSupportActionBar().setTitle(R.string.genre_menu);

        } else if (fragmentTitle instanceof TopChartFragment) {
            getSupportActionBar().setTitle(R.string.top_chart_menu);

        } else if (fragmentTitle instanceof HistoryExchangeFragment) {
            getSupportActionBar().setTitle(R.string.history_menu);

        } else if (fragmentTitle instanceof RecordFragment) {
            getSupportActionBar().setTitle(R.string.record_live_karaoke);

        } else if (fragmentTitle instanceof PlayerFragment) {
            getSupportActionBar().setTitle(R.string.player_live_karaoke);
        }else if (fragmentTitle instanceof HistoryKaraokeFragment) {
            getSupportActionBar().setTitle(R.string.karaoke_menu);
        }else if (fragmentTitle instanceof RecomendedFragment) {
            getSupportActionBar().setTitle(getString(R.string.label_recomended_for_you));
        }else if (fragmentTitle instanceof NowPlayingActivity) {
            getSupportActionBar().setTitle(getString(R.string.label_now_playing));
        }else if (fragmentTitle instanceof MyProfileFragment) {
            getSupportActionBar().setTitle(getString(R.string.label_myprofile));
        }
    }
}
