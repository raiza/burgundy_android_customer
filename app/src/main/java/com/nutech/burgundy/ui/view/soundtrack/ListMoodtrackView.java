package com.nutech.burgundy.ui.view.soundtrack;

import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public interface ListMoodtrackView extends BaseView {

    void showMoodtracks(List<Moodtrack> dataMoodtracks);

    void showErrorMoodtracks(String error);

    void showMoodtracksPlayingStore(List<Moodtrack> dataMoodtracks);

    void showErrorMoodtracksPlayingStore(String error);

    void showSuccessRemove(String success);

    void showErrorRemove(String error);

    void showSuccessSchedule(String success);

    void showErrorSchedule(String error);
}
