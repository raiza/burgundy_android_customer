package com.nutech.burgundy.ui.view.playback;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.lib.musicplayerview.MusicPlayerView;
import com.nutech.burgundy.presenter.PlayingPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Arif Setiawan on 9/22/17.
 * Xeranta Mobile Solutions
 */

@SuppressLint("ValidFragment")
public class NowPlayingFragment extends BaseFragment implements PlayingView, JcPlayerView.JcPlayerViewServiceListener {

    @BindView(R.id.mpv)
    MusicPlayerView mpvMusic;
    @BindView(R.id.btn_play)
    ImageButton btnPlay;
    @BindView(R.id.btn_next)
    ImageButton btnNext;
    @BindView(R.id.btn_prev)
    ImageButton btnPrev;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loading;
    @BindView(R.id.iv_backdrop)
    ImageView llBackdrop;
    @BindView(R.id.btn_favorite)
    ShineButton btnFavorite;
    @BindView(R.id.seekbar_duration)
    SeekBar seekBarDuration;
    @BindView(R.id.tv_current_progress)
    TextView tvCurrentProgress;
    @BindView(R.id.tv_max_progress)
    TextView tvMaxprogress;
    @BindView(R.id.ll_lirick)
    LinearLayout llLiricks;
    @BindView(R.id.txt_lirick_passed)
    TextView txtLiricksPassed;
    @BindView(R.id.iv_shuffle)
    ImageView ivShuffle;
    @BindView(R.id.txt_lirick_focused)
    TextView txtLiricksFocused;
    @BindView(R.id.txt_lirick_next)
    TextView txtLiricksNext;
    @BindView(R.id.btn_karaoke)
    Button btnKaraoke;
    @BindView(R.id.ll_karaoke)
    LinearLayout llKaraoke;
    @BindView(R.id.tv_track_name)
    TextView tvTrackName;
    @BindView(R.id.btn_list_track)
    ImageButton btnListTrack;
    @BindView(R.id.linearLayout)
    RelativeLayout linearLayout;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;

    private PlayingPresenter presenter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;
    private String id, includeKaraoke, karaokeId, urlTrack;

    public NowPlayingFragment() {

    }

    @SuppressLint("ValidFragment")
    public NowPlayingFragment(String idTracks, String pIncludeKaraoke, String pKaraokeId,
                              String sourceTrack) {
        this.id = idTracks;
        this.includeKaraoke = pIncludeKaraoke;
        this.karaokeId = pKaraokeId;
        this.urlTrack = sourceTrack;
    }

    @Override
    public void onStart() {
        super.onStart();
//        initEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PlayingPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        dmApplication = (DmApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_now_playing, container, false);
        ButterKnife.bind(this, view);

        initViews();
        initEvent();

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initViews() {

//        if (!TextUtils.isEmpty(includeKaraoke)){
//            if (includeKaraoke.equals("1")){
//                btnKaraoke.setVisibility(View.VISIBLE);
//            }else if (includeKaraoke.equals("0")){
//                btnKaraoke.setVisibility(View.GONE);
//            }
//        }


    }

    public void initEvent() {

        navigation.updatePlayer(mpvMusic, btnPlay, btnNext, btnPrev, loading, llBackdrop,
                seekBarDuration, tvCurrentProgress, tvMaxprogress, txtLiricksPassed, txtLiricksFocused,
                txtLiricksNext, llLiricks, tvTrackName, btnKaraoke, includeKaraoke, karaokeId, urlTrack, ivShuffle, btnListTrack);

        btnFavorite.setOnClickListener(view -> {
            Map<String, String> params = new HashMap<String, String>();
            try {
                params.put("customer_id", sessionManager.getDataMember().getId());
                params.put("track_id", sessionManager.getCurrentTracksId());
            } catch (Exception ignored) {

            }
            presenter.addTracksFavorite(params);
        });
        btnKaraoke.setOnClickListener(view -> {
            navigation.stopMusic();
            navigation.updateControll(true);

            dmApplication.setNavigation(navigation);

//                Intent intent = new Intent(getActivity(), SampleUploadVideo.class);
//                startActivity(intent);

            Intent intent = new Intent(getActivity(), LiveKaraokeActivity.class);
            intent.putExtra("karaoke_id", karaokeId);
            intent.putExtra("track_id", sessionManager.getCurrentTracksId());
            intent.putExtra("url_track", urlTrack);
            startActivity(intent);
        });

        Constants.ParamValue.isNOW_PLAYING = true;
        navigation.updateControll(true);

    }

    @Override
    public void showSucces(String success) {
//        btnFavorite.setBackgroundResource(R.drawable.ic_favorite_pink_500_48dp);
    }

    @Override
    public void showError(String error) {
//        Toast.makeText(getActivity(), "error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
    }

    @Override
    public void onCompletedAudio() {

    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onContinueAudio() {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void addLogAds(String id, String trackId) {

    }

    @Override
    public void onTimeChanged(long currentTime) {

    }

    @Override
    public void updateTitle(String title, String artisName) {

    }

    @Override
    public void updateCover(String cover) {

    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {
        if (val) {
            ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
        } else {
            ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        }
    }

    @Override
    public void removeNotification() {

    }

    @OnClick(R.id.iv_shuffle)
    public void onClick() {
    }

    static
    class ViewHolder {
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
