package com.nutech.burgundy.ui.adapter.soundtrack;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.soundtrack.ListMoodTrackFragment;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class MoodtrackAdapter extends BaseAdapter<MoodtrackAdapter.Holder> {

    private Context context;
    private List<Moodtrack> list;
    private ListMoodTrackFragment parrent;
    private SessionManager sessionManager;
    private EditText timer;

    public MoodtrackAdapter(Context context, SessionManager sessionManager, List<Moodtrack> moodtrackList, ListMoodTrackFragment listMoodTrackFragment) {
        this.context = context;
        this.list = moodtrackList;
        this.sessionManager = sessionManager;
        this.parrent = listMoodTrackFragment;
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_cover)
        ImageView imgCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.img_options)
        ImageView imgOptions;
        @BindView(R.id.ll_desc)
        LinearLayout llDesc;
        View childView;

        Holder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @NonNull
    @Override
    public MoodtrackAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_moodtrack, parent, false);
        return new Holder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(Holder holder, int position) {

        if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
            if (!sessionManager.getUserStore().getRole().equals(Constants.StoreMemberRole.SUPER_ADMIN)) {
                holder.imgOptions.setVisibility(View.GONE);
            } else {
                holder.imgOptions.setVisibility(View.VISIBLE);
            }
        }

        holder.tvTitle.setText(list.get(position).getName());
        String url = Constants.URL_IMAGE_MOODTRACK + list.get(position).getImage();
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        ColorDrawable cd = new ColorDrawable(color);
        loadImagesMoodFromURL(context, holder.imgCover, url, cd);

        holder.imgOptions.setOnClickListener(view -> {
            showOptions(holder.imgOptions, list.get(position).getId(), list.get(position).getName());
        });

        holder.llDesc.setOnClickListener(view -> parrent.moveToDetail(position));
        holder.imgCover.setOnClickListener(view -> parrent.moveToDetail(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showOptions(ImageView btnOptions, String idMood, String name) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.mood_options_del);
        popup.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.remove) {
                confirmation(idMood, name);
            } else if (item.getItemId() == R.id.add_schedule) {
                add_to_schedule(idMood,name);
            }
            return false;
        });
        makePopForceShowIcon(popup);
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void confirmation(String idMood, String name) {
        String msg = context.getResources().getString(R.string.label_delete)+" '"+name+"' "+
                    context.getResources().getString(R.string.msg_confirm_deleted_mood);
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_warning_sign);
        alertDialog.setTitle(context.getResources().getString(R.string.label_confirmation_delete));
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getResources().getString(R.string.button_ok),
                (dialog, which) -> {
                    dialog.dismiss();
                    parrent.removeMoodtrack(idMood);
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, context.getResources().getString(R.string.label_cancel),
                (dialog, which) -> {
                    dialog.dismiss();
                });
        Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;

        alertDialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void add_to_schedule(String idMood, String name) {

        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.form_add_to_schedule);
        dialog.setCancelable(false);
        Button save = dialog.findViewById(R.id.btn_save);
        NumberPicker numberPicker = dialog.findViewById(R.id.np_day_of_week);
        Button cancel = dialog.findViewById(R.id.btn_cancel);
        timer = dialog.findViewById(R.id.et_timer);
        CheckBox repeat = dialog.findViewById(R.id.cb_repeat);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;
        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(context.getResources().getStringArray(R.array.days_array).length-1);
        numberPicker.setDisplayedValues(context.getResources().getStringArray(R.array.days_array));

        dialog.show();

        timer.setOnClickListener(v -> {
            showTimeDialog();
        });

        cancel.setOnClickListener(v -> {
            dialog.dismiss();
        });

        save.setOnClickListener(view -> {
            if (timer.getText().toString().matches("")) {
                Toast.makeText(context,context.
                        getString(R.string.err_field_required),Toast.LENGTH_SHORT).show();
            } else {
                parrent.setSchedulerMoodtrack(context,idMood,
                        String.valueOf(numberPicker.getValue()),
                        timer.getText().toString(),repeat.isChecked());
                Toast.makeText(context,R.string.label_saved,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    private void showTimeDialog() {

        Calendar calendar = Calendar.getInstance();

        @SuppressLint({"SetTextI18n", "DefaultLocale"}) TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> {
                    timer.setText(String.format("%02d:%02d", hourOfDay, minute));
        },
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(context));
        Objects.requireNonNull(timePickerDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;

        timePickerDialog.show();
    }

    private void showDayOfWeekDialog() {

        Calendar calendar = Calendar.getInstance();

        @SuppressLint({"SetTextI18n", "DefaultLocale"}) TimePickerDialog timePickerDialog = new TimePickerDialog(context, (view, hourOfDay, minute) -> timer.setText(String.format("%02d:%02d", hourOfDay, minute)),
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                DateFormat.is24HourFormat(context));
        Objects.requireNonNull(timePickerDialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;

        timePickerDialog.show();
    }
}