package com.nutech.burgundy.ui.view.point;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataPoint;
import com.nutech.burgundy.presenter.PointPresenter;
import com.nutech.burgundy.ui.adapter.PointAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * @author Dafian on 11/10/17
 */

public class PointFragment extends BaseFragment implements PointView {

    @BindView(R.id.rv_point)
    RecyclerView rvPoint;

    private Unbinder unbinder;
    private PointPresenter presenter;

    private List<DataPoint> pointList;
    private PointAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PointPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_point, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();
        loadingData();

        return view;
    }

    @Override
    public void showPointAll(List<DataPoint> dataPointList) {

        this.pointList.clear();
        this.pointList.addAll(dataPointList);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String error) {

        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private void initView() {

        pointList = new ArrayList<>();
        adapter = new PointAdapter(pointList);

        rvPoint.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvPoint.setItemAnimator(new DefaultItemAnimator());
        rvPoint.setAdapter(adapter);
    }

    private void initEvent() {

        rvPoint.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvPoint, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

//                showErrorAdsList(pointList.get(position).getName());
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void loadingData() {
        presenter.getPointAll();
    }
}
