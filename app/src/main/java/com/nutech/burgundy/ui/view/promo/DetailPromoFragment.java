package com.nutech.burgundy.ui.view.promo;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.presenter.PromoPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 12/29/17.
 * Xeranta Mobile Solutions
 */

public class DetailPromoFragment extends BaseFragment implements PromoView {

    @BindView(R.id.image_promo)
    ImageView imgPromo;
    @BindView(R.id.title_promo)
    TextView tvTitlePromo;
    @BindView(R.id.content_promo)
    TextView tvContentPromo;
    @BindView(R.id.sv_promo)
    ScrollView svPromo;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private Unbinder unbinder;
    private PromoPresenter presenter;
    private String promotionId;

    public DetailPromoFragment(String id) {
        this.promotionId = id;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PromoPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_promo, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        getData();

        return view;
    }

    private void getData(){
        presenter.getPromoById(promotionId);
    }

    private void initView(){
        svPromo.setVisibility(View.GONE);
        loadingIndicatorView.smoothToShow();
    }

    @Override
    public void showPromoDetail(DataPromoSlide dataPromoSlide) {

        svPromo.setVisibility(View.VISIBLE);
        loadingIndicatorView.smoothToHide();

        String imageUrl = Constants.URL_IMAGE_PROMOTIONS + dataPromoSlide.getImage();

        Picasso.with(getActivity())
                .load(imageUrl)
                .error(R.drawable.image_dm_error)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(imgPromo);

        tvTitlePromo.setText(dataPromoSlide.getName());
        tvContentPromo.setText(dataPromoSlide.getContent());

    }

    @Override
    public void showError(String error) {
        svPromo.setVisibility(View.GONE);
        loadingIndicatorView.smoothToHide();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }
}
