package com.nutech.burgundy.ui.view.playlist;

import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 9/1/17.
 * Xeranta Mobile Solutions
 */

public interface PlaylistView extends BaseView {

    void showTracks(List<DataTracks> tracksList);
    void showAllPlaylist(List<DataPlaylist> dataPlaylists);
    void showErrorAllPlaylist(String error);
    void showErrorPlaylist(String error);
    void showAddSucces(String data);
    void showError(String error);
    void showRemoveSucces(String data);
    void showRemoveError(String error);

}
