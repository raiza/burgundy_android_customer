package com.nutech.burgundy.ui.view.playlist;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.presenter.NewPlaylistPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 11/14/17.
 * Xeranta Mobile Solutions
 */

public class UpdatePlaylistFragment extends BaseFragment implements NewPlaylistView {

    @OnClick(R.id.btn_cancel)
    public void cancel(){
    }

    @OnClick(R.id.btn_save)
    public void save() {
        if (!TextUtils.isEmpty(etName.getText())){

            loadingIndicatorView.smoothToShow();
            Map<String, String> params = new HashMap<String, String>();

            String pType = "";
            if (switch_type.getText().toString().contains("Public")){
                pType = "1";
            }else {
                pType = "0";
            }
            try {
                params.put("id", tempDataPlaylist.getId());
                params.put("name", etName.getText().toString());
                params.put("description", etDescription.getText().toString());
                params.put("public", pType);
                params.put("tags", "");
                params.put("image", baseImage);
            } catch (Exception e) {

            }
            System.out.println("** Param ** "+ params);
            presenter.updateNewPlaylist(params);
        }
    }

    @OnClick(R.id.rl_cover)
    public void uploadCover() {
        showDialog();
    }

    @BindView(R.id.switch_type)
    Switch switch_type;
    @BindView(R.id.imageView)
    ImageView imageType;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_description)
    EditText etDescription;
    @BindView(R.id.image_cover)
    ImageView imageCover;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    private NewPlaylistPresenter presenter;
    private SessionManager sessionManager;
    private Unbinder unbinder;

    private int SELECT_IMAGE = 2;
    private int REQUEST_IMAGE_CAPTURE = 1;
    private String baseImage = "";
    private DataPlaylist tempDataPlaylist = new DataPlaylist();

    public UpdatePlaylistFragment(DataPlaylist dataPlaylists) {
        super();
        this.tempDataPlaylist = dataPlaylists;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new NewPlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);

        switch_type.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    switch_type.setText("Private Playlist");
                    imageType.setBackgroundResource(R.drawable.ic_action_private);
                }else {
                    switch_type.setText("Public Playlist");
                    imageType.setBackgroundResource(R.drawable.ic_action_public);
                }
            }
        });

        initView();

        return view;
    }

    private void initView() {

        etName.setText(tempDataPlaylist.getName());
        etDescription.setText(tempDataPlaylist.getDescription());
        if (tempDataPlaylist.getPublicPlaylist().equals("1")){
           switch_type.setChecked(true);
        } else {
            switch_type.setChecked(false);
        }

        String ImageUrl = Constants.URL_IMAGE_PLAYLIST_WS + tempDataPlaylist.getImage();
        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(imageCover);
        convertingToBase64(imageCover.getDrawable());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void success(String success) {
        loadingIndicatorView.smoothToHide();
        getActivity().onBackPressed();

    }

    @Override
    public void error(String error) {
        loadingIndicatorView.smoothToHide();
        getActivity().onBackPressed();

    }

    private void showDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_choice_update_photo, null);

        RelativeLayout rlTakePhoto = (RelativeLayout) customView.findViewById(R.id.rl_take_photo);
        RelativeLayout rlSelectGallery = (RelativeLayout) customView.findViewById(R.id.rl_take_photo_gallery);
        RelativeLayout rlCancel = (RelativeLayout) customView.findViewById(R.id.rl_cancel);

        final BottomDialog dialog = new BottomDialog.Builder(getActivity())
                .setTitle("Upload Image")
                .setCustomView(customView)
                .build();

        rlTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

        rlSelectGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_IMAGE );
            }
        });

        rlCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                if (data != null)
                {
                    try
                    {

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        imageCover.setImageBitmap(bitmap);
                        converToBase64(bitmap);

                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                }
            } else if (resultCode == Activity.RESULT_CANCELED)
            {
            }
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageCover.setImageBitmap(imageBitmap);
            converToBase64(imageBitmap);
        }

    }

    private void converToBase64(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();

        baseImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void convertingToBase64(Drawable drawable){

        System.out.println("*** drawable *** "+drawable);
        try {
            BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
            Bitmap bitmap = bitmapDrawable .getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] imageInByte = stream.toByteArray();

            baseImage = Base64.encodeToString(imageInByte, Base64.DEFAULT);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
