package com.nutech.burgundy.ui.view.artists;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.presenter.ArtistsPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;
import com.nutech.burgundy.utils.Helper;
import com.roger.gifloadinglibrary.GifLoadingView;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 19/05/19.
 * Xeranta NuTech
 */

public class DetailArtisTracksFragment extends BaseFragment implements
        ArtistsView {

    private static final String KEY_ID_ARTIST = "key_id_artist";

    @BindView(R.id.rv_tracks_artists)
    RecyclerView rvTracksArtists;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.fab_play)
    FloatingActionButton fabPlay;

    private Unbinder unbinder;
    private ArtistsPresenter presenter;
    private String idArtists;
    private TracksAdapter adapter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;
    private GifLoadingView gifLoadingView;
    private List<DataTracks> mTracksList;
    private List<String> selectedTrack;
    private boolean isCheckbox = false;
    private TracksAdapter.OnItemCheckListener onItemCheckListener;

    public DetailArtisTracksFragment newInstance(String id) {
        DetailArtisTracksFragment fragment = new DetailArtisTracksFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_ID_ARTIST, id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.getTracksArtist(idArtists);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        idArtists = getArguments().getString(KEY_ID_ARTIST);

        presenter = new ArtistsPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        dmApplication = (DmApplication) getActivity().getApplicationContext();
        gifLoadingView = new GifLoadingView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_artist_tracks, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadingIndicatorView.smoothToShow();
        presenter.getTracksArtist(idArtists);
        selectedTrack = new ArrayList<>();
        onItemCheckListener = new TracksAdapter.OnItemCheckListener() {
            @Override
            public void onItemCheck(DataTracks dataTracks) {
                Log.e("check", "clicked");
                selectedTrack.add(dataTracks.getId());
            }

            @Override
            public void onItemUncheck(DataTracks dataTracks) {
                selectedTrack.remove(dataTracks.getId());
            }

        };

        fabPlay.setOnClickListener(view1 -> {
            if (mTracksList.size() > 0){
                navigation.playingMusic(0, mTracksList);
                navigation.pushFragment(
                        new NowPlayingFragment(
                                mTracksList.get(0).getId(),
                                mTracksList.get(0).getInclude_karaoke(),
                                mTracksList.get(0).getKaraoke_id(),
                                mTracksList.get(0).getUrl() + mTracksList.get(0).getSource()),
                        mTracksList.get(0).getName(),
                        true);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_track_moodtrack,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_to_moodtrack :
                Intent intent = new Intent(context, MoodListDialogActivity.class);
                Log.e("data", selectedTrack.toString());

                intent.putExtra("track_id", Helper.convertStringArrayToString(selectedTrack,","));
                intent.putExtra("track_name", getString(R.string.label_tracks));
                context.startActivity(intent);
                break;
            case R.id.select_all :
                adapter.selectAllItem();
                for (DataTracks tracks : mTracksList) {
                    selectedTrack.add(tracks.getId());
                }
                break;
            case R.id.unselect_all :
                adapter.unSelectAllItem();
                selectedTrack.clear();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isCheckbox() {
        return isCheckbox;
    }

    public void setCheckbox( boolean checkbox) {
        if (!checkbox) {
            setHasOptionsMenu(checkbox);
            adapter.isSelector(false,0);
            selectedTrack.clear();
        }

        isCheckbox = checkbox;
    }

    @Override
    public void showAllArtists(DataArtistList artistsList) {

    }

    @Override
    public void showGenre(List<DataGenre> genreList) {

    }

    @Override
    public void showArtistsTracks(final List<DataTracks> tracksList) {

        loadingIndicatorView.smoothToHide();
        rvTracksArtists.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracksArtists.setItemAnimator(new DefaultItemAnimator());
        rvTracksArtists.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
        adapter = new TracksAdapter(getActivity(), tracksList, navigation, "", onItemCheckListener);
        rvTracksArtists.setAdapter(adapter);

        mTracksList = tracksList;

        rvTracksArtists.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTracksArtists,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {
                        if (!isCheckbox) {
                            adapter.isSelector(true,position);
                            isCheckbox = true;
                            selectedTrack.add(mTracksList.get(position).getId());
                            setHasOptionsMenu(true);
                        }

                    }
                }));

    }

    @Override
    public void onPause(){
        super.onPause();
//        player.createNotification();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        player.kill();
    }

    @Override
    public void showArtistsAlbums(List<DataAlbums> albumsList) {

    }

    @Override
    public void showArtistsSearch(List<DataArtists> artistsList) {

    }

    @Override
    public void showErrorSearch(String error) {

    }

    @Override
    public void showError(String error) {
        Log.e("Error", error);
        loadingIndicatorView.smoothToHide();

    }


}
