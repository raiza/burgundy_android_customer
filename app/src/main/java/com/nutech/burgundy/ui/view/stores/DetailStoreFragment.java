package com.nutech.burgundy.ui.view.stores;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.chip.Chip;
import com.google.android.material.tabs.TabLayout;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.presenter.store.StorePresenter;
import com.nutech.burgundy.ui.adapter.TabAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetailStoreFragment extends BaseFragment implements StoresView {
    @BindView(R.id.iv_store_logo)
    ImageView ivStoreLogo;
    @BindView(R.id.vp_page)
    ViewPager vpPage;
    @BindView(R.id.tl_layout)
    TabLayout tlLayout;
    @BindView(R.id.rl_store_image)
    RelativeLayout rlStoreImage;
    @BindView(R.id.chip_favorite)
    Chip chipFavorite;
    @BindView(R.id.chip_follow)
    Chip chipFollow;

    private DataStore store;
    private StorePresenter storePresenter;
    private Unbinder unbinder;
    private TabAdapter tabAdapter;

    public DetailStoreFragment(DataStore store) {
        this.store = store;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        storePresenter = new StorePresenter(getDataManager(), getNetworkHelper());
        storePresenter.attachView(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_store, container, false);
        unbinder = ButterKnife.bind(this, view);

        initEvent();
        initView();

        return view;
    }

    private void initView() {
        String url = Constants.URL_STORE_LOGO + store.getLogo();
        if (getFragmentManager() != null) {
            tabAdapter = new TabAdapter(getFragmentManager());
            tabAdapter.addFragment(new ListInStoreFragment(store.getStore_id()), "List");
            tabAdapter.addFragment(new InfoStoreFragment(store), "Info");
        }


        chipFavorite.setVisibility(View.VISIBLE);
        chipFollow.setVisibility(View.VISIBLE);
        vpPage.setAdapter(tabAdapter);
        tlLayout.setupWithViewPager(vpPage);

        Picasso.with(getContext())
                .load(url)
                .into(ivStoreLogo);
    }

    private void initEvent() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void ShowStores(List<DataStore> storeList) {

    }

    @Override
    public void ShowStoresError(String error) {

    }
}
