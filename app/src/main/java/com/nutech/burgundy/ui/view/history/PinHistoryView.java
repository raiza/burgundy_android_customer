package com.nutech.burgundy.ui.view.history;

import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * @author Dafian on 10/17/17
 */

public interface PinHistoryView extends BaseView {

    void showDetailHistory(DataHistoryExchange dataHistoryExchanges);

    void showSuccess(String success);
    void showSuccessUpdate(String success);
    void showSuccessUsed(String success);

    void showError(String error);
    void showErrorUpdate(String error);
    void showErrorUsed(String error);
}
