package com.nutech.burgundy.ui.view.register;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.presenter.RegisterPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 9/19/17.
 * Xeranta Mobile Solutions
 */

public class SignUpFragment extends BaseFragment implements RegisterView {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_mobile_phone)
    EditText etMobilePhone;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_repassword)
    EditText etRePassword;
    @BindView(R.id.button_signup)
    Button btnSignUp;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.country_code)
    CountryCodePicker countryCodePicker;
    @BindView(R.id.et_phone_number)
    EditText etNumberPhone;
    @BindView(R.id.img_validity)
    ImageView imgValidity;
    @BindView(R.id.tv_validity)
    TextView tvValidity;

    private Unbinder unbinder;
    private RegisterPresenter presenter;
    private SessionManager sessionManager;
    private String tempActivationCode = "";
    private String android_id = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RegisterPresenter(getDataManager(), getNetworkHelper(), getSessionManager(), getActivity().getApplicationContext());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        unbinder = ButterKnife.bind(this, view);

        countryCodePicker.setDefaultCountryUsingNameCode("ID");
        registerCarrierEditText();
        android_id = Settings.Secure.getString(getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return view;
    }

    @OnClick(R.id.button_signup)
    public void toDoNext() {

        if (TextUtils.isEmpty(etName.getText().toString())) {
            etName.setError("Fields Required");
        }else if (TextUtils.isEmpty(etNumberPhone.getText().toString())){
            etNumberPhone.setError("Fields Required");
        } else {

            Helper.startAnimProgress(loadingIndicatorView);
            String android_id = Settings.Secure.getString(getContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            Map<String, String> params = new HashMap<String, String>();
            try {
                params.put("name", etName.getText().toString());
                params.put("country_code", countryCodePicker.getSelectedCountryCodeWithPlus());
                params.put("mobile_phone", countryCodePicker.getFullNumber().replace(countryCodePicker.getSelectedCountryCode(),""));
                params.put("device_id", android_id);
                params.put("register_id", sessionManager.getRegisterId());
                params.put("activation_code", Helper.generateCode());
            } catch (Exception e) {
            }

            tempActivationCode = params.get("activation_code");
            presenter.Register(params);
        }
    }

    private void registerCarrierEditText() {
        countryCodePicker.registerCarrierNumberEditText(etNumberPhone);
        countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                if (isValidNumber) {
                    if (etNumberPhone.getText().toString().startsWith("0")){
                        imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                        tvValidity.setText(R.string.error_wrong_number);
                        tvValidity.setTextColor(Color.parseColor("#FF0000"));
                    } else {
                        imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_validated));
                        tvValidity.setText(R.string.label_valid_number);
                        tvValidity.setTextColor(Color.parseColor("#FFFFFF"));
                    }
                } else {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                    tvValidity.setText(R.string.label_invalid_number);
                    tvValidity.setTextColor(Color.parseColor("#FF0000"));
                }
            }
        });
    }

    @Override
    public void showRegisterSuccess(String success) {
        Helper.stopAnimProgress(loadingIndicatorView);
        nextTo();
    }

    @Override
    public void showRegisterFailed(String error) {
        Helper.stopAnimProgress(loadingIndicatorView);
        System.out.println("Error Code "+error);
        if (error.equals("500")){
            nextTo();
        } else if (error.equals("200")){
            nextTo();
        }else {
            Toast.makeText(getActivity(), getString(R.string.err_msg_conection_server), Toast.LENGTH_SHORT).show();
        }

    }

    private void nextTo() {
        String numberPhone = countryCodePicker.getFullNumber();
        String textContent = getString(R.string.msg_sms_login)+
                " "+ tempActivationCode;
        presenter.sendSms(numberPhone, textContent);

        String np = countryCodePicker.getFullNumber().replace(countryCodePicker.getSelectedCountryCode(),"");
        Intent intent = new Intent(getActivity(), PinVerificationActivity.class);
        intent.putExtra("country_code", countryCodePicker.getSelectedCountryCodeWithPlus());
        intent.putExtra("mobile_phone", countryCodePicker.getFullNumber().replace(countryCodePicker.getSelectedCountryCode(),""));
        intent.putExtra("device_id", android_id);
        intent.putExtra("register_id", sessionManager.getRegisterId());
        intent.putExtra("activation_code", Helper.generateCode());
        startActivity(intent);
    }

}
