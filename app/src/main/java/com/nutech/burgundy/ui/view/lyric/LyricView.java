package com.nutech.burgundy.ui.view.lyric;

import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 10/17/17.
 * Xeranta Mobile Solutions
 */

public interface LyricView extends BaseView {

    void showLiricks(List<DataLiricks> dataLiricksList);

    void showDataMember(DataMember dataMember);

    void showErrorMember(String error);

    void showDataUserStore(DataUserStore dataUserStore);

    void showErrorUserStore(String error);

    void showErrorLiricks(String error);
}
