package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.playlist.DetailPlaylistDeleteFragment;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class TracksDelAdapter extends RecyclerView.Adapter<TracksDelAdapter.TracksHolder> {

    private Context context;
    private List<DataTracks> list;
    private SessionManager sessionManager;
    private SparseArray<Float> progressMap = new SparseArray<>();
    private FragmentNavigation fragmentNavigation;
    private String pId;
    private PlaylistPresenter playlistPresenter;
    private DetailPlaylistDeleteFragment fragment;
    private RecyclerView rvTracks;

    public TracksDelAdapter(Context context, List<DataTracks> list, FragmentNavigation navigation,
                            String playlistId, PlaylistPresenter presenter,
                            DetailPlaylistDeleteFragment detailPlaylistDeleteFragment, RecyclerView rvTrackList) {
        this.context = context;
        this.list = list;
        this.fragmentNavigation = navigation;
        this.pId = playlistId;
        this.playlistPresenter = presenter;
        this.fragment = detailPlaylistDeleteFragment;
        this.rvTracks = rvTrackList;

    }

    class TracksHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_tracks)
        TextView tvTitleTracks;
        @BindView(R.id.tv_title_album)
        TextView tvTitleAlbum;
        @BindView(R.id.ll_play)
        LinearLayout llPlay;
        @BindView(R.id.btn_options)
        ImageView ivOptions;
        @BindView(R.id.bt_sing)
        Button btSing;

        View childView;

        TracksHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public TracksHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_tracks, parent, false);
        sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        return new TracksHolder(view);
    }

    @Override
    public void onBindViewHolder(final TracksHolder holder, final int position) {

        if (!TextUtils.isEmpty(list.get(position).getInclude_karaoke())){
            if (list.get(position).getInclude_karaoke().equals("1")){
                holder.btSing.setVisibility(View.VISIBLE);
            }else {
                holder.btSing.setVisibility(View.GONE);
            }
        }else {
            holder.btSing.setVisibility(View.GONE);
        }

        holder.tvTitleTracks.setText(list.get(position).getName());
        holder.tvTitleAlbum.setText(
                list.get(position).getAlbum_name() + " - "
                        +list.get(position).getArtistnames());

        holder.ivOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOptions(holder.ivOptions, position, view);
            }
        });

        holder.tvTitleTracks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentNavigation.playingMusic(position, list);
                toDoNowPlaying(
                        list.get(position).getId(),
                        list.get(position).getInclude_karaoke(),
                        list.get(position).getKaraoke_id(),
                        list.get(position).getName(),
                        list.get(position).getArtistnames(),
                        list.get(position).getUrl()+list.get(position).getSource());
            }
        });

        holder.llPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentNavigation.playingMusic(position, list);
                toDoNowPlaying(
                        list.get(position).getId(),
                        list.get(position).getInclude_karaoke(),
                        list.get(position).getKaraoke_id(),
                        list.get(position).getName(), list.get(position).getArtistnames(),
                        list.get(position).getUrl() + list.get(position).getSource());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void showOptions(ImageView btnOptions, final int position, View view) {

        PopupMenu popup = new PopupMenu(context, btnOptions);
        popup.inflate(R.menu.tracks_options_del);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(android.view.MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.add:

                        break;
                    case R.id.play:
                        fragmentNavigation.playingMusic(position, list);
                        toDoNowPlaying(
                                list.get(position).getId(),
                                list.get(position).getInclude_karaoke(),
                                list.get(position).getKaraoke_id(), list.get(position).getName(),
                                list.get(position).getArtistnames(), list.get(position).getUrl() + list.get(position).getSource());
                        break;
                    case R.id.share:
                        String applicationName = context.getResources().getString(R.string.app_name);
                        String content = context.getResources().getString(R.string.content_share)+" "+
                                list.get(position).getName()+"." +
                                context.getResources().getString(R.string.content_share_only_this_app)+" "+
                                applicationName+" "+context.getResources().getString(R.string.content_share_only_this_app)+"\n"+
                                context.getResources().getString(R.string.content_share_download_app);
                        Helper.shareContent(context, content, applicationName, sessionManager.getDataMember().getId(), Constants.ActivityPoint.SHARE_SOSMED);

                        break;
                    case R.id.remove:

                        fragment.remove(list.get(position).getId());
                        remove(position, list);

                        break;
                }
                return false;
            }
        });
        makePopForceShowIcon(popup);
    }

    public void toDoNowPlaying(String id, String includeKaraoke, String karaokeId, String name,
                               String artistnames, String sourceTrack) {

        fragmentNavigation.pushFragment(
                new NowPlayingFragment(id, includeKaraoke, karaokeId, sourceTrack), name,
                true);

    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }

    private void remove(int position, List<DataTracks> list) {
        ((SimpleItemAnimator) rvTracks.getItemAnimator()).setSupportsChangeAnimations(true);

        System.out.println("position: "+position);
        list.remove(position);
        notifyItemRemoved(position);
        ((SimpleItemAnimator) rvTracks.getItemAnimator()).setSupportsChangeAnimations(false);
    }

}
