package com.nutech.burgundy.ui.view.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.presenter.MyProfilePresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.view.register.PhoneAuthActivity;
import com.nutech.burgundy.ui.view.vip.VipFragment;
import com.nutech.burgundy.utils.Helper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Arif Setiawan on 10/6/17.
 * Xeranta Mobile Solutions
 */

public class MyProfileFragment extends BaseFragment implements MyProfileView {

    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;

    @BindView(R.id.name)
    TextView tvName;
    @BindView(R.id.username)
    TextView tvUsername;
    @BindView(R.id.email)
    TextView tvEmail;
    @BindView(R.id.numberphone)
    TextView tvNumberPhone;

    @BindView(R.id.subscribe)
    TextView tvSubscribe;
    @BindView(R.id.register_type)
    TextView tvRegisterType;
    @BindView(R.id.type)
    TextView tvType;
    @BindView(R.id.remain)
    TextView tvRemain;
    @BindView(R.id.point)
    TextView tvMyPoint;
    @BindView(R.id.end_date)
    TextView tvEndDate;

    @BindView(R.id.ll_remain)
    LinearLayout llRemain;
    @BindView(R.id.ll_end_date)
    LinearLayout llEndDate;
    @BindView(R.id.bt_submit)
    Button btSubmit;
    @BindView(R.id.btn_infomation)
    Button btnInformation;

    @OnClick(R.id.bt_edit)
    public void nextToEdit(){
        Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_logout)
    public void logout() {

        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        sessionManager.logout();
        Intent intent = new Intent(getActivity(), PhoneAuthActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }

    @OnClick(R.id.bt_change_pin)
    public void nextToChange(){
        Intent intent = new Intent(getActivity(), ChangePinActivity.class);
        startActivity(intent);
    }

    private Unbinder unbinder;
    private MyProfilePresenter presenter;
    private SessionManager sessionManager;
    private String ImageUrl = "";

    @Override
    public void onStart() {
        super.onStart();
        initView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MyProfilePresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_myprofile, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();
        loadingData();

        return view;
    }

    @Override
    public void showSubscribeStatus(DataUserSubscribe userSubscribe) {

        sessionManager.setStatusPackage(userSubscribe.getStatus());
        sessionManager.setPackage(userSubscribe.getPackageName());
        sessionManager.setPackageCode(userSubscribe.getSubscribeCode());

        Log.e("** Status ++",userSubscribe.getStatus());

        // is Status = 0 -> Not Subscribe/inactive
        // is Status = 1 -> Subscribe/active

        //-- Step Lama
        if (userSubscribe.getStatus().equals("1")) {
            tvSubscribe.setText(R.string.label_subscribe);

            tvRemain.setText(Helper.dateDifferentSubs(userSubscribe.getStartDate(),userSubscribe.getEndDate()));

            tvEndDate.setText(Helper.formatedEndDateSubs(userSubscribe.getEndDate()));

            llRemain.setVisibility(View.VISIBLE);
            llEndDate.setVisibility(View.VISIBLE);
            btSubmit.setVisibility(View.VISIBLE);
            btSubmit.setText(R.string.label_bt_cancel_subscribe);
            btSubmit.setTag(R.string.label_bt_cancel_subscribe);

        } else {
            tvSubscribe.setText(R.string.label_not_subscribe);
            tvType.setVisibility(View.GONE);
            tvRemain.setVisibility(View.GONE);
            llRemain.setVisibility(View.GONE);
            llEndDate.setVisibility(View.GONE);
            btSubmit.setVisibility(View.VISIBLE);
            btSubmit.setText(R.string.label_bt_chose_packet);
            btSubmit.setTag(R.string.label_bt_chose_packet);
        }
    }

    @Override
    public void showCustomer(DataMember member) {

        Log.e("showCustomer", member.getMobile_phone());

        tvName.setText(member.getName());

        if (!TextUtils.isEmpty(member.getBirth_date())) {
            tvUsername.setText(Helper.formatedEndDateBorn(member.getBirth_date()));
        } else {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date today = Calendar.getInstance().getTime();
            String currentDate = df.format(today);

            tvUsername.setText(Helper.formatedEndDateBorn(currentDate));
        }
        tvEmail.setText(member.getEmail());
        tvNumberPhone.setText(member.getMobile_phone());

        ImageUrl = Constants.URL_IMAGE_MEMBER + member.getImage();
        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.default_user)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ivProfile);
    }

    @Override
    public void showSuccess(String success) {

//        Toast.makeText(getActivity(), success, Toast.LENGTH_SHORT).show();

        loadingData();
    }

    @Override
    public void showErrorCustomer(String error) {

    }

    @Override
    public void showErrorSubscribeStatus(String error) {

        Log.e("12324343434","Errorrrooorroror" );

        //-- Step Lama
        tvSubscribe.setText(R.string.label_not_subscribe);
        tvType.setVisibility(View.GONE);
        tvRemain.setVisibility(View.GONE);
        llRemain.setVisibility(View.GONE);
        llEndDate.setVisibility(View.GONE);
        btSubmit.setText(R.string.label_bt_chose_packet);
        btSubmit.setTag(R.string.label_bt_chose_packet);
    }

    @Override
    public void showError(String error) {

//        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    public void initView() {

        tvMyPoint.setText(sessionManager.getDataMember().getPoint());
        tvName.setText(sessionManager.getDataMember().getName());
        if (!TextUtils.isEmpty(sessionManager.getDataMember().getBirth_date())){
            tvUsername.setText(Helper.formatedEndDateBorn(sessionManager.getDataMember().getBirth_date()));
        } else {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date today = Calendar.getInstance().getTime();
            String currentDate = df.format(today);

            tvUsername.setText(Helper.formatedEndDateBorn(currentDate));
        }
        tvEmail.setText(sessionManager.getDataMember().getEmail());
        tvNumberPhone.setText(sessionManager.getDataMember().getMobile_phone());

    }

    public void initEvent() {

        navigation.updateProfile();

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigation.pushFragment(new UpdateImageFragment(ImageUrl), "Photo Profile", true);
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btSubmit.getTag().equals(R.string.label_bt_cancel_subscribe)) {
                    dialog();
                } else {
                    navigation.pushFragment(new VipFragment(), "Subscribe", true);
                }
            }
        });
    }

    private void loadingData() {

        tvRegisterType.setText(R.string.msg_registrasi_app);
        String customerId = sessionManager.getDataMember().getId();
        presenter.getData(customerId);
    }

    private void unSubscribeCustomer() {

        String customerId = sessionManager.getDataMember().getId();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);

        presenter.unSubscribeCustomer(fields);
    }

    private void dialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(getString(R.string.title_confirmation));
        alertDialog.setMessage(getString(R.string.msg_cancel_subscribe));
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.bt_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        unSubscribeCustomer();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.bt_later),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
