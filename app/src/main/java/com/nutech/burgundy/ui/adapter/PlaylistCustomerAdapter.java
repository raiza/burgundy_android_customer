package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playlist.UpdatePlaylistFragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class PlaylistCustomerAdapter extends RecyclerView.Adapter<PlaylistCustomerAdapter.PlaylistHolder> {

    private Context context;
    private List<DataPlaylist> list;
    private FragmentNavigation fragmentNavigation;
    private PlaylistPresenter playlistPresenter;
    private SessionManager sessionManager;
    private DmApplication dmApplication;
    private String states = "";

    public PlaylistCustomerAdapter(
            Context context, List<DataPlaylist> list, FragmentNavigation navigation,
            PlaylistPresenter presenter, SessionManager sessionManager, String state) {
        this.context = context;
        this.list = list;
        this.fragmentNavigation = navigation;
        this.playlistPresenter = presenter;
        this.sessionManager = sessionManager;
        this.dmApplication = (DmApplication) context.getApplicationContext();
        this.states = state;

    }
    class PlaylistHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_playlist_name)
        TextView tvPlaylistName;
        @BindView(R.id.tv_count_tracks)
        TextView tvCountTracks;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.iv_options)
        ImageView ivOptions;
        @BindView(R.id.iv_playlist)
        ImageView ivPlaylist;
        @BindView(R.id.rl_more)
        RelativeLayout rlMore;
        @BindView(R.id.ll_playlist)
        LinearLayout llPlaylist;
        View childView;

        PlaylistHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public PlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_playlist_customer, parent, false);
        return new PlaylistHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaylistHolder holder, final int position) {

        if (states.equals(Constants.ParamKey.KEY_STATE_ACTION_UPDATE)){
            holder.ivOptions.setBackgroundResource(R.drawable.ic_action_edit);
            holder.ivOptions.setTag(R.drawable.ic_action_edit);
        } else if (states.equals(Constants.ParamKey.KEY_STATE_ACTION_DELETE)){
            holder.ivOptions.setBackgroundResource(R.drawable.ic_action_delete);
            holder.ivOptions.setTag(R.drawable.ic_action_delete);
        } else if (states.equals(Constants.ParamKey.KEY_STATE_ACTION_ADD_TRACK)) {
            holder.ivOptions.setBackgroundResource(R.drawable.ic_action_add);
            holder.ivOptions.setTag(R.drawable.ic_action_add);
        }

        holder.tvPlaylistName.setText(list.get(position).getName());
        holder.tvCountTracks.setText(list.get(position).getCount_tracks());
        holder.tvLikes.setText(list.get(position).getLikes());

        String ImageUrl = Constants.URL_IMAGE_PLAYLIST_WS + list.get(position).getImage();
        Picasso.with(context.getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivPlaylist);

        holder.ivOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (holder.ivOptions.getTag().equals(R.drawable.ic_action_edit)){
                    fragmentNavigation.pushFragment(
                            new UpdatePlaylistFragment(list.get(position)),
                            list.get(position).getName(),
                            true);
                } else if (holder.ivOptions.getTag().equals(R.drawable.ic_action_delete)){
                    confirmDelete(list.get(position).getId(), position);
                } else if (holder.ivOptions.getTag().equals(R.drawable.ic_action_add)){
                    addTracksToPlaylist(list.get(position).getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void confirmDelete(final String id, final int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Delete This Playlist?");
        alertDialog.setMessage("The playlist you want to delete can not be reappeared");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Map<String, String> params = new HashMap<String, String>();
                        try {
                            params.put("id", id);
                        } catch (Exception e) {

                        }
                        System.out.println("** Param ** "+ params);
                        playlistPresenter.removePlaylist(params);
                        list.remove(position);
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private void addTracksToPlaylist(String playlist_id) {
        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("playlist_id", playlist_id);
            params.put("track_id", Constants.ParamValue.KEY_TRACKS_ID);
        } catch (Exception e) {

        }
        System.out.println("** Param ** "+ params);
        playlistPresenter.addTracksToPlaylist(params);
    }

}
