package com.nutech.burgundy.ui.view.playback;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.nutech.burgundy.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/20/17.
 * Xeranta Mobile Solutions
 */

public class PlaybackControllerFragment extends Fragment {

    @BindView(R.id.control_title)
    TextView tvTitle;
    @BindView(R.id.control_artist)
    TextView tvArtist;
    @BindView(R.id.play_pause)
    ImageButton btnPlayPause;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_playback_controls, container, false);
        ButterKnife.bind(getActivity());
        return rootView;
    }
}
