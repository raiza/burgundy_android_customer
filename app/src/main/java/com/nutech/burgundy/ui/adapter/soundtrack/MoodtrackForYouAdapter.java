package com.nutech.burgundy.ui.adapter.soundtrack;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseAdapter;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class MoodtrackForYouAdapter extends BaseAdapter<MoodtrackForYouAdapter.Holder> {

    private Context context;
    private List<Moodtrack> list;

    public MoodtrackForYouAdapter(Context context, List<Moodtrack> moodtrackList) {
        this.context = context;
        this.list = moodtrackList;
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_cover)
        ImageView imgCover;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.img_options)
        ImageView imgOptions;
        View childView;

        Holder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_moodtrack, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        holder.tvTitle.setText(list.get(position).getName());
        String url = Constants.URL_IMAGE_MOODTRACK + list.get(position).getImage();
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        ColorDrawable cd = new ColorDrawable(color);
        loadImagesMoodFromURL(context, holder.imgCover, url, cd);
        holder.imgOptions.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}