package com.nutech.burgundy.ui.view.register;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.hbb20.CountryCodePicker;
import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.presenter.LoginPresenter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.PinView;
import com.nutech.burgundy.utils.Helper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 9/19/17.
 * Xeranta Mobile Solutions
 */

public class LogInFragment extends BaseFragment implements LoginView {

    public static final int REQUEST_CODE = 100;

    @BindView(R.id.et_userid)
    EditText etUserid;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.country_code)
    CountryCodePicker countryCodePicker;
    @BindView(R.id.et_phone_number)
    EditText etNumberPhone;
    @BindView(R.id.img_validity)
    ImageView imgValidity;
    @BindView(R.id.tv_validity)
    TextView tvValidity;
    @BindView(R.id.pv_pin_login)
    PinView pin;
    @BindView(R.id.tv_req_pin)
    TextView tvReqpin;

    private Unbinder unbinder;
    private LoginPresenter presenter;
    private SessionManager sessionManager;
    private String tempActivationCode = "";
    private String android_id = "";
    private Map<String, String> params = new HashMap<String, String>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new LoginPresenter(getDataManager(), getNetworkHelper(), getSessionManager(), getActivity().getApplicationContext());
        sessionManager = getSessionManager();
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (BuildConfig.DEBUG) {
            etUserid.setText("ajis");
            etPassword.setText("123");

            etNumberPhone.setText("87871750708");
        }

        Helper.hideSoftKeyBoard(getActivity());
        countryCodePicker.setDefaultCountryUsingNameCode("ID");
        registerCarrierEditText();

        checkPermssion();

        tvReqpin.setOnClickListener(view1 -> {
            Intent intent = new Intent(getActivity(), ResetPINActivity.class);
            startActivity(intent);
        });

        return view;
    }

    @OnClick(R.id.button_login)
    public void toDoNext() {

        String imei0 = Helper.getIMEI(getActivity(), 0);
        String imei1 = Helper.getIMEI(getActivity(), 1);
        String uniqueDeviceId = Helper.getDeviceUniqueID(getActivity());

        Helper.hideSoftKeyBoard(getActivity());

        if (TextUtils.isEmpty(etNumberPhone.getText().toString())) {
            etNumberPhone.setError(getString(R.string.err_field_required));
        } else {

            Helper.startAnimProgress(loadingIndicatorView);

            android_id = Settings.Secure.getString(getContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);

            try {
//                    params.put("country_code", countryCodePicker.getSelectedCountryCodeWithPlus());
                params.put("mobile_phone", countryCodePicker.getFullNumber());
                params.put("device_id", android_id);
                params.put("register_id", sessionManager.getRegisterId());
                params.put("activation_code", Helper.generateCode());
                params.put("imei", imei0);
            } catch (Exception e) {

            }

            tempActivationCode = params.get("activation_code");
            System.out.println("Params : "+params);
            presenter.checkMemberToIndosis(params);
        }
    }

    @Override
    public void showLoginSuccess(MemberCheck data) {
        Log.e("****","Sukses  Login");
        Helper.stopAnimProgress(loadingIndicatorView);

    }

    @Override
    public void showLoginFailed(String error) {
        Helper.stopAnimProgress(loadingIndicatorView);
        System.out.println("Error Code "+error);
        if (error.equals("500")){
            nextTo();
        } else if (error.equals("200")){
            nextTo();
        }else {
            Toast.makeText(getActivity(), getString(R.string.err_msg_conection_server), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void sucessCheckMember(MemberCheck data, DataMember dataMember) {
        Helper.stopAnimProgress(loadingIndicatorView);
        /*
         ** Status
         * 0 = Invalid Parameter
         * 1 = Subscribers
         * 2 = Unsubscribers
         */
        Log.e("-- Status --", data.getStatus());
        if (data.getStatus().equals("0")){
            alertShowing();
        } else if (data.getStatus().equals("1")){
            nextTo();
        } else if (data.getStatus().equals("2")){
            nextTo();
        } else {
        }
    }

    @Override
    public void sucessLoginStore() {

    }

    @Override
    public void errorCheckMember(String error) {
        Helper.stopAnimProgress(loadingIndicatorView);
    }

    private void registerCarrierEditText() {
        countryCodePicker.registerCarrierNumberEditText(etNumberPhone);
        countryCodePicker.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                if (isValidNumber) {
                    if (etNumberPhone.getText().toString().startsWith("0")){
                        imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                        tvValidity.setText(R.string.error_wrong_number);
                        tvValidity.setTextColor(Color.parseColor("#FF0000"));
                    } else {
                        imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_validated));
                        tvValidity.setText(R.string.label_valid_number);
                        tvValidity.setTextColor(Color.parseColor("#FFFFFF"));
                    }

                } else {
                    imgValidity.setImageDrawable(getResources().getDrawable(R.drawable.ic_not_validated));
                    tvValidity.setText(R.string.label_invalid_number);
                    tvValidity.setTextColor(Color.parseColor("#FF0000"));
                }
            }
        });
    }

    private void nextTo() {

        //-- Step Baru
//        Intent intent = new Intent(getActivity(), PrimaryActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);

        //-- Step Lama
        String numberPhone = countryCodePicker.getFullNumber();
        String textContent = getString(R.string.msg_sms_login)+
                                    " "+ tempActivationCode;
        presenter.sendSms(numberPhone, textContent);

        Intent intent = new Intent(getActivity(), PinVerificationActivity.class);
        intent.putExtra("mobile_phone", countryCodePicker.getFullNumber());
        intent.putExtra("device_id", android_id);
        intent.putExtra("register_id", sessionManager.getRegisterId());
        intent.putExtra("activation_code", tempActivationCode);
        intent.putExtra("verification_id", "");
        intent.putExtra("resend_token", "");
        startActivity(intent);
    }

    private void checkPermssion() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[] {Manifest.permission.READ_PHONE_STATE},
                    REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }

    private void alertShowing() {

        AlertDialog dialogInfo = new AlertDialog.Builder(getActivity()).create();
        dialogInfo.setTitle(getString(R.string.label_info));
        dialogInfo.setMessage(getString(R.string.msg_not_member_dai));
        dialogInfo.setCancelable(true);
        dialogInfo.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.button_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialogInfo.show();
    }
}
