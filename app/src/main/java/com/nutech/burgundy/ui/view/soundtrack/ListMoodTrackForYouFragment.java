package com.nutech.burgundy.ui.view.soundtrack;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.presenter.soundtrack.ListMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.soundtrack.ListMoodtrackForYouAdapter;
import com.nutech.burgundy.ui.adapter.soundtrack.MoodtrackAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class ListMoodTrackForYouFragment extends BaseFragment implements ListMoodtrackView{

    @BindView(R.id.rv_moodtrack)
    RecyclerView rvMoodtrack;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.main_error_layout)
    RelativeLayout rlMainError;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private Unbinder unbinder;
    private ListMoodtrackPresenter presenter;
    private ListMoodtrackForYouAdapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Moodtrack> moodtrackList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ListMoodtrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_moodtracks, container, false);

        initViews(view);
        initEvents(view);

        return view;
    }

    private void loadingData(){
        swipeRefreshLayout.setRefreshing(true);
        presenter.getListMoodtrackForYou();
    }

    private void initViews(View view) {
        unbinder = ButterKnife.bind(this, view);
        loadingData();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    private void initEvents(View view) {
        rvMoodtrack.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvMoodtrack,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        swipeRefreshLayout.setOnRefreshListener(this::loadingData);

        btnRetry.setOnClickListener((View v)-> {
            swipeRefreshLayout.setRefreshing(true);
            loadingData();
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showMoodtracks(List<Moodtrack> dataMoodtracks) {
        swipeRefreshLayout.setRefreshing(false);
        if (dataMoodtracks.size() > 0) {
            rlMainError.setVisibility(View.GONE);
            moodtrackList = dataMoodtracks;
            gridLayoutManager = new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false);
            rvMoodtrack.setLayoutManager(gridLayoutManager);
            adapter = new ListMoodtrackForYouAdapter(getActivity(), dataMoodtracks, this);
            rvMoodtrack.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            rlMainError.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorMoodtracks(String error) {
        swipeRefreshLayout.setRefreshing(false);
        rlMainError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMoodtracksPlayingStore(List<Moodtrack> dataMoodtracks) {

    }

    @Override
    public void showErrorMoodtracksPlayingStore(String error) {

    }

    @Override
    public void showSuccessRemove(String success) {
        rlMainError.setVisibility(View.GONE);
        loadingData();
    }

    @Override
    public void showErrorRemove(String error) {
        rlMainError.setVisibility(View.GONE);
        loadingData();
    }

    @Override
    public void showSuccessSchedule(String success) {

    }

    @Override
    public void showErrorSchedule(String error) {

    }

    public void moveToDetail(int position) {
        if (moodtrackList != null) {
            navigation.pushFragment(
                    new DetailMoodtrackFragment(
                            moodtrackList.get(position).getId(),false),
                    moodtrackList.get(position).getName(),
                    true);
        }
    }

    public void removeMoodtrack(String idMood) {
        Map<String, String> params = new HashMap<>();
        params.put("id", idMood);
        presenter.removeMood(params);
    }
}
