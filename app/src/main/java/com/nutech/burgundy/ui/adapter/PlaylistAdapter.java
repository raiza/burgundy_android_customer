package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.presenter.PlaylistPresenter;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.playlist.DetailPlaylistFragment;
import com.nutech.burgundy.utils.Helper;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 19/05/19.
 * Xeranta NuTech
 */

public class PlaylistAdapter extends BaseAdapter<PlaylistAdapter.PlaylistHolder> {

    private Context context;
    private List<DataPlaylist> list;
    private FragmentNavigation fragmentNavigation;
    private PlaylistPresenter playlistPresenter;
    private SessionManager sessionManager;

    private DmApplication dmApplication;

    public PlaylistAdapter(
            Context context, List<DataPlaylist> list, FragmentNavigation navigation,
            PlaylistPresenter presenter, SessionManager sessionManager) {
        this.context = context;
        this.list = list;
        this.fragmentNavigation = navigation;
        this.playlistPresenter = presenter;
        this.sessionManager = sessionManager;

        this.dmApplication = (DmApplication) context.getApplicationContext();
    }

    class PlaylistHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_playlist_name)
        TextView tvPlaylistName;
        @BindView(R.id.tv_count_tracks)
        TextView tvCountTracks;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.iv_options)
        ImageView ivOptions;
        @BindView(R.id.iv_playlist)
        ImageView ivPlaylist;
        @BindView(R.id.rl_more)
        RelativeLayout rlMore;
        @BindView(R.id.ll_playlist)
        LinearLayout llPlaylist;
        View childView;

        PlaylistHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public PlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_playlist, parent, false);
        return new PlaylistHolder(view);
    }

    @Override
    public void onBindViewHolder(final PlaylistHolder holder, final int position) {

        holder.tvPlaylistName.setText(list.get(position).getName());
        holder.tvCountTracks.setText(list.get(position).getCount_tracks());
        holder.tvLikes.setText(list.get(position).getFavorites());

        String ImageUrl = Constants.URL_IMAGE_PLAYLIST + list.get(position).getImage();
        loadImageAvatarFromURL(context, holder.ivPlaylist, ImageUrl);

        holder.rlMore.setOnClickListener(v -> {
            PopupMenu popup = new PopupMenu(context, holder.ivOptions);
            popup.inflate(R.menu.playlist_options);
            popup.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.add_fav:
                        String memberId = sessionManager.getDataMember().getId();
                        Map<String, String> params = new HashMap<String, String>();
                        try {
                            params.put("customer_id", memberId);
                            params.put("playlist_id", list.get(position).getId());
                        } catch (Exception e) {

                        }
                        System.out.println("** Param ** "+ params);
                        playlistPresenter.sendPlaylist(params);

                        break;
                    case R.id.menu2:
                        String applicationName = context.getResources().getString(R.string.app_name);
                        String content = context.getResources().getString(R.string.content_share_playlist)+" "+
                                list.get(position).getName()+"." +
                                context.getResources().getString(R.string.content_share_only_this_app)+" "+
                                applicationName+" "+context.getResources().getString(R.string.content_share_only_this_app)+"\n"+
                                context.getResources().getString(R.string.content_share_download_app);
                        Helper.shareContent(context, content, applicationName, sessionManager.getDataMember().getId(), Constants.ActivityPoint.SHARE_SOSMED);
                        playlistPresenter.updateStatusPoint(
                                sessionManager.getDataMember().getId(),
                                Constants.ActivityPoint.SHARE_SOSMED);
                        break;
                }
                return false;
            });
            makePopForceShowIcon(popup);
        });

        holder.ivPlaylist.setOnClickListener(v -> {
            dmApplication.setTitle(list.get(position).getName());
            fragmentNavigation.pushFragment(
                    new DetailPlaylistFragment(
                            list.get(position).getId(),
                            list.get(position).getImage(),
                            Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_PLAYLIST),
                    list.get(position).getName(),
                    true);
        });

        holder.llPlaylist.setOnClickListener(v -> {
            dmApplication.setTitle(list.get(position).getName());
            fragmentNavigation.pushFragment(
                    new DetailPlaylistFragment(
                            list.get(position).getId(),
                            list.get(position).getImage(),
                            Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_PLAYLIST),
                    list.get(position).getName(),
                    true);
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @SuppressLint("RestrictedApi")
    private void makePopForceShowIcon(PopupMenu popupMenu) {
        try {
            Field mFieldPopup=popupMenu.getClass().getDeclaredField("mPopup");
            mFieldPopup.setAccessible(true);
            MenuPopupHelper mPopup = (MenuPopupHelper) mFieldPopup.get(popupMenu);
            mPopup.setForceShowIcon(true);
            mPopup.show();
        } catch (Exception e) {

        }
    }

}
