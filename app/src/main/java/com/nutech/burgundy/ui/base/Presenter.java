package com.nutech.burgundy.ui.base;

/**
 * Created by Arif-Setiawan on 8/28/17.
 */

public interface Presenter<V extends BaseView> {

    void attachView(V view);

    void detachView();
}
