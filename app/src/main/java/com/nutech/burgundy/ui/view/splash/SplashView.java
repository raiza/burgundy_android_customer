package com.nutech.burgundy.ui.view.splash;

import com.nutech.burgundy.data.model.DataCheckDevice;
import com.nutech.burgundy.data.model.DataTimer;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 12/4/17.
 * Xeranta Mobile Solutions
 */

public interface SplashView extends BaseView{

    void success(DataCheckDevice dataCheckDevice);

    void error(String error);

    void successTimer(DataTimer dataTimer);

    void errorTimer(String error);
}
