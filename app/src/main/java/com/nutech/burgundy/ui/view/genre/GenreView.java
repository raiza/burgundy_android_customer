package com.nutech.burgundy.ui.view.genre;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 10/12/17.
 * Xeranta Mobile Solutions
 */

public interface GenreView extends BaseView {

    void showTracksList(List<DataTracks> dataTracksList);

    void showError(String error);
}
