package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.model.DataVideos;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class CategoryVideoAdapter extends RecyclerView.Adapter<CategoryVideoAdapter.TimelineHolder> {

    private Context context;
    private List<DataVideos> list;

    public CategoryVideoAdapter(
            Context context, List<DataVideos> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public TimelineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_category_video, parent, false);
        return new TimelineHolder(view);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final TimelineHolder holder, int position) {

        holder.tvTitleVideo.setText(list.get(position).getName());
        holder.tvLikes.setText(list.get(position).getLikes());
        holder.tvViewers.setText(list.get(position).getViewers());

        final String mediaUrl = list.get(position).getUrl()+ "thumbs/" + list.get(position).getSource() + ".jpg";
        Log.e("mediaUrl"," = " +mediaUrl);

//        String ImageUrl = Constants.URL_IMAGE_ARTISTS+list.get(position).getImage_artis();
        Picasso.with(context.getApplicationContext())
                .load(mediaUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivVideo);


//        Observable.create(new ObservableOnSubscribe<Bitmap>() {
//
//            @Override
//            public void subscribe(ObservableEmitter<Bitmap> e) {
//                Bitmap image = Helper.retrieveVideoFrameFromVideo(mediaUrl);
//                if (image != null) {
//                    e.onNext(image);
//                }
//            }
//        })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<Bitmap>() {
//                    @Override
//                    public void accept(Bitmap bitmap) throws Exception {
//                        if(bitmap ==  null){
//                            return;
//                        }
//                        loadImagesFromURL(context, holder.ivVideo, bitmap);
//                    }
//                });

    }

//    protected void loadImagesFromURL(Context context, ImageView view, Bitmap bitmap) {
//
//        if (bitmap == null) {
//            return;
//        }
//
//        int radius = context.getResources().getDimensionPixelSize(R.dimen.image_corner);
//        RequestOptions requestOptions = new RequestOptions();
//        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(radius));
//
//
////        Glide.with(context)
////                .asBitmap()
////                .load(bitmap)
////                .apply(requestOptions)
////                .into(view);
//
//    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class TimelineHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_video)
        TextView tvTitleVideo;
        @BindView(R.id.iv_video)
        ImageView ivVideo;
        @BindView(R.id.tv_likes)
        TextView tvLikes;
        @BindView(R.id.tv_viewers)
        TextView tvViewers;
        View childView;

        TimelineHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

}
