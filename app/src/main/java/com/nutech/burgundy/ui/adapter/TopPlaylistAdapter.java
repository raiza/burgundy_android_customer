package com.nutech.burgundy.ui.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataPlaylist;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Dafian on 10/4/17
 */

public class TopPlaylistAdapter extends RecyclerView.Adapter<TopPlaylistAdapter.TopPlaylistHolder> {

    private Context context;
    private List<DataPlaylist> playlist;

    public TopPlaylistAdapter(Context context, List<DataPlaylist> playlist) {
        this.context = context;
        this.playlist = playlist;
    }

    class TopPlaylistHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_playlist_cover)
        ImageView ivPlaylist;
        @BindView(R.id.tv_playlist_name)
        TextView tvPlaylistName;

        TopPlaylistHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public TopPlaylistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_cover_playlist, parent, false);
        return new TopPlaylistHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopPlaylistHolder holder, int position) {

        holder.tvPlaylistName.setText(playlist.get(position).getName());

        String url = Constants.URL_IMAGE_PLAYLIST + playlist.get(position).getImage();
        Picasso.with(context)
                .load(url)
                .placeholder(R.drawable.image_dm_error)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(holder.ivPlaylist);
    }

    @Override
    public int getItemCount() {
        return playlist.size();
    }
}
