package com.nutech.burgundy.ui.view.feedback;

import com.nutech.burgundy.ui.base.BaseView;

/**
 * @author Dafian on 9/29/17.
 * @company Xeranta Mobile
 */

public interface FeedbackView extends BaseView {

    void showSuccess(String success);

    void showError(String Error);

}
