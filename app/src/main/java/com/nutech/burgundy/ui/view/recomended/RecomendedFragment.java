package com.nutech.burgundy.ui.view.recomended;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.RecomendedPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * Created by Arif Setiawan on 1/4/18.
 * Xeranta Mobile Solutions
 */

public class RecomendedFragment extends BaseFragment implements RecomendedView {

    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.image_backdrop)
    ImageView ivBackdrop;
    @BindView(R.id.image_recomended)
    CircleImageView civArtist;
    @BindView(R.id.rv_track_list)
    RecyclerView rvTrackList;

    private Unbinder unbinder;
    private RecomendedPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RecomendedPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recomended, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvTrackList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTrackList.setItemAnimator(new DefaultItemAnimator());
        rvTrackList.addItemDecoration(new DividerItemDecoration(
                getActivity(), DividerItemDecoration.VERTICAL));

        getData();

        return view;
    }

    private void getData(){
        loadingIndicatorView.smoothToShow();
        presenter.getData();
    }

    @Override
    public void showRecomendedData(List<DataTracks> listDataTracks) {

        try {
            loadingIndicatorView.smoothToHide();
        }catch (Exception e){
            e.printStackTrace();
        }

        if (listDataTracks.size() > 0) {

                    String ImageUrl = Constants.URL_IMAGE_ARTISTS + listDataTracks.get(0).getArtisimage();

            System.out.println("Image "+ImageUrl);

                    Picasso.with(getActivity())
                            .load(ImageUrl)
                            .error(R.drawable.image_dm_error)
                            .fit()
                            .into(civArtist);

                    Picasso.with(getActivity())
                            .load(ImageUrl)
                            .error(R.drawable.image_dm_error)
                            .transform(new BlurTransformation(getActivity()))
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .into(ivBackdrop);


                    rvTrackList.setAdapter(new TracksAdapter(getActivity(), listDataTracks, navigation, "",
                            new TracksAdapter.OnItemCheckListener() {
                                @Override
                                public void onItemCheck(DataTracks dataTracks) {

                                }

                                @Override
                                public void onItemUncheck(DataTracks dataTracks) {

                                }
                            }));
                    rvTrackList.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTrackList,
                            new RecyclerTouchListener.ClickListener() {
                                @Override
                                public void onClick(View view, int position) {
                                }

                                @Override
                                public void onLongClick(View view, int position) {

                                }
                            }));

        } else {
        }

    }

    @Override
    public void showError(String error) {
        try {
            loadingIndicatorView.smoothToHide();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
