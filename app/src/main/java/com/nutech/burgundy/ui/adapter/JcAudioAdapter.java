package com.nutech.burgundy.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.ui.base.BaseAdapter;
import com.nutech.burgundy.ui.view.FragmentNavigation;
import com.nutech.burgundy.ui.view.track.ItemTouchHelperAdapter;
import com.nutech.burgundy.ui.view.track.ListTrackMoodtrackFragment;
import com.nutech.burgundy.utils.Helper;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class JcAudioAdapter extends BaseAdapter<JcAudioAdapter.JcAudioHolder>
        implements ItemTouchHelperAdapter, JcPlayerView.JcPlayerViewServiceListener {

    private Context context;
    private List<JcAudio> list;
    private SessionManager sessionManager;
    private SparseArray<Float> progressMap = new SparseArray<>();
    private FragmentNavigation fragmentNavigation;
    private ListTrackMoodtrackFragment fragment;
    private JcPlayerView jcPlayerView;

    public JcAudioAdapter(Context context, List<JcAudio> list, ListTrackMoodtrackFragment fragment,
                          FragmentNavigation navigation, JcPlayerView jcPlayerView) {
        this.context = context;
        this.list = list;
        this.fragment = fragment;
        this.fragmentNavigation = navigation;
        this.jcPlayerView = jcPlayerView;

        setContext(context);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(list, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(list, i, i - 1);
            }
        }

        list.get(fromPosition).setPosition(fromPosition + 1);
        list.get(toPosition).setPosition(toPosition + 1);
        jcPlayerView.initPlaylist(list);
        fragment.updatePositionTracks(list.get(fromPosition), toPosition + 1, fromPosition + 1);
        notifyItemMoved(fromPosition, toPosition);
//        this.notifyDataSetChanged();
        return true;
    }

    @Override
    public void onItemSelectedChange() {
        this.notifyDataSetChanged();
    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
        this.notifyDataSetChanged();
    }

    @Override
    public void onCompletedAudio() {

    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onContinueAudio() {

    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void addLogAds(String id, String trackId) {

    }

    @Override
    public void onTimeChanged(long currentTime) {

    }

    @Override
    public void updateTitle(String title, String artisName) {

    }

    @Override
    public void updateCover(String cover) {

    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {

    }

    @Override
    public void removeNotification() {

    }

    class JcAudioHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_tracks)
        TextView tvTitleTracks;
        @BindView(R.id.tv_artists_name)
        TextView tvArtistsName;
        @BindView(R.id.ll_play)
        LinearLayout llPlay;
        @BindView(R.id.rl_play)
        RelativeLayout rlPlay;
        @BindView(R.id.indicator_playing)
        AVLoadingIndicatorView indicatorPlaying;

        View childView;

        JcAudioHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public JcAudioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_audio, parent, false);
        sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        return new JcAudioHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final JcAudioHolder holder, final int position) {


        holder.tvTitleTracks.setText(list.get(position).getPosition() + ". " + list.get(position).getTitle());

        holder.tvArtistsName.setText(list.get(position).getArtistName());
        Log.e("track",sessionManager.getCurrentTracksId());
        Log.e("track",String.valueOf(list.get(position).getId()));

        if (sessionManager.getCurrentTracksId().matches(String.valueOf(list.get(position).getId()))) {
//            holder.rlPlay.setBackground(context.getDrawable(R.color.colorAccent));
            holder.indicatorPlaying.smoothToShow();
        } else {
//            holder.rlPlay.setBackground(context.getDrawable(R.color.white));
            holder.indicatorPlaying.smoothToHide();

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
