package com.nutech.burgundy.ui.view.register;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.widget.FrameLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.ui.base.BaseActivity;
import com.nutech.burgundy.ui.extention.SegmentedButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtherAccountActivity extends BaseActivity {
    @BindView(R.id.segmented)
    SegmentedButton buttons;
    @BindView(R.id.frame)
    FrameLayout mainFrame;

    SessionManager sessionManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        sessionManager = getSessionManager();

        getRegisterId();
        setUpTabLayout();
        replaceFragment(new LogInFragment());
    }

    @OnClick(R.id.iv_back)
    public void back(){onBackPressed();}

    private void setUpTabLayout() {

        buttons.clearButtons();
        buttons.addButtons(
                getString(R.string.label_signin),
                getString(R.string.label_signup));

        // First button is selected
        buttons.setPushedButtonIndex(0);

        // Some example click handlers. Note the click won't get executed
        // if the segmented button is already selected (dark blue)
        buttons.setOnClickListener(new SegmentedButton.OnClickListenerSegmentedButton() {
            @Override
            public void onClick(int index) {
                if (index == 0) {
                    replaceFragment(new LogInFragment());
                } else {
                    replaceFragment(new SignUpFragment());
                }
            }
        });


    }

    private void replaceFragment(Fragment fragment) {

        if (mainFrame != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(mainFrame.getId(), fragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        System.exit(0);
    }

    public void getRegisterId() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    String token = FirebaseInstanceId.getInstance().getToken();
                    System.out.println("=====" + " : token :" + token);
                    if (token != null || !token.isEmpty()) {

                        sessionManager.setRegisterId(token);
                    }

                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                System.out.println("*** Prepare Registrasion ID ***");
                return msg;
            }
            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }
}
