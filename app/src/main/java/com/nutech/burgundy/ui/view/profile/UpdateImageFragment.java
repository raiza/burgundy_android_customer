package com.nutech.burgundy.ui.view.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.github.javiersantos.bottomdialogs.BottomDialog;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.lib.imageviewers.PhotoView;
import com.nutech.burgundy.presenter.UpdateProfilePresenter;
import com.nutech.burgundy.ui.base.BaseFragment;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by Arif Setiawan on 10/9/17.
 * Xeranta Mobile Solutions
 */

public class UpdateImageFragment extends BaseFragment implements UpdateProfileView {

    @BindView(R.id.photo_views)
    PhotoView photoView;
    @BindView(R.id.btn_update_photo)
    Button btnUpdatePhoto;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    public static final int REQUEST_CODE_CAMERA = 100;

    private Unbinder unbinder;
    private int SELECT_IMAGE = 2;
    private int REQUEST_IMAGE_CAPTURE = 1;
    private UpdateProfilePresenter presenter;
    private SessionManager sessionManager;
    private String baseImage = "";
    private String dImageMember = "";
    private ProgressDialog pDialog;
    private String currentPhotoPath = "";
    private Uri imageUri;
    private File output = null;
    private int width;
    private int height;
    private double UWQHD = 3440;
    private double WQHD = 2560;
    private double HD = 1920;
    private double WXGA = 1600;
    private double WVGA = 854;
    private double DUWQHD = 6;
    private double DWQHD = 5;
    private double DHD = 3;
    private double DWXGA = 2.5;
    private double DWVGA = 1.5;

    @SuppressLint("ValidFragment")
    public UpdateImageFragment(String imageUrl) {
        dImageMember = imageUrl;
    }

    public UpdateImageFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new UpdateProfilePresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        presenter.attachView(this);
        sessionManager = getSessionManager();


    }

    @Override
    public void onResume() {
        super.onResume();
        initImageBitmap();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update_image, container, false);
        unbinder = ButterKnife.bind(this, view);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        initImage();
        initEvent();
        return view;
    }

    public void initImage() {

        //Clear
        Constants.ParamValue.pVIEW = null;
        Constants.ParamValue.B_IMAGE = null;

        if (!TextUtils.isEmpty(dImageMember)) {
            Picasso.with(getActivity())
                    .load(dImageMember)
                    .error(R.drawable.default_user)
                    .fit()
                    .into(photoView);
        } else {

            Picasso.with(getActivity())
                    .load(Constants.URL_IMAGE_MEMBER)
                    .error(R.drawable.default_user)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(photoView);
        }
    }

    public void initImageBitmap() {

        if (Constants.ParamValue.B_IMAGE != null &&
                Constants.ParamValue.pVIEW != null) {
            photoView = Constants.ParamValue.pVIEW;
            photoView.setImageBitmap(Constants.ParamValue.B_IMAGE);
        }
    }

    public void initEvent() {

        btnUpdatePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

    }

    private void showDialog() {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_choice_update_photo, null);

        RelativeLayout rlTakePhoto = (RelativeLayout) customView.findViewById(R.id.rl_take_photo);
        RelativeLayout rlSelectGallery = (RelativeLayout) customView.findViewById(R.id.rl_take_photo_gallery);
        RelativeLayout rlCancel = (RelativeLayout) customView.findViewById(R.id.rl_cancel);

        final BottomDialog dialog = new BottomDialog.Builder(getActivity())
                .setTitle(R.string.label_ubah_foto_profil)
                .setCustomView(customView)
                .build();

        rlTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_CAMERA);
                }else if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED){
                    requestPermissions(
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_CAMERA);
                }else {
                    dispatchTakePictureIntent();
                }

                dialog.dismiss();

            }
        });

        rlSelectGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_IMAGE);

                dialog.dismiss();
            }
        });

        rlCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_CAMERA: {
                // If request is cancelled, the result arrays are empty.

                System.out.println("*** grantResults " + grantResults.length);
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    dispatchTakePictureIntent();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        width = bitmap.getWidth();
                        height = bitmap.getHeight();

                        if (height >= UWQHD) {
                            width = (int) Math.round(width / DUWQHD);
                            height = (int) Math.round(height / DUWQHD);
                        } else if (height >= WQHD) {
                            width = (int) Math.round(width / DWQHD);
                            height = (int) Math.round(height / DWQHD);
                        } else if (height >= HD) {
                            width = (int) Math.round(width / DHD);
                            height = (int) Math.round(height / DHD);
                        } else if (height >= WXGA) {
                            width = (int) Math.round(width / DWXGA);
                            height = (int) Math.round(height / DWXGA);
                        } else if (height >= WVGA) {
                            width = (int) Math.round(width / DWVGA);
                            height = (int) Math.round(height / DWVGA);
                        }

                        bitmap = BITMAP_RESIZER(bitmap, width, height);
                        setImage(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {

            imageUri = Uri.fromFile(output);
            getActivity().getContentResolver().notifyChange(imageUri, null);
            ContentResolver cr = getActivity().getContentResolver();
            Bitmap bitmap;

            Log.e("uri", String.valueOf(imageUri));

            try {
                bitmap = MediaStore.Images.Media.getBitmap(cr, imageUri);
                width = bitmap.getWidth();
                height = bitmap.getHeight();

                if (height >= UWQHD) {
                    width = (int) Math.round(width / 4.5);
                    height = (int) Math.round(height / 4.5);
                } else if (height >= WQHD) {
                    width = (int) Math.round(width / 4);
                    height = (int) Math.round(height / 4);
                } else if (height >= HD) {
                    width = (int) Math.round(width / 3.5);
                    height = (int) Math.round(height /3.5);
                } else if (height >= WXGA) {
                    width = (int) Math.round(width / DWXGA);
                    height = (int) Math.round(height / DWXGA);
                } else if (height >= WVGA) {
                    width = (int) Math.round(width / 1.5);
                    height = (int) Math.round(height / 1.5);
                }

                bitmap = BITMAP_RESIZER(bitmap, width, height);
                setImage(bitmap);

            } catch (Exception e) {
//                Context context = getApplicationContext();
                e.printStackTrace();
//                Toast.makeText(context, "Failed to load", Toast.LENGTH_SHORT).show();
//                Log.d("grabImage", "Failed to load", e);
            }

        }
    }

    private void setImage(Bitmap imageBitmap) {

        Constants.ParamValue.pVIEW = photoView;
        Constants.ParamValue.B_IMAGE = imageBitmap;
        Intent intent = new Intent(getActivity(), EditedImageActivty.class);
        startActivity(intent);

    }

    public Bitmap BITMAP_RESIZER(Bitmap bitmap,int newWidth,int newHeight) {

        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        return scaledBitmap;

    }

    private void dispatchTakePictureIntent() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

//        File photo = null;
//
//        try {
//            // place where to store camera taken picture
//            photo = this.createTemporaryFile("picture", ".jpg");
//            photo.delete();
//        } catch (Exception e) {
//            Log.v("take picture intent : ", "Can't create file to take picture!");
////
////            Context context = getApplicationContext();
////            CharSequence text = "Hello toast!";
////            int duration = Toast.LENGTH_SHORT;
////
////            Toast.makeText(context, "Please check SD card! Image shot is impossible!", duration);
//        }
//
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
//
//        imageUri = Uri.fromFile(photo);
//        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        //start camera intent

//        Intent i=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

        output = new File(dir, "profile_dai.jpeg");
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);


    }

    private File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/dangdutasik/pictures");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void showDataMember(DataMember dataMember) {

    }

    @Override
    public void errorDataMember(String error) {

    }

    @Override
    public void succesUpdate(String success) {
        hideProgressDialog();
//        loadingIndicatorView.smoothToHide();
    }

    @Override
    public void error(String error) {
        hideProgressDialog();
//        loadingIndicatorView.smoothToHide();
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Uploading...");
        pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }
}
