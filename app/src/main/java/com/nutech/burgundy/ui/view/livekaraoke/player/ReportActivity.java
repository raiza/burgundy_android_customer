package com.nutech.burgundy.ui.view.livekaraoke.player;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.presenter.livekaraoke.PlayerPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportActivity extends BaseAppCompatActivity implements PlayerView {

    @BindView(R.id.et_content_report)
    EditText etContentReport;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;

    @OnClick(R.id.btn_send_report)
    public void sendReport() {

        if (!TextUtils.isEmpty(etContentReport.getText())){
            Map<String, String> params = new HashMap<String, String>();
            try {
                params.put("room_code", bundle.getStringExtra("room_code"));
                params.put("track_id", bundle.getStringExtra("trackId"));
                params.put("customer_id", bundle.getStringExtra("customerId"));
//                params.put("live", etContentFeedback.getText().toString());
//                params.put("create_date", etContentFeedback.getText().toString());
                params.put("message", etContentReport.getText().toString());
                params.put("report_date", Helper.todayDateFull());
                params.put("reporter", sessionManager.getDataMember().getId());
            } catch (Exception e) {

            }
            System.out.println("params "+params.toString());
            loadingIndicatorView.smoothToShow();
            presenter.sendReport(params);
        }

    }

    private Intent bundle;
    private PlayerPresenter presenter;
    private SessionManager sessionManager;

    @Override
    protected void onStart() {
        super.onStart();
        presenter = new PlayerPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
        bundle = getIntent();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
    }

    @Override
    public void initPlayer(String rtmpUrl) {

    }

    @Override
    public void showErrorKaraoke(String rtmpUrl) {

    }

    @Override
    public void showVideoNotExist() {

    }

    @Override
    public void showRoomVideo(String roomId) {

    }

    @Override
    public void showError(String error) {
    }

    @Override
    public void showDataKaraoke(DataKaraoke dataKaraoke) {

    }

    @Override
    public void successReport(String dataTimer) {
        loadingIndicatorView.smoothToHide();
        Toast.makeText(getApplicationContext(),
                getString(R.string.msg_send_report),
                Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void errorReport(String error) {
        loadingIndicatorView.smoothToHide();
        Toast.makeText(getApplicationContext(),
                getString(R.string.msg_send_report),
                Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
