package com.nutech.burgundy.ui.view.track;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.presenter.TrackPresenter;
import com.nutech.burgundy.ui.adapter.TracksAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;

/**
 * Created by Arif Setiawan on 12/28/17.
 * Xeranta Mobile Solutions
 */

public class DetailTracksFragment extends BaseFragment implements TracksView {

    @BindView(R.id.recycle_tracks)
    RecyclerView rvTracks;
    @BindView(R.id.img_backdrop)
    ImageView ImageBackdrop;
    @BindView(R.id.img_cover)
    CircleImageView imageCover;

    private Unbinder unbinder;
    private TrackPresenter presenter;
    private String tempId;
    private String tempImage;
    private List<DataTracks> listDataTracks = new ArrayList<>();

    public DetailTracksFragment(String id, String image) {
        tempId = id;
        tempImage = image;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new TrackPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_tracks, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        getData();

        return view;
    }

    private void getData() {
        presenter.getTrackById(tempId);
    }

    private void initView() {

        String ImageUrl = Constants.URL_IMAGE_ARTISTS+tempImage;
        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .fit()
                .into(imageCover);

        Picasso.with(getActivity())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .transform(new BlurTransformation(getActivity()))
                .fit()
                .into(ImageBackdrop);

        rvTracks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvTracks.setItemAnimator(new DefaultItemAnimator());
        rvTracks.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));
    }

    @Override
    public void showTracks(DataTracks dataTracks) {

        listDataTracks.clear();

        listDataTracks.add(dataTracks);
        rvTracks.setAdapter(new TracksAdapter(getActivity(), listDataTracks, navigation, "",
                new TracksAdapter.OnItemCheckListener() {
                    @Override
                    public void onItemCheck(DataTracks dataTracks) {

                    }

                    @Override
                    public void onItemUncheck(DataTracks dataTracks) {

                    }
                }));
        rvTracks.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvTracks,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
