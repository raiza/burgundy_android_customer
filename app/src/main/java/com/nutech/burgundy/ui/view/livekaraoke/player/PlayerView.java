package com.nutech.burgundy.ui.view.livekaraoke.player;

import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * @author Dafian on 12/5/17
 */

public interface PlayerView extends BaseView {

    void showRoomVideo(String roomId);

    void showError(String error);

    void showDataKaraoke(DataKaraoke dataKaraoke);

    void successReport(String dataTimer);

    void errorReport(String error);

    void initPlayer(String rtmpUrl);

    void showErrorKaraoke(String rtmpUrl);

    void showVideoNotExist();
}
