package com.nutech.burgundy.ui.view.video;

import com.nutech.burgundy.data.model.DataVideos;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 9/27/17.
 * Xeranta Mobile Solutions
 */

public interface CategoryVideoView extends BaseView {

    void showVideos(List<DataVideos> videosList);

    void showError(String error);
}
