package com.nutech.burgundy.ui.view.home;

import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 11/17/17.
 * Xeranta Mobile Solutions
 */

public interface FavoritePlaylistView extends BaseView {

    void showSuccess(List<DataPlaylistMember> dataPlaylistMembers);
    void showSuccessRemove(String success);
    void showError(String error);
}
