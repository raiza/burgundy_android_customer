package com.nutech.burgundy.ui.view.playlist;

import com.nutech.burgundy.ui.base.BaseView;

/**
 * Created by Arif Setiawan on 11/14/17.
 * Xeranta Mobile Solutions
 */

public interface NewPlaylistView extends BaseView {

    void success(String success);

    void error(String error);
}
