package com.nutech.burgundy.ui.view.video;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataVideos;
import com.nutech.burgundy.presenter.CategoryVideoPresenter;
import com.nutech.burgundy.ui.base.BaseAppCompatActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoPlayingActivity extends BaseAppCompatActivity implements CategoryVideoView {

    public static final int REQUEST_CODE = 100;

    @OnClick(R.id.btn_back)
    public void back() {
        onBackPressed();
    }

    @OnClick(R.id.btn_rotate)
    public void rotate() {
        if (lanscape) {
            lanscape = false;
//            hideWidget();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            lanscape = true;
//            showWidget();
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @BindView(R.id.btn_favorite)
    ShineButton btnFav;
    @BindView(R.id.btn_share)
    ShineButton btnShare;
    @BindView(R.id.btn_back)
    ImageView btnBack;

    @OnClick(R.id.btn_favorite)
    public void addTofav() {
//        btnFav.setBackgroundResource(R.drawable.ic_favorite_pink_500_48dp);
        Map<String, String> param = new HashMap<String, String>();
        try {
            param.put("id", intent.getStringExtra("id"));
        }catch (Exception e){
            e.printStackTrace();
        }
        presenter.updateLikes(param, btnFav);
    }

    private SimpleExoPlayerView simpleExoPlayerView;
    private SimpleExoPlayer player;

    private Timeline.Window window;
    private DataSource.Factory mediaDataSourceFactory;
    private DefaultTrackSelector trackSelector;
    private boolean shouldAutoPlay;
    private BandwidthMeter bandwidthMeter;
    private boolean lanscape = true;
    private Intent intent;
    private CategoryVideoPresenter presenter;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //Remove title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //Remove notification bar
//        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_video_playing);
        ButterKnife.bind(this);

        presenter = new CategoryVideoPresenter(getDataManager(), getNetworkHelper(), this);
        presenter.attachView(this);
        sessionManager = getSessionManager();

        intent = getIntent();

        shouldAutoPlay = true;
        bandwidthMeter = new DefaultBandwidthMeter();
        mediaDataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"), (TransferListener<? super DataSource>) bandwidthMeter);
        window = new Timeline.Window();


        presenter.updateStatusPoint(
                sessionManager.getDataMember().getId(),
                Constants.ActivityPoint.PLAY_VIDEO,
                intent.getStringExtra("id"));

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                String applicationName = getResources().getString(R.string.app_name);
                String content = getResources().getString(R.string.content_share)+" "+
                        intent.getStringExtra("name")+"." +
                        getResources().getString(R.string.content_share_only_this_app)+" "+
                        applicationName+" "+getResources().getString(R.string.content_share_only_this_app)+"\n"+
                        getResources().getString(R.string.content_share_download_app);

                Helper.shareContent(VideoPlayingActivity.this, content, applicationName, sessionManager.getDataMember().getId(), Constants.ActivityPoint.SHARE_SOSMED);

//                shareIntent();
            }
        });

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("***** requestCode *** "+requestCode);
        System.out.println("***** resultCode *** "+resultCode);
        System.out.println("***** data *** "+data);
    }

    private void shareIntent() {

        final Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT,"text");

        final List<ResolveInfo> activities = getPackageManager().queryIntentActivities (i, 0);

        List<String> appNames = new ArrayList<String>();
        for (ResolveInfo info : activities) {
            appNames.add(info.loadLabel(getPackageManager()).toString());
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Complete Action using...");
        builder.setItems(appNames.toArray(new CharSequence[appNames.size()]), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ResolveInfo info = activities.get(item);
                if (info.activityInfo.packageName.equals("com.facebook.katana")) {
                    // Facebook was chosen
                    System.out.println("**** facebook choose");
                } else if (info.activityInfo.packageName.equals("com.twitter.android")) {
                    // Twitter was chosen
                } else {
                    System.out.println("**** other ****");
                }

                // start the selected activity
                i.setPackage(info.activityInfo.packageName);
                startActivity(i);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
//
//        ShareDialog shareDialog = new ShareDialog(VideoPlayingActivity.this);
//
//        String applicationName = getResources().getString(R.string.app_name);
//        String fcontent = getResources().getString(R.string.content_share)+" "+
//                intent.getStringExtra("name")+"." +
//                getResources().getString(R.string.content_share_only_this_app)+" "+
//                applicationName+" "+getResources().getString(R.string.content_share_only_this_app)+"\n"+
//                getResources().getString(R.string.content_share_download_app)+"\n"+ Constants.GOOGLE_PLAY;
//
//        ShareLinkContent content = new ShareLinkContent.Builder()
//                .setContentUrl(Uri.parse(Constants.GOOGLE_PLAY))
//                .setContentTitle("Dangdut Asik")
//                .setContentDescription(fcontent)
//                .build();
//
//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_SUBJECT, applicationName );
//        intent.putExtra(Intent.EXTRA_TEXT, content);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivityForResult(intent, REQUEST_CODE);

//        shareDialog.show(VideoPlayingActivity.this,content);
//        Helper.shareContent(VideoPlayingActivity.this, content, applicationName);
    }

    private void initializePlayer() {

        simpleExoPlayerView = (SimpleExoPlayerView) findViewById(R.id.player_view);
        simpleExoPlayerView.requestFocus();

        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);

        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, new DefaultLoadControl());

        simpleExoPlayerView.setPlayer(player);

        player.setPlayWhenReady(shouldAutoPlay);
/*        MediaSource mediaSource = new HlsMediaSource(Uri.parse("https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"),
                mediaDataSourceFactory, mainHandler, null);*/

        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(intent.getStringExtra("videourl")),
                mediaDataSourceFactory, extractorsFactory, null, null);

        player.prepare(mediaSource);

        lanscape = true;
    }

    private void releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if ((Util.SDK_INT <= 23 || player == null)) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    private void hideWidget(){
        btnBack.setVisibility(View.GONE);
        btnShare.setVisibility(View.GONE);
        btnFav.setVisibility(View.GONE);
    }

    private void showWidget() {
        btnBack.setVisibility(View.VISIBLE);
        btnShare.setVisibility(View.VISIBLE);
        btnFav.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void showVideos(List<DataVideos> videosList) {

    }

    @Override
    public void showError(String error) {

    }
}
