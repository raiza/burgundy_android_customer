package com.nutech.burgundy.ui.view.home;

import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public interface DiscoverView extends BaseView {

    void showGenre(List<DataGenre> genreList);

    void showPromo(List<DataPromoSlide> promoSlideList);

    void showMoodtrackPlayingStore(List<Moodtrack> moodtrackList);

    void showNearStores(List<DataStore> storeList);

    void showTopChart(List<DataTracks> tracks);

    void showTopPlayList(List<DataPlaylist> playlist);

    void showRecomended(List<DataTracks> listDataTracks);

    void showMoodtrackForYou(List<Moodtrack> listDataTracks);

    void showTopKaraoke(List<DataRoom> listDataRoom);

    void showErrorTopKaraoke(String error);

    void showError(String error);

    void showErrorMoodtrackForYou(String error);

    void showNearStoresError(String error);

    void showMoodtrackPlayingStoreError(String error);

    void showErrorRecomended(String error);
}
