package com.nutech.burgundy.ui.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataTracks;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class TopHitsAdapter extends RecyclerView.Adapter<TopHitsAdapter.TopHitsHolder> {

    private Context context;
    private List<DataTracks> list;

    public TopHitsAdapter(
            Context context, List<DataTracks> list) {
        this.context = context;
        this.list = list;
    }

    class TopHitsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_top_name1)
        TextView tvHits1;
        @BindView(R.id.tv_top_name2)
        TextView tvHits2;
        @BindView(R.id.tv_play_and_like)
        TextView tvPlayLike;
        @BindView(R.id.image_top_hits)
        ImageView ivHits;
        View childView;

        TopHitsHolder(View childView) {
            super(childView);
            this.childView = childView;
            ButterKnife.bind(this, childView);
        }
    }

    @Override
    public TopHitsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_top_hits, parent, false);
        return new TopHitsHolder(view);
    }

    @Override
    public void onBindViewHolder(TopHitsHolder holder, int position) {

            holder.tvHits1.setText(list.get(position).getName());
            holder.tvHits2.setText(list.get(position).getArtists());
        holder.tvPlayLike.setText(list.get(position).getPlays()+" Play "+list.get(position).getLikes()+" Likes");

            String ImageUrl = Constants.URL_IMAGE_ARTISTS+list.get(position).getImage();

            Picasso.with(context.getApplicationContext())
                    .load(ImageUrl)
                    .error(R.drawable.default_user)
                    .fit()
                    .into(holder.ivHits);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
