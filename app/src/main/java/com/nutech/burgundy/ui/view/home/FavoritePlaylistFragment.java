package com.nutech.burgundy.ui.view.home;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wang.avi.AVLoadingIndicatorView;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.presenter.FavoritePlaylistPresenter;
import com.nutech.burgundy.ui.adapter.FavoritePlaylistAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.DividerItemDecoration;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;
import com.nutech.burgundy.ui.view.playlist.DetailPlaylistFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 11/17/17.
 * Xeranta Mobile Solutions
 */

public class FavoritePlaylistFragment extends BaseFragment implements FavoritePlaylistView {

    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.recycle_playlist_favorite)
    RecyclerView rvFavPlaylist;

    private Unbinder unbinder;
    private FavoritePlaylistPresenter presenter;
    private SessionManager sessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new FavoritePlaylistPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite_playlist, container, false);
        unbinder = ButterKnife.bind(this, view);

        rvFavPlaylist.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFavPlaylist.setItemAnimator(new DefaultItemAnimator());
        rvFavPlaylist.addItemDecoration(new DividerItemDecoration(
                getActivity(), LinearLayoutManager.VERTICAL));

        getData();


        return view;
    }

    private void getData() {
        loadingIndicatorView.smoothToShow();
        String memberId = sessionManager.getDataMember().getId();
        presenter.getPlaylistMember(memberId);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showSuccess(final List<DataPlaylistMember> dataPlaylistMembers) {
        loadingIndicatorView.smoothToHide();
        rvFavPlaylist.setAdapter(new FavoritePlaylistAdapter(getActivity(), dataPlaylistMembers, navigation,
                presenter, getSessionManager(), rvFavPlaylist));

        rvFavPlaylist.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvFavPlaylist,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, int position) {

//                        dmApplication.setTitle(list.get(position).getName());
                        navigation.pushFragment(
                                new DetailPlaylistFragment(
                                        dataPlaylistMembers.get(position).getId(),
                                        dataPlaylistMembers.get(position).getImage(),
                                        Constants.ParamKey.KEY_DETAIL_PLAYLIST_FROM_MYMUSIC),
                                dataPlaylistMembers.get(position).getName(),
                                true);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

    }

    @Override
    public void showSuccessRemove(String success) {
        getActivity().onBackPressed();
    }

    @Override
    public void showError(String error) {
        loadingIndicatorView.smoothToHide();
    }


}
