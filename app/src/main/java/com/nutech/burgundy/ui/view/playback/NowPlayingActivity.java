package com.nutech.burgundy.ui.view.playback;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataAds;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.lib.jcplayer.AdsPlayView;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.lib.jcplayer.JcStatus;
import com.nutech.burgundy.lib.musicplayerview.MusicPlayerView;
import com.nutech.burgundy.presenter.AdsPresenter;
import com.nutech.burgundy.presenter.LyricksPresenter;
import com.nutech.burgundy.presenter.PlayingPresenter;
import com.nutech.burgundy.ui.adapter.TrackListAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.ui.view.lyric.LyricView;
import com.nutech.burgundy.ui.view.track.ListTrackMoodtrackFragment;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.BlurTransformation;

public class NowPlayingActivity extends BaseFragment implements
        JcPlayerView.OnInvalidPathListener, JcPlayerView.JcPlayerViewStatusListener,
        JcPlayerView.JcPlayerViewServiceListener, PlayingView, AdsPlayView, LyricView {

    @BindView(R.id.iv_backdrop)
    ImageView ivBackdrop;
    @BindView(R.id.ll_karaoke)
    LinearLayout llKaraoke;
    @BindView(R.id.mpv)
    MusicPlayerView mpv;
    @BindView(R.id.loading)
    AVLoadingIndicatorView loadingIndicatorView;
    @BindView(R.id.ll_lirick)
    LinearLayout llLyric;
    @BindView(R.id.iv_shuffle)
    ImageView ivShuffle;
    @BindView(R.id.txt_lirick_passed)
    TextView tvLyricPassed;
    @BindView(R.id.txt_lirick_focused)
    TextView tvLyricFocused;
    @BindView(R.id.txt_lirick_next)
    TextView tvLyricNext;
    @BindView(R.id.tv_current_progress)
    TextView tvCurrectProgress;
    @BindView(R.id.seekbar_duration)
    SeekBar seekBar;
    @BindView(R.id.tv_max_progress)
    TextView tvMaxProgress;
    @BindView(R.id.btn_prev)
    ImageButton btnPrev;
    @BindView(R.id.btn_play)
    ImageButton btnPlay;
    @BindView(R.id.btn_favorite)
    ShineButton btnFav;
    @BindView(R.id.progress_bar_player)
    ProgressBar progressBar;
    @BindView(R.id.btn_next)
    ImageButton btnNext;
    @BindView(R.id.btn_karaoke)
    Button btnKaraoke;
    @BindView(R.id.tv_track_name)
    TextView tvTrackName;
    @BindView(R.id.btn_list_track)
    ImageButton btnListTrack;
    @BindView(R.id.linearLayout)
    RelativeLayout linearLayout;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;

    private PlayingPresenter presenter;
    private AdsPresenter adsPresenter;
    private LyricksPresenter lyricksPresenter;
    private JcPlayerView jcPlayer;
    private SessionManager sessionManager;
    private ImageView ivPlayPauseControll;
    private List<DataLiricks> dataLiricks;
    private List<DataTracks> tempDataTracks;
    private Context mContext;
    private int duration;
    private String isKaraoke;
    private String titleNameArtist;
    private DmApplication dmApplication;
    private String karaokeId;
    private String urlTrack;
    private String sDuration;
    private int pstn;

    public NowPlayingActivity(Context applicationContext, SeekBar seekBarControls, JcPlayerView jcPlayerView,
                              MusicPlayerView musicPlayerView, ImageView ivPlayPause, LinearLayout llControl,
                              List<DataLiricks> mDataLiricks, int tempDuration, String isKaraoke,
                              String titleNameArtist, String karaokeId, String urlTrack, List<DataTracks> tempDataTracks, int pstn) {
        this.mContext = applicationContext;
//        this.seekBar = seekBarControls;
        this.jcPlayer = jcPlayerView;
        this.mpv = musicPlayerView;
        this.ivPlayPauseControll = ivPlayPause;
        this.dataLiricks = mDataLiricks;
        this.duration = tempDuration;
        this.isKaraoke = isKaraoke;
        this.titleNameArtist = titleNameArtist;
        this.karaokeId = karaokeId;
        this.urlTrack = urlTrack;
        this.tempDataTracks = tempDataTracks;
        this.pstn = pstn;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = getSessionManager();
        dmApplication = (DmApplication) Objects.requireNonNull(getActivity()).getApplicationContext();
        lyricksPresenter = new LyricksPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        lyricksPresenter.attachView(this);
        presenter = new PlayingPresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        adsPresenter = new AdsPresenter(getDataManager(), getNetworkHelper());
        adsPresenter.attachView((AdsPlayView) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_now_playing, container, false);
        ButterKnife.bind(this, view);

        jcPlayer.registerInvalidPathListener(this);
        jcPlayer.registerStatusListener(this);
        jcPlayer.registerServiceListener(this);

        initViews();
        initEvent();
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("sDuration",sDuration);
    }

    //    private void updateIsPlaying(String is_playing) {
//        Map<String,String> params = new HashMap<>();
//        params.put("moodtrack_id",sessionManager.getCurrentMoodTrack());
//        params.put("is_playing",is_playing);
//
//        lyricksPresenter.updateIsPlayingStatus(params);
//    }

    private void initViews() {
        Log.e("status", String.valueOf(jcPlayer.isPlaying()));

        tvTrackName.setText(titleNameArtist);

        if (!TextUtils.isEmpty(isKaraoke)) {
            if (isKaraoke.equals("1")) {
                btnKaraoke.setVisibility(View.VISIBLE);
            } else if (isKaraoke.equals("0")) {
                btnKaraoke.setVisibility(View.GONE);
            }
        }

        if (jcPlayer.isShuffle()) {
            ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
        } else {
            ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        }

        if (!TextUtils.isEmpty(Constants.ParamValue.URLIMAGEARTIST)) {
            System.out.println("*** image **** " + Constants.ParamValue.URLIMAGEARTIST);
            mpv.setCoverURL(Constants.ParamValue.URLIMAGEARTIST);

            Picasso.with(getActivity())
                    .load(Constants.ParamValue.URLIMAGEARTIST)
                    .error(R.drawable.image_dm_error)
                    .transform(new BlurTransformation(getActivity()))
                    .fit()
                    .into(ivBackdrop);
        } else {
            mpv.setCoverDrawable(R.drawable.image_dm_error);
            ivBackdrop.setImageResource(R.drawable.image_dm_error);
        }


        if (duration != 0) {

            int newDuration = duration / 1000;
            seekBar.setMax(duration);
            mpv.setMax(newDuration);
            if (jcPlayer.isPlaying()) {
                Log.e("play", String.valueOf(mpv.isRotating()));
                if (!mpv.isRotating()) {
                    mpv.stop();
                    mpv.setmIsPlay(true);
                    mpv.start();
                }
            }

            int minute = newDuration / 60;
            int second = newDuration % 60;

            final String sDuration = // Minutes
                    (minute < 10 ? "0" + minute : minute + "")
                            + ":" +
                            // Seconds
                            (second < 10 ? "0" + second : second + "");
            this.sDuration = sDuration;

            tvMaxProgress.post(() -> tvMaxProgress.setText(sessionManager.getMaxDuration()));
        }

        Constants.ParamValue.isNOW_PLAYING = true;
        navigation.updateControll(true);


    }

    @SuppressLint("ClickableViewAccessibility")
    private void initEvent() {

        btnKaraoke.setOnClickListener(view -> {
            navigation.stopMusic();
            navigation.updateControll(true);

            dmApplication.setNavigation(navigation);

//                Intent intent = new Intent(getActivity(), SampleUploadVideo.class);
//                startActivity(intent);

            Intent intent = new Intent(getActivity(), LiveKaraokeActivity.class);
            intent.putExtra("karaoke_id", karaokeId);
            intent.putExtra("track_id", sessionManager.getCurrentTracksId());
            intent.putExtra("url_track", urlTrack);
            startActivity(intent);
        });

        mpv.setOnClickListener(v -> {
            Log.e("status", String.valueOf(jcPlayer.isPlaying()));
            if (mpv.isRotating()) {
//                    updateIsPlaying(Constants.IsPlaying.NOT_PLAYING);
                jcPlayer.pause();
            } else {
//                    updateIsPlaying(Constants.IsPlaying.PLAYING);
                jcPlayer.continueAudio();
            }
        });

        ivShuffle.setOnClickListener(v -> {
            if (jcPlayer.isShuffle()) {
                ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                jcPlayer.setShuffle(false);
            } else {
                ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
                jcPlayer.setShuffle(true);
            }
        });

        try {
            ivPlayPauseControll.setOnClickListener(v -> {
                if (ivPlayPauseControll.getTag().equals(R.drawable.ic_pause_white)) {
                    if (jcPlayer != null) {
//                            updateIsPlaying(Constants.IsPlaying.NOT_PLAYING);
                        jcPlayer.pause();
                    }
                } else {
                    if (jcPlayer != null) {
//                            updateIsPlaying(Constants.IsPlaying.PLAYING);
                        jcPlayer.continueAudio();
                    }
                }
            });

            btnListTrack.setOnClickListener(v -> {
                navigation.pushFragment(new ListTrackMoodtrackFragment(jcPlayer.getMyPlaylist(), jcPlayer),
                        getString(R.string.tracks_list_label),true);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                if (fromUser) {
                    Log.e("onProgressChanged", String.valueOf(i));
                    jcPlayer.seekToFromPrimary(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBar.setOnTouchListener((view, motionEvent) -> true);

        btnPlay.setOnClickListener(v -> {
            if (sessionManager.getSessionStatePlaying().equals("PLAY")) {
//                    updateIsPlaying(Constants.IsPlaying.NOT_PLAYING);

                jcPlayer.pause();
            } else if (sessionManager.getSessionStatePlaying().equals("PAUSE")) {
//                    updateIsPlaying(Constants.IsPlaying.PLAYING);
                jcPlayer.continueAudio();
            }
        });

        btnNext.setOnClickListener(v -> {
            if (jcPlayer != null) {
                navigation.setWaitingNext(0);
                navigation.setWaitingPrevious(0);
                if (jcPlayer.getWaitBetweenAds() < 1) {
                    adsPresenter.getAds(1, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                    insertLogReport();
                } else {
                    sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                    insertLogReport();

                    adsPresenter.getAds(0, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                }
//                jcPlayer.next();
            }
        });

        btnPrev.setOnClickListener(v -> {
            if (jcPlayer != null) {
                navigation.setWaitingNext(0);
                navigation.setWaitingPrevious(0);

                if (jcPlayer.getWaitBetweenAds() < 1) {
                    adsPresenter.getAds(1, false, true, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                    insertLogReport();
                } else {
                    sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                    insertLogReport();

                    adsPresenter.getAds(0, false, true, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                }
//                jcPlayer.previous();
            }
        });

        btnFav.setOnClickListener(view -> {
            Map<String, String> params = new HashMap<>();
            try {
                params.put("customer_id", sessionManager.getDataMember().getId());
                params.put("track_id", sessionManager.getCurrentTracksId());
            } catch (Exception ignored) {

            }
            Log.e("== params == ", params.toString());
            presenter.addTracksFavorite(params);
            btnFav.setBackgroundResource(R.drawable.ic_favorite_pink_500_48dp);
        });

    }

    @Override
    public void onPathError(JcAudio jcAudio) {

    }

    @Override
    public void onPausedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPlayingStatus(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChangedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onCompletedAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPreparedAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
        Log.e("******* ", "onPreparedAudio");

        int newDuration = duration / 1000;
        seekBar.setMax(duration);
        mpv.setMax(newDuration);
        if (!mpv.isRotating()) {
            mpv.start();
        }

        tempDataTracks = navigation.getDataTracks();
//            updateIsPlaying(Constants.IsPlaying.PLAYING);

        int minute = newDuration / 60;
        int second = newDuration % 60;

        final String sDuration = // Minutes
                (minute < 10 ? "0" + minute : minute + "")
                        + ":" +
                        // Seconds
                        (second < 10 ? "0" + second : second + "");

        tvMaxProgress.post(() -> tvMaxProgress.setText(sDuration));
        this.sDuration = sDuration;
    }

    @Override
    public void onCompletedAudio() {

        if (mpv.isRotating()) {
            mpv.stop();
        }
//        if (sessionManager.isComplete()) {
//            if (jcPlayer.getWaitBetweenAds() <1) {
//                adsPresenter.getAds(1,true, sessionManager.getStore().getTypeBlock());
//            } else {
//                if (jcPlayer.isBetweenAds()){
//                    Log.e("aduh","1");
////                    insertLogAds(sessionManager.getCurrentTracksId(),sessionManager.getPrevTracksId());
//                    jcPlayer.setIsBetweenAds(false);
//                } else {
//                    Log.e("aduh","2");
//                    sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());
//                    insertLogReport();
//                }
//                Log.e("report","3");
//
//                adsPresenter.getAds(0,true, sessionManager.getStore().getTypeBlock());
//            }
//            sessionManager.setIsComplete(false);
//        } else {
//            sessionManager.setIsComplete(true);
//        }
    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {
        if (mpv.isRotating()) {
            mpv.stop();
        }
//        updateIsPlaying(Constants.IsPlaying.NOT_PLAYING);

        ivPlayPauseControll.setImageResource(R.drawable.ic_play_arrow);
        ivPlayPauseControll.setTag(R.drawable.ic_play_arrow);
        btnPlay.setBackgroundResource(R.drawable.ic_play_arrow);
    }

    @Override
    public void onContinueAudio() {

    }

    @Override
    public void onPlaying() {
        if (!mpv.isRotating()) {
            mpv.start();
        }

        ivPlayPauseControll.setImageResource(R.drawable.ic_pause_white);
        ivPlayPauseControll.setTag(R.drawable.ic_pause_white);
        btnPlay.setBackgroundResource(R.drawable.ic_pause_white);
    }

    @Override
    public void addLogAds(String id, String trackId) {
//        insertLogAds(id,trackId);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onTimeChanged(long currentTime) {
        final int newCurrentTime = (int) currentTime / 1000;
        mpv.setProgress(newCurrentTime);
        seekBar.setProgress((int) currentTime);

//        sessionManager.setCurrentDuration(newCurrentTime);

        int minutes = newCurrentTime / 60;
        int seconds = newCurrentTime % 60;
        final String sMinutes = minutes < 10 ? "0" + minutes : minutes + "";
        final String sSeconds = seconds < 10 ? "0" + seconds : seconds + "";

        tvCurrectProgress.post(() -> {
            tvCurrectProgress.setText(sMinutes + ":" + sSeconds);
            String startOriginalLyrics = sMinutes + ":" + sSeconds;

            if (newCurrentTime == sessionManager.getTimer()) {
                if (!jcPlayer.isIklan()) {

                    if (jcPlayer.getWaitBetweenAds() > 0) {
                        if (jcPlayer.getLengthAds() > 0) {
                            DataAds ads = jcPlayer.getFirstAds();
                            jcPlayer.playAds(Constants.URL_AUDIO_ADS + ads.getSource(),
                                    ads.getId(),
                                    sessionManager.getCurrentTracksId());

                            if (jcPlayer.getLengthAds() > 1 && !jcPlayer.isBetweenAds())
                                sessionManager.setTimer(sessionManager.getTimer() * 2);
                        }
                    }

//                    musicPlayerView.stop();
//                    jcPlayerView.kill();
//                    try {
//                        new Handler().postDelayed(() -> {
//                            if(!((Activity) context).isFinishing())
//                            {
//                                alertPlaying();
//                            }
//                        }, delayed);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
                }
            }
            cangesLyrics(startOriginalLyrics);
        });
    }

    @Override
    public void updateTitle(String title, String artisName) {
        String titleArtistName = title + " - " + artisName;
        tvTrackName.setText(titleArtistName);
    }

    @Override
    public void updateCover(String cover) {
        System.out.println("*** image **** " + Constants.ParamValue.URLIMAGEARTIST);
        if (cover.isEmpty()) {
            mpv.setCoverDrawable(R.drawable.image_dm_error);
        } else {
            mpv.setCoverURL(cover);
        }
        mpv.setCoverURL(Constants.ParamValue.URLIMAGEARTIST);

        Picasso.with(getActivity())
                .load(Constants.ParamValue.URLIMAGEARTIST)
                .error(R.drawable.image_dm_error)
                .transform(new BlurTransformation(mContext))
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(ivBackdrop);

    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {
        Log.e(NowPlayingActivity.class.getSimpleName(), "shuffle");
        if (isAdded()) {
            if (val) {
                ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
            } else {
                ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
            }
        }

    }

    @Override
    public void removeNotification() {
//        updateIsPlaying(Constants.IsPlaying.NOT_PLAYING);

    }

    private void insertLogReport() {
        Log.e("report beuh", "1");
        Map<String, String> param = new HashMap<>();
        try {
            String trackId = "";
            String albumId = "";
            String artistId = "";
            String genreId = "";
            String releaseYear = "";
            String publisheId = "";

            for (int i = 0; i < tempDataTracks.size(); i++) {

                if (tempDataTracks.get(i).getId().equals(sessionManager.getCurrentTracksId())) {
                    trackId = tempDataTracks.get(i).getId();
                    albumId = tempDataTracks.get(i).getAlbum_id();
                    artistId = tempDataTracks.get(i).getArtist_id();
                    releaseYear = tempDataTracks.get(i).getReleaseyear();
                    genreId = tempDataTracks.get(i).getGenreid();
                    publisheId = tempDataTracks.get(i).getPublisher_id();
                }

            }

            if (TextUtils.isEmpty(genreId)) {
                genreId = "";
            }

            if (TextUtils.isEmpty(releaseYear)) {
                releaseYear = "";
            }

            if (TextUtils.isEmpty(albumId)) {
                albumId = "";
            }

            if (TextUtils.isEmpty(publisheId)) {
                publisheId = "";
            }

            if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
                param.put("store_id", sessionManager.getUserStore().getStore_id());
            } else {
                param.put("customer_id", sessionManager.getDataMember().getId());
            }

            param.put("moodtrack_id", sessionManager.getCurrentMoodTrack());
            param.put("customer_id", sessionManager.getDataMember().getId());
            param.put("track_id", trackId);
            param.put("album_id", albumId);
            param.put("artist_id", artistId);
            param.put("release_year", releaseYear);
            param.put("genre_id", genreId);
            param.put("subscribe_code", sessionManager.getPackageCode());
            param.put("duration", String.valueOf(sessionManager.getCurrentDuration()));
            param.put("publisher_id", publisheId);

            lyricksPresenter.insertLogReport(param);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    private void insertLogAds(String id,String trackId) {
//        Map<String, String> param = new HashMap<>();
//        try {
//
//
//            if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
//                param.put("store_id", sessionManager.getUserStore().getStore_id());
//            } else {
//                param.put("customer_id", sessionManager.getDataMember().getId());
//            }
//
//            param.put("moodtrack_id", sessionManager.getCurrentMoodTrack());
//            param.put("track_id", trackId);
//            param.put("ads_id", id);
//
//            lyricksPresenter.insertLogAds(param);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    private void cangesLyrics(String startOriginalLyrics) {

//        Log.e("- mDataLiricks size -", String.valueOf(mDataLiricks.size()));
        for (int i = 0; i < dataLiricks.size(); i++) {

            if (startOriginalLyrics.equals(dataLiricks.get(i).getStart_at())) {
                final int finalI = i;
                tvLyricFocused.post(() -> {
                    try {
                        tvLyricPassed.setText(dataLiricks.get(finalI - 1).getText());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    tvLyricFocused.setText(dataLiricks.get(finalI).getText());

                    try {
                        tvLyricNext.setText(dataLiricks.get(finalI + 1).getText());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }

//            if (startOriginalLyrics > mDataLiricks.get(i).getStart_at())


        }
    }

//    private void alertPlaying() {
//        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
//        alertDialog.setCancelable(false);
//        alertDialog.setTitle(getString(R.string.label_info));
//        alertDialog.setMessage(getString(R.string.msg_full_fiture));
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.label_subscribe),
//                (dialog, which) -> {
//                    dialog.dismiss();
//                    jcPlayer.kill();
//                    navigation.pushFragment(new VipFragment(),
//                            getString(R.string.label_subscribe),
//                            true);
//
//                });
//
//        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.label_no),
//                (dialog, which) -> {
//                    dialog.dismiss();
//                    jcPlayer.kill();
//                    Intent intent = new Intent(getActivity(), PrimaryActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//
//                });
//        alertDialog.show();
//    }

    @Override
    public void showSucces(String success) {
        btnFav.setBackgroundResource(R.drawable.ic_favorite_pink_500_48dp);
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showAdsList(List<DataAds> dataAdsList, boolean isContinue, boolean isPrevious, int between) {
//        //jika lebih dari 2 maka ada iklan diantara lagu
//        if (dataAdsList.size() > 2) {
//            DataAds ads = dataAdsList.get(dataAdsList.size()-1);
//            JcAudio audio = JcAudio.createFromURL("-1",ads.getName(),
//                    Constants.URL_AUDIO_ADS+ads.getSource(),
//                    Constants.URL_IMAGE_ADS+ads.getImage(),ads.getId(),"Adsense");
//
//            //validasi apakah moodtrack baru di mulai atau next track
//            if (isContinue) {
//                jcPlayer.playAudio(audio);
//            } else {
//                sessionManager.setPrevTracksId(String.valueOf(jcPlayer.getMyPlaylist().get(pstn).getAudioId()));
//                jcPlayer.playAudio(audio);
//                jcPlayer.createNotification(R.mipmap.ic_launcher);
//            }
//
//            sessionManager.setCurrentTracksId(ads.getId());
//            jcPlayer.setIsBetweenAds(true);
//            //remove Last Ads and submit
//            dataAdsList.remove(dataAdsList.size()-1);
//            jcPlayer.initAds(dataAdsList);
//
//        } else {
//            if (isContinue) {
//                jcPlayer.nextAudio();
//            } else {
//                jcPlayer.playAudio(jcPlayer.getMyPlaylist().get(pstn));
//                jcPlayer.createNotification(R.mipmap.ic_launcher);
//            }
//            jcPlayer.initAds(dataAdsList);
//        }
    }

    @Override
    public void showErrorAdsList(String error, boolean isContinue,boolean isPrevious) {

//        if (isContinue) {
//            jcPlayer.nextAudio();
//        } else {
//            jcPlayer.playAudio(jcPlayer.getMyPlaylist().get(0));
//            jcPlayer.createNotification(R.mipmap.ic_launcher);
//        }
    }

    @Override
    public void showLiricks(List<DataLiricks> dataLiricksList) {

    }

    @Override
    public void showDataMember(DataMember dataMember) {

    }

    @Override
    public void showErrorMember(String error) {

    }

    @Override
    public void showDataUserStore(DataUserStore dataUserStore) {

    }

    @Override
    public void showErrorUserStore(String error) {

    }

    @Override
    public void showErrorLiricks(String error) {

    }

    /*

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_now_playing);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Now Playing");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    */
}
