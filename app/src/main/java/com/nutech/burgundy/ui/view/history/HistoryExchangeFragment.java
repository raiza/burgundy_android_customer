package com.nutech.burgundy.ui.view.history;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.presenter.HistoryExchangePresenter;
import com.nutech.burgundy.ui.adapter.HistoryPointExchangeAdapter;
import com.nutech.burgundy.ui.base.BaseFragment;
import com.nutech.burgundy.ui.extention.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Arif Setiawan on 11/25/17.
 * Xeranta Mobile Solutions
 */

public class HistoryExchangeFragment extends BaseFragment implements HistoryView {

    @BindView(R.id.recycler_history)
    RecyclerView rvHistory;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private Unbinder unbinder;
    private HistoryExchangePresenter presenter;
    private SessionManager sessionManager;

    private HistoryPointExchangeAdapter adapter;
    private List<DataHistoryExchange> historyExchangeList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new HistoryExchangePresenter(getDataManager(), getNetworkHelper());
        presenter.attachView(this);
        sessionManager = getSessionManager();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_exchange, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        initEvent();

        presenter.getHistoryPointExchange(sessionManager.getDataMember().getId());

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showHistory(List<DataHistoryExchange> dataHistoryExchange) {
        if (historyExchangeList.size() > 0) {
            historyExchangeList.clear();
        }

        historyExchangeList.addAll(dataHistoryExchange);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError(String error) {

    }

    private void initView() {

        historyExchangeList = new ArrayList<>();
        adapter = new HistoryPointExchangeAdapter(historyExchangeList, navigation, getActivity());

        rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvHistory.setItemAnimator(new DefaultItemAnimator());
        rvHistory.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        rvHistory.setAdapter(adapter);
    }

    private void initEvent() {

        rvHistory.addOnItemTouchListener(new RecyclerTouchListener(
                getActivity(), rvHistory, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
