package com.nutech.burgundy.ui.view.vip;

import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.ui.base.BaseView;

/**
 * @author Dafian on 10/17/17
 */

public interface PinVipView extends BaseView {

    void showDetailPrice(DataSubscribePrice subscribePrice);

    void showSuccess(String success);
    void showSuccessUpdate(String success);
    void showSuccessUpdateCode(String success);

    void showError(String error);
    void showErrorUpdate(String error);
    void showErrorUpdateCode(String error);

    void showSubscribeStatus(DataUserSubscribe dataUserSubscribe);
    void showErrorSubscribeStatus(String error);
}
