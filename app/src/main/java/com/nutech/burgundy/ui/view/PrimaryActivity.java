package com.nutech.burgundy.ui.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.nutech.burgundy.DmApplication;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataAds;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.lib.ads.AdsMediaPlayerService;
import com.nutech.burgundy.lib.jcplayer.AdsPlayView;
import com.nutech.burgundy.lib.jcplayer.JcAudio;
import com.nutech.burgundy.lib.jcplayer.JcPlayerService;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.lib.jcplayer.JcStatus;
import com.nutech.burgundy.lib.musicplayerview.MusicPlayerView;
import com.nutech.burgundy.presenter.AdsPresenter;
import com.nutech.burgundy.presenter.LyricksPresenter;
import com.nutech.burgundy.presenter.SchedulesPresenter;
import com.nutech.burgundy.presenter.soundtrack.DetailMoodtrackPresenter;
import com.nutech.burgundy.ui.adapter.TrackListAdapter;
import com.nutech.burgundy.ui.base.BaseActivity;
import com.nutech.burgundy.ui.view.album.DetailAlbumFragment;
import com.nutech.burgundy.ui.view.artists.DetailArtisTracksFragment;
import com.nutech.burgundy.ui.view.feedback.FeedbackFragment;
import com.nutech.burgundy.ui.view.home.FragmentDiscover;
import com.nutech.burgundy.ui.view.home.FragmentMyMusic;
import com.nutech.burgundy.ui.view.lyric.LyricView;
import com.nutech.burgundy.ui.view.playback.InitPlaylistAsync;
import com.nutech.burgundy.ui.view.playback.NowPlayingActivity;
import com.nutech.burgundy.ui.view.playback.NowPlayingFragment;
import com.nutech.burgundy.ui.view.playback.PlaylistView;
import com.nutech.burgundy.ui.view.register.StoreLoginActivity;
import com.nutech.burgundy.ui.view.schedule.ScheduleView;
import com.nutech.burgundy.ui.view.search.SearchFragment;
import com.nutech.burgundy.ui.view.setting.SettingFragment;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackView;
import com.nutech.burgundy.ui.view.track.ListTrackMoodtrackFragment;
import com.nutech.burgundy.ui.view.vip.VipFragment;
import com.nutech.burgundy.utils.Helper;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;

import static com.nutech.burgundy.ui.view.register.PhoneAuthActivity.REQUEST_CODE;

/**
 * @author Arif-Setiawan on 29/5/19.
 * @company Nusantara Teknologi
 */

public class PrimaryActivity extends BaseActivity implements
        // Navigation Drawer
        NavigationView.OnNavigationItemSelectedListener,
        // Fragment Navigation
        FragmentNavigation,
        // Fragment Navigation Controller
        FragNavController.RootFragmentListener, FragNavController.TransactionListener,
        // JC Player
        JcPlayerView.OnInvalidPathListener, JcPlayerView.JcPlayerViewStatusListener,
        //initPlaylist
        PlaylistView,
        DetailMoodtrackView,
        ScheduleView,
        BottomNavigationView.OnNavigationItemSelectedListener,
        JcPlayerView.JcPlayerViewServiceListener, LyricView, AdsPlayView {

    @BindView(R.id.bnv_primary)
    BottomNavigationView bnvPrimary;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.primary_jcplayer)
    JcPlayerView jcPlayerView;
    @BindView(R.id.linearLayout2)
    LinearLayout llControl;
    @BindView(R.id.cv_cover_artist)
    CircleImageView cvCoverArtist;
    @BindView(R.id.iv_play_pause_control)
    ImageView ivPlayPauseControll;
    @BindView(R.id.iv_shuffle)
    ImageView ivShuffle;
    @BindView(R.id.tv_title_tracks)
    TextView tvTitleTracks;
    @BindView(R.id.tv_title_artis)
    TextView tvTitleArtist;
    @BindView(R.id.indicator_playing)
    AVLoadingIndicatorView indicatorPlaying;
    @BindView(R.id.ll_main_shorcut)
    LinearLayout llMainShortcut;
    @BindView(R.id.rl_search)
    RelativeLayout rlSearch;
    @BindView(R.id.iv_next)
    ImageView ivNext;

    private FragNavController navController;
    private SessionManager sessionManager;
    private DmApplication dmApplication;
    private ImageView ivShufflePlaying;
    private LyricksPresenter lyricksPresenter;
    private AdsPresenter adsPresenter;
    private SchedulesPresenter schedulesPresenter;
    private DetailMoodtrackPresenter detailMoodtrackPresenter;
    private static final String TAG = "Primary";

    private final int INDEX_HOME = FragNavController.TAB1;
    private final int INDEX_MOODTRACK = FragNavController.TAB2;
    private final int INDEX_FEEDBACK = FragNavController.TAB3;
    private final int INDEX_SETTING = FragNavController.TAB4;
    private int position;

    private int on_complete = 0;
    private boolean doubleBackToExitPressedOnce = false;

    //---- Widget Params ------//
    private MusicPlayerView musicPlayerView;
    private ImageButton btnListTrack;
    private ImageButton btnPlay;
    private ImageButton btnNext;
    private AVLoadingIndicatorView pLoading;
    private ImageView ll_backdrop;
    private SeekBar seekBarControls;
    private TextView txtCurrentDurations;
    private TextView txtMaxDurations;
    private TextView txtLyricsPassed;
    private TextView txtLyricsFocused;
    private TextView txtLyricsNext;
    private List<DataLiricks> mDataLiricks = new ArrayList<>();
    private LinearLayout llLyric;
    private TextView tvTrackName;
    private Button btnKaraoke;
    private String isKaraoke;

    private List<DataTracks> tempDataTracks = new ArrayList<>();

    private int tempDuration = 0;

    private boolean serviceBound = true;
    private String titleNameArtist;
    private String urlCover;
    private String karaokeId;
    private String urlTrack;
    private String sDuration;
    private int waitingNext = 0;
    private int waitingPrevious = 0;
    private InitPlaylistAsync initPlaylistAsync;
    private TrackListAdapter trackListAdapter;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            intentHandler(intent);
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        setContentView(R.layout.activity_primary);
        ButterKnife.bind(this);

        sessionManager = getSessionManager();

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, new IntentFilter("moodtrack_controls"));
        dmApplication = (DmApplication) getApplicationContext();
        lyricksPresenter = new LyricksPresenter(getDataManager(), getNetworkHelper(), getSessionManager());
        lyricksPresenter.attachView(this);
        adsPresenter = new AdsPresenter(getDataManager(), getNetworkHelper());
        adsPresenter.attachView(this);
        detailMoodtrackPresenter = new DetailMoodtrackPresenter(getDataManager(), getNetworkHelper());
        detailMoodtrackPresenter.attachView(this);
        schedulesPresenter = new SchedulesPresenter(getDataManager(),getNetworkHelper());
        schedulesPresenter.attachView(this);
        Context context = this;

        navController = new FragNavController(getSupportFragmentManager(), R.id.container);
        navController.setRootFragmentListener(this);
        navController.setTransactionListener(this);
        navController.setFragmentHideStrategy(FragNavController.DETACH_ON_NAVIGATE_HIDE_ON_SWITCH);
        navController.setCreateEager(true);
        navController.setDefaultTransactionOptions(FragNavTransactionOptions.Companion.newBuilder().customAnimations(
                R.anim.slide_in_from_right,
                R.anim.slide_out_to_left,
                R.anim.slide_in_from_left,
                R.anim.slide_out_to_right)
                .build());
        navController.initialize(INDEX_HOME, savedInstanceState);

        intentHandler(getIntent());
        setupToolbar(getString(R.string.label_home));

        bnvPrimary.setOnNavigationItemSelectedListener(this);
        dmApplication.setJcPlayerView(jcPlayerView);

        jcPlayerView.registerInvalidPathListener(this);
        jcPlayerView.registerStatusListener(this);
        jcPlayerView.registerServiceListener(this);

//        if (!sessionManager.getSessionStatePlaying().equals("PLAY") ||
//                !sessionManager.getSessionStatePlaying().equals("PAUSE") ){
//            linearLayout.setVisibility(View.GONE);
//        } else {
//            linearLayout.setVisibility(View.VISIBLE);
//        }

        initialService();
        views();
        event();
//        initialService();

        Calendar scheduleTime = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date = sdf.parse("15:00");

            scheduleTime.set(Calendar.DAY_OF_WEEK,5);
            scheduleTime.set(Calendar.HOUR_OF_DAY, Objects.requireNonNull(date).getHours());
            scheduleTime.set(Calendar.MINUTE, Objects.requireNonNull(date).getMinutes());
            scheduleTime.set(Calendar.SECOND, 0);
            Log.e("tanggal",String.format("%1$tA %1$tb %1$td %1$tY at %1$tI:%1$tM %1$Tp", scheduleTime));


            if (currentDate.getTimeInMillis() > scheduleTime.getTimeInMillis()) {
                scheduleTime.add(Calendar.DATE,7);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e("tanggal",String.format("%1$tA %1$tb %1$td %1$tY at %1$tI:%1$tM %1$Tp", scheduleTime));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "OnStop ");
    }

    public void views() {

        ViewGroup dismissableContainer = findViewById(R.id.linearLayout2);

        hideWidgetControls();
        if (!sessionManager.getUserType().contains(Constants.UserType.USER_STORE)) {
            loadDataMember();
        } else {
            loadDataUserStore();
        }

        try {
            if (jcPlayerView != null) {
                if (jcPlayerView.isShuffle()) {
                    ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
                } else {
                    ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public void event() {

//        accountImage.setOnClickListener(v -> {
//
//
//            if (navController != null) {
//                if (getSupportActionBar() != null) {
//                    getSupportActionBar().setTitle(R.string.label_myprofile);
//                }
//                navController.pushFragment(new MyProfileFragment());
//                drawerLayout.closeDrawer(GravityCompat.START);
//            }
//        });

        llControl.setOnClickListener(v -> {

            pushFragment(new NowPlayingActivity(
                    getApplicationContext(), seekBarControls, jcPlayerView, musicPlayerView, ivPlayPauseControll,
                    llControl, mDataLiricks, tempDuration, isKaraoke, titleNameArtist, karaokeId, urlTrack, tempDataTracks, position), getString(R.string.label_now_playing), true);

//            Intent intent = new Intent(PrimaryActivity.this, NowPlayingActivity.class);
//            startActivity(intent);
        });

//        llControl.setOnTouchListener(new SwipeDismissTouchListener(
//                llControl,
//                null,
//                new SwipeDismissTouchListener.DismissCallbacks() {
//                    @Override
//                    public boolean canDismiss(Object token) {
//                        return true;
//                    }
//
//                    @Override
//                    public void onDismiss(View view, Object token) {
//                        if (sessionManager.getStatusPackage().equals("1")) {
//                            if (sessionManager.getCurrentDuration() > sessionManager.getTimer()) {
//                                Log.e("report", "1");
//                                insertLogReport();
//                            }
//                        }
//                        dismissableContainer.removeView(llControl);
//                        jcPlayerView.kill();
//                        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.STOP);
//                        llControl.setVisibility(View.GONE);
//                    }
//                }
//        ));

        try {
            ivPlayPauseControll.setOnClickListener(v -> {
                if (ivPlayPauseControll.getTag().equals(R.drawable.ic_pause_white)) {
                    if (jcPlayerView != null) {
                        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.PAUSE);

                        jcPlayerView.pause();
                    }
                } else {
                    if (jcPlayerView != null) {
                        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.CONTINUE);
                        jcPlayerView.continueAudio();
                    }
                }
            });

            ivNext.setOnClickListener(v -> {
                if (jcPlayerView != null) {
                    waitingPrevious = 0;
                    waitingNext = 0;
                    updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                            : Constants.MoodtrackControl.PAUSE);
                    if (jcPlayerView.getWaitBetweenAds() < 1) {
                        adsPresenter.getAds(1, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                        insertLogReport();
                    } else {
                        sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                        insertLogReport();

                        adsPresenter.getAds(0, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                    }
//                    jcPlayerView.next();
                }
            });


            ivShuffle.setOnClickListener(v -> {
                if (jcPlayerView.isShuffle()) {
                    if (jcPlayerView.setShuffle(false)) {
                        updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                                : Constants.MoodtrackControl.PAUSE);
                        ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                    }
                } else {
                    if (jcPlayerView.setShuffle(true)) {
                        updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                                : Constants.MoodtrackControl.PAUSE);
                        ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        rlSearch.setOnClickListener(view -> pushFragment(new SearchFragment(), getString(R.string.search_menu), true));
    }

    private void playingMoodtrackControl(List<DataTracks> dataTracks) {
        tempDataTracks = dataTracks;
        sessionManager.setCurrentTracksId(tempDataTracks.get(0).getId());
        playingMusic(0, dataTracks);

        if (Objects.requireNonNull(Objects.requireNonNull(navController.getCurrentFrag()).
                getTag()).contains(NowPlayingActivity.class.getSimpleName()) ||
                Objects.requireNonNull(Objects.requireNonNull(navController.getCurrentFrag()).
                        getTag()).contains(NowPlayingFragment.class.getSimpleName()) ||
                Objects.requireNonNull(Objects.requireNonNull(navController.getCurrentFrag()).
                        getTag()).contains(ListTrackMoodtrackFragment.class.getSimpleName())) {

            if (trackListAdapter != null) {
                trackListAdapter.clear();
                trackListAdapter.addAll(tempDataTracks);
                trackListAdapter.notifyDataSetChanged();
            }
            updateControll(true);
        } else {
            musicPlayerView = new MusicPlayerView(this);
            btnPlay = new ImageButton(this);
            tvTrackName = new TextView(this);
            ll_backdrop = new ImageView(this);
            seekBarControls = new SeekBar(this);
            pLoading = new AVLoadingIndicatorView(this);
            ivShufflePlaying = new ImageView(this);
            txtCurrentDurations = new TextView(this);
            txtMaxDurations = new TextView(this);
            txtLyricsPassed = new TextView(this);
            txtLyricsFocused = new TextView(this);
            txtLyricsNext = new TextView(this);
            llLyric = new LinearLayout(this);
            btnListTrack = new ImageButton(this);
            updateControll(false);
        }
    }

    private void loadDataMember() {
        lyricksPresenter.getDataDetailMember(
                sessionManager.getDataMember().getId(),
                sessionManager.getDataMember().getMobile_phone());
    }

    private void updateIsPlaying(String is_app_run, String status) {
        Map<String, String> params = new HashMap<>();
        String nextTrack = "";

        //mengecek untuk next track apakah ke previous atau next dan mengambil track idnya
        if (waitingPrevious > 0) {
            int currentTrackIndex = jcPlayerView.getMyPlaylist().indexOf(jcPlayerView.getCurrentAudio());
            try {
                nextTrack = String.valueOf(jcPlayerView.getMyPlaylist().get(currentTrackIndex-1-waitingPrevious).getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(waitingNext > 0) {
            int currentTrackIndex = jcPlayerView.getMyPlaylist().indexOf(jcPlayerView.getCurrentAudio());
            try {
                nextTrack = String.valueOf(jcPlayerView.getMyPlaylist().get(currentTrackIndex+1+waitingNext).getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.e("masuk","update");

        if (!is_app_run.equals(Constants.IsAppRun.NOT_RUN)) {
            params.put("moodtrack_id", sessionManager.getCurrentMoodTrack());
            params.put("track_id", sessionManager.getCurrentTracksId());
            params.put("next_track_id", nextTrack);
        } else {
            params.put("moodtrack_id", "");
            params.put("track_id", "");
            params.put("next_track_id", "");
        }

        try {
            params.put("is_shuffle", jcPlayerView.isShuffle() ? Constants.IsShuffle.ON : Constants.IsShuffle.OFF);
            params.put("is_playing", jcPlayerView.isPlaying() ? Constants.IsPlaying.PLAYING : Constants.IsPlaying.NOT_PLAYING);
        } catch (Exception e) {
            e.printStackTrace();
            params.put("is_shuffle", Constants.IsShuffle.OFF);
            params.put("is_playing", Constants.IsPlaying.NOT_PLAYING);
        }

        if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
            params.put("store_id", sessionManager.getUserStore().getStore_id());
            params.put("store_member_id", sessionManager.getUserStore().getId());
            params.put("client_id", sessionManager.getStore().getClient_id());
            params.put("is_app_run", is_app_run);
            params.put("status", status);
        }

        lyricksPresenter.updateIsPlayingStatus(params);
    }

    private void loadDataUserStore() {

        lyricksPresenter.getDataDetailUserStore(
                sessionManager.getDataMember().getId(), Constants.IsAppRun.RUN);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (navController != null) {
            navController.onSaveInstanceState(outState);
        }
    }

    //--- Navigation Drawer ---//

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Helper.hideSoftKeyBoard(this);

        switch (item.getItemId()) {

            case R.id.menu_home:
                navController.switchTab(INDEX_HOME);
                break;
            case R.id.menu_my_moodtrack:
                navController.switchTab(INDEX_MOODTRACK);
                break;
            case R.id.menu_feedback:
                navController.switchTab(INDEX_FEEDBACK);
                break;
            case R.id.menu_setting:
                navController.switchTab(INDEX_SETTING);
                break;

//            case R.id.menu_home:
//                navController.switchTab(INDEX_HOME);
//                break;
//
//            case R.id.menu_vip:
//                navController.switchTab(INDEX_VIP);
//                break;
//
//            case R.id.menu_point:
//                navController.switchTab(INDEX_POINT);
//                break;
//
//            case R.id.menu_history:
//                navController.switchTab(INDEX_HISTORY);
//                break;
//
//            case R.id.menu_follow:
//                navController.switchTab(INDEX_FOLLOW);
//                break;
//
//            case R.id.menu_feedback:
//                navController.switchTab(INDEX_FEEDBACK);
//                break;
//
//            case R.id.menu_about:
//                navController.switchTab(INDEX_ABOUT);
//                break;
//
//            case R.id.menu_setting:
//                navController.switchTab(INDEX_SETTING);
//                break;
//
//            case R.id.menu_history_karaoke:
//                navController.switchTab(INDEX_HISTORY_KARAOKE);
//                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermssion();
        }

//        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    //--- Fragment Navigation ---//

    @Override
    public void pushFragment(Fragment fragment, String title, Boolean withBackArrow) {

        Log.e(TAG, "pushFragment: " + title);
        Log.e(TAG, "Status Subs: " + sessionManager.getStatusPackage());

        if (navController != null) {
            navController.pushFragment(fragment);
            setupToolbar(title);
            setToolbar(toolbar, title, withBackArrow);
        }
//        if (sessionManager.getStatusPackage().equals("1")) {
//            tvTypeMember.setText(getString(R.string.label_subscribe));
//        } else {
//            tvTypeMember.setText(getString(R.string.label_not_subscribe));
//        }
        if (!sessionManager.getUserType().contains(Constants.UserType.USER_STORE)) {
            loadDataMember();
        } else {
            loadDataUserStore();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkPermssion() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_CODE);
        }
    }

    private void initialService() {
        if (serviceBound) {

            //Store Serializable audioList to SharedPreferences

            Intent playerIntent = new Intent(this, JcPlayerService.class);
            bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);


            Intent AdsPlayerIntent = new Intent(this, AdsMediaPlayerService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(playerIntent);
                startForegroundService(AdsPlayerIntent);
            } else {
                startService(AdsPlayerIntent);
                startService(playerIntent);
            }
        }  //Store the new audioIndex to SharedPreferences

    }

    @Override
    public void playingMusic(final int position, List<DataTracks> dataTracksList) {
        this.position = position;
        initialService();

        tempDataTracks = dataTracksList;

        initPlaylistAsync = new InitPlaylistAsync(this, jcPlayerView, Constants.InitPlayistAsyncStatus
        .PLAY);
        initPlaylistAsync.execute(dataTracksList);

    }

    @Override
    public void updatePositionTracks(List<DataTracks> dataTracksList) {

        tempDataTracks = dataTracksList;
        initPlaylistAsync = new InitPlaylistAsync(this, jcPlayerView, Constants.InitPlayistAsyncStatus
                .SORT);
        initPlaylistAsync.execute(dataTracksList);
        initPlaylistAsync.cancel(true);
    }

    @Override
    public void setWaitingNext(int waitNext) {
        this.waitingNext = waitNext;
    }

    @Override
    public void setWaitingPrevious(int waitPrevious) {
        this.waitingPrevious = waitPrevious;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void updatePlayer(MusicPlayerView mpvMusic, ImageButton pBtnPlay, ImageButton pBtnNext,
                             ImageButton pBtnPrev, AVLoadingIndicatorView loading, ImageView llBackdrop,
                             SeekBar seekBarDuration, TextView tvCurrentProgress, TextView tvMaxprogress,
                             TextView txtLiricksPassed, TextView txtLiricksFocussed,
                             TextView txtLiricksNext, LinearLayout llLiricks, TextView tvTrackName,
                             Button btKaraoke, String includeKaraoke, String karaokeId, String urlTrack, ImageView ivShufflePlaying, ImageButton btnListTrack) {

        this.musicPlayerView = mpvMusic;
        this.btnPlay = pBtnPlay;
        this.btnNext = pBtnNext;
        this.pLoading = loading;
        this.ivShufflePlaying = ivShufflePlaying;
        this.ll_backdrop = llBackdrop;
        this.seekBarControls = seekBarDuration;
        this.txtCurrentDurations = tvCurrentProgress;
        this.txtMaxDurations = tvMaxprogress;
        this.txtLyricsPassed = txtLiricksPassed;
        this.txtLyricsFocused = txtLiricksFocussed;
        this.txtLyricsNext = txtLiricksNext;
        this.llLyric = llLiricks;
        this.tvTrackName = tvTrackName;
        this.btnKaraoke = btKaraoke;
        this.isKaraoke = includeKaraoke;
        this.karaokeId = karaokeId;
        this.urlTrack = urlTrack;
        this.btnListTrack = btnListTrack;

        if (jcPlayerView.isPlaying()) {
            if (!musicPlayerView.isRotating()) {
                musicPlayerView.stop();
                musicPlayerView.setmIsPlay(true);
                musicPlayerView.start();
            }
        } else {
            musicPlayerView.start();
        }

        tvTrackName.setText(titleNameArtist);
        musicPlayerView.setCoverURL(urlCover);
        seekBarControls.setMax(tempDuration);
        musicPlayerView.setMax(tempDuration/1000);
        txtMaxDurations.setText(sDuration);

        Picasso.with(getApplicationContext())
                .load(urlCover)
                .error(R.drawable.image_dm_error)
                .transform(new BlurTransformation(getApplicationContext()))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ll_backdrop);

        this.musicPlayerView.setOnClickListener(v -> {
            if (musicPlayerView.isRotating()) {
                updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.PAUSE);


                jcPlayerView.pause();
            } else {
                updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.CONTINUE);
                jcPlayerView.continueAudio();
            }

        });


        this.ivShufflePlaying.setOnClickListener(v -> {
            if (jcPlayerView.isShuffle()) {
                if (jcPlayerView.setShuffle(false)) {
                    updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                            : Constants.MoodtrackControl.PAUSE);
                    ivShufflePlaying.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                }
            } else {
                if (jcPlayerView.setShuffle(true)) {
                    updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                            : Constants.MoodtrackControl.PAUSE);
                    ivShufflePlaying.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
                }

            }
        });

        this.btnPlay.setOnClickListener(v -> {
            if (sessionManager.getSessionStatePlaying().equals("PLAY")) {
                updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.PAUSE);

                jcPlayerView.pause();
            } else if (sessionManager.getSessionStatePlaying().equals("PAUSE")) {
                updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.PLAY);
                jcPlayerView.continueAudio();
            }
        });

        this.btnNext.setOnClickListener(v -> {
            if (jcPlayerView != null) {

                waitingPrevious = 0;
                waitingNext = 0;
                updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                        : Constants.MoodtrackControl.PAUSE);
                if (jcPlayerView.getWaitBetweenAds() < 1) {
                    adsPresenter.getAds(1, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                    insertLogReport();
                } else {
                    sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                    insertLogReport();

                    adsPresenter.getAds(0, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                }
//                jcPlayerView.next();
            }
        });

        pBtnPrev.setOnClickListener(v -> {
            if (jcPlayerView != null) {
                waitingPrevious = 0;
                waitingNext = 0;
                updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                        : Constants.MoodtrackControl.PAUSE);

                if (jcPlayerView.getWaitBetweenAds() < 1) {
                    adsPresenter.getAds(1, false, true, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                    insertLogReport();
                } else {
                    sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                    insertLogReport();

                    adsPresenter.getAds(0, false, true, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                }
//                jcPlayerView.previous();
            }
        });

        seekBarControls.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                if (fromUser) {
                    Log.e("onProgressChanged", String.valueOf(i));
                    jcPlayerView.seekToFromPrimary(i);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekBarControls.setOnTouchListener((view, motionEvent) -> true);

        this.btnListTrack.setOnClickListener(v -> {
            pushFragment(new ListTrackMoodtrackFragment(jcPlayerView.getMyPlaylist(), jcPlayerView),
                    getString(R.string.tracks_list_label),true);
        });
    }

    @Override
    public void updateControll(boolean status) {

        if (status) {
            llControl.setVisibility(View.GONE);
        } else {
            llControl.setVisibility(View.VISIBLE);
            indicatorPlaying.smoothToShow();
        }

    }

    @Override
    public void updateProfile() {
        loadDataMember();
    }

    @Override
    public void stopMusic() {

        if (sessionManager.getStatusPackage().equals("1")) {
            if (sessionManager.getCurrentDuration() > sessionManager.getTimer()) {
                insertLogReport();
                Log.e("report", "2");

            }
        }
        jcPlayerView.kill();
        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.STOP);
    }

    @Override
    public List<DataTracks> getDataTracks() {
        return tempDataTracks;
    }

    @Override
    public void backFragment() {

        if (navController != null) {
            navController.popFragment();
            setupToolbar(null);
        }

//        if (sessionManager.getStatusPackage().equals("1")) {
//            tvTypeMember.setText(getString(R.string.label_subscribe));
//        } else {
//            tvTypeMember.setText(getString(R.string.label_not_subscribe));
//        }
    }

    @Override
    public void updateTitle(String title) {
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
    }

    //--- Fragment Navigation Controller ---//

    @Override
    public Fragment getRootFragment(int i) {

        Log.e(TAG, "rootFragment " + i);
        switch (i) {

            case INDEX_HOME:
                return new FragmentDiscover();

            case INDEX_MOODTRACK:
                return new FragmentMyMusic();

            case INDEX_FEEDBACK:
                return new FeedbackFragment();

            case INDEX_SETTING:
                return new SettingFragment();
        }

        throw new IllegalStateException("Need to send an index that we know");
    }

    @Override
    public void onTabTransaction(Fragment fragment, int i) {

        /*
          Call when Menu Drawer was select
         */

        Log.i(TAG, "onTabTransaction: ");

        setupTab(fragment);
        setupToolbar(null);
    }

    @Override
    public void onFragmentTransaction(
            Fragment fragment, FragNavController.TransactionType transactionType) {

        /*
        Call when Sub Menu was select inside Fragment
         */

        setupTab(fragment);
        setupToolbar(null);
    }

    //--- Common Activity ---//

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (navController == null) {
            return false;
        }

        if (!navController.isRootFragment() && item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        removeNotification();
        if (jcPlayerView != null) {
            jcPlayerView.kill();
        }
        tabs.removeAllTabs();
    }

    @Override
    public void onBackPressed() {
        Helper.hideSoftKeyBoard(this);

        //change color shuffle when button triggered
        if (jcPlayerView != null) {
            if (jcPlayerView.isShuffle()) {
                ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
            } else {
                ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
            }
        }

        DetailArtisTracksFragment
                detailArtistTracksFragment = (DetailArtisTracksFragment)
                getSupportFragmentManager().findFragmentByTag(Constants.ParamKey.ARTIST_TRACK);

        if (Constants.ParamValue.isNOW_PLAYING) {
            llControl.setVisibility(View.VISIBLE);
            indicatorPlaying.smoothToShow();
        }

        Constants.ParamValue.isNOW_PLAYING = false;

        if (navController == null) {
            exitApp();
        }

        Log.e(TAG, Objects.requireNonNull(navController.getCurrentFrag().getTag()));

        if (navController.getCurrentFrag().getTag() != null) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else if (Objects.requireNonNull(Objects.requireNonNull(navController.getCurrentFrag()).getTag())
                    .contains("com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackFragment")) {
                navController.clearStack();
                setupToolbar(null);

//            navController.popFragments(1);
            } else if (detailArtistTracksFragment != null) {
                if (detailArtistTracksFragment.isCheckbox()) {
                    detailArtistTracksFragment.setCheckbox(false);
                } else {
                    navController.popFragment();
                    setupToolbar(null);
                }

            } else if (Objects.requireNonNull(Objects.requireNonNull(navController.getCurrentFrag()).getTag())
                    .contains("DetailAlbumFragment")) {
                DetailAlbumFragment detailAlbumFragment = (DetailAlbumFragment) navController.getCurrentFrag();
                if (detailAlbumFragment.isCheckbox()) {
                    detailAlbumFragment.setCheckbox(false);
                } else {
                    navController.popFragment();
                    setupToolbar(null);
                }

            } else if (!navController.isRootFragment()) {
                navController.popFragment();
                setupToolbar(null);
            } else {
                exitApp();
            }
        } else {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else if (!navController.isRootFragment()) {
                navController.popFragment();
                setupToolbar(null);
            } else {
                exitApp();
            }
        }
    }

    private void exitApp() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            if (jcPlayerView != null) {
                jcPlayerView.kill();
            }
//            finishAffinity();
//            System.exit(0);
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.message_tap_to_exit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    //--- Private Method ---//

    private List<Fragment> getPrimaryMenuList() {

        List<Fragment> fragmentList = new ArrayList<>();
        fragmentList.add(new FragmentDiscover());
        fragmentList.add(new FeedbackFragment());
        fragmentList.add(new SettingFragment());
        fragmentList.add(new FragmentMyMusic());

        return fragmentList;
    }

    @Override
    public void onPathError(JcAudio jcAudio) {

    }

    @Override
    public void onPausedStatus(JcStatus jcStatus) {
        Log.d(TAG, "Status Playing = " + jcStatus.getPlayState());
        sessionManager.setSessionStatePlaying(String.valueOf(jcStatus.getPlayState()));
        indicatorPlaying.smoothToHide();
    }

    @Override
    public void onContinueAudioStatus(JcStatus jcStatus) {

//        llControl.setVisibility(View.VISIBLE);

    }

    @Override
    public void onPlayingStatus(JcStatus jcStatus) {
        Log.d(TAG, "Status Playing = " + jcStatus.getPlayState());
        sessionManager.setSessionStatePlaying(String.valueOf(jcStatus.getPlayState()));

//        if (Constants.ParamValue.isNOW_PLAYING){
//            llControl.setVisibility(View.GONE);
//        } else {
//            llControl.setVisibility(View.VISIBLE);
//        }

        indicatorPlaying.smoothToShow();
    }

    @Override
    public void onTimeChangedStatus(final JcStatus jcStatus) {

//        final int newCurrentTime = (int) jcStatus.getCurrentPosition() / 1000;

//        PrimaryActivity.this.runOnUiThread(() -> {
//
////                System.out.println("+++ Curren Time Status = " + newCurrentTime + "\n"+
////                        "+++ Timer Setting  = " + sessionManager.getTimer() +"\n"+
////                        "+++ Subscribe Status = "+ sessionManager.getStatusPackage());
//
//            if (newCurrentTime == sessionManager.getTimer()) {
//                if (!sessionManager.getStatusPackage().equals("1")) {
//
//                    Log.e("masuk", "1");
//
//                    try {
//                        Uri myUri = Uri.parse("https://nf1f8200-a.akamaihd.net/downloads/ringtones/files/mp3/ff96376adf08cf119de3f14a034c741f-high-quality-46415.mp3"); // initialize Uri here
//                        MediaPlayer mediaPlayer = new MediaPlayer();
//                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
//                        mediaPlayer.setDataSource(context, myUri);
//                        mediaPlayer.prepare();
//                        mediaPlayer.start();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    //                    musicPlayerView.stop();
////                    jcPlayerView.kill();
////                    try {
////                        new Handler().postDelayed(new Runnable() {
////                            @Override
////                            public void run() {
////                                if(!((Activity) context).isFinishing())
////                                {
////                                    alertPlaying();
////                                }
////                            }
////                        }, delayed);
////                    }catch (Exception e){
////                        e.printStackTrace();
////                    }
//                }
//            }
//        });
    }

    @Override
    public void onCompletedAudioStatus(JcStatus jcStatus) {

//        Log.e("****", "onCompletedAudioStatus");

    }

    @Override
    public void onPreparedAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
        on_complete = 0;
        tempDuration = duration;
        int newDuration = duration / 1000;
        seekBarControls.setMax(duration);
        musicPlayerView.setMax(newDuration);

        if (!musicPlayerView.isRotating()) {
            musicPlayerView.start();
        }
        Log.e("durasi", String.valueOf(newDuration));
        Log.e("jumlah", String.valueOf(jcPlayerView.getLengthAds()));
        if (jcPlayerView.getLengthAds() > 1 && !jcPlayerView.isBetweenAds()) {
            Log.e("durasi", "lalu");
            sessionManager.setTimer(Math.round(newDuration / 3));
        } else if (jcPlayerView.getLengthAds() > 0 && jcPlayerView.getLengthAds() < 2 && !jcPlayerView.isBetweenAds()) {
            try {
                int timer = new Random().nextInt(newDuration-jcPlayerView.getFirstAds().getDuration()+1);
                Log.e("durasi", String.valueOf(timer));
                sessionManager.setTimer(timer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int minute = newDuration / 60;
        int second = newDuration % 60;

        sDuration = // Minutes
                (minute < 10 ? "0" + minute : minute + "")
                        + ":" +
                        // Seconds
                        (second < 10 ? "0" + second : second + "");

        txtMaxDurations.post(() -> txtMaxDurations.setText(sDuration));
        sessionManager.setMaxDuration(sDuration);
    }

    @Override
    public void onCompletedAudio() {

//        llControl.setVisibility(View.VISIBLE);
//        indicatorPlaying.smoothToShow();

//        Log.e("****", "onCompletedAudio");

        boolean isContinue = waitingNext > 0 || waitingPrevious < 1;
        boolean isPrevious = waitingPrevious > 0;
        if (on_complete == 0) {
            if (jcPlayerView.getWaitBetweenAds() < 1) {
                adsPresenter.getAds(1, isContinue, isPrevious, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                insertLogReport();
            } else {
                if (jcPlayerView.isBetweenAds()) {
                    insertLogAds(sessionManager.getCurrentTracksId(), sessionManager.getPrevTracksId());
                } else {
                    sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                    insertLogReport();
                }

                adsPresenter.getAds(0, isContinue, isPrevious, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
            }

            if (musicPlayerView.isRotating()) {
                musicPlayerView.stop();
            }

            on_complete = 1;
            ivPlayPauseControll.setImageResource(R.drawable.ic_play_arrow);
            ivPlayPauseControll.setTag(R.drawable.ic_play_arrow);
        }

    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {

//        llControl.setVisibility(View.VISIBLE);

        if (musicPlayerView.isRotating()) {
            musicPlayerView.stop();
        }

        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.PAUSE);

        ivPlayPauseControll.setImageResource(R.drawable.ic_play_arrow);
        ivPlayPauseControll.setTag(R.drawable.ic_play_arrow);
        btnPlay.setBackgroundResource(R.drawable.ic_play_arrow);

        indicatorPlaying.smoothToHide();
    }

    @Override
    public void onContinueAudio() {
//        if (!musicPlayerView.isRotating()) {
//            musicPlayerView.start();
//        }
//        musicPlayerView.start();
//        llControl.setVisibility(View.VISIBLE);
//        indicatorPlaying.smoothToHide();
    }

    @Override
    public void onPlaying() {

//        llControl.setVisibility(View.VISIBLE);
        Log.e("onContinueAudio", String.valueOf(musicPlayerView.isRotating()));

        if (!musicPlayerView.isRotating()) {
            musicPlayerView.start();
        }

        ivPlayPauseControll.setImageResource(R.drawable.ic_pause_white);
        ivPlayPauseControll.setTag(R.drawable.ic_pause_white);
        btnPlay.setBackgroundResource(R.drawable.ic_pause_white);

        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.CONTINUE);

        indicatorPlaying.smoothToShow();
    }

    @Override
    public void addLogAds(String id, String trackId) {
        insertLogAds(id, trackId);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onTimeChanged(final long currentTime) {
        final int newCurrentTime = (int) currentTime / 1000;
        musicPlayerView.setProgress(newCurrentTime);
        seekBarControls.setProgress((int) currentTime);

//        Log.e("timeChange", "masuk");
        if (newCurrentTime > 0) {
            sessionManager.setCurrentDuration(newCurrentTime);
        }

        int minutes = newCurrentTime / 60;
        int seconds = newCurrentTime % 60;
        final String sMinutes = minutes < 10 ? "0" + minutes : minutes + "";
        final String sSeconds = seconds < 10 ? "0" + seconds : seconds + "";

        if (newCurrentTime == sessionManager.getTimer()) {
            if (!jcPlayerView.isIklan()) {
                Log.e("iklan", String.valueOf(jcPlayerView.isBetweenAds()));
                if (!jcPlayerView.isBetweenAds()) {
                    if (jcPlayerView.getLengthAds() > 0) {
                        DataAds ads = jcPlayerView.getFirstAds();
                        jcPlayerView.playAds(Constants.URL_AUDIO_ADS + ads.getSource(),
                                ads.getId(),
                                sessionManager.getCurrentTracksId());

                        if (jcPlayerView.getLengthAds() > 1 && !jcPlayerView.isBetweenAds())
                            sessionManager.setTimer(sessionManager.getTimer() * 2);
                    }
                }

//                    musicPlayerView.stop();
//                    jcPlayerView.kill();
//                    try {
//                        new Handler().postDelayed(() -> {
//                            if(!((Activity) context).isFinishing())
//                            {
//                                alertPlaying();
//                            }
//                        }, delayed);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                    }
            }
        }

        txtCurrentDurations.post(() -> {
            txtCurrentDurations.setText(sMinutes + ":" + sSeconds);
            String startOriginalLyrics = sMinutes + ":" + sSeconds;

//                System.out.println("+++ Curren Time = " + currentTime + "\n"+
//                                    "+++ Timer Setting = " + sessionManager.getTimer() +"\n"+
//                                    "+++ Subscribe Status = "+ sessionManager.getStatusPackage());


            cangesLyrics(startOriginalLyrics);
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void updateTitle(final String title, final String artistName) {
        tvTitleTracks.post(() -> tvTitleTracks.setText(title));

        tvTrackName.post(() -> tvTrackName.setText(title + "-" + artistName));

        tvTitleArtist.post(() -> tvTitleArtist.setText(artistName));

        titleNameArtist = title + "-" + artistName;
    }

    @Override
    public void updateCover(String cover) {
        String ImageUrl;
//        llControl.setVisibility(View.VISIBLE);
        ImageUrl = cover;
        urlCover = cover;

        Log.e("*** url cover ***", ImageUrl);
        Constants.ParamValue.URLIMAGEARTIST = ImageUrl;
        if (ImageUrl.isEmpty()) {
            musicPlayerView.setCoverDrawable(R.drawable.image_dm_error);
        } else {
            musicPlayerView.setCoverURL(ImageUrl);
        }

        Picasso.with(getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .transform(new BlurTransformation(getApplicationContext()))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(ll_backdrop);

        Picasso.with(getApplicationContext())
                .load(ImageUrl)
                .error(R.drawable.image_dm_error)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(cvCoverArtist);

    }

    @Override
    public void updateAudioId(String id) {

        sessionManager.setCurrentTracksId(id);
        Map<String, String> params = new HashMap<>();
        Map<String, String> params1 = new HashMap<>();

        try {

            String trackId = "";
            String albumId = "";
            String artistId = "";
            String genreId = "";
            String releaseYear = "";

            for (int i = 0; i < tempDataTracks.size(); i++) {

                if (tempDataTracks.get(i).getId().equals(id)) {
                    trackId = tempDataTracks.get(i).getId();
                    albumId = tempDataTracks.get(i).getAlbum_id();
                    artistId = tempDataTracks.get(i).getArtist_id();
                    releaseYear = tempDataTracks.get(i).getReleaseyear();
                    genreId = tempDataTracks.get(i).getGenreid();
                }
            }

            if (TextUtils.isEmpty(genreId)) {
                genreId = "";
            }

            if (TextUtils.isEmpty(releaseYear)) {
                releaseYear = "";
            }

            if (TextUtils.isEmpty(albumId)) {
                albumId = "";
            }

            params1.put("id", id);

            if (sessionManager.getUserType().contains(Constants.UserType.USER_STORE)) {
                params.put("store_id", sessionManager.getUserStore().getStore_id());
            } else {
                params.put("customer_id", sessionManager.getDataMember().getId());
            }

            params.put("moodtrack_id", sessionManager.getCurrentMoodTrack());
            params.put("track_id", trackId);
            params.put("album_id", albumId);
            params.put("artist_id", artistId);
            params.put("release_year", releaseYear);
            params.put("genre_id", genreId);

            if (sessionManager.getStatusPackage().equals("1")) {
                params.put("subscribe_code", sessionManager.getPackageCode());
                params.put("status", sessionManager.getStatusPackage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!jcPlayerView.isBetweenAds()) {
            lyricksPresenter.send(id, params1, params);
        }
        updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.CONTINUE);
    }

    @Override
    public void onShuffle(boolean val) {
        if (val) {
            ivShufflePlaying.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
            ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
        } else {
            ivShufflePlaying.setBackgroundTintList(getResources().getColorStateList(R.color.white));
            ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
        }
        updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                : Constants.MoodtrackControl.PAUSE);
    }

    @Override
    public void removeNotification() {
        updateIsPlaying(Constants.IsAppRun.NOT_RUN, Constants.MoodtrackControl.STOP);
    }

    private void setupTab(Fragment fragment) {

        if (fragment instanceof FragmentDiscover) {
            tabs.setVisibility(View.VISIBLE);
            llMainShortcut.setVisibility(View.VISIBLE);
        } else if (fragment instanceof FragmentMyMusic) {
            tabs.setVisibility(View.VISIBLE);
            llMainShortcut.setVisibility(View.VISIBLE);
        } else if (fragment instanceof FeedbackFragment) {
            tabs.setVisibility(View.VISIBLE);
            llMainShortcut.setVisibility(View.VISIBLE);
        } else if (fragment instanceof SettingFragment) {
            tabs.setVisibility(View.VISIBLE);
            llMainShortcut.setVisibility(View.VISIBLE);
        } else {
            tabs.setVisibility(View.GONE);
            llMainShortcut.setVisibility(View.GONE);
        }
    }

    private void setupToolbar(String title) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        if (navController != null) {
            int SDK_INT = Build.VERSION.SDK_INT;
            if (navController.isRootFragment()) {

                if (SDK_INT != 17) {
                    toolbar.setVisibility(View.VISIBLE);
                    setToolbar(toolbar, title, false);
                    bnvPrimary.setVisibility(View.VISIBLE);
                    params.addRule(RelativeLayout.ABOVE, R.id.bnv_primary);
                    params.removeRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    llControl.setLayoutParams(params);
                }


            } else {

                if (SDK_INT != 17) {
                    bnvPrimary.setVisibility(View.GONE);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    llControl.setLayoutParams(params);
                } else {
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                    llControl.setLayoutParams(params);
                    bnvPrimary.setVisibility(View.GONE);
                }
            }
            setFragmentTitle(navController.getCurrentFrag());
        }
    }

    private void setToolbar(Toolbar defaultToolbar, String title, boolean withBackArrow) {

        setSupportActionBar(defaultToolbar);
        if (getSupportActionBar() != null) {
            if (!TextUtils.isEmpty(title)) {
                SpannableStringBuilder str = new SpannableStringBuilder(title);
                str.setSpan(new StyleSpan(Typeface.BOLD), 0, str.length(), Spannable.SPAN_USER);
                getSupportActionBar().setTitle(str);
            }
            if (withBackArrow) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_left_arrow);
            }
        }

        if (!withBackArrow) {
            toolbar.setNavigationIcon(null);
//            toggle = new ActionBarDrawerToggle(this, drawerLayout, defaultToolbar,
//                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//            drawerLayout.addDrawerListener(toggle);
//            toggle.syncState();
        }
    }

    private void cangesLyrics(String startOriginalLyrics) {

        for (int i = 0; i < mDataLiricks.size(); i++) {

            if (startOriginalLyrics.equals(mDataLiricks.get(i).getStart_at())) {
                final int finalI = i;
                txtLyricsFocused.post(() -> {
                    try {
                        txtLyricsPassed.setText(mDataLiricks.get(finalI - 1).getText());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    txtLyricsFocused.setText(mDataLiricks.get(finalI).getText());

                    try {
                        txtLyricsNext.setText(mDataLiricks.get(finalI + 1).getText());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }

//            if (startOriginalLyrics > mDataLiricks.get(i).getStart_at())


        }
    }

    @Override
    public void showLiricks(List<DataLiricks> dataLiricksList) {
        llLyric.setVisibility(View.VISIBLE);
        dmApplication.setDataLiricksList(dataLiricksList);
        mDataLiricks.clear();
        mDataLiricks = dataLiricksList;
    }

    @Override
    public void showDataMember(DataMember dataMember) {

//        accountName.setText(dataMember.getName());
//
//        if (sessionManager.getStatusPackage().equals("1")) {
//            tvTypeMember.setText(getString(R.string.label_subscribe));
//        } else {
//            tvTypeMember.setText(getString(R.string.label_not_subscribe));
//        }
//
//        tvPoint.setText(dataMember.getPoint() + " Point");
//
//        String url = Constants.URL_IMAGE_MEMBER + dataMember.getImage();
//        Picasso.with(getApplicationContext())
//                .load(url)
//                .error(R.drawable.default_user)
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .into(accountImage);
    }

    @Override
    public void showErrorMember(String error) {
        Log.e("~~~~~~~~~~", "get Data Member Error");
//        accountName.setText(sessionManager.getDataMember().getName());
//        tvTypeMember.setText("");
//        tvPoint.setText("");
//
//        accountImage.setBackgroundResource(R.drawable.default_user);
    }

    @Override
    public void showDataUserStore(DataUserStore dataUserStore) {
//        accountName.setText(dataUserStore.getName());
//
//        if (sessionManager.getStatusPackage().equals("1")) {
//            tvTypeMember.setText(getString(R.string.label_subscribe));
//        } else {
//            tvTypeMember.setText(getString(R.string.label_not_subscribe));
//        }
//
////        tvPoint.setText(dataUserStore.getPoint() + " Point");
//
//        String url = Constants.URL_IMAGE_MEMBER + dataUserStore.getImage();
//        Picasso.with(getApplicationContext())
//                .load(url)
//                .error(R.drawable.default_user)
//                .memoryPolicy(MemoryPolicy.NO_CACHE)
//                .networkPolicy(NetworkPolicy.NO_CACHE)
//                .into(accountImage);
//        tvTypeMember.setText("");
//        tvPoint.setText("");

//        accountImage.setBackgroundResource(R.drawable.default_user);
    }

    @Override
    public void showErrorUserStore(String error) {
        Log.e("~~~~~~~~~~", "get Data Member Error");
//        accountName.setText(sessionManager.getUserStore().getName());
//        tvTypeMember.setText("");
//        tvPoint.setText("");
//
//        accountImage.setBackgroundResource(R.drawable.default_user);
    }

    @Override
    public void showErrorLiricks(String error) {
        llLyric.setVisibility(View.GONE);
    }

    private void hideWidgetControls() {

        if (btnPlay == null || btnNext == null) {
//            llControl.setVisibility(View.GONE);
        } else {
//            llControl.setVisibility(View.GONE);
        }
    }

    private void alertPlaying() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setCancelable(false);
        alertDialog.setTitle(getString(R.string.label_info));
        alertDialog.setMessage(getString(R.string.msg_full_fiture));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.label_subscribe),
                (dialog, which) -> {
                    dialog.dismiss();
                    jcPlayerView.kill();
                    pushFragment(new VipFragment(), getString(R.string.label_subscribe), true);

                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.label_no),
                (dialog, which) -> {
                    dialog.dismiss();
                    jcPlayerView.kill();
                    Intent intent = new Intent(getApplicationContext(), PrimaryActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                });
        alertDialog.show();
    }

    //Binding this Client to the AudioPlayer Service
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            JcPlayerService.JcPlayerServiceBinder binder = (JcPlayerService.JcPlayerServiceBinder) service;
            JcPlayerService player = binder.getService();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();

            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void insertLogReport() {
        Map<String, String> param = new HashMap<>();
        try {
            String trackId = "";
            String albumId = "";
            String artistId = "";
            String genreId = "";
            String releaseYear = "";
            String publisheId = "";

            for (int i = 0; i < tempDataTracks.size(); i++) {

                if (tempDataTracks.get(i).getId().equals(sessionManager.getCurrentTracksId())) {
                    trackId = tempDataTracks.get(i).getId();
                    albumId = tempDataTracks.get(i).getAlbum_id();
                    artistId = tempDataTracks.get(i).getArtist_id();
                    releaseYear = tempDataTracks.get(i).getReleaseyear();
                    genreId = tempDataTracks.get(i).getGenreid();
                    publisheId = tempDataTracks.get(i).getPublisher_id();
                }

            }

            if (TextUtils.isEmpty(genreId)) {
                genreId = "";
            }

            if (TextUtils.isEmpty(releaseYear)) {
                releaseYear = "";
            }

            if (TextUtils.isEmpty(albumId)) {
                albumId = "";
            }

            if (TextUtils.isEmpty(publisheId)) {
                publisheId = "";
            }

            if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
                param.put("store_id", sessionManager.getUserStore().getStore_id());
            }

            param.put("customer_id", sessionManager.getDataMember().getId());
            param.put("moodtrack_id", sessionManager.getCurrentMoodTrack());
            param.put("track_id", trackId);
            param.put("album_id", albumId);
            param.put("artist_id", artistId);
            param.put("release_year", releaseYear);
            param.put("genre_id", genreId);
            param.put("subscribe_code", sessionManager.getPackageCode());
            param.put("duration", String.valueOf(sessionManager.getCurrentDuration()));
            param.put("publisher_id", publisheId);

            lyricksPresenter.insertLogReport(param);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void insertLogAds(String id, String trackId) {
        Map<String, String> param = new HashMap<>();
        try {


            if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
                param.put("store_id", sessionManager.getUserStore().getStore_id());
            } else {
                param.put("customer_id", sessionManager.getDataMember().getId());
            }

            param.put("moodtrack_id", sessionManager.getCurrentMoodTrack());
            param.put("track_id", trackId);
            param.put("ads_id", id);

            lyricksPresenter.insertLogAds(param);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showAdsList(List<DataAds> dataAdsList, boolean isContinue, boolean isPrevious, int between) {
        //jika lebih dari 2 maka ada iklan diantara lagu
        if (between == 1) {
            DataAds ads = new DataAds();
            if (dataAdsList.size() > 1) {
                ads = dataAdsList.get(dataAdsList.size() - 1);
            } else {
                ads = dataAdsList.get(0);
            }
            JcAudio audio = JcAudio.createFromURL("-1","1", ads.getName(),
                    Constants.URL_AUDIO_ADS + ads.getSource(),
                    Constants.URL_IMAGE_ADS + ads.getImage(), ads.getId(), "Adsense");

            //validasi apakah moodtrack baru di mulai atau next track
            if (isContinue || isPrevious) {
                jcPlayerView.playAudio(audio);
            } else {
                sessionManager.setPrevTracksId(String.valueOf(jcPlayerView.getMyPlaylist().get(position).getAudioId()));
                jcPlayerView.playAudio(audio);
                jcPlayerView.createNotification(audio.getCover());
            }

            sessionManager.setCurrentTracksId(ads.getId());
            jcPlayerView.setIsBetweenAds(true);
            //remove Last Ads and submit
            dataAdsList.remove(dataAdsList.size() - 1);

        } else {
            if (isContinue) {
                jcPlayerView.nextAudio(waitingNext);
            } else if (isPrevious) {
                jcPlayerView.previous(waitingPrevious);
            } else {
                jcPlayerView.playAudio(jcPlayerView.getMyPlaylist().get(position));
                jcPlayerView.createNotification(jcPlayerView.getMyPlaylist().get(position).getCover());
            }
            jcPlayerView.setIsBetweenAds(false);
            waitingNext = 0;
            waitingPrevious = 0;
            jcPlayerView.initAds(dataAdsList);
        }
    }

    @Override
    public void showErrorAdsList(String error, boolean isContinue,boolean isPrevious) {
        if (isContinue) {
            jcPlayerView.nextAudio(waitingNext);
        } else if (isPrevious){
            jcPlayerView.previous(waitingPrevious);
        } else {
            jcPlayerView.playAudio(jcPlayerView.getMyPlaylist().get(position));
            jcPlayerView.createNotification(jcPlayerView.getMyPlaylist().get(position).getCover());
        }
        waitingNext = 0;
        waitingPrevious = 0;
    }

    @Override
    public void onFinishedInitPlaylist(List<JcAudio> jcAudios) {
        initPlaylistAsync.cancel(true);
        jcPlayerView.registerServiceListener(this);
        jcPlayerView.registerInvalidPathListener(this);
        jcPlayerView.registerStatusListener(this);


        //                System.out.println("===== seconds remaining ===== " + millisUntilFinished / 1000);
        //            }
        //        }
        CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
//                System.out.println("===== seconds remaining ===== " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                if (jcPlayerView.getWaitBetweenAds() < 1) {
                    adsPresenter.getAds(1, false, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                } else {
                    adsPresenter.getAds(0, false, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                }

                if (!TextUtils.isEmpty(isKaraoke)) {
                    if (isKaraoke.equals("1")) {
                        btnKaraoke.setVisibility(View.VISIBLE);
                    } else if (isKaraoke.equals("0")) {
                        btnKaraoke.setVisibility(View.GONE);
                    }
                }
//            }
//        }
            }
        }.start();
    }

    public void logout(String idMood, String storeId, String status) {

        CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        sessionManager.logout();
        Intent intent = new Intent(this, StoreLoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("moodtrack_id", idMood);
        intent.putExtra("store_id", storeId);
        intent.putExtra("status", status);
        startActivity(intent);
        finish();

    }

    private void intentHandler(Intent intent) {
        if (intent.getExtras() != null) {

            String idMood = intent.getStringExtra("moodtrack_id");
            String status = intent.getStringExtra("status");
            String idStore = intent.getStringExtra("store_id");
            boolean repeat = intent.getBooleanExtra("repeat",false);
            boolean isControl = intent.getBooleanExtra("is_control",false);
            Log.e("control", String.valueOf(isControl));
            int requestCode = intent.getIntExtra("request_code",0);

            if (!sessionManager.isLoggedIn()) {
                logout(idMood, idStore, status);
            }

            if (!repeat) {
                Map<String,String> params = new HashMap<>();
                params.put("id",String.valueOf(requestCode));
                schedulesPresenter.removeFromSchedule(params);
            }

            if (sessionManager.getUserType().equals(Constants.UserType.USER_STORE)) {
                if (sessionManager.getUserStore().getStore_id().equals(idStore) && !Objects.requireNonNull(idMood).isEmpty()) {
                    switch (Objects.requireNonNull(status)) {
                        case Constants.MoodtrackControl.PLAY:
                            Map<String, String> params = new HashMap<>();

                            try {
                                params.put("id", idMood);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            detailMoodtrackPresenter.getData(params);

                            break;
                        case Constants.MoodtrackControl.PAUSE:
                            if (jcPlayerView != null) {
                                if (jcPlayerView.isPlaying() && jcPlayerView.getMyPlaylist().size() > 0) {
                                    updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.PAUSE);

                                    jcPlayerView.pause();
                                }
                            }
                            break;
                        case Constants.MoodtrackControl.CONTINUE:
                            if (jcPlayerView != null) {
                                if (!jcPlayerView.isPlaying() && jcPlayerView.getMyPlaylist().size() > 0) {
                                    updateIsPlaying(Constants.IsAppRun.RUN, Constants.MoodtrackControl.CONTINUE);
                                    jcPlayerView.continueAudio();
                                }
                            }
                            break;
                        case Constants.MoodtrackControl.SHUFFLE:
                            if (jcPlayerView != null) {
                                if (jcPlayerView.isShuffle()) {
                                    if (jcPlayerView.setShuffle(false)) {
                                        updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                                                : Constants.MoodtrackControl.PAUSE);
                                        ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.white));
                                    }
                                } else {
                                    if (jcPlayerView.setShuffle(true)) {
                                        updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                                                : Constants.MoodtrackControl.PAUSE);
                                        ivShuffle.setBackgroundTintList(getResources().getColorStateList(R.color.colorAccentSec));
                                    }

                                }
                            }
                            break;
                        case Constants.MoodtrackControl.NEXT:
                            if (jcPlayerView != null) {
                                    if (isControl) {
                                        if ( jcPlayerView.getMyPlaylist().size()-2 >
                                                jcPlayerView.getMyPlaylist().
                                                indexOf(jcPlayerView.getCurrentAudio()) +
                                                waitingNext) {
                                            waitingNext = waitingNext+1;
                                            waitingPrevious = 0;
                                        }
                                    } else {
                                        waitingPrevious = 0;
                                        waitingNext = 0;

                                        if (jcPlayerView.getWaitBetweenAds() < 1) {
                                            adsPresenter.getAds(1, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                                            insertLogReport();
                                        } else {
                                            sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                                            insertLogReport();

                                            adsPresenter.getAds(0, true, false, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                                        }
                                    }
//                                    jcPlayerView.nextAudio();
                            }
                            updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                                    : Constants.MoodtrackControl.PAUSE);
                            break;
                        case Constants.MoodtrackControl.PREVIOUS:
                            if (jcPlayerView != null) {
                                if (isControl) {
                                    if ( 1 < jcPlayerView.getMyPlaylist().
                                                    indexOf(jcPlayerView.getCurrentAudio()) -
                                            waitingPrevious) {
                                        waitingPrevious = waitingPrevious+1;
                                        waitingNext = 0;
                                    }
                                } else {
                                    waitingPrevious = 0;
                                    waitingNext = 0;
                                    if (jcPlayerView.getWaitBetweenAds() < 1) {
                                        adsPresenter.getAds(1, false, true, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                                        insertLogReport();
                                    } else {
                                        sessionManager.setPrevTracksId(sessionManager.getCurrentTracksId());

                                        insertLogReport();

                                        adsPresenter.getAds(0, false, true, sessionManager.getStore().getTypeBlock(),sessionManager.getUserStore().getStore_id());
                                    }
                                }
//                                    jcPlayerView.previous();
                            }
                            updateIsPlaying(Constants.IsAppRun.RUN, jcPlayerView.isPlaying() ? Constants.MoodtrackControl.CONTINUE
                                    : Constants.MoodtrackControl.PAUSE);
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void showMoodtracks(Moodtrack dataMoodtracks) {
        if (!dataMoodtracks.getStoreId().equals(sessionManager.getUserStore().getStore_id())) {
            if (dataMoodtracks.getPublicPlaylist().equals("1")) {
                sessionManager.setCurrentMoodTrack(dataMoodtracks.getId());
                playingMoodtrackControl(dataMoodtracks.getDataTracks());
            }
        } else {
            sessionManager.setCurrentMoodTrack(dataMoodtracks.getId());
            playingMoodtrackControl(dataMoodtracks.getDataTracks());
        }
    }

    @Override
    public void showErrorMoodtracks(String error) {

    }

    @Override
    public void showSuccessUpdatePosition(String success) {

    }

    @Override
    public void showErrorUpdatePosition(String error) {

    }

    @Override
    public int getNumberOfRootFragments() {
        return getPrimaryMenuList().size();
    }

    @Override
    public void showSchedules(List<DataSchedules> dataSchedules) {

    }

    @Override
    public void showErrorSchedules(String error) {

    }

    @Override
    public void showSuccessRemoveSchedule(String dataSchedules) {

    }

    @Override
    public void showErrorRemoveSchedule(String error) {

    }

    @Override
    public void showSuccessSchedule(DataResult success) {

    }

    @Override
    public void showErrorSchedule(String error) {

    }
}
