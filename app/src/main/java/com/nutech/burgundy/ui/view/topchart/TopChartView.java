package com.nutech.burgundy.ui.view.topchart;

import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

/**
 * @author Dafian on 10/4/17
 */

public interface TopChartView extends BaseView {

    void showTopChart(List<DataTracks> tracks);

    void showError(String error);
}
