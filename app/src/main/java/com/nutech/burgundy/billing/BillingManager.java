/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.nutech.burgundy.billing;

import android.app.Activity;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.presenter.DetailPricePresenter;
import com.nutech.burgundy.ui.view.vip.DetailPriceActivity;
import com.nutech.burgundy.utils.Helper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * BillingManager that handles all the interactions with Play Store
 * (via Billing library), maintain connection to it through BillingClient and cache
 * temporary states/data if needed.
 */
public class BillingManager implements PurchasesUpdatedListener {
    private static final String TAG = "BillingManager";

    private final BillingClient mBillingClient;
    private final Activity mActivity;
    private final DetailPricePresenter presenter;
    private final SessionManager sessionManager;
    private final DataSubscribePrice dataSubscribePrice;
    private final String price;

    private List<SkuDetails> listSKUDetails;

    public BillingManager(DetailPriceActivity activity, DetailPricePresenter presenter, SessionManager sessionManager,
                          DataSubscribePrice subscribePrice, String price) {
        mActivity = activity;
        mBillingClient = BillingClient.newBuilder(mActivity).setListener(this).build();
        this.presenter = presenter;
        this.sessionManager = sessionManager;
        this.dataSubscribePrice = subscribePrice;
        this.price = price;
        startServiceConnectionIfNeeded(null);
    }

    @Override
    public void onPurchasesUpdated(int responseCode, List<Purchase> purchases) {
        Log.i(TAG, "onPurchasesUpdated() response: " + responseCode);
        getSKUDetails();
        String orderID = "";
        String pckgName = "";
        String purchaseToken = "";
        String signature = "";
        String sku = "";
        long date = 0;
//        String price = "";
        Date currentDate = null;

        if (responseCode != 7 || responseCode != 1) {
            for (int i = 0; i < purchases.size(); i++) {

                orderID = purchases.get(i).getOrderId();
                pckgName = purchases.get(i).getPackageName();
                purchaseToken = purchases.get(i).getPurchaseToken();
                signature = purchases.get(i).getSignature();
                sku = purchases.get(i).getSku();
                date = purchases.get(i).getPurchaseTime();

                System.out.println("Original Json = "+purchases.get(i).getOriginalJson());
            }

            currentDate = new Date(date);

//            for (int i = 0; i <listSKUDetails.size(); i++) {
//
//                if (sku.equals(listSKUDetails.get(i).getSku())) {
//                    price = listSKUDetails.get(i).getPrice();
//                }
//            }

            subscribeCustomer(orderID, pckgName, purchaseToken, signature, sku, currentDate, price);

        }



        System.out.println("Response Purchase "+"\n"+
                "Order ID: " + "\n" +
                orderID + "\n" +
                "Package Name: " + "\n" +
                pckgName + "\n" +
                "purchase Token: " + "\n" +
                purchaseToken + "\n" +
                "Signature: " + "\n" +
                signature + "\n" +
                "SKU: " + "\n" +
                sku + "\n" +
                "Date: " + "\n" +
                currentDate);
    }

    /**
     * Trying to restart service connection if it's needed or just execute a request.
     * <p>Note: It's just a primitive example - it's up to you to implement a real retry-policy.</p>
     *
     * @param executeOnSuccess This runnable will be executed once the connection to the Billing
     *                         service is restored.
     */
    private void startServiceConnectionIfNeeded(final Runnable executeOnSuccess) {
        if (mBillingClient.isReady()) {
            if (executeOnSuccess != null) {
                executeOnSuccess.run();
            }
        } else {
            mBillingClient.startConnection(new BillingClientStateListener() {
                @Override
                public void onBillingSetupFinished(@BillingResponse int billingResponse) {
                    if (billingResponse == BillingResponse.OK) {
                        Log.i(TAG, "onBillingSetupFinished() response: " + billingResponse);
                        if (executeOnSuccess != null) {
                            executeOnSuccess.run();
                        }
                    } else {
                        Log.w(TAG, "onBillingSetupFinished() error code: " + billingResponse);
                    }
                }

                @Override
                public void onBillingServiceDisconnected() {
                    Log.w(TAG, "onBillingServiceDisconnected()");
                }
            });
        }
    }

    public void querySkuDetailsAsync(@BillingClient.SkuType final String itemType,
                                     final List<String> skuList, final SkuDetailsResponseListener listener) {
        // Specify a runnable to start when connection to Billing client is established
        Runnable executeOnConnectedService = new Runnable() {
            @Override
            public void run() {
                SkuDetailsParams skuDetailsParams = SkuDetailsParams.newBuilder()
                        .setSkusList(skuList).setType(itemType).build();
                mBillingClient.querySkuDetailsAsync(skuDetailsParams,
                        new SkuDetailsResponseListener() {
                            @Override
                            public void onSkuDetailsResponse(int responseCode,
                                                             List<SkuDetails> skuDetailsList) {
                                listener.onSkuDetailsResponse(responseCode, skuDetailsList);

                            }
                        });
            }
        };

        // If Billing client was disconnected, we retry 1 time and if success, execute the query
        startServiceConnectionIfNeeded(executeOnConnectedService);
    }

    public void startPurchaseFlow(final String skuId, final String billingType) {
        // Specify a runnable to start when connection to Billing client is established
        Runnable executeOnConnectedService = new Runnable() {
            @Override
            public void run() {
                BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                        .setType(billingType)
                        .setSku(skuId)
                        .build();
                mBillingClient.launchBillingFlow(mActivity, billingFlowParams);
            }
        };

        // If Billing client was disconnected, we retry 1 time and if success, execute the query
        startServiceConnectionIfNeeded(executeOnConnectedService);
    }

    public void destroy() {
        mBillingClient.endConnection();
    }

    public void subscribeCustomer(String orderID, String pckgName, String purchaseToken,
                                  String signature, String sku, Date currentDate, String price) {

        String customerId = sessionManager.getDataMember().getId();

        Map<String, String> fields = new HashMap<>();
        fields.put("subscribe_id", sku);
        fields.put("customer_id", customerId);
        fields.put("pay_with", "0");
        fields.put("pay_amount", price);
        fields.put("pay_date", Helper.todayDate());
        fields.put("payment_status", "1");
        fields.put("payment_type", Constants.PAYTIPE.GOOGLE);
        fields.put("order_id", orderID);
        fields.put("order_date", Helper.formatOrderDate(currentDate));

        System.out.println("Param : "+fields);

        presenter.subscribeCustomer(fields);
    }

    private void getSKUDetails() {
        SkuDetailsResponseListener responseListener = new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode,
                                             List<SkuDetails> skuDetailsList) {
                // If we successfully got SKUs, add a header in front of it
                if (responseCode == BillingResponse.OK && skuDetailsList != null) {
                    // Repacking the result for an adapter
                    for (SkuDetails details : skuDetailsList) {
                        Log.e(TAG, "Found sku: " + details);
                        listSKUDetails = skuDetailsList;

                        System.out.println("listSKUDetails = "+ listSKUDetails.size());
                    }
                }
            }
        };
    }
}
