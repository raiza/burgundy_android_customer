package com.nutech.burgundy.config;

import android.Manifest;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;

import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.lib.imageviewers.PhotoView;

import java.net.URI;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Arif-Setiawan on 8/28/17.
 */

public class Constants {

    public static final String SERVER = "http://172.104.55.45/"; //Nutech Server Dev
//    public static final String SERVER = "http://172.105.114.34/"; //Nutech Server Live
//    public static final String SERVER = "http://10.20.51.240/"; //LOCAL
    public static final String SERVER_ANT_MEDIA = "http://122.129.112.185:5080/";
    public static final String URL_RTMP = "rtmp://122.129.112.185/LiveApp/";
    public static final String URL_RTMP_ENDED =  SERVER_ANT_MEDIA + "LiveApp/streams/";
    public static final String URL_DANGDUT_ASIK = SERVER + "burgundy_ws/index.php/api/";
    public static final String URL_MELON = "https://payment.upoint.co.id/common/v1/";
    public static final String URL_IMAGE_MEMBER = SERVER + "burgundy_ws/assets/uploads/customers/";
    public static final String URL_IMAGE_ARTISTS = SERVER + "burgundy_manager/assets/uploads/artists/";
    public static final String URL_IMAGE_PLAYLIST = SERVER + "burgundy_manager/assets/uploads/playlists/";
    public static final String URL_IMAGE_GENRES = SERVER + "burgundy_manager/assets/uploads/genres/";
    public static final String URL_IMAGE_ADS = SERVER + "burgundy_manager/assets/uploads/ads/image/";
    public static final String URL_AUDIO_ADS = SERVER + "burgundy_manager/assets/uploads/ads/audio/";
    public static final String URL_IMAGE_ALBUMS = SERVER + "burgundy_manager/assets/uploads/albums/";
    public static final String URL_IMAGE_LOGO = SERVER + "burgundy_manager/assets/images/";
    public static final String URL_STORE_LOGO = SERVER + "burgundy_manager/assets/uploads/stores/";
    public static final String URL_IMAGE_PLAYLIST_WS = SERVER + "burgundy_ws/assets/uploads/playlists/";
    public static final String URL_IMAGE_PROMOTIONS = SERVER + "burgundy_manager/assets/uploads/promotions/";
    public static final String URL_IMAGE_OFFICIAL = SERVER + "burgundy_manager/assets/uploads/medsos/";
    public static final String URL_VIDEO_KARAOKE = SERVER + "burgundy_manager/assets/uploads/rooms/";
    public static final String URL_IMAGE_MOODTRACK = SERVER + "burgundy_ws/assets/uploads/soundtracks/";
    public static final String GOOGLE_PLAY = "https://play.google.com/store/apps/details?id="+ BuildConfig.APPLICATION_ID;
    public static final String CMS = SERVER+"burgundy_manager/index.php";


    public static class ParamKey {

        public static final String KEY_STATE_ACTION_UPDATE = "key_update";
        public static final String KEY_STATE_ACTION_DELETE = "key_delete";
        public static final String KEY_STATE_ACTION_ADD_TRACK = "key_add_track";
        public static final String KEY_DETAIL_PLAYLIST_FROM_MYMUSIC = "mymusic";
        public static final String KEY_DETAIL_PLAYLIST_FROM_PLAYLIST = "playlist";
        public static final String KEY_REGISTER = "register";
        public static final String KEY_LOGIN = "login";
        public static final String ARTIST_TRACK = "artist_track";
        public static final String ARTIST_ALBUM = "artist_album";
    }

    public static class UserAuth {
        public static final String USER = "burgundy";
        public static final String PASSWORD = "8iPIfWvY8w2pbCubCV4y";
    }

    public static class MoodtrackControl {
        public static final String PLAY = "play";
        public static final String PAUSE = "pause";
        public static final String PREVIOUS = "prev";
        public static final String NEXT = "next";
        public static final String CONTINUE = "continue";
        public static final String STOP = "stop";
        public static final String SHUFFLE = "shuffle";
    }

    public static class Key {

        public static String KEY_RSA = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu303dfjnsEgXgHoHc3OfbLUlgWIt7t2Kv9z/aXieRCZV2xEmsLnwOTgj9L1mppGvxMYhjrFss+KpfVzMM8rTjTo6eIWSvByypSps+sux2J+oz+lxlWG3jX1fUY0/7D7vT1bK3Y5a18T9J56TWBRf3cgACl974kUAVoc00LKn/HIFtxt41jsGBaFQxIMcwxPEmiepGkE0PbyQQr2CnBOlXfXYk096+XzE6UIBRjCJVq88/4o3+1R22JC2gQXwk+hgaxSnAWGMjrsCAIryMKnMWNdq64tQYnTDpCvs5cpPffzAB/Mwno4sQmWxKxaCN3eHCNTu3X4n9lB153KdibAmHwIDAQAB";
        public static String MERCHANT_ID = "15015013286811421178";
        public static String FACEBOOK_ID = "1515465951884854";
        public static String NOTIF_CHANNEL_ID = "Burgundy_apps";
        public static class CHANNEL_ID {
            private final static AtomicInteger c = new AtomicInteger(0);
            public static int getID() {
                return c.incrementAndGet();
            }
        }
        public static String REG_TYPE_WAP = "WAP";
        public static String REG_TYPE_APP = "APP";
        public static String FCM_SENDERID = "31186874094";
        public static String ADD_MOOD = "addmood";
        public static String EDIT_MOOD = "editmood";
        public static String URI_DEEP_LINK = "nutech://burgundy/playMoodtrack";
    }

    public static class InitPlayistAsyncStatus {
        public static String PLAY = "play";
        public static String SORT = "sort";
    }

    public static class RequestCode {
        public static int PHONE_PRIMARY = 100;
        public static int GPS_PRIMARY = 101;
        public static int GPS_TRACKER = 103;
        public static int GPS_NEAR_STORE = 104;
        public static int GPS_NEAR_STORE_PLAYING = 105;
        public static int ON_GPS_DISCOVER = 102;
    }

    public static class UserType {
        public static  String USER_STORE = "store";
        public static  String USER_PUBLIC = "customer";
    }

    public static class StoreMemberRole {
        public static String SUPER_ADMIN = "ROLSAD";
        public static String ADMIN = "ROLADM";
    }

    public static  class IsPlaying {
        public static String PLAYING = "1";
        public static String NOT_PLAYING = "0";
    }

    public static  class IsShuffle {
        public static String ON = "1";
        public static String OFF = "0";
    }

    public static  class IsAppRun {
        public static String RUN = "1";
        public static String NOT_RUN = "0";
    }

    public static class Distance {
        public static String KM = "KM";
        public static String M = "M";
    }

    public static class ParamValue {
        public static String KEY_TRACKS_ID = "";
        public static String URLIMAGEARTIST = "";
        public static Bitmap B_IMAGE = null;
        public static PhotoView pVIEW = null;
        public static boolean isNOW_PLAYING = false;
        public static String moodId = "";
    }

    public static class ActivityPoint {
        public static String SUBSCRIBE = "1";
        public static String NEW_PLAYLIST = "2";
        public static String SEND_FEEDBACK = "3";
        public static String SHARE_SOSMED = "4";
        public static String FOLLOW_OFFICIAL = "5";
        public static String UPDATE_PROFILE = "6";
        public static String PLAY_VIDEO = "7";
        public static String LOGIN = "10";

    }

    public static class Status {
        public static String INACTIVE = "inactive";
        public static String ACTIVE = "active";
        public static String USED = "used";
        public static String LIVE_TRUE = "1";
        public static String LIVE_FALSE = "0";
        public static String SUCCESS = "1";
        public static String PENDING = "0";

        // global topic to receive app wide push notifications
        public static final String TOPIC_GLOBAL = "global";

        // broadcast receiver intent filters
        public static final String REGISTRATION_COMPLETE = "registrationComplete";
        public static final String PUSH_NOTIFICATION = "pushNotification";

        // id to handle the notification in the notification tray
        public static final int NOTIFICATION_ID = 100;
        public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    }

    public static class DefaultActivCode {
        public static String NUMBER = "1234";
        public static String ALPHA = "abcd";
        public static String REGISTER_ID = "123456789";

    }

    public static class DefaultTimer {
        public static int s60000 = 60000;
        public static String timerCountdown = "3";

    }

    public static class currentVersion {
        public static int minSDK21 = Build.VERSION_CODES.LOLLIPOP;
    }

    public static class DataRoomTemporary {
        public static DataRoom dataRoom = null;
    }

    public static class Resize {
        public static int sizeHeigth = 50;
        public static int sizeWeigth = 50;
    }

    public static class PAYTIPE {
        public static String GOOGLE = "Google Play";
        public static String MELON = "Melon Pay";
    }

    public static class PAYMENT_CHANNEL {
        public static String TELKOMSEL = "telkomsel";
        public static String TCASH = "tcash";
        public static String XL = "xl";
        public static String THREE = "three";
        public static String INDOSAT = "indosat";
        public static String SMARTFREN = "smartfren";
        public static String TELIN_OTP = "telin_otp";
        public static String DANA = "dana";
        public static String FINPAY = "finpay";
    }

    public static class CurrentFragment {

        public static String TAG_FRAGMENT = "";
    }

    public static final String[] PERMISSIONS_PHOTO = new String[] {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

}
