package com.nutech.burgundy.config;

import androidx.annotation.IntDef;

import com.google.android.gms.location.LocationRequest;

/**
 * Created by Arif Setiawan on 15/03/19.
 * Xeranta Mobile Solutions
 */
public class ConfigLocation {

    public static class Default {

        public static final int LOCATION_PRIORITY = LocationRequest
                .PRIORITY_BALANCED_POWER_ACCURACY;
        public static final float MIN_ACCURACY = 5.0f;
        private static final int SECOND = 1000;
        public static final int WAIT_PERIOD = 20 * SECOND;
        private static final int MINUTE = 60 * SECOND;
        public static final int TIME_PERIOD = 5 * MINUTE;
        public static final int LOCATION_INTERVAL = 5 * MINUTE;
        public static final int LOCATION_FASTEST_INTERVAL = MINUTE;
    }

    public static class FailType {

        public static final int PERMISSION_DENIED = 1;
        public static final int GP_SERVICES_NOT_AVAILABLE = 2;
        public static final int GP_SERVICES_CONNECTION_FAIL = 3;
        public static final int NETWORK_NOT_AVAILABLE = 4;
        public static final int TIMEOUT = 5;
        public static final int GP_SERVICES_SETTINGS_DIALOG = 6;
        public static final int GP_SERVICES_SETTINGS_DENIED = 7;
    }

    public static class LogType {

        public static final int NONE = 0;
        public static final int IMPORTANT = 1;
        public static final int GENERAL = 2;

        @IntDef({IMPORTANT, GENERAL, NONE})
        public @interface Level {

        }
    }

    public static class ProviderType {

        public static final int NONE = 0;
        public static final int GOOGLE_PLAY_SERVICES = 1;
        public static final int GPS = 2;
        public static final int NETWORK = 3;

        /**
         * Covers both GPS and NETWORK
         */
        public static final int DEFAULT_PROVIDERS = 4;

        @IntDef({NONE, GOOGLE_PLAY_SERVICES, GPS, NETWORK, DEFAULT_PROVIDERS})
        public @interface Source {

        }
    }

    public static class RequestCode {

        public static final int RUNTIME_PERMISSION = 23;
        public static final int GOOGLE_PLAY_SERVICES = 24;
        public static final int GPS_ENABLE = 25;
        public static final int SETTINGS_API = 26;
    }
}
