package com.nutech.burgundy.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.lib.schedulerMoodtrack.SchedulerMoodtrackReceiver;
import com.pddstudio.urlshortener.URLShortener;
import com.roger.gifloadinglibrary.GifLoadingView;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class Helper {

    private static final String PREF_NAME = "dangdut_asik_pref_name";
    private static String shortUrlResult = "";

    public static SharedPreferences getDefaultPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, 0);
    }

    public static void hideSoftKeyBoard(Activity context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void startAnimProgress(AVLoadingIndicatorView loadingIndicatorView) {
        loadingIndicatorView.setVisibility(View.VISIBLE);
        loadingIndicatorView.smoothToShow();
    }

    public static void stopAnimProgress(AVLoadingIndicatorView loadingIndicatorView) {
        loadingIndicatorView.setVisibility(View.GONE);
        loadingIndicatorView.smoothToHide();
    }

    public static String todayDate() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.getDefault());
        return format.format(today);
    }

    public static String todayDateFull() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return format.format(today);
    }

    public static String formatedDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd", Locale.getDefault());
        Date today = null;
        try {
            today = format.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format.format(today);
    }

    public static String getDistance(String distance) {
        String distanceName = "";
        DecimalFormat df2digit = new DecimalFormat("#.##");
        DecimalFormat df0digit = new DecimalFormat("#");
        BigDecimal distanceDecimal = new BigDecimal(distance);
        BigDecimal distanceDecimal1 = new BigDecimal(distance)
                .add(new BigDecimal(1000.0));//di kali 1000

        if (distanceDecimal.compareTo(BigDecimal.ZERO) > 0) {
            distanceName = df2digit.format(distanceDecimal) + " " + Constants.Distance.KM;
        } else {
            distanceName = distanceDecimal1 + " " + Constants.Distance.M;
        }

        return distanceName;
    }

    public static String formatedMonth(String pDate) {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
        Date date = null;
        try {
            date = format1.parse(pDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format2.format(date);
    }

    public static String formatedEndDate(String endDate) {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm dd MMM yyyy");
        Date date = null;
        try {
            date = format1.parse(endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format2.format(date);
    }

    public static String formatedEndDateBorn(String dateBorn) {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
        Date date = null;
        try {
            date = format1.parse(dateBorn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format2.format(date);
    }

    public static String formatedEndDateSubs(String endDate) {

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
        Date date = null;
        try {
            date = format1.parse(endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format2.format(date);
    }

    public static String dateDifferentSubs(String startDate, String endDate) {

        //HH converts hour in 24 hours format (0-23), day calculation
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;

        long diffSeconds = 0;
        long diffMinutes = 0;
        long diffHours = 0;
        long diffDays = 0;

        try {
            d1 = format.parse(startDate);
            d2 = format.parse(endDate);

            //in milliseconds
            long diff = d2.getTime() - d1.getTime();

            diffSeconds = diff / 1000 % 60;
            diffMinutes = diff / (60 * 1000) % 60;
            diffHours = diff / (60 * 60 * 1000) % 24;
            diffDays = diff / (24 * 60 * 60 * 1000);

            Log.e("days", String.valueOf(diffDays));
            Log.e("hours", String.valueOf(diffHours));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(diffDays)+" Hari";
    }

    public static String remainSubscribeTime(String days, String hours) {

        String remainingTime;
        Double dResult;

        if (days.equalsIgnoreCase("0")) {
            Double dHours = Double.parseDouble(hours);
            dResult = ((dHours / 24) - 0) * 24;

        } else {
            Double dDays = Double.parseDouble(days);
            Double dHours = Double.parseDouble(hours);
            dResult = ((dHours / 24) - dDays) * 24;
        }

        Long lHours = Math.round(dResult);

        remainingTime = days + " Hari " + lHours.intValue() + " Jam";

        return remainingTime;
    }

    public static String formatedPrice(String price) {

        String resultPrice = "";

        if (price.contains(".")) {
            int endIndex = price.lastIndexOf(".");
            if (endIndex != -1) {
                String newstr = price.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
                Double prices = Double.parseDouble(newstr);
                String formattedPrice = new DecimalFormat("###,###,###,###.###").format(prices);
                resultPrice = formattedPrice.replace(",", ".");

            } else {
                Double prices = Double.parseDouble(price.toString());
                String formattedPrice = new DecimalFormat("###,###,###,###.###").format(prices);
                resultPrice = formattedPrice.replace(",", ".");

            }

        } else {

            Double prices = Double.parseDouble(price.toString());
            String formattedPrice = new DecimalFormat("###,###,###,###.###").format(prices);
            resultPrice = formattedPrice.replace(",", ".");
        }


        return resultPrice;
    }

    public static void shareContent(Context applicationContext, String content, String title,
                                    String customerId, String shareSosmed) {

        shortUrl(applicationContext, content, title, customerId, shareSosmed);

    }

    public static void shortUrl(final Context applicationContext, final String content,
                                final String title, final String customerId, final String shareSosmed) {
        String longUrl = Constants.GOOGLE_PLAY;
        URLShortener.shortUrl(longUrl, new URLShortener.LoadingCallback() {
            @Override
            public void startedLoading() {
            }

            @Override
            public void finishedLoading(@Nullable String shortUrl) {
                //make sure the string is not null
                if (shortUrl != null) shortUrlResult = shortUrl;
                else shortUrlResult = Constants.GOOGLE_PLAY;

                postUpdatePoint(applicationContext, customerId, shareSosmed);
                share(applicationContext, content, title);
            }
        });
    }

    public static void share(Context applicationContext, String content, String title) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TITLE, title);
        intent.putExtra(Intent.EXTRA_SUBJECT, content);
        intent.putExtra(Intent.EXTRA_TEXT, shortUrlResult);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        applicationContext.startActivity(Intent.createChooser(intent, applicationContext.getResources().getString(R.string.label_share)));
    }

    public static String getOpenFacebook(Context context, String url) {

        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + url;
            } else { //older versions of fb app
                return "fb://page/" + url;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return url; //normal web url
        }
    }

    public static void getOpenInstagram(Context context, String url) {

        try {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

            intent.setPackage("com.instagram.android");
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void getOpenYoutube(Context context, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            context.startActivity(intent);
        } catch (Exception e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    public static void getOpenTwitter(Context context, String url) {

        Intent intent = null;
        try {
            // get the Twitter app if possible
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } catch (Exception e) {
            // no Twitter app, revert to browser
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        }
        context.startActivity(intent);
    }

    public static String convertToBase64(Bitmap bitmap) {

        String baseImage = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        baseImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return baseImage;
    }

    public static String generateCode() {

        String result = "";

        //generate a 4 digit integer 1000 <10000
        int randomPIN = (int) (Math.random() * 900000) + 100000;
        result = String.valueOf(randomPIN);

        return result;
    }

    @NonNull
    public static String getDurationString(int seconds) {

        if (seconds < 0 || seconds > 2000000)
            seconds = 0;
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        if (hours == 0) {
            return twoDigitString(minutes) + " : " + twoDigitString(seconds);
        } else {
            return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
        }
    }

    @NonNull
    private static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    public static void showToastNetworkError(Context context) {
        Toast.makeText(context, context.getString(R.string.msg_network_error), Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("HardwareIds")
    public static String getIMEI(Activity activity, int slot) {
        String serialNumber = "";
        TelephonyManager telephonyManager = (TelephonyManager) activity
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

        }
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            serialNumber = telephonyManager.getDeviceId(slot);
            return serialNumber;
        } if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
            serialNumber = Build.getSerial();
            return serialNumber;
        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            SubscriptionManager sm = SubscriptionManager.from(activity.getApplicationContext());
            List<SubscriptionInfo> sis = sm.getActiveSubscriptionInfoList();
            SubscriptionInfo si = sis.get(0);
            serialNumber = si.getIccId();;
            return serialNumber;
        } else {
            serialNumber = "unknown";
            return serialNumber;
        }
    }

    public static String getDeviceUniqueID(Activity activity){
        String device_unique_id = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }

    public static boolean isWiredHeadsetOn(Context context){
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.isWiredHeadsetOn();

        return audioManager.isWiredHeadsetOn();
    }

    public static int mod(int x, int y){
        int result = x % y;
        if (result < 0)
            result += y;

        return result;
    }

    public static String formatOrderDate(Date curDate){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String DateToStr = format.format(curDate);
        System.out.println(DateToStr);

        return DateToStr;
    }

    static void postUpdatePoint(Context applicationContext, final String customer_id, final String activity_id) {
        String sURL = "http://122.129.112.185:8081/burgundy_ws/index.php/api/point/updatePointCustomer";
        RequestQueue queue = Volley.newRequestQueue(applicationContext);
        StringRequest postRequest = new StringRequest(Request.Method.POST, sURL,
                response -> {
                    // response
                    Log.d("Response", response);
                },
                error -> {
                    // error
                    Log.d("Error.Response", error.getMessage());
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("customer_id", customer_id);
                params.put("activity_id", activity_id);
                params.put("activity_date", Helper.todayDate());

                return params;
            }
        };
        queue.add(postRequest);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static int getDp(int myPixels){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        return (int) TypedValue.applyDimension( TypedValue.COMPLEX_UNIT_DIP, myPixels, displaymetrics );
    }

    public static void showLoading(FragmentManager fragmentManager, GifLoadingView mGifLoadingView) {
        mGifLoadingView.setImageResource(R.drawable.num2);
        mGifLoadingView.show(fragmentManager, "");
        mGifLoadingView.setRadius(50);
    }

    public static void dismissLoading(GifLoadingView mGifLoadingView) {
        mGifLoadingView.dismiss();
    }

    public static String convertStringArrayToString(List<String> strArr, String delimiter) {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr)
            sb.append(str).append(delimiter);
        return sb.substring(0, sb.length() - 1);
    }

    @SuppressLint("BatteryLife")
    public static void setSchedulerMoodtrack(Context context, int requestCode,String storeId,String idMood, String day_of_week,String timer, boolean repeat) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar scheduleTime = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date = sdf.parse(timer);

            //mengatur tanggal schedule
            scheduleTime.set(Calendar.DAY_OF_WEEK, Integer.parseInt(day_of_week));
            scheduleTime.set(Calendar.HOUR_OF_DAY, Objects.requireNonNull(date).getHours());
            scheduleTime.set(Calendar.MINUTE, Objects.requireNonNull(date).getMinutes());
            scheduleTime.set(Calendar.SECOND, 0);

            if (currentDate.getTimeInMillis() > scheduleTime.getTimeInMillis()) {
                scheduleTime.add(Calendar.DATE,8);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.e("tanggal1",String.format("%1$tA %1$tb %1$td %1$tY at %1$tI:%1$tM %1$Tp", scheduleTime));


        //membuat intent schedule
        Intent intent = new Intent(context, SchedulerMoodtrackReceiver.class);
        intent.putExtra("moodtrack_id", idMood);
        intent.putExtra("store_id", storeId);
        intent.putExtra("status", Constants.MoodtrackControl.PLAY);
        intent.putExtra("day_of_week", day_of_week);
        intent.putExtra("repeat", repeat);
        intent.putExtra("timer",timer);
        intent.putExtra("request_code",requestCode);
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                requestCode,intent,0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Objects.requireNonNull(alarmManager).setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    scheduleTime.getTimeInMillis(), pendingIntent);
        }
    }

    public static void SetAllScheduleMoodtrack(Context context, List<DataSchedules> dataSchedules) {
        if (dataSchedules.size() > 0) {
            Log.e("masuk","loading");
            for (DataSchedules schedule:
                    dataSchedules) {
                setSchedulerMoodtrack(context,Integer.valueOf(schedule.getId()),schedule.getStoreId(),
                        schedule.getSoundtrackId(),schedule.getDayOfWeek(),schedule.getTimer(),
                        schedule.getRepeat().equals("1"));
            }
        }
    }

    @SuppressLint("BatteryLife")
    public static void cancelScheduleMoodtrack(Context context,int requestCode, String storeId,
                                               String idMood, String day_of_week, String timer, boolean repeat) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Calendar scheduleTime = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date date = sdf.parse(timer);

            scheduleTime.set(Calendar.DAY_OF_WEEK, Integer.valueOf(day_of_week));
            scheduleTime.set(Calendar.HOUR_OF_DAY, Objects.requireNonNull(date).getHours());
            scheduleTime.set(Calendar.MINUTE, Objects.requireNonNull(date).getMinutes());
            scheduleTime.set(Calendar.SECOND, 0);


            if (currentDate.getTimeInMillis() > scheduleTime.getTimeInMillis()) {
                scheduleTime.add(Calendar.DATE,1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(context, SchedulerMoodtrackReceiver.class);
        intent.putExtra("moodtrack_id", idMood);
        intent.putExtra("store_id", storeId);
        intent.putExtra("status", Constants.MoodtrackControl.PLAY);
        intent.putExtra("day_of_week", day_of_week);
        intent.putExtra("repeat", repeat);
        intent.putExtra("timer",timer);
        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,requestCode,
                intent,0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Objects.requireNonNull(alarmManager).cancel(pendingIntent);
        }
    }

}
