package com.nutech.burgundy.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.nutech.burgundy.data.SessionManager;

import retrofit2.HttpException;

/**
 * Created by Arif-Setiawan on 8/24/17.
 */

/**
 * Generic reusable network methods.
 */
public class NetworkHelper {
    /**
     * @param context to use to check for network connectivity.
     * @return true if connected, false otherwise.
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private final Context context;
    private final SessionManager sessionManager;


    public NetworkHelper(Context context, SessionManager sessionManager) {
        this.context = context;
        this.sessionManager = sessionManager;
    }

    public String getStatusError(Throwable error) {

        String errorMessage = "";

        if (error instanceof HttpException) {

            HttpException httpException = (HttpException) error;
            int errorCode = httpException.code();


            return getErrorMessage(error, errorMessage);

        } else {
            String className = error.getClass().getName();
//            errorMessage = className
//                    + "\n" + context.getString(R.string.err_server_try_again);

            return getErrorMessage(error, errorMessage);
        }
    }

    private String getErrorMessage(Throwable error, String errorMessage) {

//        if (sessionManager.isEnableErrorMessage()) {
//            return error.getLocalizedMessage();
//
//        } else {
//            return errorMessage;
//        }
//    }
        return errorMessage;
    }

    public String getStatusCode(Throwable error) {

        if (error instanceof HttpException) {

            HttpException httpException = (HttpException) error;
            int errorCode = httpException.code();

            return String.valueOf(errorCode);

        } else {

            int errorCode;
            try {
                HttpException httpException = (HttpException) error;
                errorCode = httpException.response().code();
            } catch (Exception e) {
                errorCode = 200;
            }


            return String.valueOf(errorCode);
        }
    }
}
