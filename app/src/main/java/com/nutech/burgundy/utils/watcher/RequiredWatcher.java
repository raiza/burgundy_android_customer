package com.nutech.burgundy.utils.watcher;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputLayout;
import com.nutech.burgundy.R;

public class RequiredWatcher implements TextWatcher {

    private Context context;
    private TextInputLayout inputLayout;

    public RequiredWatcher(Context context, TextInputLayout inputLayout) {
        this.context = context;
        this.inputLayout = inputLayout;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        inputLayout.setError(null);
        inputLayout.setErrorEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable s) {

        if (TextUtils.isEmpty(s)) {
            inputLayout.setError(context.getString(R.string.err_field_required));
            inputLayout.setErrorEnabled(true);
        }
    }
}
