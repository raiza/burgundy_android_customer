package com.nutech.burgundy.utils.watcher;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;

import com.google.android.material.textfield.TextInputLayout;

public class EmailWatcher implements TextWatcher {

    private TextInputLayout inputLayout;

    public EmailWatcher(TextInputLayout inputLayout) {
        this.inputLayout = inputLayout;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if (s.length() < 3) {
            return;
        }

        if (!isValidEmail(s)) {
            inputLayout.setError("Not Valid Email");
            inputLayout.setErrorEnabled(true);
        } else {
            inputLayout.setError(null);
            inputLayout.setErrorEnabled(false);
        }
    }

    private boolean isValidEmail(CharSequence sequence) {
        return !TextUtils.isEmpty(sequence)
                && Patterns.EMAIL_ADDRESS.matcher(sequence).matches();
    }
}
