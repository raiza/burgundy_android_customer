package com.nutech.burgundy.utils.paging;

/**
 * Created by Arif Setiawan on 12/11/18.
 * Xeranta Mobile Solutions
 */
public interface PaginationAdapterCallback {
    void retryPageLoad();
}
