package com.nutech.burgundy.utils.paging;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Arif Setiawan on 12/11/18.
 * Xeranta Mobile Solutions
 */
public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

        LinearLayoutManager layoutManager;

/**
 * Supporting only LinearLayoutManager for now.
 *
 * @param layoutManager
 */
public PaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        }

@Override
public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

        if (!isLoading() && !isLastPage()) {
        if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
        && firstVisibleItemPosition >= 0) {
        loadMoreItems();
        }
        }

        }

protected abstract void loadMoreItems();

public abstract int getTotalPageCount();

public abstract boolean isLastPage();

public abstract boolean isLoading();

        }
