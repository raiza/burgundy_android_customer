package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataCheckDevice;
import com.nutech.burgundy.data.model.DataTimer;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.splash.SplashCustomActivity;
import com.nutech.burgundy.ui.view.splash.SplashView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 12/4/17.
 * Xeranta Mobile Solutions
 */

public class SplashPresenter extends BasePresenter<SplashView> {

    private Subscription subscription;
    private Context context;
    private SplashCustomActivity activity;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager session;

    public SplashPresenter(DataManager dataManager, NetworkHelper networkHelper,
                           SessionManager sessionManager, SplashCustomActivity splashCustomActivity) {
        this.manager = dataManager;
        this.helper = networkHelper;
        this.session = sessionManager;
        this.activity = splashCustomActivity;
    }

    @Override
    public void attachView(SplashView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getData(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                checkDevice(params),
                getTimer()
        );
    }

    public void getDataStore(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                checkDeviceStore(params)
        );
    }

    public Subscription checkDevice(Map<String, String> params) {
        return getDevice(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataCheckDevice -> {
                    if (isViewAttached()) {
                        getView().success(dataCheckDevice);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().error(helper.getStatusError(throwable));
                    }
                });

    }

    public Subscription checkDeviceStore(Map<String, String> params) {
        return getDeviceStore(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataCheckDevice -> {
                    if (isViewAttached()) {
                        getView().success(dataCheckDevice);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().error(helper.getStatusError(throwable));
                    }
                });

    }

    public Subscription getTimer() {
        return getPlayTimer()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataTimer>() {
                    @Override
                    public void call(DataTimer dataTimer) {
                        if (isViewAttached()) {
                            Log.e("*** timer ***", dataTimer.getTimer());
                            session.setTimer(Integer.parseInt(dataTimer.getTimer()));
//                            getView().successTimer(dataTimer);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
//                            getView().errorTimer(helper.getStatusError(throwable));
                        }
                    }
                });

    }

    @NonNull
    private Observable<DataCheckDevice> getDevice(Map<String, String> params) {
        return manager.checkDevice(params);
    }

    @NonNull
    private Observable<DataCheckDevice> getDeviceStore(Map<String, String> params) {
        return manager.checkDeviceStore(params);
    }

    @NonNull
    private Observable<DataTimer> getPlayTimer() {
        return manager.getTimer();
    }
}
