package com.nutech.burgundy.presenter;

import android.text.TextUtils;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.profile.ChangePinView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 07/05/18.
 * Xeranta Mobile Solutions
 */
public class ChangePinPresenter extends BasePresenter<ChangePinView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public ChangePinPresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(ChangePinView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void changePin(Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.changePin(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataMember>() {
                    @Override
                    public void call(DataMember dataMemberList) {
                        DataMember dataMember = new DataMember();
                        dataMember.setId(dataMemberList.getId());
                        dataMember.setName(dataMemberList.getName());
                        dataMember.setCf1(dataMemberList.getCf1());
                        dataMember.setCf2(dataMemberList.getCf2());
                        dataMember.setPhone(dataMemberList.getPhone());
                        dataMember.setMobile_phone(dataMemberList.getMobile_phone());
                        dataMember.setEmail(dataMemberList.getEmail());
                        dataMember.setBirth_date(dataMemberList.getBirth_date());
                        dataMember.setMember_card_id(dataMemberList.getMember_card_id());
                        dataMember.setJoin_date(dataMemberList.getJoin_date());
                        dataMember.setUsername(dataMemberList.getUsername());
                        dataMember.setActivation_code(dataMemberList.getActivation_code());
                        dataMember.setAddress(dataMemberList.getAddress());
                        dataMember.setCity_id(dataMemberList.getCity_id());
                        dataMember.setState_id(dataMemberList.getState_id());
                        dataMember.setLat(dataMemberList.getLat());
                        dataMember.setLng(dataMemberList.getLng());
                        dataMember.setRegister_type(dataMemberList.getRegister_type());
                        dataMember.setActive_status(dataMemberList.getActive_status());
                        dataMember.setRegister_id(dataMemberList.getRegister_id());
                        dataMember.setSubscribename(dataMemberList.getSubscribename());
                        dataMember.setImei(dataMemberList.getImei());
                        dataMember.setPoint(dataMemberList.getPoint());

                        sessionManager.setDataMember(dataMember);
                        if (!TextUtils.isEmpty(dataMemberList.getSubscribename())){
                            sessionManager.setPackage(dataMemberList.getSubscribename());
                        }
                        if (isViewAttached()) {
                            getView().changeSucess(dataMemberList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().changeError(helper.getStatusCode(throwable));
                        }
                    }
                });

    }
}
