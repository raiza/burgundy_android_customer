package com.nutech.burgundy.presenter.store;

import android.content.Context;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.stores.StoresView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class StorePresenter extends BasePresenter<StoresView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public StorePresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(StoresView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getNearStoreList(double lat,double lng) {
        if (subscription != null) subscription.unsubscribe();

        Map<String,String> paramMood = new HashMap<>();
        try {
            paramMood.put("lat",String.valueOf(lat));
            paramMood.put("lng",String.valueOf(lng));
            paramMood.put("distance","10");
        } catch (Exception e) {
            e.printStackTrace();
        }

        subscription = getNearStore(paramMood)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeList -> {
                    if (isViewAttached()) {
                        getView().ShowStores(storeList);
                    }
                },Throwable -> {
                    getView().ShowStoresError(helper.getStatusError(Throwable));
                });
    }

    private Observable<List<DataStore>> getNearStore(Map<String, String> data) {
        return manager.getNearStores(data);
    }

}
