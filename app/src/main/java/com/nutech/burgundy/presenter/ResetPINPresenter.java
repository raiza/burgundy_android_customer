package com.nutech.burgundy.presenter;

import android.content.Context;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.ResetPIN;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.register.ResetPINView;
import com.nutech.burgundy.utils.NetworkHelper;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 07/05/18.
 * Xeranta Mobile Solutions
 */
public class ResetPINPresenter extends BasePresenter<ResetPINView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager session;

    public ResetPINPresenter(DataManager dataManager, NetworkHelper networkHelper, SessionManager sessionManager) {
        this.manager = dataManager;
        this.helper = networkHelper;
        this.session = sessionManager;
    }

    @Override
    public void attachView(ResetPINView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void resetPIN(String numberPhone) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.resetPIN(numberPhone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResetPIN>() {
                    @Override
                    public void call(ResetPIN dataReset) {

                        if (isViewAttached()) {
                            getView().resetSuccess(dataReset);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().resetFailed(helper.getStatusCode(throwable));
                        }
                    }
                });

    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
//                            getView().showRegisterSuccess(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
//                            getView().showRegisterFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }
}
