package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.playlist.PlaylistView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 9/2/17.
 * Xeranta Mobile Solutions
 */

public class PlaylistPresenter extends BasePresenter<PlaylistView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public PlaylistPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(PlaylistView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void sendPlaylist(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> params = new HashMap<String, String>();
        try {
            params.put("id", data.get("playlist_id"));
        } catch (Exception e) {

        }

        subscription = new CompositeSubscription(
                addPlaylistMember(data),
                insertFavPlaylist(params)
        );
    }

    public void getPlaylistAll(String memberId, String createdby) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getPlaylist(memberId, createdby)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataPlaylist>>() {
                    @Override
                    public void call(List<DataPlaylist> dataPlaylists) {
                        if (isViewAttached()) {
                            getView().showAllPlaylist(dataPlaylists);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorAllPlaylist(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void getTracksByPLaylist(String playlistId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getTracksByPLaylistId(playlistId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataTracks>>() {
                    @Override
                    public void call(List<DataTracks> tracksList) {
                        if (isViewAttached()) {
                            getView().showTracks(tracksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });

    }

    public Subscription addPlaylistMember(Map<String, String> data) {

        return addPLaylist(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showAddSucces("Add Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void getPlaylistCustomer(String customerId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getAllPlaylistCustomer(customerId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataPlaylist>>() {
                    @Override
                    public void call(List<DataPlaylist> tracksList) {
                        if (isViewAttached()) {
                            getView().showAllPlaylist(tracksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorPlaylist(helper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void removePlaylist(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = removePlaylists(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showRemoveError("Remove Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showRemoveError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void addTracksToPlaylist(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = addTracksPlaylist(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showAddSucces("Remove Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public Subscription insertFavPlaylist(Map<String, String> data) {

        return insertFavPlaylists(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showAddSucces("Add Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateStatusPoint(String customerId, String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {

                    }
                }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                if (isViewAttached()) {

                                }
                            }
                        }
                );
    }

    public void removePlaylistTrack(String playlistId, String trackId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("playlist_id", playlistId);
        fields.put("track_id", trackId);

        subscription = manager.removeTrackPlaylist(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                               @Override
                               public void call(String s) {
                                   if (isViewAttached()) {
                                       getView().showRemoveSucces("Add Succes");
                                   }
                               }
                           }, new Action1<Throwable>() {
                               @Override
                               public void call(Throwable throwable) {
                                   if (isViewAttached()) {
                                       getView().showRemoveError("Add Succes");
                                   }
                               }
                           }
                );
    }

    @NonNull
    private Observable<List<DataPlaylist>> getPlaylist(String memberId, String createdby) {
        return manager.getAllPlaylist(memberId, createdby);
    }

    @NonNull
    private Observable<List<DataTracks>> getTracksByPLaylistId(String playlistId) {
        return manager.getTracksByPlaylistId(playlistId);
    }

    @NonNull
    private Observable<String> addPLaylist(Map<String, String> data) {
        return manager.addPLaylistMember(data);
    }

    @NonNull
    private Observable<List<DataPlaylist>> getAllPlaylistCustomer(String customerId) {
        return manager.getAllPlaylistCustomer(customerId);
    }

    @NonNull
    private Observable<String> removePlaylists(Map<String, String> data) {
        return manager.removePLaylist(data);
    }

    @NonNull
    private Observable<String> addTracksPlaylist(Map<String, String> data) {
        return manager.addTracksToPlaylist(data);
    }

    @NonNull
    private Observable<String> insertFavPlaylists(Map<String, String> data) {
        return manager.insertFavPlaylist(data);
    }

}
