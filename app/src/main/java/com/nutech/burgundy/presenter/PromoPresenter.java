package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.promo.PromoView;
import com.nutech.burgundy.utils.NetworkHelper;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 12/29/17.
 * Xeranta Mobile Solutions
 */

public class PromoPresenter extends BasePresenter<PromoView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public PromoPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(PromoView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getPromoById(String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getPromoById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataPromoSlide>() {
                    @Override
                    public void call(DataPromoSlide dataPromoSlide) {
                        if (isViewAttached()) {
                            getView().showPromoDetail(dataPromoSlide);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }
}
