package com.nutech.burgundy.presenter;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.recomended.RecomendedView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 1/12/18.
 * Xeranta Mobile Solutions
 */

public class RecomendedPresenter extends BasePresenter<RecomendedView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public RecomendedPresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(RecomendedView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getData() {

        Map<String, String> param = new HashMap<String, String>();
        try {
            param.put("customer_id", sessionManager.getDataMember().getId());
            param.put("limit", "10");
            param.put("offset", "0");
        }catch (Exception e){
            e.printStackTrace();
        }

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getRecomdedTracks(param)
        );
    }

    private Subscription getRecomdedTracks(Map<String, String> data) {
        return getRecomended(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataTracks>>() {
                    @Override
                    public void call(List<DataTracks> dataTracksList) {
                        if (isViewAttached()) {
                            getView().showRecomendedData(dataTracksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataTracks>> getRecomended(Map<String, String> data) {
        return manager.getRecomended(data);
    }

}
