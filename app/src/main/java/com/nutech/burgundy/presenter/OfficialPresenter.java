package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataOfficial;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.follow.OfficialView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 13/02/18.
 * Xeranta Mobile Solutions
 */

public class OfficialPresenter extends BasePresenter<OfficialView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public OfficialPresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(OfficialView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getDataOfficial() {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getAllOfficial()
        );
    }

    public Subscription getAllOfficial() {
        return getOfficial()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataOfficial>>() {
                    @Override
                    public void call(List<DataOfficial> dataOfficials) {
                        if (isViewAttached()) {
                            getView().showDataOfficial(dataOfficials);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void updateStatusPoint(String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", sessionManager.getDataMember().getId());
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataOfficial>> getOfficial() {
        return manager.getAllOfficial();
    }



}
