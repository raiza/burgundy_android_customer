package com.nutech.burgundy.presenter;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.schedule.ScheduleView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SchedulesPresenter extends BasePresenter<ScheduleView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public SchedulesPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(ScheduleView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getScheduleList(String userId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String,String> params = new HashMap<>();
        params.put("user_id",userId);

        subscription = getScheduleList(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataSchedules -> {
                    if (isViewAttached()) {
                        getView().showSchedules(dataSchedules);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorSchedules(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription editToScheduleMoodtrack(Map<String, String> params) {
        return editScheduleMoodtrack(params)
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(data ->{
                            Log.e("asik","asik");
                            getView().showSuccessSchedule(data);
                        },
                        throwable ->{
                            Log.e("asik",throwable.getMessage());
                            getView().showErrorSchedule(helper.getStatusError(throwable));
                        });
    }

    public void removeFromSchedule(Map<String,String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = removeSchedule(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataSchedules -> {
                    if (isViewAttached()) {
                        getView().showSuccessRemoveSchedule(dataSchedules);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorRemoveSchedule(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<List<DataSchedules>> getScheduleList(Map<String, String> param) {
        return manager.getScheduleList(param);
    }

    @NonNull
    private Observable<String> removeSchedule(Map<String, String> param) {
        return manager.removeSchedule(param);
    }

    private Observable<DataResult> editScheduleMoodtrack(Map<String, String> params) {
        return manager.editScheduleMoodtrack(params);
    }
}
