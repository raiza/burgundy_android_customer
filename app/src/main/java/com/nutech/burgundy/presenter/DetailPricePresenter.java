package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataSubscribeGoogle;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.data.model.melon.ResponseTransaction;
import com.nutech.burgundy.data.model.melon.Token;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.vip.DetailPriceView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 12/6/17.
 * Xeranta Mobile Solutions
 */

public class DetailPricePresenter extends BasePresenter<DetailPriceView> {

    private final String TAG = "DetailPricePresenter";

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;
    private Context context;

    public DetailPricePresenter(DataManager manager, NetworkHelper helper,
                                SessionManager sessionManager, Context context) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
        this.context = context;
    }

    @Override
    public void attachView(DetailPriceView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getDetailPrice(String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getSubscribeDetailPrice(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribePrice>() {
                    @Override
                    public void call(DataSubscribePrice subscribePrice) {
                        if (isViewAttached()) {
                            getView().showDataSubscribePrice(subscribePrice);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void subscribeCustomer(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.subscribeCustomerGoogle(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribeGoogle>() {
                    @Override
                    public void call(DataSubscribeGoogle dataSubscribe) {
                        if (isViewAttached()) {
                            updateStatusPoint(
                                    fields.get("customer_id"),
                                    Constants.ActivityPoint.SUBSCRIBE,
                                    dataSubscribe.getPayment_type());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
//                            getView().showRegisterSuccess(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
//                            getView().showRegisterFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    public void requestToken(HashMap<String, String> params) {
        if (subscription != null) subscription.unsubscribe();
        subscription = request(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Token>() {
                    @Override
                    public void call(Token token) {
                        if (isViewAttached()) {
                            getView().showSuccesTransactionToken(token);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorTransactionToken(throwable.getMessage());
                        }

                    }
                });
    }

    public void updateStatusPoint(final String customerId, String activityId, final String payment_type) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                        if (isViewAttached()) {
                            getSubscribeCustomerStatus(customerId, payment_type);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getSubscribeCustomerStatus(customerId, payment_type);
                        }
                    }
                });
    }

    public void getSubscribeCustomerStatus(final String id, final String paymentType) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getUserSubscribeStatus(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataUserSubscribe>() {
                    @Override
                    public void call(DataUserSubscribe userSubscribe) {
                        if (isViewAttached()) {
                            sessionManager.setStatusPackage(userSubscribe.getStatus());
                            sessionManager.setPackage(userSubscribe.getPackageName());
                            sessionManager.setPackageCode(userSubscribe.getSubscribeCode());
                            if (paymentType.toLowerCase().contains("google")) {
                                getView().showSuccess(userSubscribe);
                            } else if (paymentType.toLowerCase().contains("melon")) {
                                getView().showSuccessFromMelon(userSubscribe);
                            } else {
                                getView().showSuccess(userSubscribe);
                            }
                            Log.e(TAG, userSubscribe.getStatus());
                            Log.e(TAG, userSubscribe.getPackageName());
                            Log.e(TAG, userSubscribe.getSubscribeCode());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorFromMelon(throwable.getMessage());
                        }
                    }
                });
    }

    public void payTransaction(HashMap<String, String> params) {
        if (subscription != null) subscription.unsubscribe();
        subscription = payTransactionMelon(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseTransaction >() {
                    @Override
                    public void call(ResponseTransaction responseTransaction) {
                        if (isViewAttached()) {
                            getView().showPayTransaction(responseTransaction);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorPayTransaction(throwable.getMessage());
                        }

                    }
                });
    }

    public void chargeTransaction(HashMap<String, String> params) {
        if (subscription != null) subscription.unsubscribe();
        subscription = chargeTransactionMelon(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ResponseTransaction >() {
                    @Override
                    public void call(ResponseTransaction responseTransaction) {
                        if (isViewAttached()) {
                            getView().showChargeTransaction(responseTransaction);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorChargeTransaction(throwable.getMessage());
                        }

                    }
                });
    }

    @NonNull
    private Observable<Token> request(HashMap<String, String> params) {
        return manager.requestToken(params);
    }

    @NonNull
    private Observable<ResponseTransaction> payTransactionMelon(HashMap<String, String> params) {
        return manager.payTransactionMelon(params);
    }

    @NonNull
    private Observable<ResponseTransaction> chargeTransactionMelon(HashMap<String, String> params) {
        return manager.chargeTransactionMelon(params);
    }

}
