package com.nutech.burgundy.presenter.livekaraoke;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.livekaraoke.LiveView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 12/19/17.
 * Xeranta Mobile Solutions
 */

public class LivePresenter extends BasePresenter<LiveView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public LivePresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(LiveView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getLive(String liveStatus) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getHistoryLive(liveStatus)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataRoom>>() {
                    @Override
                    public void call(List<DataRoom> dataRoomList) {
                        if (isViewAttached()) {
                            getView().showDataLive(dataRoomList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataRoom>> getHistoryLive(String liveStatus) {
        return manager.getHistoryLive(liveStatus);
    }
}
