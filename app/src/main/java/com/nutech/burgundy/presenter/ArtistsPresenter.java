package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.artists.ArtistsView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 29/05/19.
 * Nusantara Teknologi
 */

public class ArtistsPresenter extends BasePresenter<ArtistsView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public ArtistsPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(ArtistsView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getArtistAndGenre() {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getAllArtists(),
                getGenreList()
        );
    }

    public Subscription getAllArtists() {
        return getArtists()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataArtistses -> {
                    if (isViewAttached()) {
                        System.out.println("@@@@ Sukses");
                        getView().showAllArtists(dataArtistses);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        System.out.println("@@@@ Gagal");
                        getView().showError(helper.getStatusError(throwable));
                    }
                });

    }

    public void getAllArtistsWithPaging(int page, int per_page) {
        Call<DataArtistList> artistListCall = getArtistsWithPaging(page, per_page);
        artistListCall.enqueue(new Callback<DataArtistList>() {
            @Override
            public void onResponse(@NonNull Call<DataArtistList> call, @NonNull Response<DataArtistList> response) {
                if (isViewAttached()) {
                    System.out.println("@@@@ Sukses");
                    if (response.isSuccessful()) {
                        getView().showAllArtists(response.body());
                    } else {
                        getView().showError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<DataArtistList> call, Throwable t) {
                if (isViewAttached()) {
                    System.out.println("@@@@ Gagal");
                    getView().showError(helper.getStatusError(t));
                }
            }
        });
    }

    public Subscription getArtistByName(String name) {
        return getArtisByName(name)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataArtistses -> {
                    if (isViewAttached()) {
                        getView().showArtistsSearch(dataArtistses);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorSearch(helper.getStatusError(throwable));
                    }
                });

    }

    public Subscription getGenreList() {
        return getGenre()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataGenres -> {
                    if (isViewAttached()) {
                        getView().showGenre(dataGenres);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    public void getTracksArtist(String idArtists) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getTracksByArtist(idArtists)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataTrackses -> {
                    if (isViewAttached()) {
                        getView().showArtistsTracks(dataTrackses);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    public void getAlbumsArtist(String artistId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getAtistsAlbum(artistId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataAlbumses -> {
                    if (isViewAttached()) {
                        getView().showArtistsAlbums(dataAlbumses);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    public void getTracksRecomended(String idArtists, String genreId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getTracksRecomend(idArtists, genreId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataTrackses -> {
                    if (isViewAttached()) {
                        getView().showArtistsTracks(dataTrackses);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(throwable.getMessage());
                    }
                });
    }

    @NonNull
    private Observable<List<DataGenre>> getGenre() {
        return manager.getGenreAll();
    }

    @NonNull
    private Observable<List<DataTracks>> getTracksByArtist(String idArtists) {
        return manager.getTracksByArtist(idArtists);
    }

    @NonNull
    private Observable<DataArtistList> getArtists() {
        return manager.artistAll();
    }

    @NonNull
    private Call<DataArtistList> getArtistsWithPaging(int page, int per_page) {
        return manager.artistAllWithPaging(page,per_page);
    }

    @NonNull
    private Observable<List<DataArtists>> getArtisByName(String name) {
        return manager.getAllArtistsByName(name);
    }

    @NonNull
    private Observable<List<DataAlbums>> getAtistsAlbum(String artistId) {
        return manager.getAlbumsByArtist(artistId);
    }

    @NonNull
    private Observable<List<DataTracks>> getTracksRecomend(String idArtists, String genreId) {
        return manager.getTracksRecomended(idArtists, genreId);
    }

}
