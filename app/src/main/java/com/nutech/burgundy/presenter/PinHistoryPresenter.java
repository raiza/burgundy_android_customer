package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.history.PinHistoryView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 10/17/17
 */

public class PinHistoryPresenter extends BasePresenter<PinHistoryView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public PinHistoryPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(PinHistoryView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getDetailHistpry(String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getDetailHistory(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataHistoryExchange>() {
                    @Override
                    public void call(DataHistoryExchange dataHistoryExchange) {
                        if (isViewAttached()) {
                            getView().showDetailHistory(dataHistoryExchange);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateStatusPointExchange(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.updateStatusPointExchange(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                            getView().showSuccessUpdate(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdate(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void usePointExchange(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.usePointExchange(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                            getView().showSuccessUsed(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUsed(helper.getStatusCode(throwable));

                        }

                    }
                });
    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
//                            getView().showRegisterSuccess(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
//                            getView().showRegisterFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }
}
