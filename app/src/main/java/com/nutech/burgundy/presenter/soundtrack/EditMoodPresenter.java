package com.nutech.burgundy.presenter.soundtrack;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.data.model.soundtrack.Sound;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.soundtrack.EditMoodView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 2019-07-23.
 * NuTech (Nusantara Teknologi)
 */
public class EditMoodPresenter extends BasePresenter<EditMoodView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public EditMoodPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(EditMoodView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getData(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getMoodtrackDetail(params)
        );
    }

    public Subscription getMoodtrackDetail(Map<String, String> params) {
        return detailMoodtrack(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataMoodtrack -> {
                    if (isViewAttached()) {
                        getView().showMood(dataMoodtrack);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription updateMoodtrack(Map<String, String> params) {
        return updateMood(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (isViewAttached()) {
                        getView().showEditSuccess(data);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showEditError(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getEnergy() {
        return energy()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<Energy>>) energyData -> {
                    if (isViewAttached()) {
                        getView().showEnergyList(energyData);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorEnergy(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getSound() {
        return sounds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<Sound>>) soundData -> {
                    if (isViewAttached()) {
                        getView().showSoundList(soundData);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorSound(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getGenresAll() {
        return genres()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<DataGenre>>) genres -> {
                    if (isViewAttached()) {
                        getView().showGenreList(genres);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorGenre(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getDecadeAll() {
        return decades()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<Decade>>) decades -> {
                    if (isViewAttached()) {
                        getView().showDecadeList(decades);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorDecade(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<Moodtrack> detailMoodtrack(Map<String, String> params) {
        return manager.detailMoodtrack(params);
    }

    @NonNull
    private Observable<String> updateMood(Map<String, String> params) {
        return manager.updateMood(params);
    }

    @NonNull
    private Observable<List<Energy>> energy() {
        return manager.getEnergyAll();
    }

    @NonNull
    private Observable<List<Sound>> sounds() {
        return manager.getSoundAll();
    }

    @NonNull
    private Observable<List<DataGenre>> genres() {
        return manager.getGenreAll();
    }

    @NonNull
    private Observable<List<Decade>> decades() {
        return manager.getDecadeAll();
    }
}
