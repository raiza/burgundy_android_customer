package com.nutech.burgundy.presenter.livekaraoke;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.livekaraoke.HistoryKaraokeView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public class HistoryKaraokePresenter extends BasePresenter<HistoryKaraokeView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public HistoryKaraokePresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(HistoryKaraokeView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getHistoryAll(String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getAll(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataRoom>>() {
                    @Override
                    public void call(List<DataRoom> dataRoomList) {
                        if (isViewAttached()) {
                            getView().showDataHistory(dataRoomList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataRoom>> getAll(String custId) {
        return manager.getHistoryAll(custId);
    }

    public void getHistoryByCustomer(String customerId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getHistoryCustomer(customerId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataRoom>>() {
                    @Override
                    public void call(List<DataRoom> dataRoomList) {
                        if (isViewAttached()) {
                            getView().showDataHistory(dataRoomList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataRoom>> getHistoryCustomer(String customerId) {
        return manager.getHistoryByCustomer(customerId);
    }

    public void removeHistoryByCustomer(Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = removeHistoryKaraoke(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showRemoveSuccess("Remove Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showRemoveError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> removeHistoryKaraoke(Map<String, String> fields) {
        return manager.removeHistoryKaraoke(fields);
    }
}
