package com.nutech.burgundy.presenter.soundtrack;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.nutech.burgundy.R;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.soundtrack.ListMoodtrackView;
import com.nutech.burgundy.ui.view.soundtrack.MoodListDialogActivity;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class ListMoodtrackPresenter extends BasePresenter<ListMoodtrackView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public ListMoodtrackPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(ListMoodtrackView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getData(String memberId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getMoodtrackList(memberId)
        );
    }

    public void getMoodtrackStore(String storeId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getMoodtrackByStoreId(storeId)
        );
    }

    public void getListMoodtrackForYou() {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> param = new HashMap<>();
        try {
            param.put("limit", String.valueOf(Integer.MAX_VALUE));
            param.put("offset", "0");
        } catch (Exception e) {
            e.printStackTrace();
        }

        subscription = new CompositeSubscription(
                getMoodtracksForYou(param)
        );
    }

    public void getMoodtrackPlayingStore(double lat, double lng) {

        if (subscription != null) subscription.unsubscribe();
        Map<String, String> paramMood = new HashMap<>();

        try {
            paramMood.put("lat",String.valueOf(lat));
            paramMood.put("lng",String.valueOf(lng));
            paramMood.put("distance","10");
        } catch (Exception e) {
            e.printStackTrace();
        }

        subscription = new CompositeSubscription(
                getPlayingMoodtrackStore(paramMood)
        );
    }

    private Subscription getMoodtracksForYou(Map<String, String> data) {
        return getMoodtrackForYou(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moodtrackList -> {
                    if (isViewAttached()) {
                        getView().showMoodtracks(moodtrackList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorMoodtracks(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getMoodtrackList(String memberId) {
        return getMoodtracks(memberId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataMoodtrack -> {
                    if (isViewAttached()) {
                        getView().showMoodtracks(dataMoodtrack);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorMoodtracks(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getMoodtrackByStoreId(String storeId) {
        return getMoodtracksByStoreId(storeId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataMoodtrack -> {
                    if (isViewAttached()) {
                        getView().showMoodtracks(dataMoodtrack);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorMoodtracks(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription removeMood(Map<String, String> params) {
        return remove(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    if (isViewAttached()) {
                        getView().showSuccessRemove(data);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorRemove(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription addToScheduleMoodtrack(Map<String, String> params) {
        return addScheduleMoodtrack(params)
                .subscribeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    getView().showSuccessSchedule(data);
                }, throwable -> {
                    getView().showErrorSchedule(helper.getStatusError(throwable));
                });
    }

    public void addTrackToMood(Map<String, String> params, MoodListDialogActivity moodListDialogActivity) {
        if (subscription != null) subscription.unsubscribe();
        subscription = manager.addTrackToMood(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        moodListDialogActivity.hideProgressDialog();
                        alertSuccess(moodListDialogActivity);
                        System.out.println("########### Sukses Add ###########");
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        moodListDialogActivity.hideProgressDialog();
                        alertSuccess(moodListDialogActivity);
                        System.out.println("@@@@@@@@@@@@ Sukses Add @@@@@@@@@@");
                    }
                });
    }

    private Subscription getPlayingMoodtrackStore(Map<String,String> data) {
        return getMoodtrackPlayingStore(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moodtrackList -> {
                    if (isViewAttached()) {
                        getView().showMoodtracksPlayingStore(moodtrackList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorMoodtracksPlayingStore(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<List<Moodtrack>> getMoodtracks(String memberId) {
        return manager.getMoodtracks(memberId);
    }

    @NonNull
    private Observable<List<Moodtrack>> getMoodtracksByStoreId(String storeId) {
        return manager.getMoodtracksByStoreId(storeId);
    }

    private Observable<String> remove(Map<String, String> params) {
        return manager.remove(params);
    }

    private Observable<String> addScheduleMoodtrack(Map<String, String> params) {
        return manager.addScheduleMoodtrack(params);
    }

    private void alertSuccess(MoodListDialogActivity activity) {
        
        String msg = "'"+activity.trackName+"' "+
                activity.getResources().getString(R.string.msg_success_add_track_to_mood)+" '"+activity.moodName+"'";
        AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
        alertDialog.setCancelable(false);
        alertDialog.setIcon(R.drawable.ic_info_outline_black_24dp);
        alertDialog.setTitle(activity.getResources().getString(R.string.label_info));
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, activity.getResources().getString(R.string.button_ok),
                (dialog, which) -> {
                    dialog.dismiss();
                    activity.finish();

                });
        alertDialog.show();
    }

    @NonNull
    private Observable<List<Moodtrack>> getMoodtrackPlayingStore(Map<String,String> data) {
        return manager.getMoodtrackPlayingStore(data);
    }

    @NonNull
    private Observable<List<Moodtrack>> getMoodtrackForYou(Map<String, String> data) {
        return manager.getMoodtrackForYou(data);
    }
}
