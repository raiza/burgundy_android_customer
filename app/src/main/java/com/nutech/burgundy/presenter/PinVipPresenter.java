package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataSubscribe;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.vip.PinVipView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 10/17/17
 */

public class PinVipPresenter extends BasePresenter<PinVipView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public PinVipPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(PinVipView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getDetailPrice(String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getSubscribeDetailPrice(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribePrice>() {
                    @Override
                    public void call(DataSubscribePrice subscribePrice) {
                        if (isViewAttached()) {
                            getView().showDetailPrice(subscribePrice);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateSubscribeCustomer(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.updateSubscribeCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribe>() {
                    @Override
                    public void call(DataSubscribe dataSubscribe) {
                        if (isViewAttached()) {
                            getView().showSuccess(dataSubscribe.getMessage());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateSubscribeActivationCode(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.updateSubscribeActivationCode(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribe>() {
                    @Override
                    public void call(DataSubscribe dataSubscribe) {
                        if (isViewAttached()) {
                            getView().showSuccessUpdateCode(dataSubscribe.getMessage());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdateCode(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateStatusPoint(String customerId, String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                        if (isViewAttached()) {
                            getView().showSuccessUpdate(s);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdate(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    public void getSubscribeCustomerStatus(final String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getUserSubscribeStatus(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataUserSubscribe>() {
                    @Override
                    public void call(DataUserSubscribe userSubscribe) {
                        if (isViewAttached()) {
//                            getCustomerById(id);
                            getView().showSubscribeStatus(userSubscribe);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorSubscribeStatus(helper.getStatusError(throwable));
                        }
                    }
                });
    }
}
