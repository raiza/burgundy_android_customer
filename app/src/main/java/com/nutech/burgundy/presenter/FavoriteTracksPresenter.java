package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.home.FavoriteTracksView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 11/15/17.
 * Xeranta Mobile Solutions
 */

public class FavoriteTracksPresenter extends BasePresenter<FavoriteTracksView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public FavoriteTracksPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(FavoriteTracksView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public Subscription getTracksMember(String memberId) {
        return getTrackMembers(memberId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataTracks>>() {
                    @Override
                    public void call(List<DataTracks> dataTracks) {
                        if (isViewAttached()) {
                            getView().showSuccess(dataTracks);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void removeTracksMember(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = removeTracks(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showSuccessRemove("Remove Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataTracks>> getTrackMembers(String memberId) {
        return manager.getTracksMember(memberId);
    }

    @NonNull
    private Observable<String> removeTracks(Map<String, String> data) {
        return manager.removeTracksMember(data);
    }
}
