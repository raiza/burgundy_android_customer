package com.nutech.burgundy.presenter;

import android.annotation.SuppressLint;
import android.content.Context;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataAds;
import com.nutech.burgundy.lib.jcplayer.AdsPlayView;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.utils.NetworkHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AdsPresenter extends BasePresenter<AdsPlayView> {
    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public AdsPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(AdsPlayView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getAds(int between, boolean isContinue, boolean isPrevious, String typeAds,String storeId) {
        if (subscription != null) subscription.unsubscribe();
        Date date = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpDate = new SimpleDateFormat("kk:mm:ss");

        Map<String, String> params = new HashMap<String, String>();
        params.put("between", String.valueOf(between));
        params.put("ads_block", typeAds);
        params.put("time", simpDate.format(date));
        params.put("store_id", storeId);
        params.put("limit",String.valueOf(new Random().nextInt(2)+1));


        subscription = getAdsList(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(adsList -> {
                    if (isViewAttached()) {
                        getView().showAdsList(adsList, isContinue, isPrevious,between);
                    }
                }, throwable -> {
                    getView().showErrorAdsList(helper.getStatusError(throwable), isContinue,isPrevious);
                });
    }

    private Observable<List<DataAds>> getAdsList(Map<String, String> data) {
        return manager.getAdsList(data);
    }
}
