package com.nutech.burgundy.presenter;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.home.MyMusicView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif-Setiawan on 8/28/17.
 */

public class MyMusicPresenter extends BasePresenter<MyMusicView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public MyMusicPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(MyMusicView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getMyFavorite(String memberId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getPlaylistMember(memberId)
        );
    }

    public Subscription getPlaylistMember(String memberId) {
        return getPLaylistMembers(memberId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataPlaylistMember>>() {
                    @Override
                    public void call(List<DataPlaylistMember> dataPlaylistMember) {
                        if (isViewAttached()) {
                            getView().showListPlaylistRegistered(dataPlaylistMember);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataPlaylistMember>> getPLaylistMembers(String memberId) {
        return manager.getPlaylistMember(memberId);
    }
}
