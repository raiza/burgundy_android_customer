package com.nutech.burgundy.presenter;

import android.util.Log;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.home.DiscoverView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Arif Setiawan on 8/28/17
 */

public class GenrePresenter extends BasePresenter<DiscoverView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public GenrePresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(DiscoverView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getAllData(double lat, double lng) {

        Map<String, String> param = new HashMap<String, String>();
        Map<String, String> paramMood = new HashMap<String, String>();
        try {
            param.put("customer_id", sessionManager.getDataMember().getId());
            param.put("limit", "3");
            param.put("offset", "0");

            if (sessionManager.getMoodtrackFilter() != null) {
                param.put("decade_id", sessionManager.getMoodtrackFilter().getDecadeIds());
                param.put("sound_id", sessionManager.getMoodtrackFilter().getSoundIds());
                param.put("genre_id", sessionManager.getMoodtrackFilter().getGenreIds());
                param.put("energy_id", sessionManager.getMoodtrackFilter().getEnergyids());
            }

            paramMood.put("lat",String.valueOf(lat));
            paramMood.put("lng",String.valueOf(lng));
            paramMood.put("distance","10");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (subscription != null) subscription.unsubscribe();

        Log.e("params",paramMood.toString());

        subscription = new CompositeSubscription(
                getPromoList(),
                getTopChart(),
                getTopPlayList(),
                getGenreList(),
                getMoodtracksForYou(param),
                getNearStore(paramMood),
                getPlayingMoodtrackStore(paramMood),
                getRecomdedTracks(param),
                getTopKaraoke()
        );
    }

    private Subscription getPromoList() {
        return getPromo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataPromoSlides -> {
                    if (isViewAttached()) {
                        getView().showPromo(dataPromoSlides);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    private Subscription getPlayingMoodtrackStore(Map<String,String> data) {
        return getMoodtrackPlayingStore(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moodtrackList -> {
                    if (isViewAttached()) {
                        getView().showMoodtrackPlayingStore(moodtrackList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showMoodtrackPlayingStoreError(helper.getStatusError(throwable));
                    }
                });
    }

    private Subscription getNearStore(Map<String,String> data) {
        return getNearStores(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeList -> {
                    if (isViewAttached()) {
                        getView().showNearStores(storeList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showNearStoresError(helper.getStatusError(throwable));
                    }
                });
    }

    private Subscription getTopChart() {
        return getTopChart(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(tracksList -> {
                    if (isViewAttached()) {
                        getView().showTopChart(tracksList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    private Subscription getTopPlayList() {
        return getTopPlaylist(5)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataPlaylist>>() {
                    @Override
                    public void call(List<DataPlaylist> playlist) {
                        if (isViewAttached()) {
                            getView().showTopPlayList(playlist);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    private Subscription getGenreList() {
        return getGenre()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataGenre>>() {
                    @Override
                    public void call(List<DataGenre> genreList) {
                        if (isViewAttached()) {
                            getView().showGenre(genreList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    private Subscription getRecomdedTracks(Map<String, String> data) {
        return getRecomended(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataTracks>>() {
                    @Override
                    public void call(List<DataTracks> dataTracksList) {
                        if (isViewAttached()) {
                            getView().showRecomended(dataTracksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorRecomended(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    private Subscription getMoodtracksForYou(Map<String, String> data) {
        return getMoodtrackForYou(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(moodtrackList -> {
                    if (isViewAttached()) {
                        getView().showMoodtrackForYou(moodtrackList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorMoodtrackForYou(helper.getStatusError(throwable));
                    }
                });
    }

    private Subscription getTopKaraoke() {
        return getKaraoke()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataRoom>>() {
                    @Override
                    public void call(List<DataRoom> dataRoomList) {
                        if (isViewAttached()) {
                            getView().showTopKaraoke(dataRoomList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorTopKaraoke(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<List<DataPromoSlide>> getPromo() {
        return manager.getPromoSlide();
    }

    @NonNull
    private Observable<List<Moodtrack>> getMoodtrackPlayingStore(Map<String,String> data) {
        return manager.getMoodtrackPlayingStore(data);
    }

    @NonNull
    private Observable<List<DataStore>> getNearStores(Map<String,String> data) {
        return manager.getNearStores(data);
    }

    @NonNull
    private Observable<List<DataTracks>> getTopChart(Integer limit) {
        return manager.getTopChart(limit);
    }

    @NonNull
    private Observable<List<DataPlaylist>> getTopPlaylist(Integer limit) {
        return manager.getTopPlaylist(limit);
    }

    @NonNull
    private Observable<List<DataGenre>> getGenre() {
        return manager.getGenreAll();
    }

    @NonNull
    private Observable<List<DataTracks>> getRecomended(Map<String, String> data) {
        return manager.getRecomended(data);
    }

    @NonNull
    private Observable<List<Moodtrack>> getMoodtrackForYou(Map<String, String> data) {
        return manager.getMoodtrackForYou(data);
    }

    @NonNull
    private Observable<List<DataRoom>> getKaraoke() {
        return manager.getTopKaraoke();
    }
}
