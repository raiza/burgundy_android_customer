package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataSubscribeGoogle;
import com.nutech.burgundy.data.model.DataSubscribtionGoogle;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.lyric.LyricView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 10/26/17.
 * Xeranta Mobile Solutions
 */

public class LyricksPresenter extends BasePresenter<LyricView> {

    private Subscription subscription;
    private Subscription subscriptionIsPlaying;
    private Context context;
    private SessionManager sessionManager;

    private DataManager manager;
    private NetworkHelper helper;

    public LyricksPresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(LyricView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
        if (subscriptionIsPlaying != null) subscriptionIsPlaying.unsubscribe();
    }

    public void send(String tracksId, Map<String, String> data, Map<String, String> data1) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getTrackLiricks(tracksId),
                sessionManager.getUserType().equals(Constants.UserType.USER_STORE) ?
                        insertLogStore(data1) : insertLog(data1),
                updatePlaysCount(data)

        );
    }

    public void updateIsPlayingStatus(Map<String, String> data) {
        if (subscriptionIsPlaying != null) subscriptionIsPlaying.unsubscribe();

        subscriptionIsPlaying = new CompositeSubscription(
                updateIsPlayingMoodtrack(data)
        );
    }

    public void insertLogReport(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                insertLogForReport(data)

        );
    }

    public void insertLogAds(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                insertLogForAds(data)

        );
    }

    public void getDataDetailMember(String id, String mobilePhone){

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getDetailMember(id),
                getSubscribeCustomerStatus(id)
//                checkMemberToIndosis(mobilePhone, id)

        );
    }

    public void getDataDetailUserStore(String id, String isAppRun){

        if (subscription != null) subscription.unsubscribe();

        Map<String,String> params = new HashMap<>();
        params.put("is_app_run",isAppRun);
        params.put("store_member_id",id);

        subscription = new CompositeSubscription(
                getDetailUserStore(id),
                updateIsPlayingMoodtrack(params)
        );
    }

    public Subscription getTrackLiricks(String tracksId) {

        return getLiricTracks(tracksId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataLiricks>>() {
                    @Override
                    public void call(List<DataLiricks> dataLiricksList) {
                        if (isViewAttached()) {
                            getView().showLiricks(dataLiricksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorLiricks(helper.getStatusError(throwable));
                        }
                    }
                });

    }

    public Subscription updatePlaysCount(Map<String, String> data) {

        System.out.println("++++ param log +++++ "+data);

        return updatePlays(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    public Subscription getDetailMember(String id) {

        return getDataMemberDetail(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataMember>() {
                    @Override
                    public void call(DataMember dataMemberList) {
                        DataMember dataMember = new DataMember();
                        dataMember.setId(dataMemberList.getId());
                        dataMember.setName(dataMemberList.getName());
                        dataMember.setCf1(dataMemberList.getCf1());
                        dataMember.setCf2(dataMemberList.getCf2());
                        dataMember.setPhone(dataMemberList.getPhone());
                        dataMember.setMobile_phone(dataMemberList.getMobile_phone());
                        dataMember.setEmail(dataMemberList.getEmail());
                        dataMember.setBirth_date(dataMemberList.getBirth_date());
                        dataMember.setMember_card_id(dataMemberList.getMember_card_id());
                        dataMember.setJoin_date(dataMemberList.getJoin_date());
                        dataMember.setUsername(dataMemberList.getUsername());
                        dataMember.setActivation_code(dataMemberList.getActivation_code());
                        dataMember.setAddress(dataMemberList.getAddress());
                        dataMember.setCity_id(dataMemberList.getCity_id());
                        dataMember.setState_id(dataMemberList.getState_id());
                        dataMember.setLat(dataMemberList.getLat());
                        dataMember.setLng(dataMemberList.getLng());
                        dataMember.setRegister_type(dataMemberList.getRegister_type());
                        dataMember.setActive_status(dataMemberList.getActive_status());
                        dataMember.setRegister_id(dataMemberList.getRegister_id());
                        dataMember.setSubscribename(dataMemberList.getSubscribename());
                        dataMember.setImei(dataMemberList.getImei());
                        dataMember.setPoint(dataMemberList.getPoint());

                        sessionManager.setDataMember(dataMember);
                        if (!TextUtils.isEmpty(dataMemberList.getSubscribename())){
                            sessionManager.setPackage(dataMemberList.getSubscribename());
                        }
                        if (isViewAttached()) {
                            getView().showDataMember(dataMemberList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorMember(helper.getStatusError(throwable));
                        }
                    }
                });

    }

    public Subscription getDetailUserStore(String id) {

        return getDataUserStoreDetail(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataUserStore -> {

                    sessionManager.setUserStore(dataUserStore);
                    sessionManager.setStore(dataUserStore.getStore());
                    if (isViewAttached()) {
                        getView().showDataUserStore(dataUserStore);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorUserStore(helper.getStatusError(throwable));
                    }
                });

    }

    public Subscription getSubscribeCustomerStatus(String id) {

        return getSubsCusStatus(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataUserSubscribe>() {
                    @Override
                    public void call(DataUserSubscribe userSubscribe) {
                        sessionManager.setStatusPackage(userSubscribe.getStatus());
                        if (isViewAttached()) {
                            sessionManager.setTokenGPA(userSubscribe.getToken());
                            sessionManager.setSkuGPA(userSubscribe.getProductSku());
                            sessionManager.setPaymentType(userSubscribe.getPayment_type());
                            Log.e("+++ TOKEN +++ ", userSubscribe.getToken());
                            Log.e("+++ SKU +++ ", userSubscribe.getProductSku());

                            HashMap<String, String> fields = new HashMap<>();
                            fields.put("package_name", BuildConfig.APPLICATION_ID);
                            fields.put("subscribe_id", userSubscribe.getProductSku());
                            fields.put("token", userSubscribe.getToken());

                            Log.e("+++ PAY TYPE +++ ", userSubscribe.getPayment_type());
                            if (userSubscribe.getPayment_type().equals(Constants.PAYTIPE.GOOGLE)) {
                                getSubsribtionGoogleStatus(fields, userSubscribe.getSignature(),
                                        userSubscribe.getPay_amount(), userSubscribe.getProductSku(),
                                        userSubscribe.getToken(), userSubscribe.getId());
                            }

                        }
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        Log.e("+++ ERROR +++ ", Objects.requireNonNull(throwable.getMessage()));
                        if (throwable.getMessage().contains("404")){
                            sessionManager.setStatusPackage("0");
                        }
                    }
                });

    }

    public Subscription insertLog(Map<String, String> data) {
        return addLog(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (isViewAttached()) {
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                    }
                });
    }

    public Subscription insertLogStore(Map<String, String> data) {
        return addLogStore(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (isViewAttached()) {
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                    }
                });
    }

    public Subscription updateIsPlayingMoodtrack(Map<String,String> data) {
        Log.e("params", data.toString());
        return updateIsPlaying(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(playing -> {
                    isViewAttached();
                },throwable -> {
                    isViewAttached();
                });
    }

    public Subscription insertLogForReport(Map<String, String> data) {
        return addLogReport(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (isViewAttached()) {
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                    }
                });
    }

    public Subscription insertLogForAds(Map<String, String> data) {
        return addLogAds(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (isViewAttached()) {
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                    }
                });
    }

    public Subscription checkMemberToIndosis(String mobilePhone, final String id) {

        return getCheckMemberToIndosis(mobilePhone)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<MemberCheck>() {
                    @Override
                    public void call(MemberCheck dataMemberCheck) {
                        MemberCheck memberCheck = new MemberCheck();
                        memberCheck.setStatus(dataMemberCheck.getStatus());
                        memberCheck.setMsisdn(dataMemberCheck.getMsisdn());
                        memberCheck.setDtm_start(dataMemberCheck.getDtm_start());
                        memberCheck.setLast_sucess_dr(dataMemberCheck.getLast_sucess_dr());
                        memberCheck.setMessage(dataMemberCheck.getMessage());
                        sessionManager.setDataMember(memberCheck);

                        Map<String, String> params = new HashMap<String, String>();
                        try {
                            params.put("customer_id", id);
                            params.put("start_date", dataMemberCheck.getDtm_start());
                            params.put("end_date", dataMemberCheck.getLast_sucess_dr());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        insertDataLanggananIndosis(params);

                        Log.e("++ Sukses Update", "Member check ++");
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Log.e("++ Gagal Update", "Member check ++");
                    }
                });

    }

    public void insertDataLanggananIndosis(Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.insertDataLanggananIndosis(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    public void getSubsribtionGoogleStatus(Map<String, String> fields, final String signature,
                                           final String Price, final String sku, final String token,
                                           final String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getSubsribtionGoogleStatus(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribtionGoogle>() {
                    @Override
                    public void call(DataSubscribtionGoogle dataSubscribtionGoogle) {
                        Log.e("expiryTimeMillis", dataSubscribtionGoogle.getExpiryTimeMillis());
                        Log.e("startTimeMillis", dataSubscribtionGoogle.getStartTimeMillis());

                        long dateMillis = Long.parseLong(dataSubscribtionGoogle.getExpiryTimeMillis());

                        String status = "";
                        if (dataSubscribtionGoogle.isAutoRenewing()){
                            status = Constants.Status.SUCCESS;
                        } else {
                            status = Constants.Status.PENDING;
                        }

                        Date currentDate = new Date(dateMillis);
                        Map<String, String> fields = new HashMap<>();
                        fields.put("subscribe_id", sku);
                        fields.put("customer_id", sessionManager.getDataMember().getId());
                        fields.put("pay_with", "0");
                        fields.put("pay_amount", Price);
                        fields.put("pay_date", Helper.todayDate());
                        fields.put("payment_status", "1");
                        fields.put("payment_type", Constants.PAYTIPE.GOOGLE);
                        fields.put("order_id", dataSubscribtionGoogle.getOrderId());
                        fields.put("order_date", Helper.formatOrderDate(currentDate));
                        fields.put("status", status);
                        fields.put("token", token);
                        fields.put("signature", signature);
                        fields.put("end_date", Helper.formatOrderDate(currentDate));
                        fields.put("id", id);

                        insertNewRenewal(fields);

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }

    public void insertNewRenewal(Map<String, String> fields) {

        System.out.println("Params : "+fields.toString());

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.updateSubscribeCustomerGoogle(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribeGoogle>() {
                    @Override
                    public void call(DataSubscribeGoogle dataSubscribeGoogle) {
                        Log.e("Sukses", "Insert");
                        sessionManager.setStatusPackage(dataSubscribeGoogle.getStatus());
                        Log.e("Order ID", dataSubscribeGoogle.getOrder_id());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            Log.e("Gagal", "Insert");
                        }
                    }
                });
    }

    @NonNull
    private Observable<DataUserSubscribe> getSubsCusStatus(String id) {
        return manager.getUserSubscribeStatus(id);
    }

    @NonNull
    private Observable<DataMember> getDataMemberDetail(String id) {
        return manager.getDataMember(id);
    }

    @NonNull
    private Observable<DataUserStore> getDataUserStoreDetail(String id) {
        return manager.getDataUserStore(id);
    }

    @NonNull
    private Observable<List<DataLiricks>> getLiricTracks(String tracksId) {
        return manager.getTrackLirick(tracksId);
    }

    @NonNull
    private Observable<String> updatePlays(Map<String, String> data) {
        return manager.updatePlaysTracksCount(data);
    }

    @NonNull
    private Observable<String> addLog(Map<String, String> data) {
        return manager.addLogCustomer(data);
    }

    @NonNull
    private Observable<String> addLogStore(Map<String, String> data) {
        return manager.addLogStore(data);
    }

    @NonNull
    private Observable<Map> updateIsPlaying(Map<String, String> data) {
        return manager.updateIsPlayingMoodtrack(data);
    }

    @NonNull
    private Observable<String> addLogReport(Map<String, String> data) {
        return manager.addLogReport(data);
    }

    @NonNull
    private Observable<String> addLogAds(Map<String, String> data) {
        return manager.addLogAds(data);
    }

    @NonNull
    private Observable<MemberCheck> getCheckMemberToIndosis(String mobile_phone) {
        return manager.checkMemberToIndosis(mobile_phone);
    }
}
