package com.nutech.burgundy.presenter;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataSubscribe;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.profile.MyProfileView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Dafian on 10/17/17
 */

public class MyProfilePresenter extends BasePresenter<MyProfileView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public MyProfilePresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(MyProfileView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getData(String customerId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getMemberById(customerId),
                getSubscribeCustomerStatus(customerId)

        );
    }

    public void getCustomerById(String customerId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getCustomerById(customerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataMember>() {
                    @Override
                    public void call(DataMember member) {
                        if (isViewAttached()) {
                            getView().showCustomer(member);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorCustomer(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public Subscription getMemberById(String id) {

        return getMember(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataMember>() {
                    @Override
                    public void call(DataMember member) {
                        if (isViewAttached()) {
                            getView().showCustomer(member);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorCustomer(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public Subscription getSubscribeCustomerStatus(String id) {

        return getSubscribeCustomer(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataUserSubscribe>() {
                    @Override
                    public void call(DataUserSubscribe userSubscribe) {
                        sessionManager.setStatusPackage(userSubscribe.getStatus());
                        if (isViewAttached()) {
                            getView().showSubscribeStatus(userSubscribe);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorSubscribeStatus(throwable.getMessage());
                        }
                    }
                });
    }

    public void unSubscribeCustomer(Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.unSubscribeCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataSubscribe>() {
                    @Override
                    public void call(DataSubscribe dataSubscribe) {

                        if (isViewAttached()) {
                            getView().showSuccess(dataSubscribe.getMessage());
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }


    @NonNull
    private Observable<DataMember> getMember(String id) {
        return manager.getDataMember(id);
    }

    @NonNull
    private Observable<DataUserSubscribe> getSubscribeCustomer(String id) {
        return manager.getUserSubscribeStatus(id);
    }

}
