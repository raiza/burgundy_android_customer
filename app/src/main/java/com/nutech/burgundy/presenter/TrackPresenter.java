package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackFragment;
import com.nutech.burgundy.ui.view.track.TracksView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 12/28/17.
 * NuTech (Nusantara Teknologi)
 */

public class TrackPresenter extends BasePresenter<TracksView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public TrackPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(TracksView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getTrackById(String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getTrackById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataTracks>() {
                    @Override
                    public void call(DataTracks tracksList) {
                        if (isViewAttached()) {
                            getView().showTracks(tracksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void removeTrackFromMood(Map<String, String> params, DetailMoodtrackFragment fragment) {
        if (subscription != null) subscription.unsubscribe();
        subscription = manager.removeTrackFromMood(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    fragment.loadData();
                    System.out.println("########### Sukses Remove ###########");
                }, throwable -> {
                    fragment.loadData();
                    System.out.println("@@@@@@@@@@@@ Sukses Remove @@@@@@@@@@");
                });
    }

}
