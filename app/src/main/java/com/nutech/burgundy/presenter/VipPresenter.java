package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.vip.VipView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 10/16/17
 */

public class VipPresenter extends BasePresenter<VipView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public VipPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(VipView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getSubscribePriceAll() {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getSubscribePriceAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataSubscribePrice>>() {
                    @Override
                    public void call(List<DataSubscribePrice> dataSubscribePrices) {
                        if (isViewAttached()) {
                            getView().showPriceAll(dataSubscribePrices);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

}
