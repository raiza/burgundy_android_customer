package com.nutech.burgundy.presenter;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.profile.UpdateProfileView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 11/28/17.
 * Xeranta Mobile Solutions
 */

public class UpdateProfilePresenter extends BasePresenter<UpdateProfileView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public UpdateProfilePresenter(DataManager manager, NetworkHelper helper, SessionManager sessionManager) {
        this.manager = manager;
        this.helper = helper;
        this.sessionManager = sessionManager;
    }

    @Override
    public void attachView(UpdateProfileView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void uploadImageProfile(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.uploadImageProfile(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            updateStatusPoint(Constants.ActivityPoint.UPDATE_PROFILE);
                            getView().succesUpdate("Add Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            updateStatusPoint(Constants.ActivityPoint.UPDATE_PROFILE);
                            getView().error(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateDataProfile(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.updateDataProfile(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataMember>() {
                    @Override
                    public void call(DataMember dataMemberList) {
                        DataMember dataMember = new DataMember();
                        dataMember.setId(dataMemberList.getId());
                        dataMember.setName(dataMemberList.getName());
                        dataMember.setMobile_phone(dataMemberList.getMobile_phone());
                        dataMember.setEmail(dataMemberList.getEmail());
                        dataMember.setBirth_date(dataMemberList.getBirth_date());
                        dataMember.setJoin_date(dataMemberList.getJoin_date());
                        dataMember.setImei(dataMemberList.getImei());
                        dataMember.setPoint(dataMemberList.getPoint());

                        sessionManager.setDataMember(dataMember);
                        if (isViewAttached()) {
                            updateStatusPoint(Constants.ActivityPoint.UPDATE_PROFILE);
                            getView().showDataMember(dataMemberList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            updateStatusPoint(Constants.ActivityPoint.UPDATE_PROFILE);
                            getView().errorDataMember(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateStatusPoint(String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", sessionManager.getDataMember().getId());
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }
}


