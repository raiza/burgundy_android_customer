package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.history.HistoryView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 10/16/17
 */

public class HistoryExchangePresenter extends BasePresenter<HistoryView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public HistoryExchangePresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(HistoryView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getHistoryPointExchange(String customerId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getHistoryPointExchange(customerId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataHistoryExchange>>() {
                    @Override
                    public void call(List<DataHistoryExchange> dataHistoryExchanges) {
                        if (isViewAttached()) {
                            getView().showHistory(dataHistoryExchanges);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }
}
