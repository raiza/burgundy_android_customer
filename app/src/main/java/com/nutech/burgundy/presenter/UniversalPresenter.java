package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.playback.PlayingView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 10/17/17.
 * Xeranta Mobile Solutions
 */

public class UniversalPresenter extends BasePresenter<PlayingView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public UniversalPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(PlayingView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void addTracksFavorite(Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = addTracks(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String result) {
                        if (isViewAttached()) {
                            getView().showSucces(result);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> addTracks(Map<String, String> fields) {
        return manager.addTracksFav(fields);
    }
}
