package com.nutech.burgundy.presenter.soundtrack;

import androidx.annotation.NonNull;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Sound;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.soundtrack.SoundtrackView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 2019-07-03.
 * NuTech (Nusantara Teknologi)
 */
public class AddSoundtrackPresenter extends BasePresenter<SoundtrackView> {
    private Subscription subscription;
    private DataManager manager;
    private NetworkHelper helper;

    public AddSoundtrackPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(SoundtrackView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public Subscription getEnergy() {
        return energy()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<Energy>>) energyData -> {
                    if (isViewAttached()) {
                        getView().showEnergyList(energyData);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorEnergy(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getSound() {
        return sounds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<Sound>>) soundData -> {
                    if (isViewAttached()) {
                        getView().showSoundList(soundData);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorSound(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getGenresAll() {
        return genres()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<DataGenre>>) genres -> {
                    if (isViewAttached()) {
                        getView().showGenreList(genres);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorGenre(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription getDecadeAll() {
        return decades()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Action1<List<Decade>>) decades -> {
                    if (isViewAttached()) {
                        getView().showDecadeList(decades);
                    }
                }, (Action1<Throwable>) throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorDecade(helper.getStatusError(throwable));
                    }
                });
    }

    private void updateStatusPoint(String customerId, String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    // Success
                }, throwable -> {
                    if (isViewAttached()) {
                    }
                });
    }

    public void saveNewMoodtrack(final Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = saveMoodTrack(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (isViewAttached()) {
                        updateStatusPoint(data.get("customer_id"), Constants.ActivityPoint.NEW_PLAYLIST);
                        getView().showSuccessAdd("Add Moodtrack Success!");
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorAdd(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<List<Energy>> energy() {
        return manager.getEnergyAll();
    }

    @NonNull
    private Observable<List<Sound>> sounds() {
        return manager.getSoundAll();
    }

    @NonNull
    private Observable<List<DataGenre>> genres() {
        return manager.getGenreAll();
    }

    @NonNull
    private Observable<List<Decade>> decades() {
        return manager.getDecadeAll();
    }

    @NonNull
    private Observable<String> saveMoodTrack(Map<String, String> data) {
        return manager.saveMoodtrack(data);
    }
}
