package com.nutech.burgundy.presenter.livekaraoke;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.api.DangdutAsikService;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.livekaraoke.record.RecordView;
import com.nutech.burgundy.utils.NetworkHelper;
import com.nutech.burgundy.utils.ProgressRequestBody;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import io.antmedia.android.broadcaster.ILiveVideoBroadcaster;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Dafian on 12/5/17
 */

public class RecordPresenter extends BasePresenter<RecordView> implements  ProgressRequestBody.UploadCallbacks {

    private Subscription subscription;
    private DataManager dataManager;
    private NetworkHelper networkHelper;
    private Context mContext;
    private ProgressBar progressBar;
    private TextView tvProgress;

    public RecordPresenter(DataManager dataManager, NetworkHelper networkHelper, Context applicationContext) {
        this.dataManager = dataManager;
        this.networkHelper = networkHelper;
        this.mContext = applicationContext;
    }

    @Override
    public void attachView(RecordView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void sendDataToServer(String roomId) {

        // Create logic in here
        // Send data to server
        // Keep room id in Database

        // Start Live
        if (isViewAttached()) {
            getView().startBroadcast(roomId);
        }
    }

    public void updateStatusVideo(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.updateRoom(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String room) {
                        if (isViewAttached()) {
                            getView().showUpdateRoom(room);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdateRoom(networkHelper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void updateStatusVideoOnCompletedKaraoke(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.updateRoom(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String room) {
                        if (isViewAttached()) {
                            getView().showUpdateRoomOnCompletedKaraoke(room);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdateRoomOnCompletedKaraoke(networkHelper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void updateStatusVideoBeforeExit(Map<String, String> params) {

        Log.i("Masuk Presenter ", "=============================");

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.updateRoom(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String room) {
                        if (isViewAttached()) {
                            getView().showUpdateRoomBeforeExit(room);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdateRoomBeforeExit(networkHelper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void updateStatusUploadVideo(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.updateStatusUploadVideo(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String room) {
                        if (isViewAttached()) {
                            getView().showUpdateRoom(room);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorUpdateRoom(networkHelper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void startLiveStreaming(
            final ILiveVideoBroadcaster liveVideoBroadcaster, final String roomId) {

        if (subscription != null) subscription.unsubscribe();

        if (liveVideoBroadcaster == null) {
            if (isViewAttached()) {
                getView().showError("liveVideoBroadcaster null");
            }
            return;
        }

        Log.e("RTMP", Constants.URL_RTMP);
        Log.e("RoomId", roomId);

        subscription = Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return liveVideoBroadcaster.startBroadcasting(Constants.URL_RTMP + roomId);
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean result) {
                        if (isViewAttached()) {
                            getView().resultBroadcast(result);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError("Error Live Video : " + throwable.getMessage());
                        }
                    }
                });
    }

    public void getDetailKaraoke(String karaokeId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.getDataKaraoke(karaokeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataKaraoke>() {
                    @Override
                    public void call(DataKaraoke dataKaraoke) {
                        if (isViewAttached()) {
                            getView().showDataKaraoke(dataKaraoke);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorKaraoke(networkHelper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void send(String tracksId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getTrackLiricks(tracksId)
        );
    }

    public Subscription getTrackLiricks(String tracksId) {

        return getLiricTracks(tracksId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataLiricks>>() {
                    @Override
                    public void call(List<DataLiricks> dataLiricksList) {
                        if (isViewAttached()) {
                            getView().showLiricks(dataLiricksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorLiricks(networkHelper.getStatusError(throwable));
                        }
                    }
                });

    }

    public void removeRoom(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.removeRoom(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String room) {
                        if (isViewAttached()) {

                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {

                        }
                    }
                });

    }

    @NonNull
    private Observable<List<DataLiricks>> getLiricTracks(String tracksId) {
        return dataManager.getTrackLirick(tracksId);
    }

    public void addRomm(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.addRoom(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String room) {
                        if (isViewAttached()) {
                            getView().showRoom(room);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorRoom(networkHelper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void uploadVideoKaraoke(MultipartBody.Part file) {

        if (subscription != null) subscription.unsubscribe();

        subscription = dataManager.uploadVideoKaraoke(file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String data) {
                        if (isViewAttached()) {
                            getView().uploadVideoDone(data);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().errorUploadVideo(networkHelper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void uploadFile(String pathVideo, String pathMusic, String roomCode, ProgressBar progressBarIndicator,
                           TextView tvMsgLoadingIndicator, String useHeadPhone){

        progressBar = progressBarIndicator;
        tvProgress = tvMsgLoadingIndicator;

        Uri fileUriVideo = Uri.fromFile(new File(pathVideo));
        Uri fileUriMusic = Uri.fromFile(new File(pathMusic));

        String filePathVideo = getRealPathFromUri(fileUriVideo);
        String filePathMusic = getRealPathFromUri(fileUriMusic);
        if (filePathVideo != null && !filePathVideo.isEmpty()) {
            File fileVideo = new File(filePathVideo);
            File fileMusic = new File(filePathMusic);
            if (fileVideo.exists() && fileMusic.exists()) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.URL_DANGDUT_ASIK)
                        .build();

                DangdutAsikService service = retrofit.create(DangdutAsikService.class);

                ProgressRequestBody fileBodyVideo = new ProgressRequestBody(fileVideo, this);
                ProgressRequestBody fileBodyMusic = new ProgressRequestBody(fileMusic, this);
                MultipartBody.Part filePartVideo = MultipartBody.Part.createFormData("video", fileVideo.getName(), fileBodyVideo);
                MultipartBody.Part filePartMusic = MultipartBody.Part.createFormData("music", fileMusic.getName(), fileBodyMusic);

                // creates RequestBody instance from file
//                RequestBody requestFile = RequestBody.create(MediaType.parse(""), file);
                // MultipartBody.Part is used to send also the actual filename
//                MultipartBody.Part body = MultipartBody.Part.createFormData("userfile", file.getName(), fileBody);
                // adds another part within the multipart request
                RequestBody pRoomCode = RequestBody.create(MediaType.parse("multipart/form-data"), roomCode);
                RequestBody puseHeadPhone = RequestBody.create(MediaType.parse("multipart/form-data"), useHeadPhone);

                // executes the request

                Call<ResponseBody> call = service.postFile(filePartVideo, filePartMusic, pRoomCode, puseHeadPhone);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        Log.i("** RecordPresenter **", "success");
                        getView().uploadVideoDone("success");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("** RecordPresenter **", t.getMessage());
                        getView().errorUploadVideo(networkHelper.getStatusError(t));
                    }
                });
            }
        }

    }

    public String getRealPathFromUri(final Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(mContext, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(mContext, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(mContext, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(mContext, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onProgressUpdate(int percentage) {
        progressBar.setProgress(percentage);
        tvProgress.setText(percentage+"/"+"100");
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {
        progressBar.setProgress(100);
    }
}
