package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.genre.GenreView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 8/31/17.
 * Xeranta Mobile Solutions
 */

public class DetailGenrePresenter extends BasePresenter<GenreView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public DetailGenrePresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(GenreView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getGenreTracks(String genreId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getTracksGenre(genreId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataTracksList -> {
                    if (isViewAttached()) {
                        getView().showTracksList(dataTracksList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<List<DataTracks>> getTracksGenre(String genreId) {
        return manager.getGenreTracks(genreId);
    }

}
