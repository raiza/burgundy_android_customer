package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.widget.ImageView;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataVideos;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.video.CategoryVideoView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif-Setiawan on 8/28/17.
 */

public class CategoryVideoPresenter extends BasePresenter<CategoryVideoView> {

    private Subscription subscription;
    private Context context;
    private ImageView imgLikes;

    private DataManager manager;
    private NetworkHelper helper;

    public CategoryVideoPresenter(DataManager manager, NetworkHelper helper, FragmentActivity activity) {
        this.manager = manager;
        this.helper = helper;
        this.context = activity;
    }

    @Override
    public void attachView(CategoryVideoView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getVideosList() {

        if (subscription != null) subscription.unsubscribe();

        subscription = getVideos()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataVideos>>() {
                    @Override
                    public void call(List<DataVideos> videosList) {
                        if (isViewAttached()) {
                            getView().showVideos(videosList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateStatusPoint(String customerId, String activityId, String id) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                updatePointCust(customerId, activityId),
                updateViewersVideos(id)
        );
    }

    private Subscription updatePointCust(String customerId, String activityId) {
        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());
        return updatePoint(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String string) {
                        if (isViewAttached()) {
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    private Subscription updateViewersVideos(String id) {
        Map<String, String> fields = new HashMap<>();
        fields.put("id", id);
        return updateViewers(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String dataPromoSlides) {
                        if (isViewAttached()) {
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    public Subscription updateLikes(Map<String, String> fields, ImageView imglike) {

        imgLikes = imglike;

        return likes(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            System.out.println("+++++ Sukses +++++");
//                            imgLikes.setBackgroundResource(R.drawable.ic_favorite_pink_500_48dp);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            System.out.println("+++++ Gagal +++++");
//                            imgLikes.setBackgroundResource(R.drawable.ic_favorite_pink_500_48dp);
                        }
                    }
                });
    }


    @NonNull
    private Observable<String> updatePoint(Map<String, String> fields) {
        return manager.updatePointCustomer(fields);
    }

    @NonNull
    private Observable<String> updateViewers(Map<String, String> param) {
        return manager.updateViewersVideos(param);
    }

    @NonNull
    private Observable<List<DataVideos>> getVideos() {
        return manager.getVideosAll();
    }

    @NonNull
    private Observable<String> updateView(Map<String, String> param) {
        return manager.updateViewersVideos(param);
    }

    @NonNull
    private Observable<String> likes(Map<String, String> fields) {
        return manager.updateLikesVideos(fields);
    }

}
