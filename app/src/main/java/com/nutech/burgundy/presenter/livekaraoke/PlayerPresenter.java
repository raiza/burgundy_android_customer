package com.nutech.burgundy.presenter.livekaraoke;

import androidx.annotation.NonNull;
import android.widget.ImageView;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.livekaraoke.player.PlayerView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;
import java.util.concurrent.Callable;

import okhttp3.ResponseBody;
import retrofit2.Call;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * @author Dafian on 12/5/17
 */

public class PlayerPresenter extends BasePresenter<PlayerView> {

    private Subscription subscription;
    private DataManager manager;
    private ImageView imgLikes;
    private NetworkHelper helper;

    public PlayerPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        manager = dataManager;
        helper = networkHelper;
    }

    @Override
    public void attachView(PlayerView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void update(Map<String, String> fields, ImageView imglike) {

        imgLikes = imglike;

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                updateLikes(fields)

        );
    }

    public Subscription updateLikes(Map<String, String> fields) {

        return likes(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    public void updateViewers(final Map<String, String> param) {

        if (subscription != null) subscription.unsubscribe();
        subscription = updateView(param)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {

                        if (isViewAttached()) {

                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                        if (isViewAttached()) {

                        }
                    }
                });
    }

    public void sendReport(final Map<String, String> param) {

        if (subscription != null) subscription.unsubscribe();
        subscription = report(param)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {

                        if (isViewAttached()) {
                            getView().successReport("Add Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                        if (isViewAttached()) {
                            getView().errorReport(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void checkVideoExist(final String roomId, final String rtmpUrl){

        Observable.fromCallable(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                Call<ResponseBody> response = manager.checkVideoExist(roomId);
                return response.execute().code();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer code) {
                        if (isViewAttached()) {
                            if (code == 404) {
                                getView().showVideoNotExist();
                            } else if (code == 200) {
                                getView().initPlayer(rtmpUrl);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(throwable.getMessage());
                        }
                    }
                });
    }

    public void getDetailKaraoke(String karaokeId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getDataKaraoke(karaokeId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataKaraoke>() {
                    @Override
                    public void call(DataKaraoke dataKaraoke) {
                        if (isViewAttached()) {
                            getView().showDataKaraoke(dataKaraoke);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showErrorKaraoke(throwable.getMessage());
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> updateView(Map<String, String> param) {
        return manager.updateViewersVideosKaraoke(param);
    }

    @NonNull
    private Observable<String> likes(Map<String, String> fields) {
        return manager.updateLikesVideosKaraoke(fields);
    }

    @NonNull
    private Observable<String> report(Map<String, String> fields) {
        return manager.sendReport(fields);
    }

}
