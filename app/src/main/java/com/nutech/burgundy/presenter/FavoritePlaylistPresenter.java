package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.home.FavoritePlaylistView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 11/15/17.
 * Xeranta Mobile Solutions
 */

public class FavoritePlaylistPresenter extends BasePresenter<FavoritePlaylistView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public FavoritePlaylistPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(FavoritePlaylistView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public Subscription getPlaylistMember(String memberId) {
        return getPlaylistMembers(memberId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataPlaylistMember>>() {
                    @Override
                    public void call(List<DataPlaylistMember> dataPlaylistMember) {
                        if (isViewAttached()) {
                            getView().showSuccess(dataPlaylistMember);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void removePlaylistMember(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = removePLaylist(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().showSuccessRemove("Add Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void updateStatusPoint(String customerId, String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                    }
                });
    }

    @NonNull
    private Observable<List<DataPlaylistMember>> getPlaylistMembers(String memberId) {
        return manager.getPlaylistFavorite(memberId);
    }

    @NonNull
    private Observable<String> removePLaylist(Map<String, String> data) {
        return manager.removePLaylistMember(data);
    }
}
