package com.nutech.burgundy.presenter.soundtrack;

import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.soundtrack.DetailMoodtrackView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Arif Setiawan on 2019-07-17.
 * NuTech (Nusantara Teknologi)
 */
public class DetailMoodtrackPresenter extends BasePresenter<DetailMoodtrackView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public DetailMoodtrackPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(DetailMoodtrackView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getData(Map<String, String> params) {

        if (subscription != null) subscription.unsubscribe();

        subscription = new CompositeSubscription(
                getMoodtrackDetail(params)
        );
    }

    public Subscription getMoodtrackDetail(Map<String, String> params) {
        return detailMoodtrack(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataMoodtrack -> {
                    if (isViewAttached()) {
                        getView().showMoodtracks(dataMoodtrack);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorMoodtracks(helper.getStatusError(throwable));
                    }
                });
    }

    public Subscription updatePositionMoodtrackTracks(String idMood, String trackId,
                                                      String fromPosition, String toPosition) {
        Map<String,String> params = new HashMap<>();
        params.put("moodtrack_id",idMood);
        params.put("track_id",trackId);
        params.put("from_position",fromPosition);
        params.put("to_position",toPosition);

        return updatePositionMoodtrackTrack(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    if (isViewAttached()) {
                        getView().showSuccessUpdatePosition(success);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showErrorUpdatePosition(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<Moodtrack> detailMoodtrack(Map<String, String> params) {
        return manager.detailMoodtrack(params);
    }

    private Observable<String> updatePositionMoodtrackTrack(Map<String,String> params) {
        return manager.updatePositionMoodtrackTrack(params);
    }
}
