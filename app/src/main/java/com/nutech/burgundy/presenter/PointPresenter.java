package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataPoint;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.point.PointView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 11/13/17
 */

public class PointPresenter extends BasePresenter<PointView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public PointPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(PointView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getPointAll() {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getPointAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataPoint>>() {
                    @Override
                    public void call(List<DataPoint> dataPointList) {
                        if (isViewAttached()) {
                            getView().showPointAll(dataPointList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }
}
