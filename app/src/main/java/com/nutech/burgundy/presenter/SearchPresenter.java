package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataSearch;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.search.SearchView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 9/13/17.
 * Xeranta Mobile Solutions
 */

public class SearchPresenter extends BasePresenter<SearchView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public SearchPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(SearchView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getSearchByName(Map<String, String> param) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getSearch(param)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataSearchList -> {
                    if (isViewAttached()) {
                        getView().showResultSearch(dataSearchList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<List<DataSearch>> getSearch(Map<String, String> param) {
        return manager.getSearchByName(param);
    }
}
