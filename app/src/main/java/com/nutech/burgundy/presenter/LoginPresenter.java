package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataLoginMember;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.register.LoginView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 9/2/17.
 * Xeranta Mobile Solutions
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager sessionManager;

    public LoginPresenter(DataManager dataManager, NetworkHelper networkHelper, SessionManager sessionManager,
                          Context applicationContext) {
        this.manager = dataManager;
        this.helper = networkHelper;
        this.sessionManager = sessionManager;
        this.context = applicationContext;
    }

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getLogin(final Map<String, String> dataLogin) {

        if (subscription != null) subscription.unsubscribe();
        subscription = getLoginMember(dataLogin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {

                        if (isViewAttached()) {
//                            getView().showLoginSuccess("Login Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                        if (isViewAttached()) {
//                            getView().showLoginFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                        }
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                    }
                });
    }

    public void checkMemberToIndosis(Map<String, String> params) {
        if (subscription != null) subscription.unsubscribe();
        subscription = checkMember(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataLoginMember -> {
                    MemberCheck memberCheck = new MemberCheck();
                    memberCheck.setStatus(dataLoginMember.getMemberCheck().getStatus());
                    memberCheck.setMsisdn(dataLoginMember.getMemberCheck().getMsisdn());
                    memberCheck.setDtm_start(dataLoginMember.getMemberCheck().getDtm_start());
                    memberCheck.setLast_sucess_dr(dataLoginMember.getMemberCheck().getLast_sucess_dr());
                    memberCheck.setMessage(dataLoginMember.getMemberCheck().getMessage());
                    sessionManager.setDataMember(memberCheck);

                    DataMember dataMember = new DataMember();
                    dataMember.setId(dataLoginMember.getDataMember().getId());
                    dataMember.setName(dataLoginMember.getDataMember().getName());
                    dataMember.setCf1(dataLoginMember.getDataMember().getCf1());
                    dataMember.setCf2(dataLoginMember.getDataMember().getCf2());
                    dataMember.setPhone(dataLoginMember.getDataMember().getPhone());
                    dataMember.setMobile_phone(dataLoginMember.getDataMember().getMobile_phone());
                    dataMember.setEmail(dataLoginMember.getDataMember().getEmail());
                    dataMember.setBirth_date(dataLoginMember.getDataMember().getBirth_date());
                    dataMember.setMember_card_id(dataLoginMember.getDataMember().getMember_card_id());
                    dataMember.setJoin_date(dataLoginMember.getDataMember().getJoin_date());
                    dataMember.setUsername(dataLoginMember.getDataMember().getUsername());
                    dataMember.setActivation_code(dataLoginMember.getDataMember().getActivation_code());
                    dataMember.setAddress(dataLoginMember.getDataMember().getAddress());
                    dataMember.setCity_id(dataLoginMember.getDataMember().getCity_id());
                    dataMember.setState_id(dataLoginMember.getDataMember().getState_id());
                    dataMember.setLat(dataLoginMember.getDataMember().getLat());
                    dataMember.setLng(dataLoginMember.getDataMember().getLng());
                    dataMember.setRegister_type(dataLoginMember.getDataMember().getRegister_type());
                    dataMember.setActive_status(dataLoginMember.getDataMember().getActive_status());
                    dataMember.setRegister_id(dataLoginMember.getDataMember().getRegister_id());
                    dataMember.setSubscribename(dataLoginMember.getDataMember().getSubscribename());
                    dataMember.setImei(dataLoginMember.getDataMember().getImei());
                    dataMember.setPoint(dataLoginMember.getDataMember().getPoint());

                    sessionManager.setDataMember(dataMember);
                    if (!TextUtils.isEmpty(dataLoginMember.getDataMember().getSubscribename())){
                        sessionManager.setPackage(dataLoginMember.getDataMember().getSubscribename());
                    }

                    if (dataLoginMember.getMemberCheck().getStatus().equals("1")){
                        Map<String, String> params1 = new HashMap<String, String>();
                        try {
                            params1.put("customer_id", dataLoginMember.getDataMember().getId());
                            params1.put("start_date", dataLoginMember.getMemberCheck().getDtm_start());
                            params1.put("end_date", dataLoginMember.getMemberCheck().getLast_sucess_dr());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        insertDataLanggananIndosis(params1);
                    }

                    Log.e("view : ",Boolean.toString(isViewAttached()));

                    if (isViewAttached()) {
                        getView().sucessCheckMember(memberCheck, dataMember);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().errorCheckMember(helper.getStatusCode(throwable));
                    }
                });
    }

    public void checkStoreLogin(Map<String, String> params) {
        if (subscription != null) subscription.unsubscribe();
        subscription = storeLogin(params)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userStore -> {

                    sessionManager.setUserStore(userStore);
                    sessionManager.setStore(userStore.getStore());
                    if (userStore.getScheduler() != null) {
                        Helper.SetAllScheduleMoodtrack(context,userStore.getScheduler());
                    }

                    if (userStore.getMoodtrack() != null) {
                        sessionManager.setMoodtrackFilter(userStore.getMoodtrack());
                    }
                    if (isViewAttached()) {
                        getView().sucessLoginStore();
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        Log.e("error",throwable.toString());
                        getView().errorCheckMember(helper.getStatusCode(throwable));
                    }
                });



    }

    public Subscription insertDataLanggananIndosis(Map<String, String> fields) {

        return insertLanggananIndosis(fields)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> Log.e("++ Sukses Insert Data", "Dari Indosis ++"), throwable -> Log.e("++ Gagal Insert", "Dari Indosis ++"));

    }

    @NonNull
    private Observable<String> getLoginMember(Map<String, String> dataLogin) {
        return manager.getLogin(dataLogin);
    }

    @NonNull
    private Observable<DataLoginMember> checkMember(Map<String, String> dataLogin) {
        return manager.checkMember(dataLogin);
    }

    @NonNull
    private Observable<DataUserStore> storeLogin(Map<String, String> dataLogin) {
        return manager.storeLogin(dataLogin);
    }

    @NonNull
    private Observable<String> insertLanggananIndosis(Map<String, String> fields) {
        return manager.insertDataLanggananIndosis(fields);
    }
}
