package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataAbout;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.about.AboutView;
import com.nutech.burgundy.utils.NetworkHelper;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 11/6/17.
 * Xeranta Mobile Solutions
 */

public class AboutPresenter extends BasePresenter<AboutView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public AboutPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(AboutView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getAboutUs() {

        if (subscription != null) subscription.unsubscribe();

        subscription = getAbout()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<DataAbout>() {
                    @Override
                    public void call(DataAbout dataAbout) {
                        if (isViewAttached()) {
                            getView().showData(dataAbout);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<DataAbout> getAbout() {
        return manager.getAboutUs();
    }
}
