package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataReward;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.point.RewardView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;
import java.util.Map;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 11/13/17
 */

public class RewardPresenter extends BasePresenter<RewardView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public RewardPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(RewardView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getRewardAll() {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getRewardAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataReward>>() {
                    @Override
                    public void call(List<DataReward> dataRewardList) {
                        if (isViewAttached()) {
                            getView().showRewardAll(dataRewardList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    public void addPointExchange(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.addPointExchange(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                            getView().successAddPointExchnge(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().errorAddPointExchange(helper.getStatusError(throwable));
                        }
                    }
                });
    }

}
