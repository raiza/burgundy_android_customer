package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.playlist.NewPlaylistView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 11/15/17.
 * Xeranta Mobile Solutions
 */

public class NewPlaylistPresenter extends BasePresenter<NewPlaylistView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public NewPlaylistPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(NewPlaylistView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void saveNewPlaylist(final Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = savePlaylist(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (isViewAttached()) {
                        updateStatusPoint(data.get("customer_id"), Constants.ActivityPoint.NEW_PLAYLIST);
                        getView().success("Add Succes");
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        updateStatusPoint(data.get("customer_id"), Constants.ActivityPoint.NEW_PLAYLIST);
                        getView().error(helper.getStatusError(throwable));
                    }
                });
    }

    private void updateStatusPoint(String customerId, String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> savePlaylist(Map<String, String> data) {
        return manager.savePlaylist(data);
    }

    public void updateNewPlaylist(Map<String, String> data) {

        if (subscription != null) subscription.unsubscribe();

        subscription = updatePlaylist(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        if (isViewAttached()) {
                            getView().success("Add Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().error(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> updatePlaylist(Map<String, String> data) {
        return manager.updatePlaylist(data);
    }
}
