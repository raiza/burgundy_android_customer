package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataOperator;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.register.PinVerifiedView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 12/3/17.
 * Xeranta Mobile Solutions
 */

public class PinVerifiedPresenter extends BasePresenter<PinVerifiedView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;
    private SessionManager session;

    public PinVerifiedPresenter(DataManager dataManager, NetworkHelper networkHelper, SessionManager sessionManager) {
        this.manager = dataManager;
        this.helper = networkHelper;
        this.session = sessionManager;
    }

    @Override
    public void attachView(PinVerifiedView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void otpVerification(Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.otpVerification(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataMemberList -> {
                    if (isViewAttached()) {
                        DataMember dataMember = new DataMember();
                        dataMember.setId(dataMemberList.getId());
                        dataMember.setName(dataMemberList.getName());
                        dataMember.setCf1(dataMemberList.getCf1());
                        dataMember.setCf2(dataMemberList.getCf2());
                        dataMember.setPhone(dataMemberList.getPhone());
                        dataMember.setMobile_phone(dataMemberList.getMobile_phone());
                        dataMember.setEmail(dataMemberList.getEmail());
                        dataMember.setBirth_date(dataMemberList.getBirth_date());
                        dataMember.setMember_card_id(dataMemberList.getMember_card_id());
                        dataMember.setJoin_date(dataMemberList.getJoin_date());
                        dataMember.setUsername(dataMemberList.getUsername());
                        dataMember.setActivation_code(dataMemberList.getActivation_code());
                        dataMember.setAddress(dataMemberList.getAddress());
                        dataMember.setCity_id(dataMemberList.getCity_id());
                        dataMember.setState_id(dataMemberList.getState_id());
                        dataMember.setLat(dataMemberList.getLat());
                        dataMember.setLng(dataMemberList.getLng());
                        dataMember.setRegister_type(dataMemberList.getRegister_type());
                        dataMember.setActive_status(dataMemberList.getActive_status());
                        dataMember.setRegister_id(dataMemberList.getRegister_id());
                        dataMember.setPoint(dataMemberList.getPoint());
                        dataMember.setSubscribename(dataMemberList.getSubscribename());
                        dataMember.setImei(dataMemberList.getImei());
                        dataMember.setPoint(dataMemberList.getPoint());

                        session.setDataMember(dataMember);
                        session.login();
                        updateStatusPoint(Constants.ActivityPoint.LOGIN);
                        getView().showSuccess(dataMember);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    public void getLogin(final Map<String, String> dataLogin) {

        if (subscription != null) subscription.unsubscribe();
        subscription = getLoginMember(dataLogin)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {

                        if (isViewAttached()) {
                            getView().successResend("Resend Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                        if (isViewAttached()) {
                            getView().errorResend(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
//                            getView().showRegisterSuccess(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
//                            getView().showRegisterFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    public void getOperator(String number) {

        if (subscription != null) subscription.unsubscribe();
        subscription = getOperatorByNumber(number)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataOperator>>() {
                    @Override
                    public void call(List<DataOperator> dataOperator) {
                        if (isViewAttached()) {
                            getView().showOperator(dataOperator);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                        if (isViewAttached()) {
                            getView().showErrorOperator(throwable.getMessage());
                        }
                    }
                });
    }

    public void updateStatusPoint(String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", session.getDataMember().getId());
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> getLoginMember(Map<String, String> dataLogin) {
        return manager.getLogin(dataLogin);
    }

    @NonNull
    private Observable<List<DataOperator>> getOperatorByNumber(String number) {
        return manager.getOperator(number);
    }
}
