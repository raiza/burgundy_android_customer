package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.album.AlbumView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 8/31/17.
 * Xeranta Mobile Solutions
 */

public class AlbumsPresenter extends BasePresenter<AlbumView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public AlbumsPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(AlbumView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getAlbumsTracks(String albumId) {

        if (subscription != null) subscription.unsubscribe();

        subscription = getTracksAlbum(albumId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(dataTracksList -> {
                    if (isViewAttached()) {
                        getView().showDataAlbumsTracks(dataTracksList);
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().showError(helper.getStatusError(throwable));
                    }
                });
    }

    @NonNull
    private Observable<List<DataTracks>> getTracksAlbum(String albumId) {
        return manager.getAlbumsTracks(albumId);
    }

}
