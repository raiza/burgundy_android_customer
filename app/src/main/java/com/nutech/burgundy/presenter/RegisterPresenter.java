package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.register.RegisterView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 9/2/17.
 * Xeranta Mobile Solutions
 */

public class RegisterPresenter extends BasePresenter<RegisterView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public RegisterPresenter(DataManager dataManager, NetworkHelper networkHelper,
                             SessionManager sessionManager, Context ctx) {
        this.manager = dataManager;
        this.helper = networkHelper;
        this.context = ctx;
    }

    @Override
    public void attachView(RegisterView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void Register(final Map<String, String> fields) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.postRegister(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
                            getView().showRegisterSuccess(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showRegisterFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    public void sendSms(String phonenumber, String text) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.sendSms(phonenumber, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String success) {
                        if (isViewAttached()) {
//                            getView().showRegisterSuccess(success);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
//                            getView().showRegisterFailed(helper.getStatusCode(throwable));
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> postRegisterMember(Map<String, String> dataRegister) {
        return manager.postRegister(dataRegister);
    }
}
