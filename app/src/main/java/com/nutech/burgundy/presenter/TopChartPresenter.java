package com.nutech.burgundy.presenter;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.topchart.TopChartView;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * @author Dafian on 10/4/17
 */

public class TopChartPresenter extends BasePresenter<TopChartView> {

    private Subscription subscription;

    private DataManager manager;
    private NetworkHelper helper;

    public TopChartPresenter(DataManager manager, NetworkHelper helper) {
        this.manager = manager;
        this.helper = helper;
    }

    @Override
    public void attachView(TopChartView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void getTopChart(Integer limit) {

        if (subscription != null) subscription.unsubscribe();

        subscription = manager.getTopChart(limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DataTracks>>() {
                    @Override
                    public void call(List<DataTracks> tracksList) {
                        if (isViewAttached()) {
                            getView().showTopChart(tracksList);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }
}
