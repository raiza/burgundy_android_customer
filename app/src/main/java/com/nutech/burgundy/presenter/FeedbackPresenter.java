package com.nutech.burgundy.presenter;

import android.content.Context;
import androidx.annotation.NonNull;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.ui.base.BasePresenter;
import com.nutech.burgundy.ui.view.feedback.FeedbackView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Arif Setiawan on 10/31/17.
 * Xeranta Mobile Solutions
 */

public class FeedbackPresenter extends BasePresenter<FeedbackView> {

    private Subscription subscription;
    private Context context;

    private DataManager manager;
    private NetworkHelper helper;

    public FeedbackPresenter(DataManager dataManager, NetworkHelper networkHelper) {
        this.manager = dataManager;
        this.helper = networkHelper;
    }

    @Override
    public void attachView(FeedbackView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (subscription != null) subscription.unsubscribe();
    }

    public void sendFeedback(final Map<String, String> dataFeedback) {

        if (subscription != null) subscription.unsubscribe();
        subscription = getLoginMember(dataFeedback)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String data) {

                        if (isViewAttached()) {
                            updateStatusPoint(dataFeedback.get("customer_id"), Constants.ActivityPoint.SEND_FEEDBACK);
                            getView().showSuccess("Succes");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            updateStatusPoint(dataFeedback.get("customer_id"), Constants.ActivityPoint.SEND_FEEDBACK);
                            getView().showError(helper.getStatusError(throwable));
                        }
                    }
                });
    }

    private void updateStatusPoint(String customerId, String activityId) {

        if (subscription != null) subscription.unsubscribe();

        Map<String, String> fields = new HashMap<>();
        fields.put("customer_id", customerId);
        fields.put("activity_id", activityId);
        fields.put("activity_date", Helper.todayDate());

        subscription = manager.updatePointCustomer(fields)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        // Success
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (isViewAttached()) {
                            //error
                        }
                    }
                });
    }

    @NonNull
    private Observable<String> getLoginMember(Map<String, String> dataFeedback) {
        return manager.sendFeedback(dataFeedback);
    }
}
