package com.nutech.burgundy.lib.jcplayer;

import com.nutech.burgundy.data.model.DataAds;
import com.nutech.burgundy.ui.base.BaseView;

import java.util.List;

public interface AdsPlayView extends BaseView {
    void showAdsList(List<DataAds> dataAdsList, boolean isContinue, boolean isPrevious, int between);
    void showErrorAdsList(String error, boolean isContinue,boolean isPrevious);
}
