package com.nutech.burgundy.lib.gps;

import android.location.Location;

public interface IOnlocationChanged {
    void onLocationChanged(Location location);
}
