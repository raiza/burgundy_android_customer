package com.nutech.burgundy.lib.cast;

import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.Session;
import com.google.android.gms.cast.framework.SessionManagerListener;

public class SessionManagerListenerImpl implements SessionManagerListener {


    @Override
    public void onSessionStarting(Session session) {

    }

    @Override
    public void onSessionStarted(Session session, String s) {

    }

    @Override
    public void onSessionStartFailed(Session session, int i) {

    }

    @Override
    public void onSessionEnding(Session session) {

    }

    @Override
    public void onSessionEnded(Session session, int i) {

    }

    @Override
    public void onSessionResuming(Session session, String s) {

    }

    @Override
    public void onSessionResumed(Session session, boolean b) {

    }

    @Override
    public void onSessionResumeFailed(Session session, int i) {

    }

    @Override
    public void onSessionSuspended(Session session, int i) {

    }
}
