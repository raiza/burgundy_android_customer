package com.nutech.burgundy.lib.jcplayer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.lib.jcplayer.JcPlayerExceptions.AudioListNullPointerException;
import com.nutech.burgundy.utils.Helper;

import java.util.Objects;

public class JcPlayerNotificationReceiver extends BroadcastReceiver {
    SessionManager sessionManager;
    public JcPlayerNotificationReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        JcAudioPlayer jcAudioPlayer = JcAudioPlayer.getInstance();

        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        }

        String action = "";

        System.out.println("**** BroadcastReceiver intent"+intent);

        if (intent.hasExtra(JcNotificationPlayerService.ACTION)) {
            action = intent.getStringExtra(JcNotificationPlayerService.ACTION);
        }

        switch (Objects.requireNonNull(action)) {
            case JcNotificationPlayerService.PLAY:
                try {
                    jcAudioPlayer.continueAudio();
                    jcAudioPlayer.updateNotification();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case JcNotificationPlayerService.PAUSE:
                try {
                    if(jcAudioPlayer != null) {
                        jcAudioPlayer.pauseAudio();
                        jcAudioPlayer.updateNotification();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case JcNotificationPlayerService.SHUFFLE:
                try {
                    if(jcAudioPlayer != null) {
                        if (jcAudioPlayer.isShuffle()) {
                            jcAudioPlayer.setShuffle(false);
                            jcAudioPlayer.updateNotification();
                        } else {
                            jcAudioPlayer.setShuffle(true);
                            jcAudioPlayer.updateNotification();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case JcNotificationPlayerService.NEXT:
                try {
                    Intent moodtrackIntent = new Intent("moodtrack_controls");
                    moodtrackIntent.putExtra("moodtrack_id",sessionManager.getCurrentMoodTrack());
                    moodtrackIntent.putExtra("store_id",sessionManager.getUserStore().getStore_id());
                    moodtrackIntent.putExtra("status", Constants.MoodtrackControl.NEXT);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(moodtrackIntent);
//                    jcAudioPlayer.nextAudio();
                } catch (AudioListNullPointerException e) {
                    try {
                        jcAudioPlayer.continueAudio();
                    } catch (AudioListNullPointerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;

            case JcNotificationPlayerService.PREVIOUS:
                try {
                    Intent moodtrackIntent = new Intent("moodtrack_controls");
                    moodtrackIntent.putExtra("moodtrack_id",sessionManager.getCurrentMoodTrack());
                    moodtrackIntent.putExtra("store_id",sessionManager.getUserStore().getStore_id());
                    moodtrackIntent.putExtra("status", Constants.MoodtrackControl.PREVIOUS);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(moodtrackIntent);
//                    jcAudioPlayer.previousAudio();
                } catch (Exception e) {
                    try {
                        jcAudioPlayer.continueAudio();
                    } catch (AudioListNullPointerException e1) {
                        e1.printStackTrace();
                    }
                }
                break;
        }
    }
}
