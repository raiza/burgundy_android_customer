package com.nutech.burgundy.lib.schedulerMoodtrack;

import android.app.IntentService;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.ui.view.PrimaryActivity;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NotificationUtils;

import java.util.Objects;

public class SchedulerMoodtackService extends IntentService {
    private SessionManager sessionManager;

    public SchedulerMoodtackService() {
        super("Schedule Moodtrack");
    }

    private void handleDataMessage(Context context, String moodtrackId, String storeId,
                                   String status,int requestCode,boolean repeat) {

        if (sessionManager == null) {
            sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
        }

        try {

            if (!NotificationUtils.isAppIsInBackground(context)) {

                if (sessionManager.isLoggedIn()) {
                    broadcastMoodtrackControls(context,moodtrackId,storeId,status,requestCode,repeat);
                }

            } else {

                if (!NotificationUtils.isActivityIsInBackground(context)) {
                    if (sessionManager.isLoggedIn()) {
                        checkTypeControlsInBackground(context,moodtrackId,storeId,status, requestCode, repeat);
                    }
                } else {
                    KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                    if( Objects.requireNonNull(myKM).inKeyguardRestrictedInputMode()) {
                        if (sessionManager.isLoggedIn()) {
                            checkTypeControlsInBackground(context,moodtrackId,storeId,status, requestCode, repeat);
                        }

                    } else {
                        if (sessionManager.isLoggedIn()) {
                            checkTypeControlsInBackground(context,moodtrackId,storeId,status,requestCode,repeat);
                        }
                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkTypeControlsInBackground(Context context,String moodtrackId, String storeId,
                                               String status, int requestCode,boolean repeat) {

        if (!status.equals(Constants.MoodtrackControl.PLAY)) {
            broadcastMoodtrackControls(context,moodtrackId,storeId,status, requestCode,repeat);
        } else {
            startActivityMoodtrackControls(context,moodtrackId,storeId,status, requestCode,repeat);
        }
    }

    private void broadcastMoodtrackControls(Context context,String moodtrackId, String storeId,
                                            String status,int requestCode, boolean repeat) {
        Intent moodtrackIntent = new Intent("moodtrack_controls");
        moodtrackIntent.putExtra("moodtrack_id",moodtrackId);
        moodtrackIntent.putExtra("store_id",storeId);
        moodtrackIntent.putExtra("status",status);
        moodtrackIntent.putExtra("repeat",repeat);
        moodtrackIntent.putExtra("request_code",requestCode);
        LocalBroadcastManager.getInstance(context).sendBroadcast(moodtrackIntent);
    }

    private void startActivityMoodtrackControls(Context context, String moodtrackId,
                                                String storeId, String status,
                                                int requestCode,boolean repeat) {
        Intent moodtrackIntent = new Intent(context, PrimaryActivity.class);

        moodtrackIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        moodtrackIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        moodtrackIntent.putExtra("moodtrack_id",moodtrackId);
        moodtrackIntent.putExtra("store_id",storeId);
        moodtrackIntent.putExtra("status",status);
        moodtrackIntent.putExtra("repeat",repeat);
        moodtrackIntent.putExtra("request_code",requestCode);

        context.startActivity(moodtrackIntent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null) {
            String idMood = intent.getStringExtra("moodtrack_id");
            String storeId = intent.getStringExtra("store_id");
            String status = intent.getStringExtra("status");
            String dayOfWeek = intent.getStringExtra("day_of_week");
            boolean repeat = intent.getBooleanExtra("repeat",false);
            String timer = intent.getStringExtra("timer");
            int requestCode = intent.getIntExtra("request_code",0);

            handleDataMessage(getApplicationContext(),idMood,storeId,status, requestCode, repeat);

            if (repeat && sessionManager.isLoggedIn()) {
                Helper.setSchedulerMoodtrack(getApplicationContext(),requestCode,
                        storeId,idMood, dayOfWeek,timer, true);
            } else {
                Helper.cancelScheduleMoodtrack(getApplicationContext(),requestCode,
                        storeId,idMood, dayOfWeek,timer, false);
            }


        }
    }
}
