package com.nutech.burgundy.lib.jcplayer;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.NotificationTarget;
import com.nutech.burgundy.R;
import com.nutech.burgundy.config.Constants;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by jean on 12/07/16.
 */
class JcNotificationPlayerService implements JcPlayerView.JcPlayerViewServiceListener {
    //Controls
    static final String NEXT = "NEXT";
    static final String PREVIOUS = "PREVIOUS";
    static final String PAUSE = "PAUSE";
    static final String PLAY = "PLAY";
    static final String SHUFFLE = "SHUFFLE";
    static final String ACTION = "ACTION";
    static final String PLAYLIST = "PLAYLIST";
    static final String CURRENT_AUDIO = "CURRENT_AUDIO";

    //Image Type
    static final String IMAGE_RESOURCE = "IMAGE_RESOURCE";
    static final String IMAGE_URL = "IMAGE_URL";

    private static final int NOTIFICATION_ID = 100;
    private static final int NEXT_ID = 0;
    private static final int PREVIOUS_ID = 1;
    private static final int PLAY_ID = 2;
    private static final int PAUSE_ID = 3;
    private static final int SHUFFLE_ID = 4;

    private NotificationManager notificationManager;
    private Context context;
    private String title;
    private String artist;
    private String time  = "00:00";
    private int iconResource;
    private int imageResource;
    private String imageType;
    private String urlImage;
    private Notification notification;
    private NotificationChannel notificationChannel;
    private NotificationCompat.Builder notificationCompat;
    private Handler handler;

    public JcNotificationPlayerService(Context context){
        this.context = context;
    }

    public void createNotificationPlayer(String title,String artist, int imageResourceResource, int iconResourceResource) {
        this.title = title;
        this.artist = artist;
        this.iconResource = iconResourceResource;
        this.imageResource = imageResourceResource;
        this.imageType = IMAGE_RESOURCE;
        notificationPlayer();
    }

    public void createNotificationPlayer(String title,String artist, String imageResourceResource, int iconResourceResource) {
        this.title = title;
        this.artist = artist;
        this.iconResource = iconResourceResource;
        this.urlImage = imageResourceResource;
        this.imageType = IMAGE_URL;
        notificationPlayer();
    }

    @SuppressLint("WrongConstant")
    private void notificationPlayer() {
        handler = new Handler(Looper.getMainLooper());
        Intent openUi = new Intent(context, context.getClass());
        openUi.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        try {
            JcAudioPlayer.getInstance().registerNotificationListener(this);
        }catch (Exception e) {
            e.printStackTrace();
        }


        if (notificationManager == null) {
            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(Constants.Key.NOTIF_CHANNEL_ID,
                    Constants.Key.NOTIF_CHANNEL_ID,
                    NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setName(Constants.Key.NOTIF_CHANNEL_ID);
            notificationChannel.setDescription(Constants.Key.NOTIF_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationCompat = new NotificationCompat.Builder(context)
                //TODO: Set to API below Build.VERSION.SDK_INT
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setSmallIcon(iconResource)
                .setContentTitle("")
                .setContentText("")
                .setPriority(Notification.PRIORITY_MAX)
                .setChannelId(Constants.Key.NOTIF_CHANNEL_ID)
                .setOngoing(true)
                .setCustomBigContentView(createNotificationPlayerView())
                .setContentIntent(PendingIntent.getActivity(context, NOTIFICATION_ID, openUi, PendingIntent.FLAG_CANCEL_CURRENT))
                .setCategory(Notification.CATEGORY_REMINDER);

        notification = notificationCompat.build();

        notificationManager.notify(NOTIFICATION_ID, notification);

        try {
            JcAudioPlayer.getInstance().registerNotification(NOTIFICATION_ID,notification);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateNotification() {
        if (imageType.contains(IMAGE_RESOURCE)) {
            createNotificationPlayer(title,artist, imageResource, iconResource);
        } else {
            createNotificationPlayer(title,artist, urlImage, iconResource);
        }
    }

    public void removeNotification() {
        if (notificationManager != null) {
            notificationManager.cancelAll();
            notificationManager.cancel(NOTIFICATION_ID);
        }
    }

    public Notification getNotification() {
        return notificationCompat.build();
    }

    private RemoteViews createNotificationPlayerView() {
        RemoteViews remoteView = null;

        try {
            if (!JcAudioPlayer.getInstance().isPlaying()) {
                remoteView = new RemoteViews(context.getPackageName(), R.layout.notification_pause_update);
                remoteView.setOnClickPendingIntent(R.id.btn_pause_notification, buildPendingIntent(PLAY, PLAY_ID));
                remoteView.setInt(R.id.btn_pause_notification, "setBackgroundResource",R.drawable.ic_play_arrow);
            } else {
                remoteView = new RemoteViews(context.getPackageName(), R.layout.notification_pause_update);
                remoteView.setOnClickPendingIntent(R.id.btn_pause_notification, buildPendingIntent(PAUSE, PAUSE_ID));
                remoteView.setInt(R.id.btn_pause_notification,"setBackgroundResource",R.drawable.ic_pause_white);
            }



            remoteView.setTextViewText(R.id.txt_current_music_notification, title);
            remoteView.setTextViewText(R.id.txt_current_artist_notification, artist);
            remoteView.setTextViewText(R.id.txt_duration_notification, time);

            if (imageType.contains(IMAGE_RESOURCE)) {
                remoteView.setImageViewResource(R.id.icon_player, imageResource);
            } else {
                NotificationTarget notificationTarget = new NotificationTarget(
                        context,
                        R.id.icon_player,
                        remoteView,
                        notification,
                        NOTIFICATION_ID);

                Glide.with(context)
                        .asBitmap()
                        .load(urlImage)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                        .error(R.mipmap.ic_launcher)
                        .into(notificationTarget );
            }

            remoteView.setOnClickPendingIntent(R.id.btn_next_notification, buildPendingIntent(NEXT, NEXT_ID));
            remoteView.setOnClickPendingIntent(R.id.btn_prev_notification, buildPendingIntent(PREVIOUS, PREVIOUS_ID));
            remoteView.setOnClickPendingIntent(R.id.iv_shuffle, buildPendingIntent(SHUFFLE, SHUFFLE_ID));

            if (JcAudioPlayer.getInstance().isShuffle()) {
                remoteView.setImageViewResource(R.id.iv_shuffle,R.drawable.ic_shuffle_accent_sec);
            } else {
                remoteView.setImageViewResource(R.id.iv_shuffle,R.drawable.ic_shuffle_white);
            }
        } catch (Exception e){
            e.printStackTrace();
        }


        return remoteView;
    }

    private PendingIntent buildPendingIntent(String action, int id) {
        Intent playIntent = new Intent(context.getApplicationContext(), JcPlayerNotificationReceiver.class);
        playIntent.putExtra(ACTION, action);

        return PendingIntent.getBroadcast(context.getApplicationContext(), id, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
        handler.postDelayed(this::updateNotification,500);
    }

    @Override
    public void onCompletedAudio() {
        handler.postDelayed(this::updateNotification,500);
    }

    @Override
    public void removeFirstAds() {

    }

    @Override
    public void onPaused() {
        handler.postDelayed(this::updateNotification,500);
    }

    @Override
    public void onContinueAudio() {
        handler.postDelayed(this::updateNotification,500);
    }

    @Override
    public void onPlaying() {
        handler.postDelayed(this::updateNotification,500);
    }

    @Override
    public void addLogAds(String id,String trackId) {

    }

    @Override
    public void onTimeChanged(long currentTime) {
        long aux = currentTime / 1000;
        int minutes = (int) (aux / 60);
        int seconds = (int) (aux % 60);
        final String sMinutes = minutes < 10 ? "0" + minutes : minutes + "";
        final String sSeconds = seconds < 10 ? "0" + seconds : seconds + "";
        this.time = sMinutes + ":" + sSeconds;
        updateNotification();
//        if (imageType.contains(IMAGE_RESOURCE)) {
//            createNotificationPlayer(title,artist, imageResource, iconResource);
//        } else {
//            createNotificationPlayer(title,artist, urlImage, iconResource);
//        }
    }

    @Override
    public void updateTitle(String title, String artistName) {
        if (imageType.contains(IMAGE_RESOURCE)) {
            createNotificationPlayer(title,artistName, imageResource, iconResource);
        } else {
            createNotificationPlayer(title,artistName, urlImage, iconResource);
        }
    }

    @Override
    public void updateCover(String cover) {
        urlImage = cover;
    }

    @Override
    public void updateAudioId(String id) {

    }

    @Override
    public void onShuffle(boolean val) {
        handler.postDelayed(this::updateNotification,500);
    }

    public void destroyNotificationIfExists() {
        if (notificationManager != null) {
            try {
                notificationManager.cancel(NOTIFICATION_ID);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}