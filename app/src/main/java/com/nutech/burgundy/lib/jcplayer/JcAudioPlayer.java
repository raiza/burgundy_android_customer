package com.nutech.burgundy.lib.jcplayer;

import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;
import com.nutech.burgundy.lib.jcplayer.JcPlayerExceptions.AudioListNullPointerException;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

/**
 * Created by jean on 12/07/16.
 */

class JcAudioPlayer {
    private JcPlayerService jcPlayerService;
    private JcPlayerView.JcPlayerViewServiceListener listener;
    private JcPlayerView.OnInvalidPathListener invalidPathListener;
    private JcPlayerView.JcPlayerViewStatusListener statusListener;
    private JcNotificationPlayerService jcNotificationPlayer;
    private List<JcAudio> playlist;
    private JcAudio currentJcAudio;
    private int currentPositionList = -1;
    private Context context;
    private static JcAudioPlayer instance = null;
    private boolean mBound = false;
    private boolean playing;
    private boolean shuffle;
    private boolean paused;
    private int waitingNext = 0;
    private int waitingPrevious = 0;
    private int position = 1;
    private int waitBetweenAds = new Random().nextInt(5);

    public JcAudioPlayer(Context context, List<JcAudio> playlist, JcPlayerView.JcPlayerViewServiceListener listener) {
        this.context = context;
        this.playlist = playlist;
        this.listener = listener;
        instance = JcAudioPlayer.this;
        this.jcNotificationPlayer = new JcNotificationPlayerService(context);
        this.shuffle = false;

        initService();
        if (currentJcAudio != null) {
            updatePositionAudioList();
        }
    }

    public void setInstance(JcAudioPlayer instance) {
        JcAudioPlayer.instance = instance;
    }

    public void registerNotificationListener(JcPlayerView.JcPlayerViewServiceListener notificationListener) {
        this.listener = notificationListener;
        if (jcNotificationPlayer != null) {
            jcPlayerService.registerNotificationListener(notificationListener);
        }
    }

    public void registerNotification(int notification_id, Notification notification) {
        if (jcNotificationPlayer != null) {
            jcPlayerService.registerNotification(notification_id,notification);
        }
    }

    public void registerInvalidPathListener(JcPlayerView.OnInvalidPathListener registerInvalidPathListener) {
        this.invalidPathListener = registerInvalidPathListener;
        if (jcPlayerService != null) {
            jcPlayerService.registerInvalidPathListener(invalidPathListener);
        }
    }

    public void registerServiceListener(JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener) {
        this.listener = jcPlayerServiceListener;
        if (jcPlayerService != null) {
            jcPlayerService.registerServicePlayerListener(jcPlayerServiceListener);
        }
    }

    public void registerStatusListener(JcPlayerView.JcPlayerViewStatusListener statusListener) {
        this.statusListener = statusListener;
        if (jcPlayerService != null) {
            jcPlayerService.registerStatusListener(statusListener);
        }
    }

    public static JcAudioPlayer getInstance() {
        return instance;
    }

    public void playAds(String url,String id,String trackId) {
        jcPlayerService.playAds(url,id,trackId);
    }

    public boolean isIklan() {
        return jcPlayerService.isIklan();
    }

    public int getWaitBetweenAds() {
        return waitBetweenAds;
    }

    public void setWaitBetweenAds(int val) {
        waitBetweenAds = val;
    }

    public void playAudio(JcAudio JcAudio) throws AudioListNullPointerException {
        if (playlist == null || playlist.size() == 0) {
            throw new AudioListNullPointerException();
        }

        if (currentJcAudio != null) {
            if (currentJcAudio.getId() == JcAudio.getId()) {
                currentJcAudio = JcAudio;
            } else {
                if (shuffle) {
                    if (getWaitBetweenAds() < 1) {
                        currentJcAudio = JcAudio;
                    } else {
                        currentJcAudio = playlist.get(new Random().nextInt(playlist.size()));
                    }
                } else {
                    currentJcAudio = JcAudio;
                }
            }
        } else {
            currentJcAudio = JcAudio;
        }

        try {
            jcPlayerService.play(currentJcAudio);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (getWaitBetweenAds() > 0) {
            updatePositionAudioList();
        }
        playing = true;
        paused = false;
    }

    public void setShuffle(boolean val) {
        shuffle = val;
        jcPlayerService.onShuffle(val);
    }

    public boolean isShuffle() {
        return this.shuffle;
    }

    private void initService(){
        if (!mBound) {
            startJcPlayerService();
        } else {
            mBound = true;
        }
    }

    public void nextAudio() {
        nextAudio(waitingNext);
    }

    public void nextAudio(int waitNext) throws AudioListNullPointerException {
        if (playlist == null || playlist.size() == 0) {
            throw new AudioListNullPointerException();
        } else {
            if (currentJcAudio != null) {
                try {
                    JcAudio nextJcAudio;
                    if (shuffle) {
                        nextJcAudio = playlist.get(new Random().nextInt(playlist.size()));
                        this.currentJcAudio = nextJcAudio;
                        jcPlayerService.stop();
                        jcPlayerService.play(nextJcAudio);
                    } else {
                        if (currentPositionList+position+waitNext > playlist.size()-1) {
                            if (!isPlaying()) {
                                this.currentJcAudio = null;
                                jcPlayerService.stop();
//                                jcPlayerService.play(this.currentJcAudio);
                            }
                        } else {
                            nextJcAudio = playlist.get(currentPositionList + position + waitNext);
                            this.currentJcAudio = nextJcAudio;
                            jcPlayerService.stop();
                            jcPlayerService.play(this.currentJcAudio);
                        }
                    }

                } catch (IndexOutOfBoundsException e) {
                    jcPlayerService.play(playlist.get(0));


//                    if (shuffle) {
//                        playAudio(playlist.get(new Random().nextInt(playlist.size())));
//                    } else {
//                        playAudio(playlist.get(0));
//                    }
                    e.printStackTrace();
                }

               if (currentJcAudio != null) {
                   if (getWaitBetweenAds() > 0) {
                       updatePositionAudioList();
                   }
                   playing = true;
                   paused = false;
               }
            } else {
                try {
                    JcAudio nextJcAudio;
                    if (shuffle) {
                        nextJcAudio = playlist.get(new Random().nextInt(playlist.size()));
                        this.currentJcAudio = nextJcAudio;
                        jcPlayerService.stop();
                        jcPlayerService.play(nextJcAudio);
                    } else {
                        this.currentJcAudio = playlist.get(0);
                        jcPlayerService.stop();
                        jcPlayerService.play(this.currentJcAudio);
                    }

                } catch (IndexOutOfBoundsException e) {
                    jcPlayerService.play(playlist.get(0));


//                    if (shuffle) {
//                        playAudio(playlist.get(new Random().nextInt(playlist.size())));
//                    } else {
//                        playAudio(playlist.get(0));
//                    }
                    e.printStackTrace();
                }

                updatePositionAudioList();
                playing = true;
                paused = false;
            }
        }
    }

    public void previousAudio() {
        previousAudio(waitingPrevious);
    }

    public void previousAudio(int waitPrev) throws AudioListNullPointerException {
        if (playlist == null || playlist.size() == 0) {
            throw new AudioListNullPointerException();
        } else {
            if (currentJcAudio != null) {
                try {
                    JcAudio previousJcAudio = playlist.get(currentPositionList - position - waitPrev);
                    this.currentJcAudio = previousJcAudio;
                    jcPlayerService.stop();
                    jcPlayerService.play(previousJcAudio);

                } catch (IndexOutOfBoundsException e) {
                    playAudio(playlist.get(0));
                    e.printStackTrace();
                }
            }

            if (getWaitBetweenAds() > 0) {
                updatePositionAudioList();
            }
            playing = true;
            paused = false;
        }
    }

    public void pauseAudio() {
        jcPlayerService.pause(currentJcAudio);
        paused = true;
        playing = false;
    }

    public void continueAudio() throws AudioListNullPointerException {
        if (playlist == null || playlist.size() == 0) {
            throw new AudioListNullPointerException();
        } else {
            if (currentJcAudio == null) {
                currentJcAudio = playlist.get(0);
            }
            playAudio(currentJcAudio);
//            playing = true;
//            paused = false;
        }
    }

    public void createNewNotification(int iconResource, int ic_stat_logo_dm_notitle) {
        if (currentJcAudio != null) {
            jcNotificationPlayer.createNotificationPlayer(currentJcAudio.getTitle(), currentJcAudio.getArtistName(), iconResource, ic_stat_logo_dm_notitle);
        }
    }

    public void createNewNotification(String iconResource, int ic_stat_logo_dm_notitle) {
        if (currentJcAudio != null) {
            jcNotificationPlayer.createNotificationPlayer(currentJcAudio.getTitle(), currentJcAudio.getArtistName(), iconResource, ic_stat_logo_dm_notitle);
        }
    }

    public void updateNotification() {
        jcNotificationPlayer.updateNotification();
    }

    public void seekTo(int time) {
        if (jcPlayerService != null) {
            jcPlayerService.seekTo(time);
        }
    }

    private void updatePositionAudioList() {
        Gson gson = new Gson();
        for (int i = 0; i < playlist.size(); i++) {
            if (playlist.get(i).getId() == currentJcAudio.getId()) {
                this.currentPositionList = i;
            }
        }
    }

    private synchronized void startJcPlayerService() {
        if (!mBound) {
            Intent intent = new Intent(context.getApplicationContext(), JcPlayerService.class);
            intent.putExtra(JcNotificationPlayerService.PLAYLIST, (Serializable) playlist);
            intent.putExtra(JcNotificationPlayerService.CURRENT_AUDIO, currentJcAudio);
            context.bindService(intent, mConnection, context.getApplicationContext().BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            JcPlayerService.JcPlayerServiceBinder binder = (JcPlayerService.JcPlayerServiceBinder) service;
            jcPlayerService = binder.getService();

            if (listener != null) {
                jcPlayerService.registerServicePlayerListener(listener);
            }
            if (invalidPathListener != null) {
                jcPlayerService.registerInvalidPathListener(invalidPathListener);
            }
            if (statusListener != null) {
                jcPlayerService.registerStatusListener(statusListener);
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
            playing = false;
            paused = true;
        }
    };

    boolean isPaused() {
        return paused;
    }

    boolean isPlaying() {
        boolean bool = false;
        try {
            bool = jcPlayerService.isPlaying();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bool;
    }

    void removeNotification() {
        Log.e("berhasil","1");
        jcNotificationPlayer.removeNotification();
    }

    public void kill() {
        if (jcPlayerService != null) {
            jcPlayerService.stop();
            jcPlayerService.destroy();
            playing = false;
            paused = true;
        }

        if (jcNotificationPlayer != null) {
            jcNotificationPlayer.removeNotification();
            jcNotificationPlayer.destroyNotificationIfExists();
        }

        if (mBound)
            try {
                context.unbindService(mConnection);
            } catch (IllegalArgumentException e) {
                //TODO: Add readable exception here
            }

        if (JcAudioPlayer.getInstance() != null)
            JcAudioPlayer.getInstance().setInstance(null);
    }

    public List<JcAudio> getPlaylist() {
        return playlist;
    }

    public void setPlayList(List<JcAudio> playList) {
        this.playlist = playList;
        updatePositionAudioList();
    }

    public JcAudio getCurrentAudio() {
        return jcPlayerService.getCurrentAudio();
    }
}
