package com.nutech.burgundy.lib.schedulerMoodtrack;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.legacy.content.WakefulBroadcastReceiver;

import com.nutech.burgundy.data.DataManager;
import com.nutech.burgundy.data.SessionManager;
import com.nutech.burgundy.data.api.AntMediaService;
import com.nutech.burgundy.data.api.AntMediaServiceFactory;
import com.nutech.burgundy.data.api.DangdutAsikService;
import com.nutech.burgundy.data.api.DangdutAsikServiceFactory;
import com.nutech.burgundy.data.api.MelonService;
import com.nutech.burgundy.data.api.MelonServiceFactory;
import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.presenter.SchedulesPresenter;
import com.nutech.burgundy.ui.view.schedule.ScheduleView;
import com.nutech.burgundy.utils.Helper;
import com.nutech.burgundy.utils.NetworkHelper;

import java.util.List;
import java.util.Objects;

public class SchedulerMoodtrackReceiver extends WakefulBroadcastReceiver implements ScheduleView {
    private SessionManager sessionManager;
    private SchedulesPresenter presenter;
    private DataManager dataManager;
    private DangdutAsikService dangdutAsikService;
    private AntMediaService antMediaService;
    private MelonService melonService;
    private NetworkHelper networkHelper;
    private Context context;

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            if (intent.getAction() != null) {
                Log.e("test",intent.getAction());
                if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                    if (sessionManager == null) {
                        sessionManager = new SessionManager(Helper.getDefaultPreferences(context));
                    }

                    if (sessionManager.isLoggedIn()) {
                        this.context = context;
                        if (presenter == null) {
                            presenter = new SchedulesPresenter(getDataManager(),getNetworkHelper());
                        }
                        presenter.attachView(this);
                        presenter.getScheduleList(sessionManager.getUserStore().getId());
                    }
                } else {
                    ComponentName comp = new ComponentName(context.getPackageName(),
                            SchedulerMoodtackService.class.getName());
                    startWakefulService(context, (intent.setComponent(comp)));
                    setResultCode(Activity.RESULT_OK);
                }
            } else {
                ComponentName comp = new ComponentName(context.getPackageName(),
                        SchedulerMoodtackService.class.getName());
                startWakefulService(context, (intent.setComponent(comp)));
                setResultCode(Activity.RESULT_OK);
            }
        }
    }

    public DangdutAsikService getDangdutAsikService() {
        if (dangdutAsikService == null) {
            dangdutAsikService = DangdutAsikServiceFactory.create();
        }
        return dangdutAsikService;
    }

    public AntMediaService getAntMediaService(){
        if(antMediaService == null){
            antMediaService = AntMediaServiceFactory.create();
        }
        return antMediaService;
    }

    public MelonService getMelonService(){
        if(melonService == null){
            melonService = MelonServiceFactory.create();
        }
        return melonService;
    }

    public DataManager getDataManager() {
        if (dataManager == null) {
            dataManager = new DataManager(getDangdutAsikService(),getAntMediaService(), getMelonService());
        }
        return dataManager;
    }

    public NetworkHelper getNetworkHelper() {
        if (networkHelper == null) {
            networkHelper = new NetworkHelper(context, sessionManager);
        }
        return networkHelper;
    }

    @Override
    public void showSchedules(List<DataSchedules> dataSchedules) {
        Helper.SetAllScheduleMoodtrack(context,dataSchedules);
    }

    @Override
    public void showErrorSchedules(String error) {

    }

    @Override
    public void showSuccessRemoveSchedule(String dataSchedules) {

    }

    @Override
    public void showErrorRemoveSchedule(String error) {

    }

    @Override
    public void showSuccessSchedule(DataResult success) {

    }

    @Override
    public void showErrorSchedule(String error) {

    }
}
