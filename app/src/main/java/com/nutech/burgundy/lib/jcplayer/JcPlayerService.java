package com.nutech.burgundy.lib.jcplayer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.nutech.burgundy.R;
import com.nutech.burgundy.lib.jcplayer.JcPlayerExceptions.AudioAssetsInvalidException;
import com.nutech.burgundy.lib.jcplayer.JcPlayerExceptions.AudioFilePathInvalidException;
import com.nutech.burgundy.lib.jcplayer.JcPlayerExceptions.AudioRawInvalidException;
import com.nutech.burgundy.lib.jcplayer.JcPlayerExceptions.AudioUrlInvalidException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Objects;

public class JcPlayerService extends Service implements
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnInfoListener,
        MediaPlayer.OnBufferingUpdateListener,
        AudioManager.OnAudioFocusChangeListener {

    private static final String TAG = JcPlayerService.class.getSimpleName();

    private final IBinder mBinder = new JcPlayerServiceBinder();
    private MediaPlayer mediaPlayer;
    private MediaPlayer adsPlayer;
    private boolean isPlaying;
    private boolean isIklan = false;
    private int duration;
    private int currentTime;
    private JcAudio currentJcAudio;
    private JcStatus jcStatus = new JcStatus();
    private List<JcPlayerView.JcPlayerViewServiceListener> jcPlayerServiceListeners;
    private List<JcPlayerView.OnInvalidPathListener> invalidPathListeners;
    private List<JcPlayerView.JcPlayerViewStatusListener> jcPlayerStatusListeners;
    private JcPlayerView.JcPlayerViewServiceListener notificationListener;
    private AssetFileDescriptor assetFileDescriptor = null; // For Asset and Raw file.

    private String TAGS = "*** JcPlayer Service ***";

    //Last Added
    //Handle incoming phone calls
    private boolean ongoingCall = false;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;
    //AudioFocus
    private AudioManager audioManager;

    @Override
    public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
        Log.i(TAG, "MediaPlayer INFO code = " + i + " extra=" + i1);
        if (i == MediaPlayer.MEDIA_INFO_BUFFERING_START) {
            Log.d(TAG, "Start buffering...");

        } else if (i == MediaPlayer.MEDIA_INFO_BUFFERING_END) {
            Log.d(TAG, "End buffering...");
            Log.d(TAG, "Start playing...");

        }

        return false;
    }

    @Override
    public void onSeekComplete(MediaPlayer mediaPlayer) {

    }

    public class JcPlayerServiceBinder extends Binder {
        public JcPlayerService getService() {
            return JcPlayerService.this;
        }
    }

    public void registerNotificationListener(JcPlayerView.JcPlayerViewServiceListener notificationListener) {
        this.notificationListener = notificationListener;
    }

    public void registerNotification(int notification_id, Notification notification) {
        startForeground(notification_id,notification);
    }

    public void registerServicePlayerListener(JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener) {
        if (jcPlayerServiceListeners == null) {
            jcPlayerServiceListeners = new ArrayList<>();
        }

        if (!jcPlayerServiceListeners.contains(jcPlayerServiceListener)) {
            jcPlayerServiceListeners.add(jcPlayerServiceListener);
        }
    }

    public void registerInvalidPathListener(JcPlayerView.OnInvalidPathListener invalidPathListener) {
        if (invalidPathListeners == null) {
            invalidPathListeners = new ArrayList<>();
        }

        if (!invalidPathListeners.contains(invalidPathListener)) {
            invalidPathListeners.add(invalidPathListener);
        }
    }

    public void registerStatusListener(JcPlayerView.JcPlayerViewStatusListener statusListener) {
        if (jcPlayerStatusListeners == null) {
            jcPlayerStatusListeners = new ArrayList<>();
        }

        if (!jcPlayerStatusListeners.contains(statusListener)) {
            jcPlayerStatusListeners.add(statusListener);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAGS, "onCreate");

        String CHANNEL_ID = "my_channel_01";
        NotificationChannel channel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            channel = new NotificationChannel(CHANNEL_ID,
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) Objects.requireNonNull(getSystemService(Context.NOTIFICATION_SERVICE))).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }

        callStateListener();
    }

    public JcPlayerService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e(TAGS, "onStartCommand");

        //Request audio focus
        if (!requestAudioFocus()) {
//Could not gain focus
            stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    public boolean isIklan() {
        return isIklan;
    }

    public boolean isPlaying() {
        boolean bool = false;
        if (mediaPlayer != null) {
            try {
                bool = mediaPlayer.isPlaying();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return bool;
    }

    public void pause(JcAudio jcAudio) {
        if (mediaPlayer != null) {
            mediaPlayer.pause();
            duration = mediaPlayer.getDuration();
            currentTime = mediaPlayer.getCurrentPosition();
            isPlaying = false;
        }

        if (adsPlayer != null) {
            try {
                if (adsPlayer.isPlaying()) {
                    adsPlayer.pause();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            isPlaying = false;
        }

        for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
            jcPlayerServiceListener.onPaused();
        }

        if (notificationListener != null) {
            notificationListener.onPaused();
        }

        if (jcPlayerStatusListeners != null) {
            for (JcPlayerView.JcPlayerViewStatusListener jcPlayerStatusListener : jcPlayerStatusListeners) {
                jcStatus.setJcAudio(jcAudio);
                jcStatus.setDuration(duration);
                jcStatus.setCurrentPosition(currentTime);
                jcStatus.setPlayState(JcStatus.PlayState.PAUSE);
                jcPlayerStatusListener.onPausedStatus(jcStatus);
            }
        }
    }

    public void destroy() {
        stop();
        stopSelf();
    }

    public void onShuffle(boolean val) {
        for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
            jcPlayerServiceListener.onShuffle(val);
        }

        if (notificationListener != null) {
            notificationListener.onShuffle(val);
        }
    }

    public void stop() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }

        if (adsPlayer != null) {
            try {
                adsPlayer.stop();
                adsPlayer.reset();
                adsPlayer.release();
            } catch (Exception ignored) {

            }
            adsPlayer = null;
        }

        isPlaying = false;
    }

    private JcAudio tempJcAudio;

    public void playAds(String url,String adsId,String trackId) {
        try {

            adsPlayer = new MediaPlayer();
            adsPlayer.setDataSource(url);
            adsPlayer.setVolume(1.0f,1.0f);
            adsPlayer.prepareAsync();
            adsPlayer.setOnCompletionListener(mediaPlayer -> {
                isIklan = false;
                try {
                    adsPlayer.stop();
                    adsPlayer.reset();
                    adsPlayer.release();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                    jcPlayerServiceListener.addLogAds(adsId, trackId);
                    jcPlayerServiceListener.removeFirstAds();
                }

            });

            adsPlayer.setOnPreparedListener(mediaPlayer -> {
                isIklan = true;
                mediaPlayer.start();
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void play(JcAudio jcAudio) {
        tempJcAudio = this.currentJcAudio;
        this.currentJcAudio = jcAudio;

        if (isAudioFileValid(jcAudio.getPath(), jcAudio.getOrigin())) {
            try {
                if (mediaPlayer == null) {
                    mediaPlayer = new MediaPlayer();
                    adsPlayer = new MediaPlayer();
                    adsPlayer.reset();
                    adsPlayer.release();

                    mediaPlayer.setOnPreparedListener(this);
                    mediaPlayer.setOnBufferingUpdateListener(this);
                    mediaPlayer.setOnCompletionListener(this);
                    mediaPlayer.setOnErrorListener(this);
                    mediaPlayer.setOnInfoListener(this);

                    if (jcAudio.getOrigin() == Origin.URL) {
                        Log.e("link",jcAudio.getPath());
                        mediaPlayer.setDataSource(jcAudio.getPath());
                    } else if (jcAudio.getOrigin() == Origin.RAW) {
                        assetFileDescriptor = getApplicationContext().getResources().openRawResourceFd(Integer.parseInt(jcAudio.getPath()));
                        if (assetFileDescriptor == null) return; // TODO: Should throw error.
                        mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(),
                                assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
                        assetFileDescriptor.close();
                        assetFileDescriptor = null;
                    } else if (jcAudio.getOrigin() == Origin.ASSETS) {
                        assetFileDescriptor = getApplicationContext().getAssets().openFd(jcAudio.getPath());
                        mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(),
                                assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
                        assetFileDescriptor.close();
                        assetFileDescriptor = null;
                    } else if (jcAudio.getOrigin() == Origin.FILE_PATH) {
                        mediaPlayer.setDataSource(getApplicationContext(), Uri.parse(jcAudio.getPath()));
                    }

                    Log.e(TAG,"play 1");
                    mediaPlayer.setAudioAttributes(new AudioAttributes
                            .Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .build());
                    mediaPlayer.prepareAsync();

                    //} else if (isPlaying) {
                    //    stop();
                    //    play(jcAudio);
                } else {
                    if (isPlaying) {
                        Log.e(TAG,"play 2");
                        stop();
                        play(jcAudio);
                    } else {
                        Log.e(TAG,"play 3");
                        if (tempJcAudio != jcAudio) {
                            Log.e(TAG,"play 4");
                            stop();
                            play(jcAudio);
                        } else {
                            Log.e(TAG,"play 5");
                            mediaPlayer.start();
                            try {
                                adsPlayer.start();
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            }
                            isPlaying = true;


                            if (!requestAudioFocus()) {
//Could not gain focus
                                stopSelf();
                            }

                            if (jcPlayerServiceListeners != null) {
                                for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                                    jcPlayerServiceListener.onContinueAudio();
                                }
                            }

                            if (jcPlayerStatusListeners != null) {
                                for (JcPlayerView.JcPlayerViewStatusListener jcPlayerViewStatusListener : jcPlayerStatusListeners) {
                                    jcStatus.setJcAudio(jcAudio);
                                    jcStatus.setPlayState(JcStatus.PlayState.PLAY);
                                    jcStatus.setDuration(mediaPlayer.getDuration());
                                    jcStatus.setCurrentPosition(mediaPlayer.getCurrentPosition());
                                    jcPlayerViewStatusListener.onContinueAudioStatus(jcStatus);
                                }
                            }
                        }
                    }
                }
            } catch (IOException e) {
                Log.e(TAG,"play 7");
                pause(jcAudio);
                e.printStackTrace();
            }

            updateTimeAudio();

            Log.e(TAG,"play 8");
            for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                jcPlayerServiceListener.onPlaying();
            }

            if (jcPlayerStatusListeners != null) {
                for (JcPlayerView.JcPlayerViewStatusListener jcPlayerViewStatusListener : jcPlayerStatusListeners) {
                    jcStatus.setJcAudio(jcAudio);
                    jcStatus.setPlayState(JcStatus.PlayState.PLAY);
                    jcStatus.setDuration(0);
                    jcStatus.setCurrentPosition(0);
                    jcPlayerViewStatusListener.onPlayingStatus(jcStatus);
                }
            }

            if (notificationListener != null) notificationListener.onPlaying();

        } else {
            throwError(jcAudio.getPath(), jcAudio.getOrigin());
        }
    }

    public void seekTo(int time) {
        Log.d("time = ", Integer.toString(time));
        if (mediaPlayer != null) {
            mediaPlayer.seekTo(time);
        }
    }

    private void updateTimeAudio() {
        new Thread() {
            public void run() {
                while (isPlaying) {
                    try {

                        if (jcPlayerServiceListeners != null) {
                            for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                                jcPlayerServiceListener.onTimeChanged(mediaPlayer.getCurrentPosition());
                            }
                        }
                        if (notificationListener != null) {
                            notificationListener.onTimeChanged(mediaPlayer.getCurrentPosition());
                        }

                        if (jcPlayerStatusListeners != null) {
                            for (JcPlayerView.JcPlayerViewStatusListener jcPlayerViewStatusListener : jcPlayerStatusListeners) {
                                jcStatus.setPlayState(JcStatus.PlayState.PLAY);
                                jcStatus.setDuration(mediaPlayer.getDuration());
                                jcStatus.setCurrentPosition(mediaPlayer.getCurrentPosition());
                                jcPlayerViewStatusListener.onTimeChangedStatus(jcStatus);
                            }
                        }
                        Thread.sleep(200);
                    } catch (IllegalStateException | InterruptedException | NullPointerException | ConcurrentModificationException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        Log.i(TAG, "MediaPlayer BUFFER code = " + i);


    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        this.mediaPlayer.reset();
        mediaPlayer.stop();
        stop();
        if (jcPlayerServiceListeners != null) {
            for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                jcPlayerServiceListener.onCompletedAudio();
            }
        }
        if (notificationListener != null) {
            notificationListener.onCompletedAudio();
        }

        if (jcPlayerStatusListeners != null) {
            for (JcPlayerView.JcPlayerViewStatusListener jcPlayerViewStatusListener : jcPlayerStatusListeners) {
                jcPlayerViewStatusListener.onCompletedAudioStatus(jcStatus);
            }
        }
    }

    private void throwError(String path, Origin origin) {
        if (origin == Origin.URL) {
            throw new AudioUrlInvalidException(path);
        } else if (origin == Origin.RAW) {
            try {
                throw new AudioRawInvalidException(path);
            } catch (AudioRawInvalidException e) {
                e.printStackTrace();
            }
        } else if (origin == Origin.ASSETS) {
            try {
                throw new AudioAssetsInvalidException(path);
            } catch (AudioAssetsInvalidException e) {
                e.printStackTrace();
            }
        } else if (origin == Origin.FILE_PATH) {
            try {
                throw new AudioFilePathInvalidException(path);
            } catch (AudioFilePathInvalidException e) {
                e.printStackTrace();
            }
        }

        if (invalidPathListeners != null) {
            for (JcPlayerView.OnInvalidPathListener onInvalidPathListener : invalidPathListeners) {
                onInvalidPathListener.onPathError(currentJcAudio);
            }
        }
    }


    private boolean isAudioFileValid(String path, Origin origin) {
        if (origin == Origin.URL) {
            return path.startsWith("http") || path.startsWith("https");
        } else if (origin == Origin.RAW) {
            assetFileDescriptor = null;
            assetFileDescriptor = getApplicationContext().getResources().openRawResourceFd(Integer.parseInt(path));
            return assetFileDescriptor != null;
        } else if (origin == Origin.ASSETS) {
            try {
                assetFileDescriptor = null;
                assetFileDescriptor = getApplicationContext().getAssets().openFd(path);
                return assetFileDescriptor != null;
            } catch (IOException e) {
                e.printStackTrace(); //TODO: need to give user more readable error.
                return false;
            }
        } else if (origin == Origin.FILE_PATH) {
            File file = new File(path);
            //TODO: find an alternative to checking if file is exist, this code is slower on average.
            //read more: http://stackoverflow.com/a/8868140
            return file.exists();
        } else {
            // We should never arrive here.
            return false; // We don't know what the origin of the Audio File
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.e("errornya", i +" "+ i1);
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
//        this.mediaPlayer.start();
        Log.e(TAG,"onPrepared");
        mediaPlayer.start();
        isPlaying = true;
        this.duration = mediaPlayer.getDuration();
        this.currentTime = mediaPlayer.getCurrentPosition();
        updateTimeAudio();
        if (jcPlayerServiceListeners != null) {
            for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {

                jcPlayerServiceListener.updateTitle(currentJcAudio.getTitle(), currentJcAudio.getArtistName());
                jcPlayerServiceListener.updateCover(currentJcAudio.getCover());
                jcPlayerServiceListener.updateAudioId(currentJcAudio.getAudioId());
                jcPlayerServiceListener.onPreparedAudio(currentJcAudio.getTitle(), mediaPlayer.getDuration());
            }
        }

        if (notificationListener != null) {
            notificationListener.updateTitle(currentJcAudio.getTitle(), currentJcAudio.getArtistName());
            notificationListener.updateCover(currentJcAudio.getCover());
            notificationListener.updateAudioId(currentJcAudio.getAudioId());
            notificationListener.onPreparedAudio(currentJcAudio.getTitle(), mediaPlayer.getDuration());
        }

        if (jcPlayerStatusListeners != null) {
            for (JcPlayerView.JcPlayerViewStatusListener jcPlayerViewStatusListener : jcPlayerStatusListeners) {
                jcStatus.setJcAudio(currentJcAudio);
                jcStatus.setPlayState(JcStatus.PlayState.PLAY);
                jcStatus.setDuration(duration);
                jcStatus.setCurrentPosition(currentTime);
                jcPlayerViewStatusListener.onPreparedAudioStatus(jcStatus);
            }
        }
    }

    public JcAudio getCurrentAudio() {
        return currentJcAudio;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onTaskRemoved(rootIntent);
        Log.e(TAGS, "onTaskRemoved");
//        if (jcPlayerServiceListeners != null) {
//            for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
//                jcPlayerServiceListener.removeNotification();
//            }
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAGS, "onDestroy");

        //Disable the PhoneStateListener
        if (phoneStateListener != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        }

        if (jcPlayerServiceListeners != null) {
            for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                jcPlayerServiceListener.removeNotification();
            }
        }
    }

    /**
     * Handle PhoneState changes
     */
    private void callStateListener() {
        final int[] length = {0};
        // Get the telephony manager
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //Starting listening for PhoneState changes
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    //if at least one call exists or the phone is ringing
                    //pause the MediaPlayer
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (mediaPlayer != null) {
//                            pause(currentJcAudio);
                            mediaPlayer.pause();
                            length[0] = mediaPlayer.getCurrentPosition();
                            ongoingCall = true;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        // Phone idle. Start playing.
                        if (mediaPlayer != null) {
                            if (ongoingCall) {
                                ongoingCall = false;
                                //Resume Play
                                mediaPlayer.seekTo(length[0]);
                                mediaPlayer.start();
                            }
                        }
                        break;
                }
            }
        };
        // Register the listener with the telephony manager
        // Listen for changes to the device call state.
        telephonyManager.listen(phoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    /**
     * ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs
     */
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            System.out.println("******* pause *******");
            pause(currentJcAudio);
//            buildNotification(PlaybackStatus.PAUSED);

        }
    };

    private void registerBecomingNoisyReceiver() {
        //register after getting audio focus
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }

    /**
     * AudioFocus
     */
    private boolean requestAudioFocus() {
        Log.e(TAGS,"requestAudioFocus");
        int result = 0;
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setSpeakerphoneOn(true);
            result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        }
        //Focus gained
        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
        //Could not gain focus
    }

    private boolean removeAudioFocus() {
        return AudioManager.AUDIOFOCUS_REQUEST_GRANTED ==
                audioManager.abandonAudioFocus(this);
    }

    /**
     * Service Binder
     */
    public class LocalBinder extends Binder {
        public JcPlayerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return JcPlayerService.this;
        }
    }

    @Override
    public void onAudioFocusChange(int focusState) {
        //Invoked when the audio focus of the system is updated.
        switch (focusState) {
            case AudioManager.AUDIOFOCUS_GAIN:
                Log.e(TAGS, "AUDIOFOCUS_GAIN");
                try {
                    if (mediaPlayer == null) new MediaPlayer();
//                    else if (!mediaPlayer.isPlaying()) mediaPlayer.start();
                    mediaPlayer.setVolume(1.0f, 1.0f);
//                    else if (!mediaPlayer.isPlaying()) mediaPlayer.pause();
                } catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case AudioManager.AUDIOFOCUS_LOSS:
                Log.e(TAGS, "AUDIOFOCUS_LOSS");

                try {
                    if (mediaPlayer.isPlaying()) pause(currentJcAudio);
                } catch (Exception e){
                    e.printStackTrace();
                }

//                try {
//                    mediaPlayer.reset();
//                    mediaPlayer.release();
//                    mediaPlayer = null;
//                } catch (Exception e){
//                    e.printStackTrace();
//                }

                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Log.e(TAGS, "AUDIOFOCUS_LOSS_TRANSIENT");
                try {
                    if (mediaPlayer.isPlaying()) pause(currentJcAudio);

                    if (jcPlayerServiceListeners != null) {
                        for (JcPlayerView.JcPlayerViewServiceListener jcPlayerServiceListener : jcPlayerServiceListeners) {
                            jcPlayerServiceListener.onContinueAudio();
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                Log.e(TAGS, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK");
                try {
                    if (mediaPlayer.isPlaying()) mediaPlayer.setVolume(0.1f, 0.1f);
                } catch (Exception e){
                    e.printStackTrace();
                }

                break;
        }
    }

}
