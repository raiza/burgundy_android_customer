package com.nutech.burgundy.lib.jcplayer;

import android.content.Intent;

import androidx.annotation.RawRes;

import java.io.Serializable;

/**
 * Created by jean on 27/06/16.
 */

public class JcAudio implements Serializable {
    private long id;
    private String title;
    private int position;
    private String path;
    private Origin origin;
    private String cover;
    private String audioId;
    private String artistName;


    public JcAudio(String id,String title, String path, Origin origin, String cover, String audioId, String ArtistName) {
        // It looks bad
        //int randomNumber = path.length() + title.length();

        // We init id  -1 and position with -1. And let JcPlayerView define it.
        // We need to do this because there is a possibility that the user reload previous playlist
        // from persistence storage like sharedPreference or SQLite.
        this.id = Long.decode(id);
        this.position = -1;
        this.title = title;
        this.path = path;
        this.origin = origin;
        this.cover = cover;
        this.audioId = audioId;
        this.artistName = ArtistName;
    }

    public JcAudio(String title, String path, String id, String position, Origin origin, String cover,
                   String audioId, String ArtistName) {
        this.id = Long.decode(id);
        this.position = Integer.valueOf(position);
        this.title = title;
        this.path = path;
        this.origin = origin;
        this.cover = cover;
        this.audioId = audioId;
        this.artistName = ArtistName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getAudioId() {
        return audioId;
    }

    public void setAudioId(String audioId) {
        this.audioId = audioId;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public static JcAudio createFromRaw(@RawRes int rawId, String cover, String audioId, String artistName) {
        return new JcAudio("1",String.valueOf(rawId), String.valueOf(rawId), Origin.RAW, cover, audioId, artistName);
    }

    public static JcAudio createFromRaw(String title, @RawRes int rawId, String cover, String audioId, String artistName) {
        return new JcAudio("1",title, String.valueOf(rawId), Origin.RAW, cover, audioId, artistName);
    }

    public static JcAudio createFromAssets(String assetName, String cover, String audioId, String artistName) {
        return new JcAudio("1",assetName, assetName, Origin.ASSETS, cover, audioId, artistName);
    }

    public static JcAudio createFromAssets(String title, String assetName, String cover, String audioId, String artistName) {
        return new JcAudio("1",title, assetName, Origin.ASSETS, cover, audioId, artistName);
    }

    public static JcAudio createFromURL(String url, String cover, String audioId, String artistName) {
        return new JcAudio("1",url, url, Origin.URL, cover, audioId, artistName);
    }

    public static JcAudio createFromURL(String id, String position, String title, String url, String cover, String audioId, String artistName) {
        return new JcAudio(title, url, id, position, Origin.URL, cover, audioId, artistName);
    }

    public static JcAudio createFromFilePath(String filePath, String cov, String audioId, String artistName) {
        return new JcAudio("1",filePath, filePath, Origin.FILE_PATH, cov, audioId, artistName);
    }

    public static JcAudio createFromFilePath(String title, String filePath, String cover,String audioId, String artistName) {
        return new JcAudio("1",title, filePath, Origin.FILE_PATH, cover, audioId, artistName);
    }
}