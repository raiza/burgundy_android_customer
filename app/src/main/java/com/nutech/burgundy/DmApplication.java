package com.nutech.burgundy;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.lib.jcplayer.JcPlayerView;
import com.nutech.burgundy.ui.view.FragmentNavigation;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

/**
 * @author Arif Setiawan on 7/17/17
 */

public class DmApplication extends Application {

    private JcPlayerView jcPlayerView;
    private FragmentNavigation navigation;

    private List<DataLiricks> dataLiricksList = new ArrayList<>();

    private String title;

    @Override
    public void onCreate() {
        super.onCreate();

        initFont();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initFont() {

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Roboto-RobotoRegular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                .setDefaultFontPath("fonts/Lato-Regular.ttf")
//                .setFontAttrId(R.attr.fontPath)
//                .build()
//        );
    }

    public JcPlayerView getJcPlayerView() {
        return jcPlayerView;
    }

    public void setJcPlayerView(JcPlayerView jcPlayerView) {
        this.jcPlayerView = jcPlayerView;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<DataLiricks> getDataLiricksList() {
        return dataLiricksList;
    }

    public void setDataLiricksList(List<DataLiricks> dataLiricksList) {
        this.dataLiricksList = dataLiricksList;
    }

    public FragmentNavigation getNavigation() {
        return navigation;
    }

    public void setNavigation(FragmentNavigation navigation) {
        this.navigation = navigation;
    }
}
