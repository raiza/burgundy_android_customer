package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 10/24/17.
 * Xeranta Mobile Solutions
 */

public class DataLiricks {

    @SerializedName("track_id")
    @Expose
    private String track_id;
    @SerializedName("no")
    @Expose
    private String no;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("start_at")
    @Expose
    private String start_at;
    @SerializedName("end_at")
    @Expose
    private String end_at;
    @SerializedName("track_name")
    @Expose
    private String track_name;
    @SerializedName("artist_name")
    @Expose
    private String artist_name;

    public String getTrack_id() {
        return track_id;
    }

    public void setTrack_id(String track_id) {
        this.track_id = track_id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStart_at() {
        return start_at;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public String getEnd_at() {
        return end_at;
    }

    public void setEnd_at(String end_at) {
        this.end_at = end_at;
    }

    public String getTrack_name() {
        return track_name;
    }

    public void setTrack_name(String track_name) {
        this.track_name = track_name;
    }

    public String getArtist_name() {
        return artist_name;
    }

    public void setArtist_name(String artist_name) {
        this.artist_name = artist_name;
    }
}
