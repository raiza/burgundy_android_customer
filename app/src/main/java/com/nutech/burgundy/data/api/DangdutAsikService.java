package com.nutech.burgundy.data.api;

import com.nutech.burgundy.data.model.DataAbout;
import com.nutech.burgundy.data.model.DataAds;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataCheckDevice;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.data.model.DataLoginMember;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataOfficial;
import com.nutech.burgundy.data.model.DataOperator;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.data.model.DataPoint;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataReward;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.data.model.DataSearch;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.data.model.DataSubscribe;
import com.nutech.burgundy.data.model.DataSubscribeGoogle;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataSubscribtionGoogle;
import com.nutech.burgundy.data.model.DataTimer;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.data.model.DataVideos;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.data.model.ResetPIN;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.data.model.melon.ResponseTransaction;
import com.nutech.burgundy.data.model.melon.Token;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.data.model.soundtrack.Sound;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public interface DangdutAsikService {

    String MEMBER_REGISTER = "customers/registerCustomer";
    String MEMBER_LOGIN = "customers/login";
    String CUSTOMER_DETAIL = "customers/getCustomers/id/";

    String GET_GENRE = "genres/getGenresAll";
    String GET_ARTISTSALL = "artists/getArtistsAll";
    String GET_ALL_PLAYLIST = "playlists/getPlaylistsAll/customer_id/";
    String GET_PROMO_SLIDE = "promotions/getPromotionsAll";
    String GET_TRACKS_ALL = "tracks/getTracksAll";
    String GET_ALBUMS_BY_ARTISTS = "albums/getAlbumsByGenreOrArtist/genre_id/0/artist_id/";
    String GET_TRACKS_BY_ARTISTS = "tracks/getTracksByArtist/artist_id/";
    String GET_TRACKS_BY_PLAYLIST = "tracks/getTracksByPlaylist/playlist_id/";
    String GET_TRACKS_BY_ALBUM = "tracks/getTracksByAlbum/album_id/";
    String GET_SEARCH_BY_ALBUM_ARTISTS_TRACKS_PLAYLIST = "tracks/searchArtistsOrAlbumsOrTracksOrPlaylistsByName";
    String GET_TRACKS_BY_PLAYLISTID = "tracks/getTracksByPlaylist/playlist_id/";
    String GET_PLAYLIST_MEMBER = "playlists/getPlaylistsByCustomer/customer_id/";
    String GET_TRACKS_MEMBER = "tracks/getTracksCustomer/customer_id/";
    String GET_TOP_CHART = "tracks/getTopTracks/limit/";
    String GET_TOP_PLAYLIST = "playlists/getTopPlaylists/limit/";

    String ADD_PLAYLIST_MEMBER = "playlists/addCustomerPlaylist";
    String REMOVE_PLAYLIST_MEMBER = "playlists/removeCustomerPlaylist";
    String REMOVE_TRACKS_MEMBER = "tracks/removeCustomerTrack";
    String GET_ALBUMS_TRACKS = "tracks/getTracksByAlbum/album_id/";
    String GET_GENRE_TRACKS = "tracks/getTracksByGenre/genre_id/";
    String ADD_TRACKS_MEMBER = "tracks/addCustomerTrack";
    String GET_SUBSCRIBE_PRICE_ALL = "subscribeprices/getSubscribePricesAll";
    String GET_SUBSCRIBE_DETAIL_PRICE = "subscribeprices/getSubscribePrices/id/";
    String GET_SUBSCRIBE_STATUS = "subscribecustomers/getSubscriptionStatus/customer_id/";
    String SUBSCRIBE_CUSTOMER = "subscribecustomers/subscribeCustomer";
    String UPDATE_SUBSCRIBE_CUSTOMER = "subscribecustomers/updateSubscribeCustomer";
    String ACTIVATION_SUBSCRIBE_CUSTOMER = "subscribecustomers/activationSubscribeCustomer";
    String UPDATE_SUBSCRIBE_ACTIVATION_CODE = "subscribecustomers/updateSubscribeActivationCode";
    String UN_SUBSCRIBE_CUSTOMER = "subscribecustomers/unsubscribeCustomer";
    String GET_VIDEOS = "videos/getVideosAll";
    String GET_TRACKS_LIRICKS = "tracks/getTrackLyrics/track_id/";
    String SEND_FEEDBACK = "feedback/sendFeedback";
    String GET_SETTING = "settings/getSettings/id/1";
    String GET_TRACKS_RECOMENDED = "tracks/getRecommendTracks/id/";
    String GET_POINT_ALL = "point/getActivityPointAll";
    String GET_REWARD_ALL = "point/getPointRewardAll";
    String UPDATE_POINT_CUSTOMER = "point/updatePointCustomer";
    String SAVE_NEW_PLAYLIST = "playlists/addPlaylist";
    String UPDATE_PLAYLIST = "playlists/editPlaylist";
    String GET_ALL_PLAYLIST_CUSTOMER = "playlists/getAllCustomerPlaylists/customer_id/";
    String REMOVE_PLAYLIST = "playlists/removePlaylist";
    String ADD_TRACKS_TO_PLAYLIST = "playlists/addPlaylistTrack";
    String GET_ALL_PLAYLIST_FAVORITE = "playlists/getPlaylistsByCustomer/customer_id/";
    String INSERT_FAV_PLAYLIST = "playlists/updatePlaylistFav";
    String UPDATE_PLAYS_TRACK_COUNT = "tracks/updateTrackPlays";
    String ADD_POINT_EXCHANGE = "point/addPointExchange";
    String GET_HISTORY_POINT_EXCHANGE = "point/getPointExchangeByCustomer/customer_id/";
    String GET_DETAIL_HISTORY_POINT_EXCHANGE = "point/getPointExchange/id/";
    String UPDATE_STATUS_POINT_EXCHANGE = "point/editPointExchange";
    String USE_POINT_EXCHANGE = "point/usePointExchange";
    String GET_DATA_MEMBER_BY_ID = "customers/getCustomersById/id/";
    String GET_DATA_USER_STORE_BY_ID = "storemember/getUserById/id/";
    String UPLOAD_IMAGE_PROFILE = "customers/uploadImageCustomer";
    String GET_MOODTRACK_PLAYING_STORE = "soundtracks/getPlayingSoundtrack";
    String GET_NEAR_STORE = "store/GetNearestStore";
    String LOGIN_WITH_NUMBER_PHONE = "customers/loginCustomer"; // --
    String LOGIN_STORE = "storemember/loginMember"; // --
//    String LOGIN_WITH_NUMBER_PHONE = "customers/loginMember"; // --
    String OTP_VERIFICATION = "customers/otpCustomer";
    String CHECK_DEVICE = "customers/checkDevice";
    String CHECK_DEVICE_STORE = "storemember/checkDevice";
    String SMS_REQUEST_BASE = "http://202.43.173.13/api_otp/send_sms.php?";
    String GET_DATA_KARAOKE = "karaokes/getKaraokes/id/";
    String REMOVE_ROOM_KARAOKE_BY_ID = "karaokes/removeCustomerKaraoke";
    String ADD_ROOM = "room/addRoom";
    String UPDATE_ROOM = "room/updateRoom";
    String REMOVE_ROOM = "room/removeRoom";
    String GET_ROOM_ALL = "room/getRoomAll/customer_id/";
    String GET_ROOM_BY_CUSTOMER = "room/getRoomByCustomer/customer_id/";
    String GET_ROOM_BY_STATUS_LIVE = "room/getRoomLive/live/";
    String UPDATE_DATA_PROFILE = "customers/editCustomer";
    String GET_TRACK_BY_ID = "tracks/getTracks/id/";
    String GET_PROMO_BY_ID = "promotions/getPromotions/promotion_id/";
    String INSERT_LOG = "logcustomer/addupdatelocgcustomer";
    String INSERT_LOG_STORE = "logcustomer/addupdatelogstore";
    String UPDATE_IS_PLAYING = "soundtracks/updateIsPlayingSoundtrack";
    String UPDATE_SOUNDTRACK_IS_PLAYING = "soundtracks/updateIsPlayingSoundtrack";
    String ADS_LIST = "ads/getAds";
    String INSERT_LOG_REPORT = "logcustomer/addReportLog";
    String INSERT_LOG_ADS = "adslog/insert_ads_log";
    String GET_RECOMENDED = "logcustomer/getGenreAlbumArtistrelease";
    String GET_MOODTRACK_FOR_YOU = "soundtracks/getSoundtracksForYou";
    String LIKES_VIDEOS_KARAOKE = "room/updateLikes";
    String VIEWERS_VIDEOS_KARAOKE = "room/updateViewers";
    String LIKES_VIDEOS = "videos/updateLikes";
    String VIEWERS_VIDEOS = "videos/updateViewers";
    String SEND_REPORT = "karaokes/addReportKaraoke";
    String GET_TIMER_PLAY = "tracks/getPlayTimer";
    String GET_TOP_KARAOKE = "room/getTopKaraoke";
    String GET_OFFICIAL = "medsos/getMedsosAll";
    String REMOVE_PLAYLIST_TRACK = "playlists/removePlaylistTrack";
    String CHANGE_PIN_MEMBER = "customers/changeActivationCode";
    String RESET_ACTIVATION_CODE = "customers/resetActivationCode/mobile_phone/";
    String UPLOAD_VIDEO_KARAOKE = "room/uploadVideoRoom";
    String UPDATE_STATUS_UPLOAD_VIDEO = "room/updateStatusUpload";
    String CHECK_MEMBER_TO_INDOSIS = "customers/checkMember/mobile_phone/";
    String SUBSCRIBE_CUSTOMER_GOOGLE = "subscribecustomers/insertSubscribeCustomer";
    String SUBSCRIBE_CUSTOMER_GOOGLE_UPDATE = "subscribecustomers/updateSubscribeGoogle";
    String GET_ARTISTS_BY_NAME = "artists/getArtistsByName/name/";
    String INSERT_DATA_LANGGANAN_INDOSIS = "subscribecustomers/insertSubscribeCustomerWAP";
    String GET_OPERATOR_NAME = "operator/getOperatorByNumber/number/";
    String GET_TOKEN_TRANSACTION = "melon/requestTransactionToken";
    String PAY_TRANSACTION = "melon/payTransaction";
    String CHARGE_TRANSACTION = "melon/chargeTransaction";
    String GET_SUBSCRIBTION_STATUS_GOOGLE = "subscribecustomers/checkSubscribeGoogle";

    String GET_ENERGY = "energy/getEnergyAll";
    String GET_SOUND = "sound/getSoundAll";
    String GET_DECADE = "decade/getDecadeAll";
    String SAVE_NEW_MOODTRACK = "soundtracks/addSoundtrack";
    String GET_MOODTRACKS = "soundtracks/getSoundtracksAll/customer_id/";
    String GET_MOODTRACKS_BY_STORE_ID = "soundtracks/getSoundtracksByStoreId/store_id/";
    String GET_MOODTRACKS_DETAIL = "soundtracks/getSoundtracksById/id/";
    String UPDATE_POSITION_MOODTRACK_TRACK = "soundtracks/updatePositionMoodtrackTrack";
    String REMOVE_MOODTRACKS = "soundtracks/removeSoundtrack";
    String ADD_MOODTRACK_SCHEDULE = "mood_scheduler/addMoodScheduler";
    String EDIT_MOODTRACK_SCHEDULE = "mood_scheduler/editMoodScheduler";
    String GET_SCHEDULE_LIST = "mood_scheduler/getMoodSchedulerByUserId";
    String REMOVE_SCHEDULE = "mood_scheduler/removeMoodScheduler";
    String UPDATE_MOODTRACKS = "soundtracks/editSoundtrack";
    String ADD_TRACK_TO_MOOD = "soundtracks/addTrackToMood";
    String REMOVE_TRACK_FROM_MOOD = "soundtracks/removeTrackFromMood";

    @GET(GET_GENRE)
    Observable<List<DataGenre>> getGenreAll();

    @GET(GET_ARTISTSALL)
    Observable<List<DataArtists>> getAllArtists();

    @GET(GET_ARTISTSALL)
    Observable<DataArtistList> artistAll();

    @GET(GET_ARTISTSALL)
    Call<DataArtistList> getDataArtistWithPaging(
            @Query("page") Integer page,
            @Query("per_page") Integer perPage
    );

    @GET(GET_ARTISTS_BY_NAME + "{name}")
    Observable<List<DataArtists>> getAllArtistsByName(@Path("name") String name);

    @GET(GET_ARTISTS_BY_NAME)
    Call<DataArtistList> getArtistbyNameWithPaging(
            @Query("name") String name,
            @Query("page") Integer page,
            @Query("per_page") Integer perPage
    );

    @GET(GET_ALL_PLAYLIST + "{customer_id}" + "/created_by/" + "{created_by}")
    Observable<List<DataPlaylist>> getAllPlaylist(
            @Path("customer_id") String memberId,
            @Path("created_by") String createdby);

    @GET(GET_PROMO_SLIDE)
    Observable<List<DataPromoSlide>> getPromoSlide();

    @GET(GET_TRACKS_ALL)
    Observable<List<DataTracks>> getTracksAll();

    @GET(GET_TRACKS_BY_ARTISTS + "{id}")
    Observable<List<DataTracks>> getTracksByArtistsId(@Path("id") String idArtists);

    @GET(GET_TRACKS_BY_ALBUM + "{id}")
    Observable<List<DataTracks>> getTracksByAlbumId(@Path("id") String idAlbum);

    @GET(GET_ALBUMS_BY_ARTISTS + "{id}")
    Observable<List<DataAlbums>> getAlbumsByArtist(@Path("id") String artistId);

    @FormUrlEncoded
    @POST(GET_SEARCH_BY_ALBUM_ARTISTS_TRACKS_PLAYLIST)
    Observable<List<DataSearch>> getSearch(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(LOGIN_WITH_NUMBER_PHONE)
    Observable<String> getLogin(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(LOGIN_WITH_NUMBER_PHONE)
    Observable<String> postRegister(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(LOGIN_WITH_NUMBER_PHONE)
    Observable<DataLoginMember> checkMember(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(GET_MOODTRACK_PLAYING_STORE)
    Observable<List<Moodtrack>> getMoodtrackPlayingStore(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(GET_NEAR_STORE)
    Observable<List<DataStore>> getNearStores(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(LOGIN_STORE)
    Observable<DataUserStore> storeLogin(@FieldMap Map<String, String> fields);

    @GET(CUSTOMER_DETAIL + "{id}")
    Observable<DataMember> getCustomerById(@Path("id") String customerId);

    @GET(GET_TRACKS_BY_PLAYLISTID + "{id}")
    Observable<List<DataTracks>> getTracksByPlaylistId(@Path("id") String playlistId);

    @GET(GET_ALL_PLAYLIST_CUSTOMER + "{id}")
    Observable<List<DataPlaylistMember>> getPlaylistMember(@Path("id") String memberId);

    @FormUrlEncoded
    @POST(ADD_PLAYLIST_MEMBER)
    Observable<String> addPlaylistMember(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(REMOVE_PLAYLIST_MEMBER)
    Observable<String> removePlaylistMember(@FieldMap Map<String, String> fields);

    @GET(GET_TOP_CHART + "{num_limit}")
    Observable<List<DataTracks>> getTopChart(@Path("num_limit") Integer limit);

    @GET(GET_TOP_PLAYLIST + "{num_limit}")
    Observable<List<DataPlaylist>> getTopPlaylist(@Path("num_limit") Integer limit);

    @GET(GET_ALBUMS_TRACKS + "{id}")
    Observable<List<DataTracks>> getAlbumsTracks(@Path("id") String albumId);

    @GET(GET_GENRE_TRACKS + "{id}")
    Observable<List<DataTracks>> getGenreTracks(@Path("id") String genreId);

    @FormUrlEncoded
    @POST(ADD_TRACKS_MEMBER)
    Observable<String> addTracksFav(@FieldMap Map<String, String> fields);

    @GET(GET_TRACKS_MEMBER + "{id}")
    Observable<List<DataTracks>> getTracksMember(@Path("id") String memberId);

    @GET(GET_SUBSCRIBE_PRICE_ALL)
    Observable<List<DataSubscribePrice>> getSubscribePriceAll();

    @GET(GET_SUBSCRIBE_DETAIL_PRICE + "{id}")
    Observable<DataSubscribePrice> getSubscribeDetailPrice(@Path("id") String id);

    @GET(GET_SUBSCRIBE_STATUS + "{id}")
    Observable<DataUserSubscribe> getUserSubscribeStatus(@Path("id") String id);

    @FormUrlEncoded
    @POST(SUBSCRIBE_CUSTOMER)
    Observable<DataSubscribe> subscribeCustomer(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(ACTIVATION_SUBSCRIBE_CUSTOMER)
    Observable<DataSubscribe> updateSubscribeCustomer(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(UPDATE_SUBSCRIBE_ACTIVATION_CODE)
    Observable<DataSubscribe> updateSubscribeActivationCode(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(UN_SUBSCRIBE_CUSTOMER)
    Observable<DataSubscribe> unSubscribeCustomer(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(REMOVE_TRACKS_MEMBER)
    Observable<String> removeTracksMember(@FieldMap Map<String, String> fields);

    @GET(GET_VIDEOS)
    Observable<List<DataVideos>> getVideosAll();

    @GET(GET_TRACKS_LIRICKS + "{id}")
    Observable<List<DataLiricks>> getTracksLiricks(@Path("id") String tracksId);

    @FormUrlEncoded
    @POST(SEND_FEEDBACK)
    Observable<String> sendFeedback(@FieldMap Map<String, String> fields);

    @GET(GET_SETTING)
    Observable<DataAbout> getAboutUs();

    @GET(GET_TRACKS_RECOMENDED + "{id}"+"/genre_id/"+"{genre_id}")
    Observable<List<DataTracks>> getTracksRecomended(@Path("id") String idArtists, @Path("genre_id") String genreId);

    @GET(GET_POINT_ALL)
    Observable<List<DataPoint>> getPointAll();

    @GET(GET_REWARD_ALL)
    Observable<List<DataReward>> getRewardAll();

    @FormUrlEncoded
    @POST(UPDATE_POINT_CUSTOMER)
    Observable<String> updatePointCustomer(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(SAVE_NEW_PLAYLIST)
    Observable<String> savePlaylist(@FieldMap Map<String, String> fields);

    @GET(GET_ALL_PLAYLIST_CUSTOMER + "{customer_id}")
    Observable<List<DataPlaylist>> getAllPlaylistCustomer(@Path("customer_id") String customerId);

    @FormUrlEncoded
    @POST(UPDATE_PLAYLIST)
    Observable<String> updatePlaylist(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(REMOVE_PLAYLIST)
    Observable<String> removePlaylist(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(ADD_TRACKS_TO_PLAYLIST)
    Observable<String> addTracksToPlaylist(@FieldMap Map<String, String> fields);

    @GET(GET_ALL_PLAYLIST_FAVORITE + "{id}")
    Observable<List<DataPlaylistMember>> getPlaylistFavorite(@Path("id") String memberId);

    @FormUrlEncoded
    @POST(INSERT_FAV_PLAYLIST)
    Observable<String> insertFavPlaylist(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(UPDATE_PLAYS_TRACK_COUNT)
    Observable<String> updatePlayTracksCount(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(ADD_POINT_EXCHANGE)
    Observable<String> addPointExchange(@FieldMap Map<String, String> fields);

    @GET(GET_HISTORY_POINT_EXCHANGE + "{id}")
    Observable<List<DataHistoryExchange>> getHistoryPointExchange(@Path("id") String memberId);

    @GET(GET_DETAIL_HISTORY_POINT_EXCHANGE + "{id}")
    Observable<DataHistoryExchange> getDetailHistory(@Path("id") String id);

    @FormUrlEncoded
    @POST(UPDATE_STATUS_POINT_EXCHANGE)
    Observable<String> updateStatusPointExchange(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(USE_POINT_EXCHANGE)
    Observable<String> usePointExchange(@FieldMap Map<String, String> fields);

    @GET(GET_DATA_MEMBER_BY_ID + "{id}")
    Observable<DataMember> getDataMember(@Path("id") String id);

    @GET(GET_DATA_USER_STORE_BY_ID + "{id}")
    Observable<DataUserStore> getDataUserStore(@Path("id") String id);

    @FormUrlEncoded
    @POST(UPLOAD_IMAGE_PROFILE)
    Observable<String> uploadImageProfile(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(OTP_VERIFICATION)
    Observable<DataMember> otpVerification(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(CHECK_DEVICE)
    Observable<DataCheckDevice> checkDevice(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(CHECK_DEVICE_STORE)
    Observable<DataCheckDevice> checkDeviceStore(@FieldMap Map<String, String> params);

    @GET(SMS_REQUEST_BASE)
    Observable<String> sendSms(@Query(value = "sender") String phonenumber,
                               @Query(value = "message") String textContent);

    @GET(GET_DATA_KARAOKE + "{id}")
    Observable<DataKaraoke> getDataKaraoke(@Path("id") String karaokeId);

    @FormUrlEncoded
    @POST(REMOVE_ROOM_KARAOKE_BY_ID)
    Observable<String> removeHistoryKaraoke(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(ADD_ROOM)
    Observable<String> addRoom(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UPDATE_ROOM)
    Observable<String> updateRoom(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(REMOVE_ROOM)
    Observable<String> removeRoom(@FieldMap Map<String, String> params);

    @GET(GET_ROOM_ALL + "{customer_id}")
    Observable<List<DataRoom>> getHistoryAll(@Path("customer_id") String id);

    @GET(GET_ROOM_BY_CUSTOMER + "{customer_id}")
    Observable<List<DataRoom>> getHistoryBycustomer(@Path("customer_id") String customerId);

    @GET(GET_ROOM_BY_STATUS_LIVE + "{live}")
    Observable<List<DataRoom>> getHistoryLive(@Path("live") String statusLive);

    @FormUrlEncoded
    @POST(UPDATE_DATA_PROFILE)
    Observable<DataMember> updateDataProfile(@FieldMap Map<String, String> fields);

    @GET(GET_TRACK_BY_ID + "{id}")
    Observable<DataTracks> getTrackById(@Path("id") String id);

    @GET(GET_PROMO_BY_ID + "{promotion_id}")
    Observable<DataPromoSlide> getPromoById(@Path("promotion_id") String id);

    @FormUrlEncoded
    @POST(INSERT_LOG)
    Observable<String> addLogCustomer(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(INSERT_LOG_STORE)
    Observable<String> addLogStore(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(UPDATE_IS_PLAYING)
    Observable<Map> updateIsPlayingMoodtrack(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(ADS_LIST)
    Observable<List<DataAds>> getAdsList(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(INSERT_LOG_REPORT)
    Observable<String> addLogReport(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(INSERT_LOG_ADS)
    Observable<String> addLogAds(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(GET_RECOMENDED)
    Observable<List<DataTracks>> getRecomended(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(GET_MOODTRACK_FOR_YOU)
    Observable<List<Moodtrack>> getMoodtrackForYou(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(LIKES_VIDEOS_KARAOKE)
    Observable<String> updateLikeVideosKaraoke(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(VIEWERS_VIDEOS_KARAOKE)
    Observable<String> updateViewersVideosKaraoke(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(LIKES_VIDEOS)
    Observable<String> updateLikeVideos(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(VIEWERS_VIDEOS)
    Observable<String> updateViewersVideos(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(SEND_REPORT)
    Observable<String> sendReport(@FieldMap Map<String, String> fields);

    @GET(GET_TIMER_PLAY)
    Observable<DataTimer> getTimer();

    @GET(GET_TOP_KARAOKE)
    Observable<List<DataRoom>> getTopKaraoke();

    @GET(GET_OFFICIAL)
    Observable<List<DataOfficial>> getAllOfficial();

    @FormUrlEncoded
    @POST(REMOVE_PLAYLIST_TRACK)
    Observable<String> removePlaylistTrack(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(CHANGE_PIN_MEMBER)
    Observable<DataMember> changePin(@FieldMap Map<String, String> fields);

    @GET(RESET_ACTIVATION_CODE + "{mobile_phone}")
    Observable<ResetPIN> resetPIN(@Path("mobile_phone") String nPhone);

    @Multipart
    @POST(UPLOAD_VIDEO_KARAOKE)
    Observable<String> uploadVideoKaraoke(@Part MultipartBody.Part file);

    @Multipart
    @POST(UPLOAD_VIDEO_KARAOKE)
    Call<ResponseBody> postFile(@Part MultipartBody.Part fileVide,
                                @Part MultipartBody.Part fileMusic,
                                @Part("room_code") RequestBody roomCode,
                                @Part("head_phone") RequestBody headPhohe);

    @FormUrlEncoded
    @POST(UPDATE_STATUS_UPLOAD_VIDEO)
    Observable<String> updateStatusUploadVideo(@FieldMap Map<String, String> params);

    @GET(CHECK_MEMBER_TO_INDOSIS + "{mobile_phone}")
    Observable<MemberCheck> checkMemberToIndosis(@Path("mobile_phone") String mobile_phone);

    @FormUrlEncoded
    @POST(SUBSCRIBE_CUSTOMER_GOOGLE)
    Observable<DataSubscribeGoogle> subscribeCustomerGoogle(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(INSERT_DATA_LANGGANAN_INDOSIS)
    Observable<String> insertDataLanggananIndosis(@FieldMap Map<String, String> fields);

    @GET(GET_OPERATOR_NAME + "{number}")
    Observable<List<DataOperator>> getOperatorByNumber(@Path("number") String number);

    @FormUrlEncoded
    @POST(GET_TOKEN_TRANSACTION)
    Observable<Token> requestTokenTransaction(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(PAY_TRANSACTION)
    Observable<ResponseTransaction> payTransaction(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(CHARGE_TRANSACTION)
    Observable<ResponseTransaction> chargeTransaction(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(GET_SUBSCRIBTION_STATUS_GOOGLE)
    Observable<DataSubscribtionGoogle> getSubsribtionGoogleStatus(@FieldMap Map<String, String> fields);

    @FormUrlEncoded
    @POST(SUBSCRIBE_CUSTOMER_GOOGLE_UPDATE)
    Observable<DataSubscribeGoogle> updateSubscribeCustomerGoogle(@FieldMap Map<String, String> fields);

    @GET(GET_ENERGY)
    Observable<List<Energy>> getEnergyAll();

    @GET(GET_SOUND)
    Observable<List<Sound>> getSoundAll();

    @GET(GET_DECADE)
    Observable<List<Decade>> getDecadeAll();

    @FormUrlEncoded
    @POST(SAVE_NEW_MOODTRACK)
    Observable<String> saveMoodtrack(@FieldMap Map<String, String> fields);

    @GET(GET_MOODTRACKS + "{customer_id}")
    Observable<List<Moodtrack>> getMoodtracks(@Path("customer_id") String memberId);

    @GET(GET_MOODTRACKS_BY_STORE_ID + "{store_id}")
    Observable<List<Moodtrack>> getMoodtracksByStoreId(@Path("store_id") String storeId);

    @FormUrlEncoded
    @POST(GET_MOODTRACKS_DETAIL)
    Observable<Moodtrack> detailMoodtrack(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UPDATE_POSITION_MOODTRACK_TRACK)
    Observable<String> updatePositionMoodtrackTrack(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(REMOVE_MOODTRACKS)
    Observable<String> remove(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(ADD_MOODTRACK_SCHEDULE)
    Observable<String> addScheduleMoodtrack(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(EDIT_MOODTRACK_SCHEDULE)
    Observable<DataResult> editScheduleMoodtrack(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(GET_SCHEDULE_LIST)
    Observable<List<DataSchedules>> getScheduleList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(REMOVE_SCHEDULE)
    Observable<String> removeSchedule(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(UPDATE_MOODTRACKS)
    Observable<String> updateMood(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(ADD_TRACK_TO_MOOD)
    Observable<String> addTrackToMood(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST(REMOVE_TRACK_FROM_MOOD)
    Observable<String> removeTrackFromMood(@FieldMap Map<String, String> params);
}



