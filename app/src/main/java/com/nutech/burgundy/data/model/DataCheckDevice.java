package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 12/4/17.
 * Xeranta Mobile Solutions
 */

public class DataCheckDevice {
    @SerializedName("success")
    @Expose
    private String success;
}
