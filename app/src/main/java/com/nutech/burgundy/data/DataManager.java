package com.nutech.burgundy.data;

//import com.nutech.burgundy.Manifest;
import com.nutech.burgundy.data.api.AntMediaService;
import com.nutech.burgundy.data.api.DangdutAsikService;
import com.nutech.burgundy.data.api.MelonService;
import com.nutech.burgundy.data.model.DataAbout;
import com.nutech.burgundy.data.model.DataAds;
import com.nutech.burgundy.data.model.DataAlbums;
import com.nutech.burgundy.data.model.DataCheckDevice;
import com.nutech.burgundy.data.model.DataGenre;
import com.nutech.burgundy.data.model.DataHistoryExchange;
import com.nutech.burgundy.data.model.DataKaraoke;
import com.nutech.burgundy.data.model.DataLiricks;
import com.nutech.burgundy.data.model.DataLoginMember;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataOfficial;
import com.nutech.burgundy.data.model.DataOperator;
import com.nutech.burgundy.data.model.DataPlaylist;
import com.nutech.burgundy.data.model.DataPlaylistMember;
import com.nutech.burgundy.data.model.DataPoint;
import com.nutech.burgundy.data.model.DataPromoSlide;
import com.nutech.burgundy.data.model.DataResult;
import com.nutech.burgundy.data.model.DataReward;
import com.nutech.burgundy.data.model.DataRoom;
import com.nutech.burgundy.data.model.DataSchedules;
import com.nutech.burgundy.data.model.DataSearch;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.data.model.DataSubscribe;
import com.nutech.burgundy.data.model.DataSubscribeGoogle;
import com.nutech.burgundy.data.model.DataSubscribePrice;
import com.nutech.burgundy.data.model.DataSubscribtionGoogle;
import com.nutech.burgundy.data.model.DataTimer;
import com.nutech.burgundy.data.model.DataTracks;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.data.model.DataUserSubscribe;
import com.nutech.burgundy.data.model.DataVideos;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.data.model.ResetPIN;
import com.nutech.burgundy.data.model.artist.DataArtistList;
import com.nutech.burgundy.data.model.artist.DataArtists;
import com.nutech.burgundy.data.model.melon.ResponseTransaction;
import com.nutech.burgundy.data.model.melon.Token;
import com.nutech.burgundy.data.model.soundtrack.Decade;
import com.nutech.burgundy.data.model.soundtrack.Energy;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;
import com.nutech.burgundy.data.model.soundtrack.Sound;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import rx.Observable;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */

public class DataManager {

    private DangdutAsikService dangdutAsikService;
    private AntMediaService antMediaService;
    private MelonService melonService;

    private String sessionId;

    public DataManager(DangdutAsikService dangdutAsikService,
                       AntMediaService antMediaService,
                       MelonService melonService) {

        this.dangdutAsikService = dangdutAsikService;
        this.antMediaService = antMediaService;
        this.melonService = melonService;
    }

    public Call<ResponseBody> checkVideoExist(String roomId){ return antMediaService.checkVideoExist(roomId); }

    public Observable<List<DataGenre>> getGenreAll() {
        return dangdutAsikService.getGenreAll();
    }

    public Observable<List<DataArtists>> getAllArtists() {
        return dangdutAsikService.getAllArtists();
    }

    public Observable<List<DataArtists>> getAllArtistsByName(String name) {
        return dangdutAsikService.getAllArtistsByName(name);
    }

    public Observable<List<DataPlaylist>> getAllPlaylist(String memberId, String createdby) {
        return dangdutAsikService.getAllPlaylist(memberId, createdby);
    }

    public Observable<List<DataTracks>> getTracksByPlaylistId(String playlistId) {
        return dangdutAsikService.getTracksByPlaylistId(playlistId);
    }

    public Observable<List<DataPromoSlide>> getPromoSlide() {
        return dangdutAsikService.getPromoSlide();
    }

    public Observable<List<Moodtrack>> getMoodtrackPlayingStore(Map<String,String> data) {
        return dangdutAsikService.getMoodtrackPlayingStore(data);
    }

    public Observable<List<DataStore>> getNearStores(Map<String,String> data) {
        return dangdutAsikService.getNearStores(data);
    }

    public Observable<List<DataTracks>> getTracksByArtist(String idArtists) {
        return dangdutAsikService.getTracksByArtistsId(idArtists);
    }

    public Observable<List<DataAlbums>> getAlbumsByArtist(String artistId) {
        return dangdutAsikService.getAlbumsByArtist(artistId);
    }

    public Observable<List<DataSearch>> getSearchByName(Map<String, String> param) {
        return dangdutAsikService.getSearch(param);
    }

    public Observable<List<DataTracks>> getTracksAll() {
        return dangdutAsikService.getTracksAll();
    }

    public Observable<String> getLogin(Map<String, String> dataLogin) {
        return dangdutAsikService.getLogin(dataLogin);
    }

    public Observable<DataLoginMember> checkMember(Map<String, String> dataLogin) {
        return dangdutAsikService.checkMember(dataLogin);
    }

    public Observable<DataUserStore> storeLogin(Map<String, String> dataLogin) {
        return dangdutAsikService.storeLogin(dataLogin);
    }

    public Observable<String> postRegister(Map<String, String> dataRegister) {
        return dangdutAsikService.postRegister(dataRegister);
    }

    public Observable<DataMember> getCustomerById(String customerId) {
        return dangdutAsikService.getCustomerById(customerId);
    }

    public Observable<List<DataPlaylistMember>> getPlaylistMember(String memberId) {
        return dangdutAsikService.getPlaylistMember(memberId);
    }

    public Observable<String> addPLaylistMember(Map<String, String> data) {
        return dangdutAsikService.addPlaylistMember(data);
    }

    public Observable<String> removePLaylistMember(Map<String, String> data) {
        return dangdutAsikService.removePlaylistMember(data);
    }

    public Observable<List<DataTracks>> getTopChart(Integer limit) {
        return dangdutAsikService.getTopChart(limit);
    }

    public Observable<List<DataPlaylist>> getTopPlaylist(Integer limit) {
        return dangdutAsikService.getTopPlaylist(limit);
    }

    public Observable<List<DataTracks>> getAlbumsTracks(String albumId) {
        return dangdutAsikService.getAlbumsTracks(albumId);
    }

    public Observable<List<DataTracks>> getGenreTracks(String genreId) {
        return dangdutAsikService.getGenreTracks(genreId);
    }

    public Observable<String> addTracksFav(Map<String, String> fields) {
        return dangdutAsikService.addTracksFav(fields);
    }

    public Observable<List<DataTracks>> getTracksMember(String memberId) {
        return dangdutAsikService.getTracksMember(memberId);
    }
    public Observable<List<DataSubscribePrice>> getSubscribePriceAll() {
        return dangdutAsikService.getSubscribePriceAll();
    }

    public Observable<DataSubscribePrice> getSubscribeDetailPrice(String id) {
        return dangdutAsikService.getSubscribeDetailPrice(id);
    }

    public Observable<DataUserSubscribe> getUserSubscribeStatus(String id) {
        return dangdutAsikService.getUserSubscribeStatus(id);
    }

    public Observable<DataSubscribe> subscribeCustomer(Map<String, String> fields) {
        return dangdutAsikService.subscribeCustomer(fields);
    }

    public Observable<DataSubscribe> unSubscribeCustomer(Map<String, String> fields) {
        return dangdutAsikService.unSubscribeCustomer(fields);
    }

    public Observable<String> removeTracksMember(Map<String, String> data) {
        return dangdutAsikService.removeTracksMember(data);
    }

    public Observable<List<DataVideos>> getVideosAll() {
        return dangdutAsikService.getVideosAll();
    }

    public Observable<List<DataLiricks>> getTrackLirick(String tracksId) {
        return dangdutAsikService.getTracksLiricks(tracksId);
    }

    public Observable<String> sendFeedback(Map<String, String> dataFeedback) {
        return dangdutAsikService.sendFeedback(dataFeedback);
    }

    public Observable<DataAbout> getAboutUs() {
        return dangdutAsikService.getAboutUs();
    }

    public Observable<List<DataTracks>> getTracksRecomended(String idArtists, String genreId) {
        return dangdutAsikService.getTracksRecomended(idArtists, genreId);
    }

    public Observable<List<DataPoint>> getPointAll() {
        return dangdutAsikService.getPointAll();
    }

    public Observable<List<DataReward>> getRewardAll() {
        return dangdutAsikService.getRewardAll();
    }

    public Observable<String> updatePointCustomer(Map<String, String> fields) {
        return dangdutAsikService.updatePointCustomer(fields);
    }

    public Observable<String> savePlaylist(Map<String, String> data) {
        return dangdutAsikService.savePlaylist(data);
    }

    public Observable<List<DataPlaylist>> getAllPlaylistCustomer(String customerId) {
        return dangdutAsikService.getAllPlaylistCustomer(customerId);
    }

    public Observable<String> updatePlaylist(Map<String, String> data) {
        return dangdutAsikService.updatePlaylist(data);
    }

    public Observable<String> removePLaylist(Map<String, String> data) {
        return dangdutAsikService.removePlaylist(data);
    }

    public Observable<String> addTracksToPlaylist(Map<String, String> data) {
        return dangdutAsikService.addTracksToPlaylist(data);
    }


    public Observable<List<DataPlaylistMember>> getPlaylistFavorite(String memberId) {
        return dangdutAsikService.getPlaylistFavorite(memberId);
    }

    public Observable<String> insertFavPlaylist(Map<String, String> data) {
        return dangdutAsikService.insertFavPlaylist(data);
    }

    public Observable<String> updatePlaysTracksCount(Map<String, String> data) {
        return dangdutAsikService.updatePlayTracksCount(data);
    }

    public Observable<String> addPointExchange(Map<String, String> data) {
        return dangdutAsikService.addPointExchange(data);
    }

    public Observable<List<DataHistoryExchange>> getHistoryPointExchange(String customerId) {
        return dangdutAsikService.getHistoryPointExchange(customerId);
    }

    public Observable<DataHistoryExchange> getDetailHistory(String id) {
        return dangdutAsikService.getDetailHistory(id);
    }

    public Observable<String> updateStatusPointExchange(Map<String, String> fields) {
        return dangdutAsikService.updateStatusPointExchange(fields);
    }

    public Observable<String> usePointExchange(Map<String, String> fields) {
        return dangdutAsikService.usePointExchange(fields);
    }

    public Observable<DataMember> getDataMember(String id) {
        return dangdutAsikService.getDataMember(id);
    }

    public Observable<DataUserStore> getDataUserStore(String id) {
        return dangdutAsikService.getDataUserStore(id);
    }

    public Observable<String> uploadImageProfile(Map<String, String> fields) {
        return dangdutAsikService.uploadImageProfile(fields);
    }

    public Observable<DataMember> otpVerification(Map<String, String> params) {
        return dangdutAsikService.otpVerification(params);
    }

    public Observable<DataCheckDevice> checkDevice(Map<String, String> params) {
        return dangdutAsikService.checkDevice(params);
    }

    public Observable<DataCheckDevice> checkDeviceStore(Map<String, String> params) {
        return dangdutAsikService.checkDeviceStore(params);
    }

    public Observable<String> sendSms(String phonenumber, String textContent) {
        return dangdutAsikService.sendSms(phonenumber, textContent);
    }

    public Observable<DataSubscribe> updateSubscribeCustomer(Map<String, String> fields) {
        return dangdutAsikService.updateSubscribeCustomer(fields);
    }

    public Observable<DataSubscribe> updateSubscribeActivationCode(Map<String, String> fields) {
        return dangdutAsikService.updateSubscribeActivationCode(fields);
    }

    public Observable<DataKaraoke> getDataKaraoke(String karaokeId) {
        return dangdutAsikService.getDataKaraoke(karaokeId);
    }

    public Observable<String> removeHistoryKaraoke(Map<String, String> fields) {
        return dangdutAsikService.removeHistoryKaraoke(fields);
    }

    public Observable<String> addRoom(Map<String, String> fields) {
        return dangdutAsikService.addRoom(fields);
    }

    public Observable<List<DataRoom>> getHistoryAll(String custId) {
        return dangdutAsikService.getHistoryAll(custId);
    }

    public Observable<List<DataRoom>> getHistoryByCustomer(String customerId) {
        return dangdutAsikService.getHistoryBycustomer(customerId);
    }

    public Observable<String> updateRoom(Map<String, String> fields) {
        return dangdutAsikService.updateRoom(fields);
    }

    public Observable<String> removeRoom(Map<String, String> fields) {
        return dangdutAsikService.removeRoom(fields);
    }

    public Observable<List<DataRoom>> getHistoryLive(String liveStatus) {
        return dangdutAsikService.getHistoryLive(liveStatus);
    }

    public Observable<DataMember> updateDataProfile(Map<String, String> fields) {
        return dangdutAsikService.updateDataProfile(fields);
    }

    public Observable<DataTracks> getTrackById(String id) {
        return dangdutAsikService.getTrackById(id);
    }

    public Observable<DataPromoSlide> getPromoById(String id) {
        return dangdutAsikService.getPromoById(id);
    }

    public Observable<String> addLogCustomer(Map<String, String> data) {
        return dangdutAsikService.addLogCustomer(data);
    }

    public Observable<String> addLogStore(Map<String, String> data) {
        return dangdutAsikService.addLogStore(data);
    }

    public Observable<Map> updateIsPlayingMoodtrack(Map<String, String> data) {
        return dangdutAsikService.updateIsPlayingMoodtrack(data);
    }

    public Observable<List<DataAds>> getAdsList(Map<String, String> data) {
        return dangdutAsikService.getAdsList(data);
    }

    public Observable<String> addLogReport(Map<String, String> data) {
        return dangdutAsikService.addLogReport(data);
    }

    public Observable<String> addLogAds(Map<String, String> data) {
        return dangdutAsikService.addLogAds(data);
    }

    public Observable<List<DataTracks>> getRecomended(Map<String, String> data) {
        return dangdutAsikService.getRecomended(data);
    }

    public Observable<List<Moodtrack>> getMoodtrackForYou(Map<String, String> data) {
        return dangdutAsikService.getMoodtrackForYou(data);
    }

    public Observable<String> updateLikesVideosKaraoke(Map<String, String> fields) {
        return dangdutAsikService.updateLikeVideosKaraoke(fields);
    }

    public Observable<String> updateViewersVideosKaraoke(Map<String, String> fields) {
        return dangdutAsikService.updateViewersVideosKaraoke(fields);
    }

    public Observable<String> updateLikesVideos(Map<String, String> fields) {
        return dangdutAsikService.updateLikeVideos(fields);
    }

    public Observable<String> updateViewersVideos(Map<String, String> fields) {
        return dangdutAsikService.updateViewersVideos(fields);
    }

    public Observable<String> sendReport(Map<String, String> fields) {
        return dangdutAsikService.sendReport(fields);
    }

    public Observable<DataTimer> getTimer() {
        return dangdutAsikService.getTimer();
    }

    public Observable<List<DataRoom>> getTopKaraoke() {
        return dangdutAsikService.getTopKaraoke();
    }

    public Observable<List<DataOfficial>> getAllOfficial() {
        return dangdutAsikService.getAllOfficial();
    }

    public Observable<String> removeTrackPlaylist(Map<String, String> fields) {
        return dangdutAsikService.removePlaylistTrack(fields);
    }

    public Observable<DataMember> changePin(Map<String, String> fields) {
        return dangdutAsikService.changePin(fields);
    }

    public Observable<ResetPIN> resetPIN(String nPhone) {
        return dangdutAsikService.resetPIN(nPhone);
    }

    public Observable<String> uploadVideoKaraoke(MultipartBody.Part file) {
        return dangdutAsikService.uploadVideoKaraoke(file);
    }

    public Observable<String> updateStatusUploadVideo(Map<String, String> fields) {
        return dangdutAsikService.updateStatusUploadVideo(fields);
    }

    public Observable<MemberCheck> checkMemberToIndosis(String mobilePhone) {
        return dangdutAsikService.checkMemberToIndosis(mobilePhone);
    }

    public Observable<DataSubscribeGoogle> subscribeCustomerGoogle(Map<String, String> fields) {
        return dangdutAsikService.subscribeCustomerGoogle(fields);
    }

    public Observable<String> insertDataLanggananIndosis(Map<String, String> fields) {
        return dangdutAsikService.insertDataLanggananIndosis(fields);
    }

    public Observable<Token> requestToken(HashMap<String, String> params) {
        return dangdutAsikService.requestTokenTransaction(params);
    }

    public Observable<List<DataOperator>> getOperator(String number) {
        return dangdutAsikService.getOperatorByNumber(number);
    }

    public Observable<ResponseTransaction> payTransactionMelon(HashMap<String, String> params) {
        return dangdutAsikService.payTransaction(params);
    }

    public Observable<ResponseTransaction> chargeTransactionMelon(HashMap<String, String> params) {
        return dangdutAsikService.chargeTransaction(params);
    }

    public Observable<DataSubscribtionGoogle> getSubsribtionGoogleStatus(Map<String, String> fields) {
        return dangdutAsikService.getSubsribtionGoogleStatus(fields);
    }

    public Observable<DataSubscribeGoogle> updateSubscribeCustomerGoogle(Map<String, String> fields) {
        return dangdutAsikService.updateSubscribeCustomerGoogle(fields);
    }

    public Call<DataArtistList> artistAllWithPaging(int page, int per_page) {
        return dangdutAsikService.getDataArtistWithPaging(page,per_page);
    }

    public Observable<DataArtistList> artistAll() {
        return dangdutAsikService.artistAll();
    }

    public Observable<List<Energy>> getEnergyAll() {
        return dangdutAsikService.getEnergyAll();
    }

    public Observable<List<Sound>> getSoundAll() {
        return dangdutAsikService.getSoundAll();
    }

    public Observable<List<Decade>> getDecadeAll() {
        return dangdutAsikService.getDecadeAll();
    }

    public Observable<String> saveMoodtrack(Map<String, String> data) {
        return dangdutAsikService.saveMoodtrack(data);
    }

    public Observable<List<Moodtrack>> getMoodtracks(String memberId) {
        return dangdutAsikService.getMoodtracksByStoreId(memberId);
    }

    public Observable<List<Moodtrack>> getMoodtracksByStoreId(String storeId) {
        return dangdutAsikService.getMoodtracksByStoreId(storeId);
    }

    public Observable<Moodtrack> detailMoodtrack(Map<String, String> params) {
        return dangdutAsikService.detailMoodtrack(params);
    }

    public Observable<String> updatePositionMoodtrackTrack(Map<String, String> params) {
        return dangdutAsikService.updatePositionMoodtrackTrack(params);
    }

    public Observable<String> remove(Map<String, String> params) {
        return dangdutAsikService.remove(params);
    }

    public Observable<String> addScheduleMoodtrack(Map<String, String> params) {
        return dangdutAsikService.addScheduleMoodtrack(params);
    }

    public Observable<DataResult> editScheduleMoodtrack(Map<String, String> params) {
        return dangdutAsikService.editScheduleMoodtrack(params);
    }

    public Observable<List<DataSchedules>> getScheduleList(Map<String, String> param) {
        return dangdutAsikService.getScheduleList(param);
    }

    public Observable<String> removeSchedule(Map<String, String> param) {
        return dangdutAsikService.removeSchedule(param);
    }

    public Observable<String> updateMood(Map<String, String> params) {
        return dangdutAsikService.updateMood(params);
    }

    public Observable<String> addTrackToMood(Map<String, String> params) {
        return  dangdutAsikService.addTrackToMood(params);
    }

    public Observable<String> removeTrackFromMood(Map<String, String> params) {
        return  dangdutAsikService.removeTrackFromMood(params);
    }
}


