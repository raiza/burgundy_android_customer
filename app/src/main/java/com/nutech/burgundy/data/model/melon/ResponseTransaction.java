package com.nutech.burgundy.data.model.melon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 28/01/19.
 * Xeranta Mobile Solutions
 */
public class ResponseTransaction {

    @SerializedName("response_code")
    @Expose
    private String response_code;

    @SerializedName("payment_type")
    @Expose
    private String payment_type;

    @SerializedName("transaction_id")
    @Expose
    private String transaction_id;

    @SerializedName("order_id")
    @Expose
    private String order_id;

    @SerializedName("transaction_status")
    @Expose
    private String transaction_status;

    public String getResponse_code() {
        return response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(String transaction_status) {
        this.transaction_status = transaction_status;
    }
}
