package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 12/6/17.
 * Xeranta Mobile Solutions
 */

public class DataTempSubscribe {
    @SerializedName("subscribe_code")
    @Expose
    private String subscribe_code;
    @SerializedName("activation_code")
    @Expose
    private String activation_code;

    public String getSubscribe_code() {
        return subscribe_code;
    }

    public void setSubscribe_code(String subscribe_code) {
        this.subscribe_code = subscribe_code;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }
}
