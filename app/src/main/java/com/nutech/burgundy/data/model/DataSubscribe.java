package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Dafian on 10/17/17
 */

public class DataSubscribe {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("subscribe_code")
    @Expose
    private String subscribe_code;
    @SerializedName("activation_code")
    @Expose
    private String activation_code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubscribe_code() {
        return subscribe_code;
    }

    public void setSubscribe_code(String subscribe_code) {
        this.subscribe_code = subscribe_code;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }
}
