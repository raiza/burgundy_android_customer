package com.nutech.burgundy.data;

import android.content.SharedPreferences;

import com.nutech.burgundy.config.Constants;
import com.nutech.burgundy.data.model.DataMember;
import com.nutech.burgundy.data.model.DataOperator;
import com.nutech.burgundy.data.model.DataStore;
import com.nutech.burgundy.data.model.DataUserStore;
import com.nutech.burgundy.data.model.MemberCheck;
import com.nutech.burgundy.data.model.melon.Token;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;

/**
 * Created by Arif Setiawan on 8/28/17.
 * Xeranta Mobile Solutions
 */
public class SessionManager {

    private final SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_INTRO = "IsIntro";
    private static final String IS_COMPLETE = "IsComplete";

    //Member
    public static final String M_ID = "m_id";
    public static final String M_STORE_ID = "m_store_id";
    public static final String M_NAME = "m_name";
    public static final String M_SITE_NAME = "m_site_name";
    public static final String M_STORE_ADS_BLOCK = "m_ads_block";
    public static final String M_CLIENT_ID = "m_client_id";
    public static final String M_CF1 = "m_cf1";
    public static final String M_CF2 = "m_cf2";
    public static final String M_PHONE = "m_phone";
    public static final String M_TEL = "m_tel";
    public static final String M_MOBILEPHONE = "m_mobilephone";
    public static final String M_EMAIL = "m_email";
    public static final String M_BIRTHDATE = "m_birthdate";
    public static final String M_CARDID = "m_cardid";
    public static final String M_JOINDATE = "m_joindate";
    public static final String M_USERNAME = "m_username";
    public static final String M_ACTIVCODE = "m_activcode";
    public static final String M_ADDRESS = "m_address";
    public static final String M_CITYID = "m_cityid";
    public static final String M_STATEID = "m_stateid";
    public static final String M_STOREID = "m_storeid";
    public static final String M_ROLE = "m_role";
    public static final String M_LAT = "m_lat";
    public static final String M_LNG = "m_lng";
    public static final String M_REGTYPE = "m_regtype";
    public static final String M_ACTIVSTATUS = "m_activstatus";
    public static final String M_REGISTERID = "m_registerid";
    public static final String M_POINT = "m_point";
    public static final String M_SUBCRIBE_NAME = "m_subcribe_name";
    public static final String M_IMEI = "m_imei";
    public static final String M_USER_TYPE = "m_user_type";

    //Moodtrack Filter

    public static final String M_DECADE = "m_decade";
    public static final String M_GENRE = "m_genre";
    public static final String M_SOUND = "m_sound";
    public static final String M_ENERGY = "m_energy";

    //Subscriber Status
    public static final String S_PKG_STATUS = "s_package_status";
    public static final String S_PKG_NAME = "s_package_name";
    public static final String S_PKG_CODE = "s_package_code";

    public static final String PLAYING_STATE = "playing_state";
    private static final String CURRENT_TRACKS_ID = "curent_tracks_id";
    private static final String MAX_DURATION = "max_duration";
    private static final String PREV_TRACKS_ID = "prev_tracks_id";
    public static final String KEY_REGISTER_ID = "registerid";
    public static final String KEY_TIMER_PLAY = "timer";
    public static final String CURRENT_MOODTRACK = "moodtrack";
    public static final int KEY_CURRENT_DURATION = 0;

    //--- Member Check Indosis
    public static final String I_STATUS = "STATUS";
    public static final String I_MSISDN = "MSISDN";
    public static final String I_DTM_START = "DTM_START";
    public static final String I_LAST_SUCESS_DR = "LAST_SUCESS_DR";
    public static final String I_MESSAGE = "MESSAGE";

    public static final String ID = "id";
    public static final String SHORT_NAME = "shortName";
    public static final String FULL_NAME = "fullName";
    public static final String NUMBER_LIST = "numberList";
    public static final String DATE_CREATED = "createDate";

    public static final String TOKEN = "token";

    public static final String TOKEN_GPA = "token_gpa";
    public static final String SKU_GPA = "sku_gpa";
    public static final String PAYMENT_TYPE = "payment_type";

    //validate gps
    public static final String IS_NO_GPS = "is_no_gps";


    public SessionManager(SharedPreferences preferences) {
        this.preferences = preferences;
        this.editor = preferences.edit();
    }

    public void login(){
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }

    public void logout(){
        editor.clear();
        editor.putBoolean(IS_LOGIN, false);
        editor.commit();
    }

    public boolean isLoggedIn(){
        return preferences.getBoolean(IS_LOGIN, false);
    }

    public void setIsIntro(boolean val) {
        //true = user sudah pernah ke intro, false = user belum ke intro
        editor.putBoolean(IS_INTRO, val);
        editor.commit();
    }

    public boolean isIntro() {
        return preferences.getBoolean(IS_INTRO,false);
    }

    public boolean setSessionStatePlaying(String STATE) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PLAYING_STATE, STATE);
        return editor.commit();
    }

    public String getSessionStatePlaying() {
        String playing = preferences.getString(PLAYING_STATE, "");
        return playing;
    }

    public boolean setCurrentTracksId(String Id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(CURRENT_TRACKS_ID, Id);
        return editor.commit();
    }

    public String getCurrentTracksId() {
        String TracksId = preferences.getString(CURRENT_TRACKS_ID, "");
        return TracksId;
    }

    public boolean setMaxDuration(String duration) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MAX_DURATION, duration);
        return editor.commit();
    }

    public String getMaxDuration() {
        String duration = preferences.getString(MAX_DURATION, "00:00");
        return duration;
    }

    public boolean setPrevTracksId(String Id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREV_TRACKS_ID, Id);
        return editor.commit();
    }

    public String getPrevTracksId() {
        String TracksId = preferences.getString(PREV_TRACKS_ID, "");
        return TracksId;
    }

    public boolean setIsNoGps(boolean val) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_NO_GPS, val);
        return editor.commit();
    }

    public boolean getIsNoGps() {
        boolean is_no_gps = preferences.getBoolean(IS_NO_GPS,false);
        return is_no_gps;
    }

    public boolean setPackage(String packages) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(S_PKG_NAME, packages);
        return editor.commit();
    }

    public String getPackage() {
        String packages = preferences.getString(S_PKG_NAME, "");
        return packages;
    }

    public boolean setPackageCode(String package_code) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(S_PKG_CODE, package_code);
        return editor.commit();
    }

    public String getPackageCode() {
        String packageCode = preferences.getString(S_PKG_CODE, "");
        return packageCode;
    }

    public boolean setStatusPackage(String status) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(S_PKG_STATUS, status);
        return editor.commit();
    }

    public String getStatusPackage() {
        String status = preferences.getString(S_PKG_STATUS, "");
        return status;
    }

    public boolean setCurrentDuration(int duration) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(String.valueOf(KEY_CURRENT_DURATION), duration);
        return editor.commit();
    }

    public int getCurrentDuration() {
        int duration = preferences.getInt(String.valueOf(KEY_CURRENT_DURATION), 0);
        return duration;
    }

    public String getUserType() {
        String usertType = preferences.getString(M_USER_TYPE,"");
        return usertType;
    }


    public boolean setDataMember(DataMember dataMember) {

        SharedPreferences.Editor editor = preferences.edit();
//        editor.putBoolean(IS_LOGIN, true);
        editor.putString(M_ID, dataMember.getId());
        editor.putString(M_NAME, dataMember.getName());
        editor.putString(M_CF1, dataMember.getCf1());
        editor.putString(M_CF2, dataMember.getCf2());
        editor.putString(M_PHONE, dataMember.getPhone());
        editor.putString(M_MOBILEPHONE, dataMember.getMobile_phone());
        editor.putString(M_EMAIL, dataMember.getEmail());
        editor.putString(M_BIRTHDATE, dataMember.getBirth_date());
        editor.putString(M_CARDID, dataMember.getMember_card_id());
        editor.putString(M_JOINDATE, dataMember.getJoin_date());
        editor.putString(M_USERNAME, dataMember.getUsername());
        editor.putString(M_ACTIVCODE, dataMember.getActivation_code());
        editor.putString(M_ADDRESS, dataMember.getAddress());
        editor.putString(M_CITYID, dataMember.getCity_id());
        editor.putString(M_STATEID, dataMember.getState_id());
        editor.putString(M_LAT, dataMember.getLat());
        editor.putString(M_LNG, dataMember.getLng());
        editor.putString(M_REGTYPE, dataMember.getRegister_type());
        editor.putString(M_ACTIVSTATUS, dataMember.getActive_status());
        editor.putString(M_REGISTERID, dataMember.getRegister_id());
        editor.putString(M_POINT, dataMember.getPoint());
        editor.putString(M_SUBCRIBE_NAME, dataMember.getSubscribename());
        editor.putString(M_IMEI, dataMember.getImei());
        editor.putString(M_USER_TYPE, Constants.UserType.USER_PUBLIC);
        return editor.commit();
    }

    public DataMember getDataMember() {

        DataMember dataMember = new DataMember();
        dataMember.setId(preferences.getString(M_ID, ""));
        dataMember.setName(preferences.getString(M_NAME, ""));
        dataMember.setCf1(preferences.getString(M_CF1, ""));
        dataMember.setCf2(preferences.getString(M_CF2, ""));
        dataMember.setPhone(preferences.getString(M_PHONE, ""));
        dataMember.setMobile_phone(preferences.getString(M_MOBILEPHONE, ""));
        dataMember.setEmail(preferences.getString(M_EMAIL, ""));
        dataMember.setBirth_date(preferences.getString(M_BIRTHDATE, ""));
        dataMember.setMember_card_id(preferences.getString(M_CARDID, ""));
        dataMember.setJoin_date(preferences.getString(M_JOINDATE, ""));
        dataMember.setUsername(preferences.getString(M_USERNAME, ""));
        dataMember.setActivation_code(preferences.getString(M_ACTIVCODE, ""));
        dataMember.setAddress(preferences.getString(M_ADDRESS, ""));
        dataMember.setCity_id(preferences.getString(M_CITYID, ""));
        dataMember.setState_id(preferences.getString(M_STATEID, ""));
        dataMember.setLat(preferences.getString(M_LAT, ""));
        dataMember.setLng(preferences.getString(M_LNG, ""));
        dataMember.setRegister_type(preferences.getString(M_REGTYPE, ""));
        dataMember.setActive_status(preferences.getString(M_ACTIVSTATUS, ""));
        dataMember.setRegister_id(preferences.getString(M_REGISTERID, ""));
        dataMember.setPoint(preferences.getString(M_POINT, ""));
        dataMember.setSubscribename(preferences.getString(M_SUBCRIBE_NAME, ""));
        dataMember.setImei(preferences.getString(M_IMEI, ""));

        return dataMember;
    }

    public boolean setUserStore(DataUserStore dataUserStore) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(M_ID, dataUserStore.getId());
        editor.putString(M_NAME, dataUserStore.getName());
        editor.putString(M_MOBILEPHONE, dataUserStore.getMobile_phone());
        editor.putString(M_EMAIL, dataUserStore.getEmail());
        editor.putString(M_BIRTHDATE, dataUserStore.getBirth_date());
        editor.putString(M_ADDRESS, dataUserStore.getAddress());
        editor.putString(M_IMEI, dataUserStore.getImei());
        editor.putString(M_REGISTERID, dataUserStore.getRegisterId());
        editor.putString(M_IMEI, dataUserStore.getImei());
        editor.putString(M_USER_TYPE, Constants.UserType.USER_STORE);
        editor.putString(M_STOREID, dataUserStore.getStore_id());
        editor.putString(M_ROLE, dataUserStore.getRole());
        return editor.commit();
    }

    public DataUserStore getUserStore() {

        DataUserStore dataUserStore = new DataUserStore();
        dataUserStore.setId(preferences.getString(M_ID,""));
        dataUserStore.setMobile_phone(preferences.getString(M_MOBILEPHONE,""));
        dataUserStore.setEmail(preferences.getString(M_EMAIL,""));
        dataUserStore.setBirth_date(preferences.getString(M_BIRTHDATE,""));
        dataUserStore.setAddress(preferences.getString(M_ADDRESS,""));
        dataUserStore.setImei(preferences.getString(M_IMEI,""));
        dataUserStore.setStore_id(preferences.getString(M_STOREID,""));
        dataUserStore.setRole(preferences.getString(M_ROLE,""));
        return dataUserStore;
    }

    public boolean setStore(DataStore dataStore) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(M_STORE_ID, dataStore.getStore_id());
        editor.putString(M_SITE_NAME, dataStore.getSite_name());
        editor.putString(M_STORE_ADS_BLOCK, dataStore.getTypeBlock());
        editor.putString(M_CLIENT_ID, dataStore.getClient_id());
        return editor.commit();
    }

    public DataStore getStore() {

        DataStore dataStore = new DataStore();
        dataStore.setStore_id(preferences.getString(M_STORE_ID,""));
        dataStore.setSite_name(preferences.getString(M_SITE_NAME,""));
        dataStore.setTypeBlock(preferences.getString(M_STORE_ADS_BLOCK,""));
        dataStore.setClient_id(preferences.getString(M_CLIENT_ID,""));

        return dataStore;
    }

    public boolean setMoodtrackFilter(Moodtrack moodtrack) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(M_DECADE, moodtrack.getDecadeIds());
        editor.putString(M_SOUND, moodtrack.getSoundIds());
        editor.putString(M_ENERGY, moodtrack.getEnergyids());
        editor.putString(M_GENRE, moodtrack.getGenreIds());
        return editor.commit();
    }

    public Moodtrack getMoodtrackFilter() {

        Moodtrack moodtrack = new Moodtrack();
        moodtrack.setGenreIds(preferences.getString(M_GENRE,""));
        moodtrack.setDecadeIds(preferences.getString(M_DECADE,""));
        moodtrack.setSoundIds(preferences.getString(M_SOUND,""));
        moodtrack.setEnergyids(preferences.getString(M_ENERGY,""));

        return moodtrack;
    }

    public boolean setDataMember(MemberCheck dataMember) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(I_STATUS, dataMember.getStatus());
        editor.putString(I_MSISDN, dataMember.getMsisdn());
        editor.putString(I_DTM_START, dataMember.getDtm_start());
        editor.putString(I_LAST_SUCESS_DR, dataMember.getLast_sucess_dr());
        editor.putString(I_MESSAGE, dataMember.getMessage());
        return editor.commit();
    }

    public MemberCheck getMemberCheck() {

        MemberCheck dataMember = new MemberCheck();
        dataMember.setStatus(preferences.getString(I_STATUS, ""));
        dataMember.setMsisdn(preferences.getString(I_MSISDN, ""));
        dataMember.setDtm_start(preferences.getString(I_DTM_START, ""));
        dataMember.setLast_sucess_dr(preferences.getString(I_LAST_SUCESS_DR, ""));
        dataMember.setMessage(preferences.getString(I_MESSAGE, ""));

        return dataMember;
    }

    public boolean setMemberPoint(String point){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(M_POINT, point);
        return editor.commit();
    }

    public boolean setMemberSubscribeName(String subscribe){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(M_SUBCRIBE_NAME, subscribe);
        return editor.commit();
    }

    public boolean setRegisterId(String registerid) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_REGISTER_ID, registerid);
        return editor.commit();
    }

    public String getRegisterId(){
        String registerId = preferences.getString(KEY_REGISTER_ID, "");
        return registerId;
    }

    public boolean setTimer(int timer) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(KEY_TIMER_PLAY, timer);
        return editor.commit();
    }

    public int getTimer() {
        int timer = preferences.getInt(KEY_TIMER_PLAY, 60);
        return timer;
    }

    public boolean setCurrentMoodTrack(String id) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(CURRENT_MOODTRACK, id);
        return editor.commit();
    }

    public String getCurrentMoodTrack() {
        String id = preferences.getString(CURRENT_MOODTRACK, "");
        return id;
    }

    public boolean setOperator(DataOperator operator) {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ID, operator.getId());
        editor.putString(SHORT_NAME, operator.getShortName());
        editor.putString(FULL_NAME, operator.getFullName());
        editor.putString(NUMBER_LIST, operator.getNumberList());
        editor.putString(DATE_CREATED, operator.getCreate_date());
        return editor.commit();
    }

    public DataOperator getOperator() {

        DataOperator operator = new DataOperator();
        operator.setId(preferences.getString(ID, ""));
        operator.setShortName(preferences.getString(SHORT_NAME, ""));
        operator.setFullName(preferences.getString(FULL_NAME, ""));
        operator.setNumberList(preferences.getString(NUMBER_LIST, ""));
        operator.setCreate_date(preferences.getString(DATE_CREATED, ""));

        return operator;
    }

    public boolean setToken(Token token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN, token.getToken());
        return editor.commit();
    }

    public Token getToken() {
        Token token = new Token();
        token.setToken(preferences.getString(TOKEN, ""));
        return token;
    }

    public boolean setTokenGPA(String tokenGpa) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TOKEN_GPA, tokenGpa);
        return editor.commit();
    }

    public String getTokenGPA() {
        String tokenGPA = "";
        tokenGPA = preferences.getString(TOKEN_GPA, "");
        return tokenGPA;
    }

    public boolean setSkuGPA(String skuGpa) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SKU_GPA, skuGpa);
        return editor.commit();
    }

    public String getSkuGPA() {
        String skuGPA = preferences.getString(SKU_GPA, "");
        return skuGPA;
    }

    public boolean setPaymentType(String paytype) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PAYMENT_TYPE, paytype);
        return editor.commit();
    }

    public String getPaymentType() {
        String payType = preferences.getString(PAYMENT_TYPE, "");
        return payType;
    }

}
