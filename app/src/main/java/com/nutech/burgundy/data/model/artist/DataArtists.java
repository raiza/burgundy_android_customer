package com.nutech.burgundy.data.model.artist;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 8/31/17.
 * Xeranta Mobile Solutions
 */

public class DataArtists implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("followers")
    @Expose
    private String followers;
    @SerializedName("popularity")
    @Expose
    private String popularity;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("fully_scraped")
    @Expose
    private String fully_scraped;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("genre_id")
    @Expose
    private String genre_id;
    @SerializedName("jumlahtracks")
    @Expose
    private String sumTracks;
    @SerializedName("count")
    @Expose
    private String count;


    protected DataArtists(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        followers = in.readString();
        popularity = in.readString();
        image = in.readString();
        fully_scraped = in.readString();
        bio = in.readString();
        tags = in.readString();
        genre_id = in.readString();
        sumTracks = in.readString();
        count = in.readString();
    }

    public static final Creator<DataArtists> CREATOR = new Creator<DataArtists>() {
        @Override
        public DataArtists createFromParcel(Parcel in) {
            return new DataArtists(in);
        }

        @Override
        public DataArtists[] newArray(int size) {
            return new DataArtists[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFully_scraped() {
        return fully_scraped;
    }

    public void setFully_scraped(String fully_scraped) {
        this.fully_scraped = fully_scraped;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(String genre_id) {
        this.genre_id = genre_id;
    }

    public String getSumTracks() {
        return sumTracks;
    }

    public void setSumTracks(String sumTracks) {
        this.sumTracks = sumTracks;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.code);
        parcel.writeString(this.followers);
        parcel.writeString(this.popularity);
        parcel.writeString(this.image);
        parcel.writeString(this.fully_scraped);
        parcel.writeString(this.bio);
        parcel.writeString(this.tags);
        parcel.writeString(this.genre_id);
        parcel.writeString(this.sumTracks);
        parcel.writeString(this.count);
    }

    public DataArtists() {
    }

}
