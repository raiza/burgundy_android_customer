package com.nutech.burgundy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 12/12/17.
 * Xeranta Mobile Solutions
 */

public class DataKaraoke implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("album_names")
    @Expose
    private String album_name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("artists")
    @Expose
    private String artists;
    @SerializedName("ref_id")
    @Expose
    private String ref_id;
    @SerializedName("popularity")
    @Expose
    private String popularity;
    @SerializedName("album_id")
    @Expose
    private String album_id;
    @SerializedName("artist_id")
    @Expose
    private String artist_id;
    @SerializedName("temp_id")
    @Expose
    private String temp_id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("plays")
    @Expose
    private String plays;
    @SerializedName("favorites")
    @Expose
    private String favorites;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("registers")
    @Expose
    private String registers;
    @SerializedName("publisher_id")
    @Expose
    private String publisher_id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("karaoke_id")
    @Expose
    private String karaoke_id;
    @SerializedName("include_karaoke")
    @Expose
    private String include_karaoke;

    protected DataKaraoke(Parcel in) {
        id = in.readString();
        name = in.readString();
        album_name = in.readString();
        code = in.readString();
        number = in.readString();
        duration = in.readString();
        artists = in.readString();
        ref_id = in.readString();
        popularity = in.readString();
        album_id = in.readString();
        artist_id = in.readString();
        temp_id = in.readString();
        url = in.readString();
        tags = in.readString();
        plays = in.readString();
        favorites = in.readString();
        likes = in.readString();
        registers = in.readString();
        publisher_id = in.readString();
        image = in.readString();
        source = in.readString();
        karaoke_id = in.readString();
        include_karaoke = in.readString();
    }

    public static final Creator<DataTracks> CREATOR = new Creator<DataTracks>() {
        @Override
        public DataTracks createFromParcel(Parcel in) {
            return new DataTracks(in);
        }

        @Override
        public DataTracks[] newArray(int size) {
            return new DataTracks[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlbum_name() {
        return album_name;
    }

    public void setAlbum_name(String album_name) {
        this.album_name = album_name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(String artist_id) {
        this.artist_id = artist_id;
    }

    public String getTemp_id() {
        return temp_id;
    }

    public void setTemp_id(String temp_id) {
        this.temp_id = temp_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getPlays() {
        return plays;
    }

    public void setPlays(String plays) {
        this.plays = plays;
    }

    public String getFavorites() {
        return favorites;
    }

    public void setFavorites(String favorites) {
        this.favorites = favorites;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getRegisters() {
        return registers;
    }

    public void setRegisters(String registers) {
        this.registers = registers;
    }

    public String getPublisher_id() {
        return publisher_id;
    }

    public void setPublisher_id(String publisher_id) {
        this.publisher_id = publisher_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getKaraoke_id() {
        return karaoke_id;
    }

    public void setKaraoke_id(String karaoke_id) {
        this.karaoke_id = karaoke_id;
    }

    public String getInclude_karaoke() {
        return include_karaoke;
    }

    public void setInclude_karaoke(String include_karaoke) {
        this.include_karaoke = include_karaoke;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.name);
        parcel.writeString(this.album_name);
        parcel.writeString(this.code);
        parcel.writeString(this.number);
        parcel.writeString(this.duration);
        parcel.writeString(this.artists);
        parcel.writeString(this.ref_id);
        parcel.writeString(this.popularity);
        parcel.writeString(this.album_id);
        parcel.writeString(this.artist_id);
        parcel.writeString(this.temp_id);
        parcel.writeString(this.url);
        parcel.writeString(this.tags);
        parcel.writeString(this.plays);
        parcel.writeString(this.favorites);
        parcel.writeString(this.likes);
        parcel.writeString(this.registers);
        parcel.writeString(this.publisher_id);
        parcel.writeString(this.image);
        parcel.writeString(this.source);
        parcel.writeString(this.karaoke_id);
        parcel.writeString(this.include_karaoke);
    }

    public DataKaraoke() {
    }
}
