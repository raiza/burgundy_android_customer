package com.nutech.burgundy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 13/02/18.
 * Xeranta Mobile Solutions
 */

public class DataOfficial implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("medsos_name")
    @Expose
    private String medsos_name;
    @SerializedName("medsos_image")
    @Expose
    private String medsos_image;
    @SerializedName("medsos_url")
    @Expose
    private String medsos_url;

    public DataOfficial(Parcel in) {
        id = in.readString();
        medsos_name = in.readString();
        medsos_image = in.readString();
        medsos_url = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedsos_name() {
        return medsos_name;
    }

    public void setMedsos_name(String medsos_name) {
        this.medsos_name = medsos_name;
    }

    public String getMedsos_image() {
        return medsos_image;
    }

    public void setMedsos_image(String medsos_image) {
        this.medsos_image = medsos_image;
    }

    public String getMedsos_url() {
        return medsos_url;
    }

    public void setMedsos_url(String medsos_url) {
        this.medsos_url = medsos_url;
    }

    public static final Creator<DataOfficial> CREATOR = new Creator<DataOfficial>() {
        @Override
        public DataOfficial createFromParcel(Parcel in) {
            return new DataOfficial(in);
        }

        @Override
        public DataOfficial[] newArray(int size) {
            return new DataOfficial[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.medsos_name);
        parcel.writeString(this.medsos_image);
        parcel.writeString(this.medsos_url);
    }
}
