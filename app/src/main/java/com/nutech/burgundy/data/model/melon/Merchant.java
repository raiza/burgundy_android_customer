package com.nutech.burgundy.data.model.melon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 28/01/19.
 * Xeranta Mobile Solutions
 */
public class Merchant {

    @SerializedName("merchant_id")
    @Expose
    private String merchant_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("callback_url")
    @Expose
    private String callback_url;

    @SerializedName("success_url")
    @Expose
    private String success_url;

    @SerializedName("cancel_url")
    @Expose
    private String cancel_url;

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public String getSuccess_url() {
        return success_url;
    }

    public void setSuccess_url(String success_url) {
        this.success_url = success_url;
    }

    public String getCancel_url() {
        return cancel_url;
    }

    public void setCancel_url(String cancel_url) {
        this.cancel_url = cancel_url;
    }
}
