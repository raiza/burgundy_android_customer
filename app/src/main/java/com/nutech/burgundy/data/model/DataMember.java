package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 9/27/17.
 * Xeranta Mobile Solutions
 */

public class DataMember {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("cf1")
    @Expose
    private String cf1;
    @SerializedName("cf2")
    @Expose
    private String cf2;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("mobile_phone")
    @Expose
    private String mobile_phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birth_date")
    @Expose
    private String birth_date;
    @SerializedName("member_card_id")
    @Expose
    private String member_card_id;
    @SerializedName("join_date")
    @Expose
    private String join_date;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("activation_code")
    @Expose
    private String activation_code;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city_id")
    @Expose
    private String city_id;
    @SerializedName("state_id")
    @Expose
    private String state_id;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("register_type")
    @Expose
    private String register_type;
    @SerializedName("active_status")
    @Expose
    private String active_status;
    @SerializedName("register_id")
    @Expose
    private String register_id;
    @SerializedName("point")
    @Expose
    private String point;
    @SerializedName("subscribe_name")
    @Expose
    private String subscribename;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("imei")
    @Expose
    private String imei;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCf1() {
        return cf1;
    }

    public void setCf1(String cf1) {
        this.cf1 = cf1;
    }

    public String getCf2() {
        return cf2;
    }

    public void setCf2(String cf2) {
        this.cf2 = cf2;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getMember_card_id() {
        return member_card_id;
    }

    public void setMember_card_id(String member_card_id) {
        this.member_card_id = member_card_id;
    }

    public String getJoin_date() {
        return join_date;
    }

    public void setJoin_date(String join_date) {
        this.join_date = join_date;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getRegister_type() {
        return register_type;
    }

    public void setRegister_type(String register_type) {
        this.register_type = register_type;
    }

    public String getActive_status() {
        return active_status;
    }

    public void setActive_status(String active_status) {
        this.active_status = active_status;
    }

    public String getRegister_id() {
        return register_id;
    }

    public void setRegister_id(String register_id) {
        this.register_id = register_id;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getSubscribename() {
        return subscribename;
    }

    public void setSubscribename(String subscribename) {
        this.subscribename = subscribename;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
