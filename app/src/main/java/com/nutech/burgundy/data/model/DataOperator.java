package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 11/02/19.
 * Xeranta Mobile Solutions
 */
public class DataOperator {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("shortName")
    @Expose
    private String shortName;

    @SerializedName("fullName")
    @Expose
    private String fullName;

    @SerializedName("numberList")
    @Expose
    private String numberList;

    @SerializedName("create_date")
    @Expose
    private String create_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNumberList() {
        return numberList;
    }

    public void setNumberList(String numberList) {
        this.numberList = numberList;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }
}
