package com.nutech.burgundy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 11/26/17.
 * Xeranta Mobile Solutions
 */

public class DataHistoryExchange implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("customer_id")
    @Expose
    private String customerid;
    @SerializedName("reward_id")
    @Expose
    private String rewardid;
    @SerializedName("point_used")
    @Expose
    private String pointused;
    @SerializedName("activation_code")
    @Expose
    private String activationcode;
    @SerializedName("exchange_date")
    @Expose
    private String exchangedate;
    @SerializedName("expired_date")
    @Expose
    private String expireddate;
    @SerializedName("exchange_status")
    @Expose
    private String exchangestatus;
    @SerializedName("reward_name")
    @Expose
    private String rewardname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getRewardid() {
        return rewardid;
    }

    public void setRewardid(String rewardid) {
        this.rewardid = rewardid;
    }

    public String getPointused() {
        return pointused;
    }

    public void setPointused(String pointused) {
        this.pointused = pointused;
    }

    public String getActivationcode() {
        return activationcode;
    }

    public void setActivationcode(String activationcode) {
        this.activationcode = activationcode;
    }

    public String getExchangedate() {
        return exchangedate;
    }

    public void setExchangedate(String exchangedate) {
        this.exchangedate = exchangedate;
    }

    public String getExpireddate() {
        return expireddate;
    }

    public void setExpireddate(String expireddate) {
        this.expireddate = expireddate;
    }

    public String getExchangestatus() {
        return exchangestatus;
    }

    public void setExchangestatus(String exchangestatus) {
        this.exchangestatus = exchangestatus;
    }

    public String getRewardname() {
        return rewardname;
    }

    public void setRewardname(String rewardname) {
        this.rewardname = rewardname;
    }

    public static final Creator<DataHistoryExchange> CREATOR = new Creator<DataHistoryExchange>() {
        @Override
        public DataHistoryExchange createFromParcel(Parcel in) {
            return new DataHistoryExchange(in);
        }

        @Override
        public DataHistoryExchange[] newArray(int size) {
            return new DataHistoryExchange[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.customerid);
        parcel.writeString(this.activationcode);
        parcel.writeString(this.exchangedate);
        parcel.writeString(this.exchangestatus);
        parcel.writeString(this.expireddate);
        parcel.writeString(this.rewardid);
        parcel.writeString(this.pointused);
        parcel.writeString(this.rewardname);

    }

    public DataHistoryExchange(Parcel in) {
        id = in.readString();
        customerid = in.readString();
        activationcode = in.readString();
        exchangedate = in.readString();
        exchangestatus = in.readString();
        expireddate = in.readString();
        rewardid = in.readString();
        pointused = in.readString();
        rewardname = in.readString();
    }
}
