package com.nutech.burgundy.data.model.soundtrack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nutech.burgundy.data.model.DataTracks;

import java.util.List;

/**
 * Created by Arif Setiawan on 2019-07-16.
 * NuTech (Nusantara Teknologi)
 */
public class Moodtrack {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("public")
    @Expose
    private String publicPlaylist;
    @SerializedName("popularity")
    @Expose
    private String popularity;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("favorites")
    @Expose
    private String favorites;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("registers")
    @Expose
    private String registers;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("energy_ids")
    @Expose
    private String energyids;
    @SerializedName("sound_ids")
    @Expose
    private String soundIds;
    @SerializedName("genre_ids")
    @Expose
    private String genreIds;
    @SerializedName("decade_ids")
    @Expose
    private String decadeIds;

    @SerializedName("tracks")
    @Expose
    private List<DataTracks> dataTracks;

    @SerializedName("site_name")
    @Expose
    private String siteName;

    @SerializedName("distance")
    @Expose
    private String distance;

    @SerializedName("is_playing")
    @Expose
    private String isPlaying;

    @SerializedName("store_id")
    @Expose
    private String storeId;

    public String getIsPlaying() {
        return isPlaying;
    }

    public void setIsPlaying(String isPlaying) {
        this.isPlaying = isPlaying;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPublicPlaylist() {
        return publicPlaylist;
    }

    public void setPublicPlaylist(String publicPlaylist) {
        this.publicPlaylist = publicPlaylist;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFavorites() {
        return favorites;
    }

    public void setFavorites(String favorites) {
        this.favorites = favorites;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getRegisters() {
        return registers;
    }

    public void setRegisters(String registers) {
        this.registers = registers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEnergyids() {
        return energyids;
    }

    public void setEnergyids(String energyids) {
        this.energyids = energyids;
    }

    public String getSoundIds() {
        return soundIds;
    }

    public void setSoundIds(String soundIds) {
        this.soundIds = soundIds;
    }

    public String getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(String genreIds) {
        this.genreIds = genreIds;
    }

    public String getDecadeIds() {
        return decadeIds;
    }

    public void setDecadeIds(String decadeIds) {
        this.decadeIds = decadeIds;
    }

    public List<DataTracks> getDataTracks() {
        return dataTracks;
    }

    public void setDataTracks(List<DataTracks> dataTracks) {
        this.dataTracks = dataTracks;
    }
}
