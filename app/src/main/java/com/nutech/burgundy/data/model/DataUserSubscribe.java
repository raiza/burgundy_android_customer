package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Dafian on 10/17/17
 */

public class DataUserSubscribe {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("subscribe_code")
    @Expose
    private String subscribeCode;
    @SerializedName("subscribe_id")
    @Expose
    private String subscribeId;
    @SerializedName("package_name")
    @Expose
    private String packageName;
    @SerializedName("days_remain")
    @Expose
    private String daysRemain;
    @SerializedName("hours_remain")
    @Expose
    private String hoursRemain;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("order_id")
    @Expose
    private String order_id;
    @SerializedName("order_date")
    @Expose
    private String order_date;
    @SerializedName("product_id")
    @Expose
    private String productSku;
    @SerializedName("payment_type")
    @Expose
    private String payment_type;
    @SerializedName("pay_amount")
    @Expose
    private String pay_amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubscribeCode() {
        return subscribeCode;
    }

    public void setSubscribeCode(String subscribeCode) {
        this.subscribeCode = subscribeCode;
    }

    public String getSubscribeId() {
        return subscribeId;
    }

    public void setSubscribeId(String subscribeId) {
        this.subscribeId = subscribeId;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getDaysRemain() {
        return daysRemain;
    }

    public void setDaysRemain(String daysRemain) {
        this.daysRemain = daysRemain;
    }

    public String getHoursRemain() {
        return hoursRemain;
    }

    public void setHoursRemain(String hoursRemain) {
        this.hoursRemain = hoursRemain;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getProductSku() {
        return productSku;
    }

    public void setProductSku(String productSku) {
        this.productSku = productSku;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(String pay_amount) {
        this.pay_amount = pay_amount;
    }
}
