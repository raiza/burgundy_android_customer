package com.nutech.burgundy.data.model.soundtrack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 2019-07-11.
 * NuTech (Nusantara Teknologi)
 */
public class Decade {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("year")
    @Expose
    private String year;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
