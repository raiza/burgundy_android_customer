package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.nutech.burgundy.data.model.soundtrack.Moodtrack;

import java.util.List;

/**
 * Created by Arif Setiawan on 9/6/17.
 * Xeranta Mobile Solutions
 */

public class DataUserStore {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile_phone")
    @Expose
    private String mobile_phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("birth_date")
    @Expose
    private String birth_date;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("store_id")
    @Expose
    private String store_id;
    @SerializedName("device_id")
    @Expose
    private String device_id;
    @SerializedName("imei")
    @Expose
    private String imei;
    @SerializedName("store")
    @Expose
    private DataStore store;
    @SerializedName("moodtrack")
    @Expose
    private Moodtrack moodtrack;
    @SerializedName("register_id")
    @Expose
    private String registerId;
    @SerializedName("scheduler")
    @Expose
    private List<DataSchedules> scheduler;


    public List<DataSchedules> getScheduler() {
        return scheduler;
    }

    public void setScheduler(List<DataSchedules> scheduler) {
        this.scheduler = scheduler;
    }

    public Moodtrack getMoodtrack() {
        return moodtrack;
    }

    public void setMoodtrack(Moodtrack moodtrack) {
        this.moodtrack = moodtrack;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public DataStore getStore() {
        return store;
    }

    public void setStore(DataStore store) {
        this.store = store;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }
}
