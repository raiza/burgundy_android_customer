package com.nutech.burgundy.data.model.artist;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Arif Setiawan on 12/11/18.
 * Xeranta Mobile Solutions
 */
public class DataArtistList {
    @SerializedName("data")
    private List<DataArtists> artists;

    public List<DataArtists> getArtists() {
        return artists;
    }

    public void setArtists(List<DataArtists> artists) {
        this.artists = artists;
    }

    public  void addArtistsAll(List<DataArtists> artists) {
        this.artists.addAll(artists);
    }
}
