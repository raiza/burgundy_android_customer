package com.nutech.burgundy.data.api;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface AntMediaService {

    @GET("{roomId}"+".mp4")
    @Streaming
    Call<ResponseBody> checkVideoExist(@Path("roomId") String roomId);
}
