package com.nutech.burgundy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 9/13/17.
 * Xeranta Mobile Solutions
 */

public class DataSearch implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("data_type")
    @Expose
    private String data_type;

    protected DataSearch(Parcel in) {
        id = in.readString();
        code = in.readString();
        name = in.readString();
        image = in.readString();
        url = in.readString();
        data_type = in.readString();
    }

    public static final Creator<DataSearch> CREATOR = new Creator<DataSearch>() {
        @Override
        public DataSearch createFromParcel(Parcel in) {
            return new DataSearch(in);
        }

        @Override
        public DataSearch[] newArray(int size) {
            return new DataSearch[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.code);
        parcel.writeString(this.name);
        parcel.writeString(this.image);
        parcel.writeString(this.url);
        parcel.writeString(this.data_type);
    }

    public DataSearch() {
    }
}
