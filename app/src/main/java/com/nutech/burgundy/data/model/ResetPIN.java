package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 07/05/18.
 * Xeranta Mobile Solutions
 */
public class ResetPIN {

    @SerializedName("mobile_phone")
    @Expose
    private String number_phone;
    @SerializedName("activation_code")
    @Expose
    private String activation_code;

    public String getNumber_phone() {
        return number_phone;
    }

    public void setNumber_phone(String number_phone) {
        this.number_phone = number_phone;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }
}
