package com.nutech.burgundy.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 9/1/17.
 * Xeranta Mobile Solutions
 */

public class DataPlaylist implements Parcelable{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("public")
    @Expose
    private String publicPlaylist;
    @SerializedName("popularity")
    @Expose
    private String popularity;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("favorites")
    @Expose
    private String favorites;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("registers")
    @Expose
    private String registers;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("count_tracks")
    @Expose
    private String count_tracks;

    public DataPlaylist() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPublicPlaylist() {
        return publicPlaylist;
    }

    public void setPublicPlaylist(String publicPlaylist) {
        this.publicPlaylist = publicPlaylist;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFavorites() {
        return favorites;
    }

    public void setFavorites(String favorites) {
        this.favorites = favorites;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getRegisters() {
        return registers;
    }

    public void setRegisters(String registers) {
        this.registers = registers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCount_tracks() {
        return count_tracks;
    }

    public void setCount_tracks(String count_tracks) {
        this.count_tracks = count_tracks;
    }

    public DataPlaylist(Parcel in) {
        id = in.readString();
        name = in.readString();
        code = in.readString();
        publicPlaylist = in.readString();
        popularity = in.readString();
        image = in.readString();
        favorites = in.readString();
        likes = in.readString();
        registers = in.readString();
        description = in.readString();
        tags = in.readString();
        count_tracks = in.readString();
    }

    public static final Creator<DataPlaylist> CREATOR = new Creator<DataPlaylist>() {
        @Override
        public DataPlaylist createFromParcel(Parcel in) {
            return new DataPlaylist(in);
        }

        @Override
        public DataPlaylist[] newArray(int size) {
            return new DataPlaylist[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.code);
        dest.writeString(this.publicPlaylist);
        dest.writeString(this.popularity);
        dest.writeString(this.image);
        dest.writeString(this.favorites);
        dest.writeString(this.likes);
        dest.writeString(this.registers);
        dest.writeString(this.description);
        dest.writeString(this.tags);
        dest.writeString(this.count_tracks);

    }
}
