package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 02/05/18.
 * Xeranta Mobile Solutions
 */
public class MemberCheck {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msisdn")
    @Expose
    private String msisdn;

    @SerializedName("dtm_start")
    @Expose
    private String dtm_start;

    @SerializedName("dtm_expired")
    @Expose
    private String dtm_expired;

    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getDtm_start() {
        return dtm_start;
    }

    public void setDtm_start(String dtm_start) {
        this.dtm_start = dtm_start;
    }

    public String getLast_sucess_dr() {
        return dtm_expired;
    }

    public void setLast_sucess_dr(String last_sucess_dr) {
        this.dtm_expired = last_sucess_dr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
