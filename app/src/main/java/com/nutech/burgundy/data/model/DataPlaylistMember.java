package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 9/29/17.
 * Xeranta Mobile Solutions
 */

public class DataPlaylistMember {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("public")
    @Expose
    private String sPublic;
    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("popularity")
    @Expose
    private String popularity;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("favorites")
    @Expose
    private String favorites;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("registers")
    @Expose
    private String registers;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("count_tracks")
    @Expose
    private String count_tracks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getsPublic() {
        return sPublic;
    }

    public void setsPublic(String sPublic) {
        this.sPublic = sPublic;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFavorites() {
        return favorites;
    }

    public void setFavorites(String favorites) {
        this.favorites = favorites;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getRegisters() {
        return registers;
    }

    public void setRegisters(String registers) {
        this.registers = registers;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getCount_tracks() {
        return count_tracks;
    }

    public void setCount_tracks(String count_tracks) {
        this.count_tracks = count_tracks;
    }
}
