package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataResult {
    @SerializedName("result")
    @Expose
    private String success;
}
