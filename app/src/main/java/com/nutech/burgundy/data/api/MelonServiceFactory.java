package com.nutech.burgundy.data.api;

import androidx.annotation.NonNull;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nutech.burgundy.BuildConfig;
import com.nutech.burgundy.config.Constants;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Arif Setiawan on 28/01/19.
 * Xeranta Mobile Solutions
 */
public class MelonServiceFactory {

    public static MelonService create() {
        OkHttpClient okHttp = makeClientService(makeLoggingInterceptor());
        return makeMelonService(okHttp, makeGson());
    }

    private static MelonService makeMelonService(OkHttpClient okHttp, Gson gson) {

        int proxyPort = 8081;
        String proxyHost = "122.129.112.185";
        Proxy proxy = new Proxy(Proxy.Type.SOCKS,
                new InetSocketAddress(proxyHost, proxyPort));

//        okHttp.proxy(proxy);

        okHttp = new OkHttpClient.Builder()
                .proxy(proxy)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.URL_MELON)
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(MelonService.class);
    }

    @NonNull
    private static OkHttpClient makeClientService(HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Interceptor.Chain chain) throws IOException {
                                Request original = chain.request();

                                // Request customization: add request headers
                                Request.Builder requestBuilder = original.newBuilder()
                                        .header("Content-Type", "application/json")
                                        .header("X-Server-Key", "adc7e1f7b0c4f8cc58a0f7737ccce88ef76395cb");

                                Request request = requestBuilder.build();
                                return chain.proceed(request);
                            }
                        })
                .addInterceptor(loggingInterceptor)
                .build();

    }

    @NonNull
    private static Gson makeGson() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd")
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    @NonNull
    private static HttpLoggingInterceptor makeLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE);
    }

}
