package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSchedules {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("soundtrack_id")
    @Expose
    private String soundtrackId;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("day_of_week")
    @Expose
    private String dayOfWeek;
    @SerializedName("timer")
    @Expose
    private String timer;
    @SerializedName("repeat")
    @Expose
    private String repeat;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("soundtrack")
    @Expose
    private String moodtrackName;
    @SerializedName("store")
    @Expose
    private String storeName;
    @SerializedName("image")
    @Expose
    private String moodtrackImage;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getMoodtrackImage() {
        return moodtrackImage;
    }

    public void setMoodtrackImage(String moodtrackImage) {
        this.moodtrackImage = moodtrackImage;
    }

    public String getMoodtrackName() {
        return moodtrackName;
    }

    public void setMoodtrackName(String moodtrackName) {
        this.moodtrackName = moodtrackName;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSoundtrackId() {
        return soundtrackId;
    }

    public void setSoundtrackId(String soundtrackId) {
        this.soundtrackId = soundtrackId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
