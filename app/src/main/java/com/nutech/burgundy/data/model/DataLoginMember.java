package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 03/05/18.
 * Xeranta Mobile Solutions
 */
public class DataLoginMember {

    @SerializedName("response_umb")
    @Expose
    private MemberCheck memberCheck = new MemberCheck();

    @SerializedName("data_customer")
    @Expose
    private DataMember dataMember = new DataMember();

    public MemberCheck getMemberCheck() {
        return memberCheck;
    }

    public void setMemberCheck(MemberCheck memberCheck) {
        this.memberCheck = memberCheck;
    }

    public DataMember getDataMember() {
        return dataMember;
    }

    public void setDataMember(DataMember dataMember) {
        this.dataMember = dataMember;
    }
}
