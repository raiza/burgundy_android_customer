package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 13/02/19.
 * Xeranta Mobile Solutions
 */
public class DataSubscribtionGoogle {

    @SerializedName("autoRenewing")
    @Expose
    private boolean autoRenewing;
    @SerializedName("expiryTimeMillis")
    @Expose
    private String expiryTimeMillis;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("startTimeMillis")
    @Expose
    private String startTimeMillis;
    @SerializedName("cancelReason")
    @Expose
    private String cancelReason;

    public boolean isAutoRenewing() {
        return autoRenewing;
    }

    public void setAutoRenewing(boolean autoRenewing) {
        this.autoRenewing = autoRenewing;
    }

    public String getExpiryTimeMillis() {
        return expiryTimeMillis;
    }

    public void setExpiryTimeMillis(String expiryTimeMillis) {
        this.expiryTimeMillis = expiryTimeMillis;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStartTimeMillis() {
        return startTimeMillis;
    }

    public void setStartTimeMillis(String startTimeMillis) {
        this.startTimeMillis = startTimeMillis;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }
}
