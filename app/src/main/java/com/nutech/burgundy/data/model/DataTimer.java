package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 1/19/18.
 * Xeranta Mobile Solutions
 */

public class DataTimer {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("timer")
    @Expose
    private String timer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }
}
