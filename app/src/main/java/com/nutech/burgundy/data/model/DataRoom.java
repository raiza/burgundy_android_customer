package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 12/13/17.
 * Xeranta Mobile Solutions
 */

public class DataRoom {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("room_code")
    @Expose
    private String roomcode;
    @SerializedName("customer_id")
    @Expose
    private String customerid;
    @SerializedName("create_date")
    @Expose
    private String createdate;
    @SerializedName("track_id")
    @Expose
    private String trackid;
    @SerializedName("customer_name")
    @Expose
    private String customername;
    @SerializedName("track_name")
    @Expose
    private String trackname;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("view")
    @Expose
    private String view;
    @SerializedName("live")
    @Expose
    private String live;

    @SerializedName("use_headphone")
    @Expose
    private String use_headphone;
    @SerializedName("upload_status")
    @Expose
    private String upload_status;
    @SerializedName("karaoke_track_id")
    @Expose
    private String karaoke_track_id;
    @SerializedName("image")
    @Expose
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomcode() {
        return roomcode;
    }

    public void setRoomcode(String roomcode) {
        this.roomcode = roomcode;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getTrackid() {
        return trackid;
    }

    public void setTrackid(String trackid) {
        this.trackid = trackid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getTrackname() {
        return trackname;
    }

    public void setTrackname(String trackname) {
        this.trackname = trackname;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }

    public String getUse_headphone() {
        return use_headphone;
    }

    public void setUse_headphone(String use_headphone) {
        this.use_headphone = use_headphone;
    }

    public String getUpload_status() {
        return upload_status;
    }

    public void setUpload_status(String upload_status) {
        this.upload_status = upload_status;
    }

    public String getKaraoke_track_id() {
        return karaoke_track_id;
    }

    public void setKaraoke_track_id(String karaoke_track_id) {
        this.karaoke_track_id = karaoke_track_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
