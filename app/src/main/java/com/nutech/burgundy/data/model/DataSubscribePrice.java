package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Dafian on 10/16/17
 */

public class DataSubscribePrice {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("period_type")
    @Expose
    private String periodType;
    @SerializedName("day_duration")
    @Expose
    private String dayDuration;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("fix_discount")
    @Expose
    private String fixDiscount;
    @SerializedName("percentage_discount")
    @Expose
    private String percentageDiscount;
    @SerializedName("currency_prefix")
    @Expose
    private String currencyPrefix;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("product_id")
    @Expose
    private String product_id;

    @SerializedName("item_code")
    @Expose
    private String item_code;
    @SerializedName("item_name")
    @Expose
    private String item_name;
    @SerializedName("merchant_id")
    @Expose
    private String merchant_id;
    @SerializedName("merchant_name")
    @Expose
    private String merchant_name;
    @SerializedName("product_name")
    @Expose
    private String product_name;
    @SerializedName("payment_type")
    @Expose
    private String payment_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPeriodType() {
        return periodType;
    }

    public void setPeriodType(String periodType) {
        this.periodType = periodType;
    }

    public String getDayDuration() {
        return dayDuration;
    }

    public void setDayDuration(String dayDuration) {
        this.dayDuration = dayDuration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFixDiscount() {
        return fixDiscount;
    }

    public void setFixDiscount(String fixDiscount) {
        this.fixDiscount = fixDiscount;
    }

    public String getPercentageDiscount() {
        return percentageDiscount;
    }

    public void setPercentageDiscount(String percentageDiscount) {
        this.percentageDiscount = percentageDiscount;
    }

    public String getCurrencyPrefix() {
        return currencyPrefix;
    }

    public void setCurrencyPrefix(String currencyPrefix) {
        this.currencyPrefix = currencyPrefix;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }
}
