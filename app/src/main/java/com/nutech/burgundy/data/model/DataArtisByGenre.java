package com.nutech.burgundy.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Setiawan on 9/4/17.
 * Xeranta Mobile Solutions
 */

public class DataArtisByGenre {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("followers")
    @Expose
    private String followers;
    @SerializedName("popularity")
    @Expose
    private String popularity;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("fully_scraped")
    @Expose
    private String fully_scraped;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("genre_id")
    @Expose
    private String genre_id;
    @SerializedName("genre_name")
    @Expose
    private String genre_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFully_scraped() {
        return fully_scraped;
    }

    public void setFully_scraped(String fully_scraped) {
        this.fully_scraped = fully_scraped;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(String genre_id) {
        this.genre_id = genre_id;
    }

    public String getGenre_name() {
        return genre_name;
    }

    public void setGenre_name(String genre_name) {
        this.genre_name = genre_name;
    }
}
